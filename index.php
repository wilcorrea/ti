<?php

/**
 * 
 * @index
 * @author gennesis.io < contato@gennesis.io >
 */
date_default_timezone_set('America/Sao_Paulo');

define('APP_FS', (PHP_OS == "Windows" || PHP_OS == "WINNT") ? "\\" : "/");

define('APP_PATH', realpath(dirname(__FILE__)));

define('APP_PATH_ROOT', implode(APP_FS , [APP_PATH, 'root']));

require implode(APP_FS, [APP_PATH_ROOT, 'config', 'parameters.php']);

require implode(APP_FS, [APP_PATH_ROOT, 'config', 'error.php']);

require implode(APP_FS, [APP_PATH_ROOT, 'config', 'autoload.php']);
 
require implode(APP_FS, [APP_PATH_ROOT, 'app', 'App.class.php']);


/**
 * 
 * @init
 * 
 * @param url
 * 
 * @author gennesis.io < contato@gennesis.io >
 * 
 * @dirapp:
 *   chmod 764 
 *   chown www-data:www-data
 */
$location = '';

if (strpos(APP_PATH, $_SERVER['DOCUMENT_ROOT'])) {

  $location = str_replace($_SERVER['DOCUMENT_ROOT'], '', APP_PATH);
}

App::init($location);
