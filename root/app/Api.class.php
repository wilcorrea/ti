<?php

/**
 *
 * @class Api
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Api {

  /**
   * 
   * @var string header to authenticate
   */
  private static $header = 'HTTP_X_AUTH_TOKEN';

  /**
   * 
   * //api/{version}/[post,get,nonce]/{module}/{entity}/{operation}/[{nonce}]
   * {
   *     object
   *   , filter
   *   , settings {
   *     query, sort, begin, end
   *   }
   * }
   * @param string $uri
   * 
   * @return json
   */
  public static function resolve($uri) {

    System::import('class', 'base', 'Auth', 'core');


    $status = 'fail';
    $message = 'General Error';
    $data = null;

    $request = explode('/', $uri);

    if (isset($request[4])) {

      if (isset($_SERVER[self::$header])) {

        $token = $_SERVER[self::$header];

        $version = $request[0];

        $root = 'src';
        $mode = $request[1];
        $module = $request[2];
        $entity = $request[3];
        $operation = $request[4];

        $nonce = isset($request[5]) ? $request[5] : null;

        $object = null;
        $filter = null;
        $settings = null;

        $body = Json::decode(file_get_contents('php://input'));

        if ($body) {
          
          $object = isset($body->object) ? $body->object : null;
          $filter = isset($body->filter) ? $body->filter : null;
          $settings = isset($body->settings) ? $body->settings : null;
        }

        if (Auth::isAuthorized($token, $module, $entity, $operation)) {

          switch ($mode) {

            case 'post':

              if (Security::isNonce($nonce)) {

                if ($object) {

                  $data = self::post($root, $module, $entity, $operation, $object, $nonce);
                  if ($data && isset($data['status'])) {
                    $status = $data['status'];
                    $message = $data['message'];
                    $data = $data['data'];
                  }
                } else {
                  
                  $message = 'Invalid Object';
                }
              } else {
                
                $message = 'Invalid Nonce';
              }

              break;

            case 'get':

              $query = '';
              $sort = '';
              $begin = '';
              $end = '';
              if ($settings) {

                $query = isset($settings->query) ? $settings->query : '';
                $sort = isset($settings->sort) ? $settings->sort : '';
                $begin = isset($settings->begin) ? $settings->begin : '';
                $end = isset($settings->end) ? $settings->end : '';
              }

              $complete = true;

              $data = self::get($root, $module, $entity, $operation, $filter, $object, $query, $sort, $begin, $end, $complete);
              if ($data) {
                $status = 'success';
                $message = '';
              }

              break;

            case 'nonce':

              $data = Security::createNonce($module . '.' . $entity . '.' . $operation, $token);

              if ($data) {

                $status = 'success';
                $message = 'Nonce registered';
              }

              break;

            case 'permissions':

              $data = Auth::permissions($token, $module, $entity, $operation);

              if ($data) {

                $status = 'success';
                $message = 'Permissions returned';
              } else {

                $message = 'Permissions denied';
              }

              break;

            default:

              $message = 'Action Error';
              break;
          }
        }

      } else {

        $message = 'Autentication error';
      }
    }

    $resolved = (['status'=>$status, 'message' => $message, 'data' => $data]);

    print Json::encode($resolved);
  }

  /**
   * 
   * @param string $root
   * @param string $module
   * @param string $entity
   * @param string $operation
   * @param object $filter
   * @param object $object
   * @param string $query
   * @param string $sort
   * @param string $begin
   * @param string $end
   * @param string $complete
   */
  private static function get($root, $module, $entity, $operation, $filter, $object, $query = null, $sort = null, $begin = null, $end = null, $complete = false) {

    System::import('class', 'pattern', 'Controller', 'core');

    System::import('c', $module, $entity, $root, true, '{project.rsc}');

    $classCtrl = $entity . 'Ctrl';
    
    $getPropertiesCtrl = 'getProperties' . $classCtrl;
    $getItemsCtrl = 'getItems' . $classCtrl;
    $getObjectCtrl = 'getObject' . $classCtrl;
    $getCountCtrl = 'getCount' . $classCtrl;
    $getCtrl = 'get' . $classCtrl;

    $entityCtrl = new $classCtrl(APP_PATH);

    $elements = $entityCtrl->$getItemsCtrl("");
    $properties = $entityCtrl->$getPropertiesCtrl();
    $operations = $properties['operations'];

    $get_value = 'get_' . $properties['prefix'] . '_value';
    $get_items = 'get_' . $properties['prefix'] . '_items';

    $collection = [];

    $total = 0;
    $message = '';

    $_o = [];
    $_q = [];

    if ($object) {

      foreach ($object as $key => $value) {

        //[['', ''],['3', 'Nunca'],['1', 'Sempre'],['0', 'Avançaada'],['2', 'Rápida']],

        //$id = $item['id'];

        //if (isset($object->$id) && in_array($item['fast'], [1, 0])) {

          $_f = self::searchSql($key, isset($elements[$key]) ? $elements[$key]['type'] : 'string', $value);
          if ($_f) {

            $_o[] = $_f;
          }
        //}
      }
    }

    if ($query) {

      //[['', ''],['3', 'Nunca'],['1', 'Sempre'],['0', 'Avançaada'],['2', 'Rápida']],

      foreach ($elements as $element) {

        if (in_array($element['fast'], [1, 2])) {

          $_f = self::searchSql($element['id'], $element['type'], $query);
          if ($_f) {

            $_q[] = $_f;
          }
        }

      }
    }

    $o = implode(' AND ', $_o);
    $q = implode(' OR ', $_q);

    $o = $o ? $o : 'TRUE';
    $q = $q ? $q : 'TRUE';

    $where = '(' . $o . ')' . ' AND ' . '(' . $q . ')';

    $message = $where;
    //$message = json_encode(isset($operations[$operation]) ? $operations[$operation] : 'Operation not found');

    $order = $properties['order'];

    if (isset($operations[$operation])) {

      $_operation = $operations[$operation];
      
      $_get = 'collection';
      if (isset($_operation->get)) {
        $_get = $_operation->get;
      }

      if ($_get === 'object') {

        $begin = 0;
        $end = 1;
        $order = '';
      }

      $gets = $entityCtrl->$getCtrl($where, '', $order, $begin, $end, null, false, false);

      $total = count($gets);
      if ($_get === 'collection') {

        $total = $entityCtrl->$getCountCtrl($where);
      }
  
      for ($i = 0; $i < count($gets); $i++) {
  
        $get = $gets[$i];
  
        $json = [];
  
        if ($_get === 'collection') {

          $counter = ($begin + $i) + 1;

          $json['counter'] = $counter;
        }
  
        $items = $get->$get_items();
  
        foreach ($items as $id => $item) {
  
          $exibir = $item['grid'] || $_get === 'object';
  
          if ($exibir) {
  
            $value = $item['value'];

            $type = Application::getType($item['type']);

            switch($type) {

              case 'numeric':
                $value = String::position($value, '.') !== false ? (double) $value : (int) $value;
                break;

              case 'boolean':
                $value = $value ? true : false;
                break;

              case 'collection':

                if ($_get === 'object' && isset($item['foreign'])) {

                  $_root = 'src';
                  $_module = isset($item['foreign']['module']) ? $item['foreign']['module'] : $item['foreign']['modulo'];
                  $_entity = $item['foreign']['entity'];
                  $_operation = 'list';
                  $_filter = null;
                  
                  $__object = [];
                  $__object[] = (object) [$item['foreign']['key'] => $items[$properties['reference']]['value']];
                  if ($item['type_content']) {
                    $type_contents = Json::encode($item['type_content']);
                    foreach ($type_contents as $type_content) {
                      //$__object[] = [$type_content];
                    }
                  }
                  
                  $_object = (object) [$item['foreign']['key'] => $items[$properties['reference']]['value']];
                  //$_object = (object) $__object;

                  $value = self::get($_root, $_module, $_entity, $_operation, $_filter, $_object);
                }
                break;

              default:
                $value = utf8_encode($value);
                break;
            }

            $json[$id] = $value;

            if (isset($item['reference'])) {
  
              $json[$item['foreign']['description']] = utf8_encode($item['reference']);
            }
          }

        }

        $collection[] = $json;

      }

    } else if ($operation === 'autocomplete') {

      $order = $order ? $order : $properties['description'];

      $gets = $entityCtrl->$getCtrl($where, '', $order, $begin, $end, null, false, false);

      $total = count($gets);

      foreach ($gets as $get) {

        $collection[] = $get->$get_value($properties['reference']) . '|' . utf8_encode($get->$get_value($properties['description']));
      }

    }

    $return = $collection;

    if ($complete) {
      $return = ["collection" => $collection, "total" => $total, "begin" => $begin, "end" => $end, "message" => $message];
    }

    return $return;
  }

  /**
   *
   * @param string $root
   * @param string $module
   * @param string $entity
   * @param json $operation
   * @param string $nonce
   */
  private static function post($root, $module, $entity, $operation, $object, $nonce) {

    System::import('class', 'pattern', 'Controller', 'core');

    $status = Console::$STATUS_FAIL;
    $message = "";
    $referenced = [];
    $result = "";

    if (Security::isFree($nonce)) {

      //Security::setLocked($nonce);

      $root = is_null($root) ? 'src' : $root;
      
      System::import('c', $module, $entity, $root, true, '{project.rsc}');
  
      $classCtrl = $entity . 'Ctrl';
      
      $getPropertiesCtrl = 'getProperties' . $classCtrl;
      $getItemsCtrl = 'getItems' . $classCtrl;
      $operationCtrl = 'operation' . $classCtrl;
      
      $entityCtrl = new $classCtrl(APP_PATH);
  
      $properties = $entityCtrl->$getPropertiesCtrl();
      $reference = $properties['reference'];
      $operations = $properties['operations'];

      if (isset($operations[$operation])) {

        $_operation = $operations[$operation];

        $references = explode(",", $reference);

        $conector = ",";

        if ($_operation->recover) {

          foreach ($references as $r) {

            if (isset($object->$r)) {

              $referenced[$r] = $object->$r;
            }
          }
        }
        $search = implode($conector, $referenced);

        $items = $entityCtrl->$getItemsCtrl($search);

        foreach ($items as $key => $item) {

          $id = $item['id'];

          $value = isset($object->$id) ? $object->$id : null;
          if (is_string($value)) {
            $value = utf8_decode($value);
          }

          $items[$id]['value'] = $value;
        }

        //if ($_operation->validate) {

        //  System::validate($items, $operation);
        //}

        if (Console::status() === Console::$STATUS_SUCCESS) {

          $result = $entityCtrl->$operationCtrl($operation, $items, null);
        }

        $status = Console::status();

        if ($status === Console::$STATUS_SUCCESS) {

          //Security::setExpired($nonce);
          Security::setFree($nonce);

          Controller::cachedClear($module, $entity);

        } else {

          Security::setFree($nonce);
        }

      } else {

        $message = htmlentities("A operação '" . $operation . "' não corresponde a nenhuma operação desse nível");

      }

      
    } else if (Security::isLocked($nonce)) {

      $status = Console::$STATUS_WARNING;

      $message = htmlentities("A operação já está em andamento, por favor aguarde");

    } else if (Security::isExpired($nonce)) {

      $status = Console::$STATUS_SUCCESS;

      $message = htmlentities("A operação já foi realizada com sucesso");

    }

    return ['status' => $status, 'data' => ['referenced' => $referenced, 'result' => $result], 'message' => $message];

  }

  /**
   * 
   * @param string $id
   * @param string $type
   * @param string $search
   * 
   * @return string search(sql)
   */
  private static function searchSql($id, $type, $search) {

    $multiple = false;

    $type =  Application::getType($type);

    if ($type === 'string') {

      $multiple = true;
    } else if ($type === 'numeric') {

      $searchs = self::separate($search);
      $_search = [];
      foreach ($searchs as $i) {

        if (!is_numeric(trim($i))) {
          $_search[] = '';
        } else {
          $_search[] = $i;
        }
      }
      $search = implode(' OR ', $_search);
    }

    return self::searchFormatSql($id, $search, $multiple);
  }

  /**
   * 
   * @param string $string
   * 
   * @return array
   */
  private static function separate($string) {
    
    return explode(" OR ", String::replace(String::replace(String::upper($string), " OU ", " OR "), ",", " OR "));
  }

  /**
   * 
   * @param string $name
   * @param string $string
   * @param boolean $multiple
   * 
   * @return string
   */
  private static function searchFormatSql($name, $string, $multiple) {

    $searchs = self::separate($string);
    $string = "";
    
    if (is_array($searchs)) {
      $search = array();
      foreach ($searchs as $string) {
        if ($multiple) {

          $search[] = self::getSearchFormat($name, $string);
        } else {

          $search[] = $name . " = '" . String::removeSpecial($string) . "'";
        }
      }

      if (count($search) > 0) {
        $string = "(" . implode(") OR (", $search) . ")";
      }
    }
    
    return "(" . $string . ")";
  }

  /**
   * 
   * @param string $name
   * @param string $string
   * @param string $conector
   * 
   * @return string
   */
  private static function getSearchFormat($name, $string, $conector = "AND") {

    $vetor = explode(" ", $string);
    $search = '';

    $conector = $conector . ' ';

    if (count($vetor) > 1) {
      $s = [];
      for ($a = 0; $a < count($vetor); $a++) {
        if (String::length($vetor[$a]) > 2) {
          $s[] = $name . ' LIKE CONCAT("%", "' . String::trim($vetor[$a]) . '", "%") ';
        }
      }
      $ss = implode($conector, $s);
      $search = '(' . $ss . ')';
    } else {
      $search = $name . ' LIKE CONCAT("%", "' . String::removeSpecial($string) . '", "%")';
    }

    return $search;
  }

}