<?php

/**
 * 
 * @class App
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class App {

  /**
   * 
   * @param string $relative
   */
  public static function init($relative) {

    define('APP_CURDATE', date('d/m/Y'));
    define('APP_NOW', date('d/m/Y H:i:s'));

    $_APP_HOST = '';
    if (isset($_SERVER['HTTP_HOST'])) {
      $_APP_HOST = $_SERVER['HTTP_HOST'];
    }
    define('APP_HOST', $_APP_HOST);

    $_APP_PAGE_PROTOCOL = 'http';
    if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
      $_APP_PAGE_PROTOCOL = 'https';
    }

    define('APP_PAGE_PROTOCOL', $_APP_PAGE_PROTOCOL);

    define('APP_PAGE_ROOT', APP_PAGE_PROTOCOL . '://' . APP_HOST);

    define('APP_PAGE', APP_PAGE_ROOT . $relative);

    App::start();
  }

  /**
   *
   * @param string $id
   * @param string $module
   * @param string $entity
   * @param string $path
   * @param boolean $require
   * @param string $extension
   */
  public static function root($id, $module, $entity, $path = "", $require = true, $extension = 'php') {

    $layers['file'] = ['sufix' => ''];
    $layers['class'] = ['sufix' => '.class'];

    $sufix = isset($layers[$id]) ? $layers[$id]['sufix'] : '';

    $file = implode(APP_FS, [APP_PATH, 'root', $path, $module, ($entity . $sufix . '.' . $extension)]);

    if (file_exists($file)) {

      if ($require) {

        require_once $file;
      }
    } else {

      App::log("Cannot require $file");

      $file = false;
    }

    return $file;
  }

  /**
   * @param array $args
   */
  public static function log() {

    $args = func_get_args();
    if (is_array($args)) {
      foreach ($args as $arg) {

        print '<pre>';
        var_dump($arg);
        print '</pre>';
      }
    }
  }

  /**
   * 
   * @param string $method
   * @param array $args
   */
  public static function __callStatic($method, $args) {

    include implode(APP_FS, [APP_PATH_ROOT, 'app', 'alias.php']);

    if (isset($methods[$method])) {

      $settings = $methods[$method];

      self::root('class', $settings->module, $settings->entity, $settings->root);
  
      $method = isset($settings->method) ? $settings->method : $method;

      return call_user_func_array(array($settings->entity, $method), $args);
    } else {
      
      App::log("Method not found '" . $method . "'");
    }
  }

}