<?php

/**
 * 
 * @array alias methods
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
$methods = [

    'start'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'get'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'link'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'text'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'assets'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'app'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'api'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']
  , 'lang'=>(object)['root'=>'core','module'=>'standard','entity'=>'Application']

  , 'import'=>(object)['root'=>'core','module'=>'standard','entity'=>'System']
  , 'request'=>(object)['root'=>'core','module'=>'standard','entity'=>'System']

  , 'path'=>(object)['root'=>'core','module'=>'standard','entity'=>'File']
  , 'exists'=>(object)['root'=>'core','module'=>'standard','entity'=>'File']

  , 'replace'=>(object)['root'=>'core','module'=>'standard','entity'=>'String']
  , 'position'=>(object)['root'=>'core','module'=>'standard','entity'=>'String']

  , 'decrypt'=>(object)['root'=>'core','module'=>'standard','entity'=>'Security']
  , 'encrypt'=>(object)['root'=>'core','module'=>'standard','entity'=>'Security']

  , 'download'=>(object)['root'=>'core','module'=>'service','entity'=>'Download', 'method' => 'render']

  , 'login'=>(object)['root'=>'core','module'=>'base','entity'=>'Auth', 'method' => 'login']
  , 'logout'=>(object)['root'=>'core','module'=>'base','entity'=>'Auth', 'method' => 'logout']

];