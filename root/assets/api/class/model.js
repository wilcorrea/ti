
/* global angular, document, window */
'use strict';

var 

    properties = {/*{{properties}}*/}
  , items = {/*{{items}}*/}
  , operations = {/*{{operations}}*/};


module.exports = new Model('{{module}}', '{{entity}}', properties, items, operations);