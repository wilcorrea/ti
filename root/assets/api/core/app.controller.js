/* global angular, document, window */
'use strict';

angular.module('starter')

  .controller('AppCtrl', function($scope, $state, $window, $timeout, $ionicPopup, $cordovaGeolocation, $cordovaVibration, $cordovaDialogs, $cordovaLocalNotification, AuthService) {

    $scope.app = App.$config;

    $scope.username = AuthService.username();

    $scope.lang = {
      login: {
        label: 'Login'
        , placeholder: 'Seu E-mail'
      },
      logout: {
        label: 'Sair'
      },
      home: {
        label: 'Home'
      },
      exit: {
        label: 'Fechar'
      },
      password: {
        label: 'Senha'
        , placeholder: 'Sua Senha'
      },
      remember: {
        label: 'Lembrar'
        , placeholder: 'Lembre para mim'
      },
      buttom_1: {
        label: 'Login'
      },
      buttom_2: {
        label: 'Ainda não tenho Conta'
      }
    };

    $scope.setCurrentUsername = function(username) {
      $scope.username = username;
    };

    /**
     * 
     * @param string state
     * @param object data
     */
    $scope.open = function(state, data) {

      var split = state.split('.')
        , module = split[0]
        , entity = split[1]
        , operation = split[2];
        
      if (split.length === 3 && module !== 'app') {

        App.state(module, entity, operation);

        $scope.data = data;
  
        $state.go(module + '.' + entity + '.' + operation, {data: data});
      } else {
        
        $state.go(state, {data: data});
      }

      //App.compact(true);
    };
    
    
    $scope.alerts = {
        /**
         * queue
         */
        data: []
        /**
         * @param string title
         * @param string message
         * @param string status
         * @param int time
         */
      , add: function(title, message, status, time) {

        time = time ? time : 10000;
        status = status ? status : 'default';

        var _alert = {
          title: title + ': ',
          message: message,
          status: status,
          visible: false
        };

        $scope.alerts.data.push(_alert);

        $timeout(function() {

          var index = $scope.alerts.data.length - 1;

          if ($scope.alerts.data[index]) {

            $scope.alerts.data[index].visible = true;
          }
        }, 1);

        $timeout(function() {
  
          $scope.alerts.remove(_alert);
        }, time);

      }
      /**
       * @param object _alert
       */
      , remove: function(_alert) {

        var index = $scope.alerts.data.indexOf(_alert);

        if ($scope.alerts.data[index]) {

          $scope.alerts.data[index].visible = false;

          $timeout(function() {

            $scope.alerts.data.splice(index, 1);
          }, 500);

        }
      }
    };


    $scope.exit = function() {

      if (window.cordova) {
        $cordovaVibration.vibrate(50);
        $cordovaDialogs.confirm('Are you sure?', 'Exit', ['Yes','No'])
         .then(function(button) {
           // no button = 0, 'OK' = 1, 'Cancel' = 2
           if (+button === 1) {
             navigator.app.exitApp();
           }
         });
      } else {
        $window.close();
      }
    };

    $scope.getPosition = function() {
      
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
       $cordovaGeolocation
         .getCurrentPosition(posOptions)
         .then(function (position) {
           $scope.lat  = position.coords.latitude;
           $scope.long = position.coords.longitude;
           $ionicPopup.alert({
              title: 'Location Sucessfull!',
              template: '[' + position.coords.latitude + ', ' + position.coords.longitude + ']'
           });
         }, function(err) {
           $ionicPopup.alert({
              title: 'Location Failed!',
              template: err.toString()
            });
         });
    };
    
    $scope.notify = function (title, text, custom) {
      $cordovaLocalNotification.schedule({
        id: 1,
        title: 'Title here',
        text: 'Text here',
        data: {
          customProperty: 'custom value'
        }
      }).then(function(result) {
        // ...
      });
    };
    
  })

  .controller('AppLoginCtrl', function($scope, $state, $ionicPopup, AuthService) {

    App.token = '';

    $scope.app = App.$config;

    $scope.menus = App.menus;

    $scope.user = {
        username: ''
      , password: ''
      , remember: false
    };
      
    $scope.login = function(data) {

      AuthService.login(data.username, data.password).then(function(authenticated) {

        App.token = authenticated;

        $state.go('app.home', {}, {reload: true});

        $scope.setCurrentUsername(data.username);

      }, function(err) {
        
        $ionicPopup.alert({
          title: 'Login failed!',
          template: err
        });
      });
    };

  })
  
  .controller('AppMainCtrl', function($scope, $state, AuthService) {

    if (!AuthService.isAuthenticated()) {

      $state.go('login');
    }

  })
  
  .controller('AppHomeCtrl', function($scope, $state, TemplateCtrl) {

    $scope.strings = {
        by: 'por'
      , details: 'Detalhes'
      , interact: 'Interagir'
    };

    $scope.stats = [
        {
            'title': 'Chamados'
          , 'class': 'danger'
          , 'data': [
              {label: 'Abertos', value: '2'}
            , {label: 'Orçados', value: '230'}
            , {label: 'Média', value: '90%'}
          ]
        }
      , {
            'title': 'Orçamentos'
          , 'class': 'success'
          , 'data': [
              {label: 'Abertos', value: '2'}
            , {label: 'Aprovados', value: '130'}
            , {label: 'Média', value: '70%'}
          ]
        
      }
      , {
            'title': 'Processos'
          , 'class': 'info'
          , 'data': [
              {label: 'Ativos', value: '4'}
            , {label: 'Atrasados', value: '2'}
            , {label: 'Média', value: '50%'}
          ]
      }
    ];

    $scope.timeline = [
      {
          label: '01/11/2015 a 07/11/2015'
        , info: 'Atividades da Semana'
        , data: [
          {
              project: 'Projeto A'
            , icon: 'fa fa-clock-o'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto B'
            , icon: 'fa fa-tasks'
            , content: 'Contato com Sr. Jhonny sobre necessidade de outorga do empreendimento de Rio Novo. Ficou combinado de ele voltar no dia 15 para fazer o pagamento da taxa de absorção de nutrientes do solo em relação as condições inóspitas holísticas'
            , time: '2 dias atrás'
            , who: 'Matheus'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-clock-o'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-tasks'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-clock-o'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-clock-o'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          }
        ]
      },

      {
          label: '14/11/2015'
        , info: 'Atividades do Dia'
        , data: [
          {
              project: 'Projeto A'
            , icon: 'fa fa-tasks'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto B'
            , icon: 'fa fa-clock-o'
            , content: 'Contato com Sr. Jhonny sobre necessidade de outorga do empreendimento de Rio Novo'
            , time: '2 dias atrás'
            , who: 'Matheus'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-tasks'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          },
          {
              project: 'Projeto A'
            , icon: 'fa fa-clock-o'
            , content: 'Entrega de FOB na superintendência'
            , time: '11hs atrás'
            , who: 'William'
          }
        ]
      }
    ];

  })
  
  .controller('AppMenuCtrl', function($scope, $state, AuthService) {

    if (!AuthService.isAuthenticated()) {

      $state.go('login');
    } else {

      var state = $state.current.name
        , split = state.split('.');

      var module = split[0]
        , entity = split[1]
        , operation = split[2] ? split[2] : 'start';

      AuthService.permissions(module, entity, operation, function(permissions) {

        // if (!App.states[state]) {
        //   App.states[state] = {};
        // }
        // App.states[state].permissions = permissions;
        $scope.menus = permissions.menus;
      });
    }

    /**
     * 
     * @function let's go
     */
    $scope.logout = function() {

      AuthService.logout();

      window.location.href = App.APP_PAGE_LOGOUT;
    };

  });
