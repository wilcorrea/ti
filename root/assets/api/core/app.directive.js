

/* global angular, document, window */
'use strict';

App

  .directive('toggle', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {

        if (attrs.toggle == "tooltip") {
          $(element).tooltip();
        }

        if (attrs.toggle == "popover") {
          console.log($(element))
          $(element).popover();
        }
      }
    };
  })

  .directive('formField', function($compile) {
    
    return {
        replace: true
      , restrict: 'E'
      , scope: {
          field: '='
        , collection: '='
        , autocompletition: '='
        , model: '='
        , settings: '='

        , fieldClass: '@'
        , labelClass: '@'
        , labelHide: '@'
        , boxClass: '@'
        , radioClass: '@'
        , radioFieldsetClass: '@'
        , checkboxClass: '@'
        , checkboxFieldsetClass: '@'
        , itemsAddButtonClass: '@'
        , itemsAddIconClass: '@'
        , itemsRemoveButtonClass: '@'
        , itemsRemoveIconClass: '@'
      }
      /**
       * 
       * 
       */
      , controller: ['$scope', 'TemplateCtrl', function($scope, TemplateCtrl) {
        
        $scope.info = function(title) {

          //alert(title);
        };
        
        $scope.change = function(_scope) {

          $scope.model.onchange(_scope.field, _scope.collection);
        };

        /**
         * 
         * @var items
         */
        $scope.items = {
    
          /**
           * 
           * @param string _id
           */
          add: function(_id) {

            try {

              var row = {}
                , items = $scope.model.items[_id].subitem.items;

              for (var i in items) {

                row[items[i].id] = '';
              }

              if (!$scope.collection[_id]) {
                $scope.collection[_id] = [];
              }

              $scope.collection[_id].unshift(row);
            } catch (e) {
              App.error(e);
            }
          },
    
          /**
           * 
           * @param string id
           * @param object _item
           */
          remove: function(_id, _row) {
    
            try {

              if (!$scope.collection[_id]) {
                $scope.collection[_id] = [];
              }

              $scope.collection[_id].splice($scope.collection[_id].indexOf(_row), 1);
            } catch (e) {
              App.error(e);
            }
          }
        };

        /**
         * 
         * @var autocompletition
         */
        $scope.autocompletition = {
          /**
           * 
           * @param string typedthings
           */
          search: function(query) {

            var id = event.target.id
              , _field = $scope.model.items[id]
              , key = event.keyCode || event.which;

            if (!_field) {

              for (var i in $scope.model.items) {

                if ($scope.model.items[i].subitem) {
                  for (var j in $scope.model.items[i].subitem.items) {
                    var item = $scope.model.items[i].subitem.items[j];
                    if (item.id === id) {
                      _field = item;
                    }
                  }
                }
              };
            }

            if (_field && _field.id) {

              if ((key) !== 13 && (key) !== 39) {

                $scope.collection[_field.id] = '';

                var action = 'autocomplete'
                  , mode = 'collection'
                  , settings = $scope.settings;

                settings.query = query;

                TemplateCtrl.get($scope, _field.foreign.module || _field.foreign.modulo, _field.foreign.entity, action, settings, mode, null, function(collection) {

                  try {

                    $scope.autocompletition[id] = collection;

                  } catch(e) {

                    App.error(e);
                  }
                });
              }
            }
          },
          /**
           * 
           * @param string suggestion
           * @param object id
           */
          select: function(suggestion, id) {

            /*var _field = $scope.model.items[id];

            if (_field) {

              $scope.collection[_field.id] = suggestion.split('|')[0];
              $scope.collection[_field.foreign.description] = suggestion.split('|')[1];

            }*/

          }
        };

      }]
      /**
       * 
       * @param $scope scope
       * @param HTMLElement scope
       * @param array scope
       */
      , link: function(scope, element, attrs) {

        scope.name = App.guid();

        var components = {};

        components['input[text]'] = 
          (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<div class="{{boxClass}}">\
            <input type="text" name="{{name}}" ng-model="collection[field.id]" ng-change="change(this)" ng-readonly="field.readonly" class="{{fieldClass}}" placeholder="{{field.placeholder}}"/>\
          </div>';

        components['autocomplete'] = 
          (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<div class="{{boxClass}}">\
            <autocomplete ng-model="collection[field.foreign.description]" ng-change="change(this)" ng-hidden="collection[field.id]" attr-input-id="{{field.id}}" attr-input-readonly="{{field.readonly}}" attr-placeholder="{{field.placeholder}}" click-activation="true" data="autocompletition[field.id]" on-type="autocompletition.search" on-select="autocompletition.select"></autocomplete>\
          </div>';

        components['input[number]'] = components['input[text]'].replace('input type="text"', 'input type="text"');

        components['input[radio]'] = 
          (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<div class="{{radioFieldsetClass}}">\
            <div ng-repeat="_option in field.options" class="{{radioClass}}">\
              <label>\
                <input type="radio" name="{{name}}" ng-model="collection[field.id]" ng-change="change(this)" ng-readonly="field.readonly" value="{{_option.value}}">\
                {{_option.label}}\
              </label>\
            </div>\
          </div>';

        components['input[checkbox]'] = 
          '<div class="form-group">' +
            (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<div class="{{checkboxFieldsetClass}}">\
            <fieldset>\
              <div class="{{checkboxClass}}">\
                <label>\
                  <input type="checkbox" name="{{name}}" ng-model="collection[field.id]" ng-change="change(this)" ng-readonly="field.readonly"/>\
                  {{ (field.placeholder) ? field.placeholder : field.title }}\
                </label>\
              </div>\
            </fieldset>\
          </div>\
        </div>';

        components['input[color]'] = 
          (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<input type="color" name="{{name}}" ng-model="collection[field.id]" ng-change="change(this)" ng-readonly="field.readonly" style="width: 100px; cursor: pointer;"/>';

        components['html'] = '<div ng-bind-html="collection[field.id]"></div>';

        components['subitem'] = 
          (!scope.labelHide ? '<label class="{{labelClass}}" ng-click="info(field.title)">{{field.label}}</label>' : '') +
          '<table class="table-items">\
            <thead>\
              <tr>\
                <th>\
                  <button class="{{itemsAddButtonClass}}" ng-click="items.add(field.id)">\
                    <i class="{{itemsAddIconClass}}"></i>\
                  </button>\
                </th>\
                <th ng-repeat="_item in field.subitem.items">\
                  <div class="{{field.subitem.entity}}"> {{_item.label}} </div>\
                </th>\
              </tr>\
            </thead>\
            <tbody>\
              <tr ng-repeat="_row in collection[field.id]">\
                <td>\
                  <button class="{{itemsRemoveButtonClass}}" ng-click="items.remove(field.id, _row)">\
                    <i class="{{itemsRemoveIconClass}}"></i>\
                  </button>\
                </td>\
                <td ng-repeat="_item in field.subitem.items">\
                  <form-field field="_item" collection="_row" label-hide="1" autocompletition="autocompletition" model="model" settings="settings" field-class="{{fieldClass}}" label-class="{{labelClass}}" box-class="{{boxClass}}" radio-class="{{radioClass}}" radio-fieldset-class="{{radioFieldsetClass}}" checkbox-class="{{checkboxClass}}" checkbox-fieldset-class="{{checkboxFieldsetClass}}" items-remove-button-class="{{itemsRemoveButtonClass}}" items-add-button-class="{{itemsAddButtonClass}}" items-remove-icon-class="{{itemsRemoveIconClass}}" items-add-icon-class="{{itemsAddIconClass}}"></form-field>\
                </td>\
              </tr>\
            </tbody>\
          </table>';

        var html = components[scope.field.component];
        
        if (!html) {
          html = '[' + scope.field.component + ']';
        }

        element.replaceWith($compile(html)(scope));
      }
    };
  });