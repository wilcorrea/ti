
/* global angular, document, window */
'use strict';

angular.module('starter')

  .factory('TemplateCtrl', ['$http', '$q', '$ionicPopup', function($http, $q, $ionicPopup) {

    var TemplateCtrl = {

      /**
       * 
       * @param object model
       * @param layouts model
       * @param string index
       */
      configure: function(model, layouts, index) {

        var operation = model.operations[index]
          , grid = []
          , toolbar = []
          , className = '';

        /**
         * 
         * parse actions
         */
        for (var j in operation.operations) {

          var action = operation.operations[j];

          if (typeof action === 'string') {

            if (model.operations[j]) {

              className = action ? action : (model.operations[j].class ? model.operations[j].class : 'default');

              if (model.operations[j].position === 'grid') {

                grid.push(TemplateCtrl.action(j, model.operations[j], className));
              } else if (model.operations[j].position === 'toolbar') {

                toolbar.push(TemplateCtrl.action(j, model.operations[j], className));
              }
            }
          } else {

            if (model.operations[j]) {

              if (typeof action.grid === 'string') {

                grid.push(TemplateCtrl.action(j, model.operations[j], action.grid ? action.grid : 'default'));
              }

              if (typeof action.toolbar === 'string') {

                toolbar.push(TemplateCtrl.action(j, model.operations[j], action.toolbar ? action.toolbar : 'default'));
              }
            }
          }
        }

        model.operations[index].actions = {
            grid: grid
          , toolbar: toolbar
        };


        switch (operation.layout) {

          case 'list':
            //{

            var items = App.clone(model.items);

            model.operations[index].list = {};

            if (layouts.list && layouts.list.operations[index]) {

              items = layouts.list.operations[index](items);

              if (layouts.list.operations[index].row) {

                model.operations[index].list.row = layouts.list.operations[index].row;
              }
            }

            if (!model.operations[index].list.row) {

                model.operations[index].list.row = function(_object) {

                var html = [];
                //var __fragments = [];

                for (var _i in _object) {

                  if (_i !== 'counter' && _i !== 'checked' && _i !== model.properties.description && _i !== '$$hashKey') {

                    var 
                        label = items[_i] ? items[_i].label : ''
                      , icon = 'fa-tag'
                      , value = TemplateCtrl.render(_i, items[_i], _object[_i]);
  
                    if (items[_i]) {
                      icon = items[_i].icon ? items[_i].icon : icon
                    }

                    html.push('<div class="fragment" title="' + label + ': ' + value + '"> <i class="fa ' + icon + '"></i> ' + value +  ' </div>');
                    /*__fragments.push({
                        label: label
                      , icon: icon
                      , value: value
                    });*/
                  }
                }

                return html.join(' ');//<i class="fa fa-tag"></i> //<span class="fragment-divider"></span> 
                //return __fragments;
              };
            }

            for (var l in items) {

              var item = items[l];

              if (item.grid === 1) {
                model.operations[index].list[item.id] = item;
              }
            }
            //}
            break;

          case 'manager':
            //{

            var items = App.clone(model.items);

            if (layouts.manager && layouts.manager.operations[index]) {
              items = layouts.manager.operations[index](items);
            }

            if (!model.properties.tabs) {
              model.properties.tabs = [''];
            }

            model.operations[index].manager = [];

            model.properties.tabs.forEach(function(tab) {

              var lines = [];

              for (var line = 1; line <= model.properties.lines; line++) {

                var fields = [];

                for (var l in items) {

                  var item = items[l]
                    , config = {};

                  if (parseInt(item.form) === 1 && (parseInt(item.line) === parseInt(line)) && (!tab || item.tab === tab)) {

                    fields.push(item);
                  }
                }

                lines.push({
                    line: line
                  , fields: fields
                });
              }

              model.operations[index].manager.push({
                  label: tab
                , lines: lines
              });

            });

            //}
            break;

        }

        return model;
      },

      /**
       *  
       * @param string id
       * @param object operation
       * @param string className
       */
      action: function(id, operation, className) {

        if (className === 'primary') {
          className = 'warning';
        }

        return {
           "id": id
          , "action": operation.action
          , "label": operation.label
          , "type": operation.type
          , "get": operation.get
          , "className": className
          , "classIcon": operation.icon ? operation.icon : 'icon-' + id
        };
      },

      /**
       * @param string key
       * @param object item
       * @param mixed value
       */
      render: function(key, item, value) {

        var rendered = value;

        if (item && item.type) {

          switch (item.type) {
            case 'yes/no':
              rendered = value ? 'Sim' : 'Não';
              break;
          }
        } else {

          //console.log('TemplateCtrl.render: ', key, item);
        }
        
        return rendered;
      },

      /**
       * 
       * @param $scope scope
       * @param string module
       * @param string entity
       * @param string operation
       * @param string settings
       * @param string mode
       * @param int page
       * @param function callback
       */
      get: function(scope, module, entity, operation, settings, mode, page, callback) {

        var 
            url = ['api', App.APP_VERSION, 'get', module, entity, operation].join('/')
            , data;

        switch (mode) {

          case 'collection':
  
            if (page) {
              settings.begin = (page - 1) * settings.end;
            }
  
            data = {object: scope.search ? scope.search.data : {}, settings: settings};

            break;

          case 'object':

            if (scope.data) {

              var id = scope.model.properties.reference
                , object = {};

              object[id] = scope.data[id];

              data = {object: object};
            }

            break;

            case 'new':

              data = false;
              callback({});

              break;

            case 'search':

              data = false;
              callback(scope.search.data ? App.clone(scope.search.data) : {});

              break;
        }


        if (data) {
          
          $http
  
            .post([App.APP_PAGE, url].join(''), data)
  
              .success(function(response, status) {

                if (response.status === 'success') {

                  var collection = response.data.collection;

                  if (mode === 'object') {

                    collection = response.data.collection[0];
                  }

                  callback(collection, response.data.total, response.data.begin, response.data.end);
                }
              });

        }
      },
      
      /**
       * 
       * @param string module
       * @param string entity
       * @param string operation
       */
      nonce: function(module, entity, operation) {
        
        var 
            deferred = $q.defer()
          , url = ['api', App.APP_VERSION, 'nonce', module, entity, operation].join('/');

        $http

          .post([App.APP_PAGE, url].join(''))

            .success(function(response, status) {
              if (response.status === 'success') {
                deferred.resolve(response.data);
              } else {
                deferred.reject(response.message);
              }
            })
            .error(function(response) {
              deferred.reject(response);
            });

        return deferred.promise;
      },

      /**
       * 
       * @param string module
       * @param string entity
       * @param string current
       * @param object operation
       * @param object nonce
       * @param object _object
       * @param object _filter
       * @param object callback
       */
      post: function(module, entity, current, operation, nonce, _object, _filter, callback) {

        var 
          url = ['api', App.APP_VERSION, 'post', module, entity, current, nonce].join('/')
        , data = {
            object: _object
          , filter: _filter
        };

        $http.post([App.APP_PAGE, url].join(''), data).success(function(response, status) {

          try {

            callback(response);
          } catch (e) {

          }
        });
      }

    };

    return TemplateCtrl;
  }]);
