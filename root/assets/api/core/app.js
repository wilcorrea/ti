
/* global angular, document, window */
'use strict';


var App = angular.module('starter', ['ionic', 'ionic-material', 'ngCordova', 'autocomplete'])

  .run(function($ionicPlatform, $ionicSideMenuDelegate, $cordovaStatusbar) {

    document.addEventListener("deviceready", function() {

      $cordovaStatusbar.overlaysWebView(true);
      $cordovaStatusBar.style(1); //Light

      document.addEventListener("menubutton", function() {

        if (!$ionicSideMenuDelegate.isOpenRight()) {
          $ionicSideMenuDelegate.toggleRight();
        }
      }, false);
    }, false);

    $ionicPlatform.ready(function() {

      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }

      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });

  })

  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $controllerProvider) {

    // Turn off caching for demo simplicity's sake
    $ionicConfigProvider.views.maxCache(0);

    /*
     // Turn off back button text
     $ionicConfigProvider.backButton.previousTitleText(false);
     */
    App.http = angular.injector(['ng']).get('$http');

    App.stateProvider = $stateProvider;

    App.stateProvider

      .state('login', {
        url: '/login',
        templateUrl: 'app/html/login'
      })

      .state('app', {
          url: '/app'
        , abstract: true
        , templateUrl: 'app/html/main'
        , controller: 'AppMainCtrl'
      })

      .state('app.main', {
          url: '/main'
        , views: {
          page: {
              templateUrl: 'app/html/index'
            , controller: 'AppMainCtrl'
          }
        }
      })

      .state('app.main.home', {
        url: '/home',
        views: {
          target: {
            templateUrl: 'app/html/home',
            controller: 'AppHomeCtrl'
          }
        }
      });
      
    $urlRouterProvider.otherwise('/app/main/home');

    App.registerCtrl = $controllerProvider.register;
  })
  
  .run(function ($rootScope, $state, AuthService, AUTH_EVENTS) {

  });
