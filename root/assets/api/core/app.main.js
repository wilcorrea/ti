
/* global angular, document, window */
'use strict';

App
  .$config = {
    name: 'Administrador'
  };

App
  .path = function(module, entity, resource, operation) {
    var path = '';
    switch (resource) {
      case 'model':
        path = 'app/model/' + module + '/' + entity + '.class.js';
        break;
      case 'view':
        path = 'app/view/' + module + '/' + entity + '/' + operation + '.html';
        break;
      case 'controller':
        path = 'app/controller/' + module + '/' + entity + '.ctrl.js';
        break;
    }
    return path;
  };

App
  .storage = 'localstorage';

App
  .drivers = {
    localstorage: {
      class: 'app/api/storage/LocalStorage.js' 
    }
  };

App
  .driver = require(App.drivers[App.storage].class);

App
  .models = {};

App
  .import = function(module, entity) {

    var 
       index = module + '.' + entity
      , model;

    if (!App.models[index]) {

      App.models[index] = require(App.path(module, entity, 'model'));
    }

    model = App.models[index];

    return model;
  };


App
  .states = {};

App
  .state = function(module, entity, operation) {

      var 
        state = module + '.' + entity + '.' + operation;

      if (!App.states[module]) {

        App.stateProvider.state(module, {
            url: '/' + module
          , abstract: true
          , templateUrl: 'app/html/main'
          , controller: 'AppMainCtrl'
        });

        App.states[module] = {};
      }

      if (!App.states[module][entity]) {

        App.stateProvider.state(module + '.' + entity, {
            url: '/' + entity
          , views: {
            page: {
                templateUrl: 'app/html/index'
              , controller: 'AppMainCtrl'
            }
          }
        });

        var imported = App.import(module, entity);
        if (imported) {

          App.states[module][entity] = {};
        }

        App.states[module][entity].search = null;
      }

      if (!App.states[state]) {

        require(App.path(module, entity, 'controller'));

        App.stateProvider.state(state, {
          url: '/' + operation,
          views: {
            target: {
                templateUrl: App.path(module, entity, 'view', operation)
              , controller: entity + 'Ctrl'
              , params: ['data']
            }
          }
        });

        App.states[state] = {};
      }

    };


App.
  WINDOW = jQuery(window);

//['MOBILE', 'WEB', 'DESKTOP']
App.
  DEVICE = 'WEB';

App.
  HEIGHT_TOP = 188;

App.
  HEIGHT_LOST = 220;

App.
  HEIGHT_ROW = 60;

App.
  APP_END = Math.floor((App.WINDOW.height() - App.HEIGHT_LOST) / App.HEIGHT_ROW);

App.
  HEIGHT_CONTENT = App.WINDOW.height() - App.HEIGHT_TOP;

App.
  WINDOW
    .resize(function() {

      App.HEIGHT_CONTENT = App.WINDOW.height() - App.HEIGHT_TOP;
      App.APP_END = Math.floor((App.WINDOW.height() - App.HEIGHT_LOST) / App.HEIGHT_ROW);
    });

App.
  compact = function(compact) {

    var $body = jQuery('body');
    if (typeof compact === 'undefined') {
      $body.toggleClass('menu-compact');
    } else if (compact) {
      $body.removeClass('menu-compact').addClass('menu-compact');
    } else {
      $body.removeClass('menu-compact');
    }
  };

App.
  isArray = function(value) {
    
    return Object.prototype.toString.call(value) === '[object Array]';
  };

App.
  isEmpty = function (obj) {

    for (var prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return true;
  };

/**
 * 
 * @param mixed obj
 * 
 * @return mixed "obj clone"
 */
App.

  clone = function (obj) {

    var copy;

    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) { 
      return obj
    };

    // Handle Date
    if (obj instanceof Date) {
      copy = new Date();
      copy.setTime(obj.getTime());
      return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
      copy = [];
      for (var i = 0, len = obj.length; i < len; i++) {
          copy[i] = App.clone(obj[i]);
      }
      return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
      copy = {};
      for (var attr in obj) {
          if (obj.hasOwnProperty(attr)) copy[attr] = App.clone(obj[attr]);
      }
      return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
  };

App.
  guid = function() {

    var _s4 = function() {
      return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };
    return _s4() + '' + _s4() + '-' + _s4() + '-' + _s4() + '-' + _s4() + '-' + _s4() + _s4() + _s4();
  };

App.
  url = function(service, url) {
    
    var _service = '';

    switch(service) {
      case 'image':
        _service = 'theme/default/public/assets/images/';
        break;
    }

    return App.APP_PAGE + _service + url;
  };

App.
  error = function(e) {
    console.log(e);
  };


App
  .create = {

    /**
     * 
     * @param string module
     * @param string entity
     * @param string properties
     * @param string items
     * @param string operations
     */
    model: function(module, entity, properties, items, operations) {
    
      return new Model(module, entity, properties, items, operations);
    },

    /**
     * 
     * @param string module
     * @param string entity
     * @param string layouts
     */
    controller: function(module, entity, layouts) {
    
      return new Controller(module, entity, layouts);
    }
  };
  
App

  .manager = {

    search: function (operation, id) {

      var search = null;

      operation.manager.forEach(function(tab) {
    
        tab.lines.forEach(function(line) {
    
          line.fields.forEach(function(field) {
    
            if (field.id === id) {
    
              search = field;
            }
          });
        });

      });

      return search;
    }
  };



