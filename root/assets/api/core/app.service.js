/* global angular, document, window */
'use strict';

angular.module('starter')

  .service('AuthService', function($q, $http, USER_ROLES) {

    var username = ''
      , isAuthenticated = false
      , role = ''
      , authToken;

    function loadUserCredentials() {
      var token = window.localStorage.getItem(LOCAL_TOKEN);
      if (token) {
        useCredentials(token);
      }
    }

    function storeUserCredentials(token) {
      window.localStorage.setItem(LOCAL_TOKEN, token);
      useCredentials(token);
    }

    function useCredentials(token) {

      username = token.split('.')[0];

      isAuthenticated = true;

      authToken = token;

      if (username == 'admin') {
        role = USER_ROLES.admin
      }
      if (username == 'user') {
        role = USER_ROLES.public
      }

      // Set the token as header for your requests!
      $http.defaults.headers.common['X-Auth-Token'] = token;
    }

    function destroyUserCredentials() {
      authToken = undefined;
      username = '';
      isAuthenticated = false;
      $http.defaults.headers.common['X-Auth-Token'] = undefined;
      window.localStorage.removeItem(LOCAL_TOKEN);
    }

    function guid() {
      function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
      }
      return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
    }

    var login = function(username, password) {

      return $q(function(resolve, reject) {

        if (true) {
          var id = guid();
          storeUserCredentials(id);
          resolve(id);
        } else {
          reject('Please check your credentials!');
        }
      });
    };

    var logout = function() {
      destroyUserCredentials();
    };

    var isAuthorized = function(authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
    };

    var token = function() {
      return window.localStorage.getItem(LOCAL_TOKEN);
    };

    loadUserCredentials();

    return {
      login: login,
      logout: logout,
      isAuthorized: isAuthorized,
      isAuthenticated: function() {
        return isAuthenticated;
      },
      token: token,
      username: function() {
        return username;
      },
      role: function() {
        return role;
      },
      permissions: function(module, entity, operation, callback) {

        var menus = App.menus;

        if (!menus) {

          var url = ['api', App.APP_VERSION, 'permissions', module, entity, operation].join('/')
  
          $http
  
            .post([App.APP_PAGE, url].join(''))
  
              .success(function(response, status) {
  
                if (response.status === 'success') {

                  App.menus = response.data;
                  menus = App.menus;
                }

                callback(menus);
              });
        } else {

          callback(menus);
        }

      }
    };
  })

  .factory('AuthInterceptor', function($rootScope, $q, AUTH_EVENTS) {
    return {
      responseError: function(response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated,
          403: AUTH_EVENTS.notAuthorized
        }[response.status], response);
        return $q.reject(response);
      }
    };
  })

  .config(function($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});
