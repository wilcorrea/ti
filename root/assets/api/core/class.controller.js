
/* global angular, document, window */
'use strict';

var Controller = function(module, entity, rotule, layouts, onchange) {
  
  return ['$scope', '$state', 'TemplateCtrl', '$ionicScrollDelegate', '$timeout', '$ionicPopup', function($scope, $state, TemplateCtrl, $ionicScrollDelegate, $timeout, $ionicPopup) {

    /**
     * setup $scope references
     */
    var

      /**
       * @var current operation
       */
        operation = $state.current.url.replace('/', '')

      /**
       * @var current state
       */
      , state = module + '.' + entity + '.' + operation

      /**
       * @bind Model with Controller
       */
      , model = App.import(module, entity);

    $scope.rotule = rotule;

    $scope.module = module;
    $scope.entity = entity;

    // recover a preloaded nonce to operation
    $scope.nonce = App.states[state].nonce ? App.states[state].nonce : 'none';

    // configure Model
    $scope.model = TemplateCtrl.configure(model, layouts, operation);

    // configure onchange
    $scope.model.onchange = function(_element, _collection, _event) {

      if (onchange[operation]) {

        var fnc = onchange[operation];

        try {

          fnc.call(this, $scope.operation, $scope.model, _element, _collection, _event);
        } catch (e) {
          
        }
      }
    };

    $scope.operation = model.operations[operation];
    
    /**
     * 
     * @var search manager
     */
    var searchTimeout;

    $scope.search = {

        data: !App.isEmpty(App.states[module][entity].search) ? App.states[module][entity].search : null

      , labels: {
          filter: ''
        , clear: 'Limpar'
        , input: 'Pesquisar...'
      }
      , query: function () {

        if (searchTimeout) {
          $timeout.cancel(searchTimeout);
        }

        searchTimeout = $timeout(function() {

          App.states[module][entity].query = $scope.settings.query;

          $scope.pagination(1);
        }, 300);
      }

      , show: function() {
        
        $scope.popup.open('Filtro', $scope.search.data);
      }

      , add: function(data) {

        var 
            _data = {}
          , counter = 0;

        for (var id in data) {
          if (data.hasOwnProperty(id) && data[id]) {
            _data[id] = data[id];
            counter++;
          }
        }
        
        if (counter === 0) {
          _data = null;
        }

        App.states[module][entity].search = _data;
      }

      , clear: function () {

        $scope.search.data = null;
        App.states[module][entity].search = null;
        
        $scope.pagination(1);
      }
    };

    /**
     * setup environment variables
     */
    $scope.device = App.DEVICE;
    $scope.height = App.HEIGHT_CONTENT;
    $scope.height_content = App.HEIGHT_CONTENT;
    $scope.all = false;


    /**
     * init the collection of operation
     */
    $scope.collection = [];
    $scope.total = 0;
    $scope.page = App.states[state].page ? App.states[state].page : 1;
    $scope.pages = $scope.page;

    $scope.settings = {
        query: App.states[module][entity].query
      , sort: ''
      , begin: 0
      , end: App.APP_END
    };


    /**
     * @function transition collection data
     * 
     * @param _page
     */
    $scope.pagination = function(_page) {

      if (_page <= 0) {

        $scope.page = 1;

      } else if (_page > $scope.pages) {

        $scope.page = $scope.pages;
      } else {
        
        $scope.page = _page;
      }

      var mode = $scope.operation.get;
      // hack
      if ($scope.operation.action === 'add') {
        mode = 'new';
      }

      TemplateCtrl.get($scope, $scope.module, $scope.entity, $scope.operation.action, $scope.settings, mode, $scope.page, function(collection, total, begin) {

        $scope.collection = collection;

        if (App.isArray(collection)) {

          if (collection.length) {
            
            $scope.height = collection.length * 60;

            $scope.collection = collection;
  
            $scope.total = total;
            $scope.page =  Math.ceil(begin / $scope.settings.end) + 1;
            $scope.pages = Math.ceil(total / $scope.settings.end);

            App.states[state].page = $scope.page;
          } else {

            $scope.collection = null;
          }

        } else {
          
          //$scope.configure
        }
      });

      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };

    $scope.pagination($scope.page);

    /**
     * @var activate infinite scroll to collection of operation
     */
    $scope.refreshedTop = 0;

    /**
     * @function infinite scroll process
     */
    $scope.checkScroll = function () {

      try {
        var currentTop = $ionicScrollDelegate.$getByHandle('libScroll').getScrollPosition().top;
        var maxTop = $ionicScrollDelegate.$getByHandle('libScroll').getScrollView().__maxScrollTop;

        if (currentTop >= maxTop && maxTop != $scope.refreshedTop) {

          $scope.refreshedTop = maxTop;

          $scope.settings.begin = $scope.settings.begin + $scope.settings.end;

          var mode = $scope.operation.get;
          // hack
          if ($scope.operation.action === 'add') {
            mode = 'new';
          }
    
          TemplateCtrl.get($scope, $scope.module, $scope.entity, $scope.operation.action, $scope.settings, mode, null, function(collection) {

            try {

              collection.forEach(function(newItem) {

                $scope.collection.push(newItem);
              });

              if (!$scope.$$phase) {
                $scope.$apply();
              }

            } catch(e) {

              App.error(e);
            }
          });

        }
      } catch(e) {

        App.error(e);
      }
    };



    /**
     * 
     * @var right panel
     */
    $scope.right = {

        data: null

      , open: function(object) {
        $scope.right.data = object;
      }

      , close: function() {
        $scope.right.data = null;
      }

      , toggle: function(object) {
        $scope.right.data = $scope.right.data && $scope.right.data === object ? null : object;
      }
    };



    /**
     * 
     * @var popup panel
     */
    $scope.popup = {

        data: null
      , title: null

      , open: function(title, data) {

        $scope.popup.title = title;
        $scope.popup.data = data;
      }

      , close: function(fnc) {

        $scope.popup.title = null;
        $scope.popup.data = null;

        if (fnc) {
          try {
            fnc.call();
          } catch (e) {
            
          }
        }
      }

      , toggle: function(object) {

        $scope.popup.data = $scope.popup.data && $scope.popup.data === object ? null : object;
        if ($scope.popup.data === null) {
          $scope.popup.title = null;
        }
      }
    };


    /**
     * 
     * @function resolve the operation scope
     * 
     * @param object _id
     * @param object _object
     */
    $scope.resolve = function(_id, _object) {

      var 
          _operation = model.operations[_id]
        , type = _operation.type
        , confirm = _operation.confirm
        , action = _operation.action;

      if (_operation.get === 'clear') {

        _object = null;
      } else if (_operation.get === 'find') {

        $scope.search.add(_object);
      }

      var resolve = function(type, operation, __object) {
        
        switch (type) {

          case 'view':

            var _state = $scope.module + '.' + $scope.entity + '.' + operation;

            if ($state.current.name !== _state) {

              $scope.open(_state, __object);
            } else if (_operation.get === 'new') {

              $scope.collection = {};
            }

            break;

          case 'alias':
          case 'post':

            var post = function(nonce) {

              App.states[state].nonce = nonce;
              $scope.nonce = App.states[state].nonce;

              if (type === 'alias') {
                operation = $scope.operation.action;
              }

              TemplateCtrl.post($scope.module, $scope.entity, operation, $scope.model.operations[operation], nonce, __object, $scope.operation.filter, function(response) {

                try {

                  switch (response.status) {

                    case 'success':

                      var reference = $scope.model.properties.reference;

                      if (App.isArray(response.data.referenced)) {

                        if (!$scope.collection[reference]) {
                          $scope.collection[reference] = response.data.result;
                        }
                      }

                      if ($scope.model.operations[operation].execute === "Application.form.reloadGrid();") {

                        $scope.pagination($scope.page);
                      }

                      if ($scope.operation.settings) {

                        $scope.alerts.add($scope.rotule, $scope.operation.settings.success);
                      }
                      break;

                    case 'warning':

                      if ($scope.operation.settings) {

                         $scope.alerts.add($scope.rotule, $scope.operation.settings.warning);
                      }
                      break;

                    case 'fail':

                      if ($scope.operation.settings) {

                         $scope.alerts.add($scope.rotule, $scope.operation.settings.fail);
                      }
                      break;
                  }

                } catch (e) {
                  App.error(e);
                }
              });
            };

            if ($scope.nonce && $scope.nonce !== 'none') {

              post($scope.nonce);
            } else {

              var promise = TemplateCtrl.nonce($scope.module, $scope.entity, operation);
              promise.then(function(nonce) {

                post(nonce);
              });
            }

            break;
        }
      };

      if (confirm) {

        $scope.confirm(confirm, function() {

          resolve(type, action, _object);
        });
        /*if (window.confirm(confirm)) {

          resolve(type, action, _object);
        }*/
      } else {

        resolve(type, action, _object);
      }
    };

    /**
     * 
     * @function check all in list
     */
    $scope.checkAll = function(checked) {

      for (var i in $scope.collection) {
        $scope.collection[i].checked = checked;
      }
      
      if (!$scope.$$phase) {
        $scope.$apply();
      }
    };


    /**
     * 
     * @function generate publics urls to services
     * 
     * @param string uri
     * 
     * @return string url
     */
    $scope.url = function(service, uri) {

      return App.url(service, uri);
    };


    /**
     * 
     * @param string title
     * @param function success
     * @param string subTitle
     * @param string ok
     * @param string cancel
     * @param string abort
     */
    $scope.prompt = function(title, success, subTitle, ok, cancel, abort) {

      var prompt = $ionicPopup.show({
        template: '<input type="text" ng-model="_prompt">',
        title: title,
        subTitle: subTitle ? subTitle : '',
        scope: $scope,
        buttons: [{
            text: cancel ? cancel : 'Cancel'
          },{
            text: ok ? ok : 'Ok',
            type: 'button-positive',
            onTap: function(e) {
              //if (!$scope._prompt) {
              //  e.preventDefault();
              //} else {
                return $scope._prompt;
              //}
            }
          },
        ]
      });

      prompt.then(function(res) {
        try {
          if (res) {
            success.call();
          } else {
            abort.call();
          }
        } catch (e) {
          App.error(e);
        }
      });

    };

    /**
     * 
     * @param string title
     * @param function success
     * @param string template
     * @param string ok
     * @param string cancel
     */
    $scope.confirm = function(title, success, template, ok, cancel) {

      var confirm = $ionicPopup.confirm({
        title: title,
        template: template,
        buttons: [{
            text: cancel ? cancel : 'Cancel'
          },{
            text: ok ? ok : 'Ok',
            type: 'button-positive'
          },
        ]
      });

      confirm.then(function(res) {
        try {
          success.call();
        } catch (e) {
          App.error(e);
        }
      });
    };
    
    /**
     * 
     * @param string title
     * @param string template
     */
    $scope.alert = function(title, template) {

      $ionicPopup.alert({
        title: title,
        template: template
      });
    };

  }];

};

