
/* global angular, document, window */
'use strict';

var Model = function(module, entity, properties, items, operations) {

  return {

    module: module,

    entity: entity,

    properties: properties,

    items: items,

    operations: operations,

    create: function(object, params, callback) {

      App.driver.insert(object, params, callback);
      /*
      var promise = $http.get('test.json').then(function (response) {
        console.log(response);
        return response.data;
      });
      return promise;
      */
    },

    read: function(object, params, callback) {

      App.driver.select(object, params, callback);
    },

    update: function(object, params, callback) {

      App.driver.update(object, params, callback);
    },

    delete: function(object, params, callback) {

      App.driver.delete(object, params, callback);
    }

  };

};

