
/**
 * 
 * @class LocalStorage class
 */
var LocalStorage = {

  /**
   * 
   * @returns {_connectionection|localStorageDB}
   */
  _connection: null,
  /**
   * 
   * @returns {_connectionection}
   */
  _: function() {

    if (LocalStorage._connection === null) {

      LocalStorage._connection = new localStorageDB('database', localStorage);
      
      if(LocalStorage._connection.isNew() ) {

        LocalStorage._connection.createTable("TBL_TAREFA", ["trf_codigo", "trf_titulo", "trf_inicio", "trf_termino", "trf_finalizada"]);

        LocalStorage._connection.commit();
      }
    }
    return LocalStorage._connection;
  },
  /**
   * 
   * @param {type} ddl
   * @param {type} params
   * @param {type} callback
   * @returns {undefined}
   */
  insert: function(ddl, params, callback) {

    try {

      var id = LocalStorage._().insert(ddl, params);
      if (id) {
        LocalStorage._().commit();
        callback.call(this, true, id);
      }
    } catch(e) {
      console.error(e);
      callback.call(this, false, null);
    }
  },
  /**
   * 
   * @param {type} ddl
   * @param {type} params
   * @param {type} callback
   * @returns {undefined}
   */
  select: function(ddl, params, callback) {

    try {

      var rows = LocalStorage._().queryAll(ddl, params);
      if (rows) {
        callback.call(this, true, rows);
      }
    } catch(e) {
      console.error(e);
      callback.call(this, false, null);
    }
  },
  /**
   * 
   * @param {type} ddl
   * @param {type} params
   * @param {type} callback
   * @returns {undefined}
   */
  update: function(ddl, params, callback) {

    try {

      var affected = LocalStorage._().insertOrUpdate(ddl, {ID: params.ID}, params);
      if (affected) {
        LocalStorage._().commit();
      }
      callback.call(this, true, affected);
    } catch(e) {
      console.error(e);
      callback.call(this, false, null);
    }
  },
  /**
   * 
   * @param {type} ddl
   * @param {type} params
   * @param {type} callback
   * @returns {undefined}
   */
  delete: function(ddl, params, callback) {

    try {

      var affected = LocalStorage._().deleteRows(ddl, params);
      if (affected) {
        LocalStorage._().commit();
      }
      callback.call(this, true, affected);
    } catch(e) {
      console.error(e);
      callback.call(this, false, null);
    }
  }
};

try {
  module.exports = LocalStorage;
} catch (e) {
  //requirejs tratment
}
