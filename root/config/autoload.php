<?php

/**
 * 
 * @magic autoload standard objects
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
 
/**
 * 
 * @param $class
 */
function __autoload($class) {
  
  $path = implode(APP_FS, [APP_PATH_ROOT, 'core', 'standard', implode('.', [$class, 'class','php'])]);

  if (App::exists($path)) {

    require_once $path;
  }
}