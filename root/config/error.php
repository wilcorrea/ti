<?php

/**
 * 
 * @magic errors handlers
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
 
/**
 * 
 * @param string $err_number
 * @param string $err_string
 * @param string $err_file
 * @param string $err_line
 * @param string $err_context
 */
function error_handler($err_number, $err_string, $err_file = "", $err_line = "", $err_context = "") {

  $dt = debug_backtrace(false);

  $index = 0;
  $err_trace = [];
  $history = [];

  foreach ($dt as $key => $trace) {

    if (isset($trace['file']) && isset($trace['line'])) {

      $tt = $trace['function'] . '(' . (isset($trace['args']) ? String::replace(json_encode($trace['args']), json_encode(APP_PATH), '') : '') . ') [' . String::replace($trace['file'], APP_PATH, '') . ' | ' . $trace['line'] . ']';

      if (isset($history[md5($tt)])) {
        break;
      }

      $index++;

      $history[md5($tt)] = true;
      $err_trace[$index] = $tt;

      if ($key == 0) {

        if ($err_file === "" and $err_line == "") {
          $err_file = $trace['file'];
          $err_line = $trace['line'];
        }
      }
    }
  }

  App::log($err_string);
  App::log($err_trace);
}

/**
 *
 * @param last
 */
function shutdown_function() {

  if (@is_array($error = @error_get_last())) {

    $code = isset($error['type']) ? $error['type'] : 0;

    $err_string = isset($error['message']) ? $error['message'] : '';
    $err_file = isset($error['file']) ? $error['file'] : '';
    $err_line = isset($error['line']) ? $error['line'] : '';

    if ($code > 0) {

      error_handler("Fatal Error", $err_string, $err_file, $err_line, null);
    }
  }
}

set_error_handler('error_handler');
register_shutdown_function('shutdown_function');

