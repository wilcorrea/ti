<?php


/**
 * 
 * @magic constants defined by users
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
 
define('DEFAULT_DATABASE', 'ti');

define('DEFAULT_THEME', 'default');

define('DEFAULT_THEME_CLIENT', '');

define('APP_COMPANY', 'Ovum / dracones.io');

define('APP_NAME', 'Ovum');

define('APP_COPY_RIGHT', 'all rights reserved © 2011~' . date('Y'));

define('APP_VERSION', '0.0.0.1');

define('APP_SECURITY', '0.0.0.1');


define('UTF8_RECOVERED', 'encode');

define('UTF8_NOT_RECOVERED', '');

define('UTF8_LANG', '');



define('UTF8_REQUEST', '');

define('UTF8_PRINT', '');


define('HTML_ENTITIES_REQUEST', '');

define('ADD_SLASHES_REQUEST', '');
