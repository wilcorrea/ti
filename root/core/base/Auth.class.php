<?php

/**
 *
 * @class Auth
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Auth {

  
  /**
   * 
   * @param string $login
   * @param string $password
   * @param boolean $remember
   */
  public static function login($login, $password, $remember = false) {
    
    $user = new Object();

    $connected = false;
    $message = '';

    $error = '';

    $token = Cookie::get(Application::$cookies['token']);

    if ($token) {

      $user = self::read($token);

      if ($user->id) {
        
        if ($user->status) {
  
          $connected = true;
        } else {
  
          $error = '3';
        }
      }
    }


    if (!$connected) {

      if ($login !== null && $password !== null) {

        if ($login && $password) {

          $query = self::query($login);

          if ($query !== null) {

            $user = self::query($login, $password);

            if ($user !== null) {

              if ($user->status) {

                $connected = true;
              } else {

                $error = '4';
              }

            } else {
              
              $error = '3';
            }
          } else {

            $error = '2';
          }
        } else {

          $error = '1';
        }
      }
    }

    if ($error) {

      $message = String::replace(App::lang('site.login.error' . $error), '{login}', $login);
    }

    if ($connected) {

      $token = self::start($user, $remember, $token);
    }

    return (object)['user' => $user, 'token' => $token, 'connected' => $connected, 'message' => $message];
  }

  /**
   * 
   * @param string $uri
   */
  public static function logout($uri) {

    //var_dump($uri);

    self::destroy();

    header('Location: ' . APP_PAGE);
  }

  /**
   * 
   * @param string $login
   * @param string $password
   */
  public static function query($login, $password = '') {

    $user = null;

    System::import('class', 'base', 'DAO', 'core');

    $dao = new DAO();

    $sql = 
        "SELECT" 
      . " usu_codigo AS id, usu_nome AS name, usu_email AS email, usu_login AS login, usu_cod_USUARIO_TIPO AS type, usu_ativo AS status " 
      . "FROM" 
      . " TBL_USUARIO " 
      . "WHERE" 
      . " (usu_login = '" . addslashes($login) . "' OR  usu_email = '" . addslashes($login) . "')" 
      . ($password ? " AND usu_password = MD5('" . addslashes($password) . "')" : "");


    $result = $dao->selectSQL($sql, false);
  
    while ($row = $dao->fetchObject($result)) {

      $user = $row;
    }
    
    return $user;
  }

  /**
   * 
   * @param string $token
   * @param string $module
   * @param string $entity
   * @param string $operation
   * 
   * @return array permissions
   */
  public static function permissions($token, $module, $entity, $operation) {


    $user = self::read($token);

    System::import('class', 'base', 'DAO', 'core');

    $dao = new DAO();

    $sql = 
          "SELECT"
        . "	men_codigo AS _codigo, men_descricao AS _menu, mef_descricao AS _name, CONCAT(mef_module, '.', mef_entity, '.', mef_action) AS _link, mef_icon AS _icon, mef_separator AS _separator "
        . "FROM"
        . "	TBL_MENU_FUNCAO"
        . " LEFT JOIN TBL_MENU ON (mef_cod_MENU = men_codigo)"
        //. " LEFT JOIN TBL_MENU_FUNCAO_USUARIO_TIPO ON (mft_cod_MENU_FUNCAO = mef_codigo)"
        //. "	LEFT JOIN TBL_USUARIO ON (usu_cod_USUARIO_TIPO = mft_cod_USUARIO_TIPO)"
        . "WHERE"
        //. "	usu_codigo = '" . (int) $user->id . "' "
        . "	TRUE " . (($module !== 'app') ? " AND mef_module = '" . String::removeSpecial($module) . "' " : "")
        . "ORDER BY"
        . " mef_order, mef_order"
        ;

    $result = $dao->selectSQL($sql, false);
  
    $menus = [];

    while ($row = $dao->fetchObject($result)) {

      $menus[$row->_codigo]['name'] = utf8_encode($row->_menu);
      $menus[$row->_codigo]['items'][] = (object) ['name' => utf8_encode($row->_name), 'link' => $row->_link, 'icon' => $row->_icon, 'separator' => $row->_separator];
    }

    $operations = [];

    return ['menus'=>$menus, 'operations'=>$operations];
  }

  /**
   * 
   * @param string $user
   * @param boolean $remember
   * @param boolean $override
   */
  public static function start($user, $remember, $token) {

    $path = '/';
    $host = APP_HOST;

    $expire = null;
    if (!$token) {
      
      $token = self::guid();

      $expire = null;
      if ($remember) {
        $expire = time() + (3600 * 24 * 30); //30 dias 
      }
      setcookie(Application::$cookies['token'], $token, $expire, $path, $host);

      self::write($token, Json::encode($user));
    }

    setcookie(Application::$cookies['session'], Json::encode($user), $expire, $path, $host);
    setcookie(Application::$cookies['remember'], $remember, null, $path, $host);

    return $token;
  }

  /**
   * 
   * @param string $token
   * @param string $user
   * 
   * @return boolean
   */
  public static function write($token, $user) {
    
    return File::saveFile(self::local($token), $user);
  }

  /**
   * 
   * @param string $token
   */
  public static function read($token) {

    $user = new Object();

    $filename = self::local($token);

    if (File::exists($filename)) {

      $user = Json::decode(File::openFile($filename));
    }

    return $user;
  }

  /**
   * 
   * @param string $token
   */
  public static function destroy($token = null) {

    $path = '/';
    $host = APP_HOST;

    $token = $token ? $token : Cookie::get(Application::$cookies['token']);

    $filename = self::local($token);

    if (File::exists($filename)) {

      File::removeFile($filename);
    }

    setcookie(Application::$cookies['token'], "", time(), $path, $host);

    setcookie(Application::$cookies['session'], "", time(), $path, $host);
    setcookie(Application::$cookies['remember'], "", time(), $path, $host);
  }
  
  /**
   * 
   * @param string $token
   * 
   * @return string
   */
  private static function local($token) {
    
    return File::path(APP_PATH, 'storage', 'session', $token);
  }

  /**
   * 
   * @return void
   */
  public static function clear() {

    if (isset($_SERVER['HTTP_COOKIE'])) {

      $cookies = explode(';', $_SERVER['HTTP_COOKIE']);

      foreach ($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time() - 1000);
        setcookie($name, '', time() - 1000, '/');
      }
    }
  }
  
  /**
   * 
   * @param string $token
   * @param string $module
   * @param string $entity
   * @param string $operation
   */
  public static function isAuthorized($token, $module, $entity, $operation) { 

    $isAuthorized = false;

    $filename = self::local($token);

    if (File::exists($filename)) {

      $isAuthorized = true;
    }

    return $isAuthorized;
  }
  
  /**
   * 
   * @param type $brackets
   * @return string
   */
  public static function guid($brackets = false) {

    mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.

    $charid = String::upper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);
    $uuid = String::substring($charid, 0, 8) . $hyphen . String::substring($charid, 8, 4) . $hyphen . String::substring($charid, 12, 4) . $hyphen . String::substring($charid, 16, 4) . $hyphen . String::substring($charid, 20, 12);
    if ($brackets) {
      $uuid = chr(123) . $uuid . chr(125);
    }

    return $uuid;
  }

}
