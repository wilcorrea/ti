<?php

/**
 * 
 * @class Imap
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Imap {

  /**
   * 
   * @var string
   */
  private $hostname = '';

  /**
   * 
   * @var string
   */
  private $username = '';

  /**
   * 
   * @var string
   */
  private $password = '';

  /**
   * 
   * @var object
   */
  private $connection = null;

  /**
   * 
   * @param string
   * @param string
   * @param string
   * @param array
   */
  public function Imap($hostname, $username, $password, $options = array("port" => 143, "settings" => array("notls"), "tagged" => "INBOX")) {

    $this->hostname = $hostname;
    $this->username = $username;
    $this->password = $password;

    if (is_array($options)) {
      if (isset($options['port'])) {
        $this->hostname = $this->hostname . ":" . $options['port'];
      }
      if (isset($options['settings'])) {
        $settings = $options['settings'];
        if (is_array($settings)) {
          foreach ($settings as $value) {
            $this->hostname = $this->hostname . "/" . $value;
          }
        }
      }
      $this->hostname = "{" . $this->hostname . "}";
      if (isset($options['tagged'])) {
        $this->hostname = $this->hostname . $options['tagged'];
      }
    }
  }

  /**
   * 
   * @return resource
   */
  public function connect() {

    if ($this->connection === null) {
      $this->connection = imap_open($this->hostname, $this->username, $this->password) or die('Cannot connect to host: ' . imap_last_error());
    }

    return $this->connection;
  }

  /**
   * 
   * @return boolean
   */
  public function disconnect() {

    if ($this->connection) {
      imap_close($this->connection);
    }

    return $this->connection = null;
  }

  /**
   * 
   * @return int
   */
  public function getCountMessages() {

    $countMessages = 0;
    if ($this->connection) {
      $countMessages = imap_num_msg($this->connection);
    }

    return $countMessages;
  }

  /**
   * 
   * @param int $items
   * 
   * @return array
   */
  public function getMessages($items = 20) {

    $messages = array();

    $countMessages = $this->getCountMessages();
    
    $limit = $countMessages - $items;
    if ($countMessages < $items) {
      $limit = 0;
    }

    for ($i = $countMessages; $i > $limit; $i--) {

      $header = imap_header($this->connection, $i);

      $toInfo = (isset($header->to)) ? $header->to[0] : null;
      $fromInfo = $header->from[0];
      $replyInfo = $header->reply_to[0];
      $uid = imap_uid($this->connection, $i);

      $message = array(
        "id" => $i,
        "to" => (isset($toInfo->mailbox) && isset($toInfo->host)) ? $toInfo->mailbox : "",
        "toAddr" => (isset($toInfo->mailbox) && isset($toInfo->host)) ? $toInfo->mailbox . "@" . $fromInfo->host : "",
        "fromAddr" => (isset($fromInfo->mailbox) && isset($fromInfo->host)) ? $fromInfo->mailbox . "@" . $fromInfo->host : "",
        "fromName" => (isset($fromInfo->personal)) ? $fromInfo->personal : "",
        "replyAddr" => (isset($replyInfo->mailbox) && isset($replyInfo->host)) ? $replyInfo->mailbox . "@" . $replyInfo->host : "",
        "replyName" => (isset($replyInfo->personal)) ? $replyInfo->personal : "",
        "subject" => (isset($header->subject)) ? $header->subject : "",
        "udate" => (isset($header->udate)) ? $header->udate : "",
        "body" => $this->getBody($uid, $this->connection)
      );

      $messages[$i] = $message;
    }

    return $messages;
  }

  /**
   * 
   * @param int $id
   * 
   * @return boolean
   */
  public function deleteMessage($id) {
    return imap_delete($this->connection, $id);
  }

  /**
   * 
   * @return boolean
   */
  public function commit() {
    return imap_expunge($this->connection);
  }

  /**
   * 
   * @param object $uid
   * @param object $imap
   * 
   * @return object
   */
  private function getBody($uid, $imap) {

    $body = $this->get_part($imap, $uid, "TEXT/HTML");
    // if HTML body is empty, try getting text body
    if ($body == "") {
      $body = $this->get_part($imap, $uid, "TEXT/PLAIN");
    }
    return $body;
  }

  /**
   * 
   * @param object $imap
   * @param object $uid
   * @param object $mimetype
   * @param boolean $structure
   * @param int $partNumber
   * 
   * @return object
   */
  private function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false) {

    if (!$structure) {
      $structure = imap_fetchstructure($imap, $uid, FT_UID);
    }

    if ($structure) {
      if ($mimetype == $this->get_mime_type($structure)) {
        if (!$partNumber) {
          $partNumber = 1;
        }
        $text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
        switch ($structure->encoding) {
          case 3: return imap_base64($text);
          case 4: return imap_qprint($text);
          default: return $text;
        }
      }

      // multipart 
      if ($structure->type == 1) {
        foreach ($structure->parts as $index => $subStruct) {
          $prefix = "";
          if ($partNumber) {
            $prefix = $partNumber . ".";
          }
          $data = $this->get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
          if ($data) {
            return $data;
          }
        }
      }
    }
    return false;
  }

  /**
   * 
   * @param object $structure
   * 
   * @return string
   */
  private function get_mime_type($structure) {

    $primaryMimetype = array("TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER");

    if ($structure->subtype) {
      return $primaryMimetype[(int) $structure->type] . "/" . $structure->subtype;
    }
    return "TEXT/PLAIN";
  }

}
