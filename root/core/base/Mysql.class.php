<?php

/**
 *
 * @extends Database
 */
System::import('class', 'pattern', 'Database', 'core');


/**
 *
 * @class Mysql
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Mysql extends Database {

  /**
   * 
   * @return object
   */
  public function connect() {

    $this->restore();

    if (!$this->getTransaction()) {

      $this->connection = mysqli_connect($this->server, $this->user, $this->password, $this->db, $this->port);
    }

    if ($this->debug) {

      print 'connect: ' . print_r($this->connection, true) . ' - ' . $this->db . '<br>';
    }

    if (is_object($this->connection)) {

      $select = mysqli_select_db($this->connection, $this->db);
      $this->connected = $select ? true : false;

    } else {

      if ($this->debug) {

        print "Mysql Conection: [" . $this->server . "], [" . $this->user . "], [" . $this->password . "]";
      }
    }

    return $this->connection;
  }

  /**
   *
   */
  public function disconnect() {

    if (!$this->getTransaction()) {

      if ($this->connected) {

        if (is_resource($this->connection)) {

          if ($this->debug) {
            print 'disconnect: ' . print_r($this->connection, true) . '<br>';
          }

          mysqli_close($this->connection);
          $this->connected = false;
        }
      }
    }
  }

  /**
   * 
   * @param string $sql
   * 
   * @return resource
   */
  protected function query($sql) {

    if ($this->debug) {
      print $sql . "<br>";
    }

    $u = App::get();
    $user = $u ? $u : "ANONIMO";

    $trace = "";
    //$dt = debug_backtrace(false);
    //if (is_array($dt)) {
    //  foreach ($dt as $k=> $v) {
    //    if (isset($v['file'])) {
    //      $trace = "[" . $v['file'] . "->" . $v['function'] . "]" . $trace;
    //    }
    //  }
    //}

    /*if (strpos($sql, 'TBL_BIBLIOTECA') === false 
        && strpos($sql, 'TBL_BIBLIOTECA_ITEM') === false
        && strpos($sql, 'A.pgn_publish AND A.pgn_home') === false
        && strpos($sql, 'UPDATE TBL_PAGINA') === false
      ) {

      die($sql);
    }*/

    $query = null;
    if ($this->connected) {

      $query = mysqli_query($this->connection, '/*' . $user . '*//*' . $trace . '*/ ' . $sql);
    }

    return $query;
  }

  /**
   * 
   * @param string $sql
   * @param boolean $save
   * @param boolean $validate
   * @param boolean $deactivate
   * 
   * @return int (auto increment id)
   */
  public function insert($sql, $save = true, $validate = true, $deactivate = false) {

    if ($validate) {
      $this->validate($sql);
    }

    $this->connect();
    if ($deactivate) {
      $this->query('SET FOREIGN_KEY_CHECKS = 0');
    }
    $this->query($sql);

    $inserted = false;
    if ($this->connected) {
      $inserted = (mysqli_insert_id($this->connection)) ? mysqli_insert_id($this->connection) : mysqli_affected_rows($this->connection);
    }
    $affected_rows = ($inserted) ? "Rows inserted: 1" : false;
    $backup = "NO BACKUP (NEW ID $inserted)";

    if ($deactivate) {
      $this->query('SET FOREIGN_KEY_CHECKS = 1');
    }

    $this->error($sql, true);
    $this->disconnect();

    if ($save) {
      $this->history($sql, $backup, $affected_rows);
    }
    return $inserted;
  }

  /**
   * 
   * @param string $sql
   * @param boolean $history
   * @param boolean $validate
   * @param string $id
   * @param boolean $transaction
   * 
   * @return int (affected rows)
   */
  public function execute($sql, $history = true, $validate = true, $id = "", $transaction = null) {

    if ($validate) {
      $this->validate($sql);
    }

    $backup = "";

    $this->connect($id);

    if ($transaction !== null) {
      $this->setTransaction($transaction);
    }

    $affected_rows = $this->query($sql, $this->connection);

    if ($this->connected) {

      $info = mysqli_info($this->connection);

      if ($affected_rows) {
        $affected_rows = mysqli_affected_rows($this->connection) ? mysqli_affected_rows($this->connection) : $affected_rows;
      }
    }

    $this->error($sql, true);

    $this->disconnect();

    if ($history) {

      $this->history($sql, $backup, $info);
    }

    return $affected_rows;
  }

  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public function fetchRow($result) {

    $fetched = mysqli_fetch_array($result);

    return $fetched;
  }

  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public function fetchObject($result) {

    $fetched = $result;

    if ($result !== null) {

      $fetched = mysqli_fetch_object($result);
    }

    return $fetched;
  }

  /**
   * 
   * @param string $sql
   * @param boolean $save
   * 
   * @return boolean
   */
  protected function error($sql, $save = true) {

    $err = false;

    if ($sql) {

      if (is_object($this->connection)) {
        $err_number = mysqli_errno($this->connection);
        $err_description = mysqli_error($this->connection);
      }

      if (isset($err_number)) {
        $err = true;

        if ($err_number > 0) {

          if ($err_number == 1451) {
            print '<span class="error_message">';
            print '  &nbsp;N&atilde;o &eacute; poss&iacute;vel excluir este registro. Remova primeiro todas as refer&ecirc;ncias feitas a ele.&nbsp;';
            print '</span>';
          } else {
            if (function_exists('core_err')) {
              core_err($err_number, $err_description, null, null, $sql, "MYSQL");
            } else {
              print "<hr /><pre>[$err_number] -  $err_description<br />$sql</pre><hr />" . PHP_EOL;
            }
          }
        }
      }
    }
    return $err;
  }

  /**
   *
   * @param array $item
   * @param string $source
   *
   * @return boolean
   */
  public function isValid($item, $source) {

    $return = true;

    $type = $item['type'];

    $invalid_types = array("", "html", "subitem", "subquery", "subquery-radio", "select-multi");
    $invalid_type_behaviors = array("child", "popup", "parent");

    $system_types = array("criador", "registro", "responsavel", "alteracao");

    if (in_array($type, $invalid_types) || in_array($item['type_content'], $invalid_type_behaviors)) {

      $return = false;
    } else if ($source === 'SELECT') {

      if ($type === 'hash') {
        $return = false;
      } else if (isset($item['select'])) {
        if (!$item['select']) {
          $return = false;
        }
      }
    } else if ($source === 'INSERT') {

      if (!$item['insert']) {
        $return = false;
      } else if ($item['value'] === null && !in_array($type, $system_types)) {
        $return = false;
      } else if (!$item['value'] && $item['fk']) {
        $return = false;
      } else if ($type === 'hash' && !$item['value']) {
        $return = false;
      }

    } else if ($source === 'UPDATE') {

      if ($type === 'criador' || $type === 'registro') {
        $return = false;
      } else if (!$item['update']) {
        $return = false;
      } else if ($item['value'] === null && !($type === 'responsavel' || $type === 'alteracao')) {
        $return = false;
      } else if (!$item['value'] && $item['fk']) {
        $return = false;
      } else if ($type === 'hash' && !$item['value']) {
        $return = false;
      }

    }

    return $return;
  }

  /**
   * 
   * @param array $item
   * @param string $source
   * @param boolean $alias
   *
   * @return array
   */
  public function prepare($item, $source, $alias = false) {

    $item['field'] = isset($item['field']) ? $item['field'] : $item['id'];

    if ($source === 'SELECT') {

      if ($item['type'] === 'calculated') {
        $item['field'] = '(' . $item['type_content'] . ') AS ' . $item['id'];
      } else if ($item['fk']) {
        if (isset($item['foreign'])) {
          $description = $item['foreign']['description'];
          $reference = $description . ' AS _' . $description;
          if ($alias) {
            $reference = strtoupper($item['foreign']['key']) . '.' . $reference;
          }
          $item['reference'] = $reference;
        }
      }

    }

    return $item;
  }

  /**
   *
   * @param array $item
   * @param mixed $value
   * @param string $source
   * 
   * @return string
   */
  public function renderer($item, $value, $source) {

    $value = String::mask($value, $item['type']);
    $user = App::get() ? App::get() : 'VISITANTE';

    if ($source === 'SELECT') {
      if ($item['type'] === 'alteracao') {
        $value = Date::mysqlbr($value, true, false);
      } else if ($item['type'] === 'registro') {
        $value = Date::mysqlbr($value, true, false);
      } else if ($item['type'] === 'datetime') {
        //$value = Date::mysqlbr($value, true, false); //Old
        //$value = Util::isDate(Date::mysqlbr($value, false, false)) ? $value : "";
        $value = Date::mysqlbr($value, true, false);
      } else if ($item['type'] === 'date') {
        $value = Date::mysqlbr($value, false, false);
      } else if ($item['type'] === 'time') {
        $value = String::substring($value, 0, 5);
      } else if ($item['type'] === 'base64') {
        $value = stripcslashes(base64_decode($value));
      } else if ($item['type'] === 'source-code') {
        $value = "" . ($value) . "";
      } else if ($item['type'] === 'rich-text') {
        $value = stripcslashes($value);
      } else if ($item['type'] === 'fk') {
        $value = ((int) $value) !== 0 ? $value : '';
      } else if ($item['type'] === 'second') {
        $value = stripcslashes(String::substring($value, 3, 5));
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = String::mask($value, $item['type']);
      } else {
        $value = stripcslashes($value);
      }
    } else if ($source === 'INSERT') {

      if ($item['type'] === 'alteracao') {
        $value = "NOW()";
      } else if ($item['type'] === 'registro') {
        $value = "NOW()";
      } else if ($item['type'] === 'datetime') {
        $value = "'" . Date::brmysql($value, true, false) . ":00'";
      } else if ($item['type'] === 'date') {
        $value = "'" . Date::brmysql($value, false, false) . "'";
      } else if ($item['type'] === 'time') {
        $value = "'" . $value . ":00'";
      } else if ($item['type'] === 'timestamp') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'second') {
        $value = "'00" . $value . "'";
      } else if ($item['type'] === 'upper') {
        $value = "UPPER('" . addslashes($value) . "')";
      } else if ($item['type'] === 'base64') {
        $value = "'" . base64_encode($value) . "'";
      } else if ($item['type'] === 'source-code') {
        $value = addslashes($value);
      } else if ($item['type'] === 'rich-text') {
        $value = "'" . addslashes($value) . "'";
      } else if ($item['type'] === 'hash') {
        $value = "MD5('" . addslashes($value) . "')";
      } else if ($item['type'] === 'user') {
        if ($value) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $user . "'";
        }
      } else if ($item['type'] === 'boolean' or $item['type'] === 'yes/no') {
        if ($value) {
          if ($value === 'false') {
            $value = "0";
          } else {
            $value = "1";
          }
        } else {
          $value = "0";
        }
      } else if ($item['type'] === 'criador') {
        $value = "'" . $user . "'";
      } else if ($item['type'] === 'responsavel') {
        $value = "'" . $user . "'";
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = "'" . String::mask($value, $item['type']) . "'";
      } else {
        $value = "'" . addslashes($value) . "'";
      }
    } else if ($source === 'UPDATE') {

      if ($item['type'] === 'alteracao') {
        $value = "NOW()";
      } else if ($item['type'] === 'datetime') {
        $value = "'" . Date::brmysql($value, true, false) . ":00'";
      } else if ($item['type'] === 'date') {
        $value = "'" . Date::brmysql($value, false, false) . "'";
      } else if ($item['type'] === 'time') {
        $value = "'" . $value . ":00'";
      } else if ($item['type'] === 'timestamp') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'second') {
        $value = "'00" . $value . "'";
      } else if ($item['type'] === 'date') {
        $value = "'" . $value . "'";
      } else if ($item['type'] === 'upper') {
        $value = "UPPER('" . addslashes($value) . "')";
      } else if ($item['type'] === 'base64') {
        $value = "'" . base64_encode($value) . "'";
      } else if ($item['type'] === 'source-code') {
        $value = "'" . addslashes($value) . "'";
      } else if ($item['type'] === 'rich-text') {
        $value = "'" . addslashes($value) . "'";
      } else if ($item['type'] === 'hash') {
        $value = "MD5('" . addslashes($value) . "')";
      } else if ($item['type'] === 'execute') {
        $value = $value;
      } else if ($item['type'] === 'user') {
        if ($value) {
          $value = "'" . addslashes($value) . "'";
        } else {
          $value = "'" . $user . "'";
        }
      } else if ($item['type'] === 'boolean' or $item['type'] === 'yes/no') {
        if ($value) {
          if ($value === 'false') {
            $value = "0";
          } else {
            $value = "1";
          }
        } else {
          $value = "0";
        }
      } else if ($item['type'] === 'criador') {
        $value = "'" . $user . "'";
      } else if ($item['type'] === 'responsavel') {
        $value = "'" . $user . "'";
      } else if (in_array($item['type'], array('cep', 'cpf', 'cnpj', 'telefone'), true)) {
        $value = "'" . String::mask($value, $item['type']) . "'";
      } else {
        $value = "'" . addslashes($value) . "'";
      }
    }

    return $value;
  }

  /**
   * 
   * @param array $items
   * @param string $tables
   * @param string $where
   * @param string $group
   * @param string $order
   * @param string $start
   * @param string $end
   * @param array $fields
   * @param boolean $validate
   * @param boolean $alias
   */
  public function buildSelect($items, $tables, $where, $group = null, $order = null, $start = null, $end = null, $fields = null, $validate = false, $alias = false) {

    if ($where) {
      $where = " WHERE " . $where;
    }

    if ($group) {
      $group = " GROUP BY " . $group;
    }

    if ($order) {
      $order = " ORDER BY " . $order;
    }

    if (!$start) {
      $start = 0;
    }

    $limit = "";
    if ($end) {
      $limit = " LIMIT " . $start . "," . $end;
    }

    $arr_fields = array();

    if (!is_array($fields)) {
      $fields = array();
      foreach ($items as $item) {
        $fields[] = $item['id'];
      }
    }
    
    $all = explode(' ', trim($tables));

    $table = $all[0];

    foreach ($items as $item) {

      if ($this->isValid($item, 'SELECT')) {

        if (in_array($item['id'], $fields)) {

          $field = $item['id'];
          if ($alias) {
            $field = $table . '.' . $item['id'] . ' AS ' . $item['id'];
          }
          $item['field'] = $field;

          $item = $this->prepare($item, 'SELECT', $alias);

          $arr_fields[] = $item['field'];

          if (isset($item['reference'])) {
            $arr_fields[] = $item['reference'];
          }

        }

      }

    }
    $_fields = implode(', ', $arr_fields);

    $sql = "SELECT " . $_fields . " FROM " . $tables . $where . $group . $order . $limit;
    //print "[$sql]" . PHP_EOL;

    return $sql;
  }

  /**
   * 
   * @param array $items
   * @param string $table
   * 
   * @return string
   */
  public function buildInsert($items, $table) {
    $arr_fields = array();
    $arr_values = array();

    foreach ($items as $item) {
      if ($this->isValid($item, 'INSERT')) {
        $key = $item['id'];
        $value = $this->renderer($item, $item['value'], 'INSERT');
        $arr_fields[] = $key;
        $arr_values[] = $value;
      }
    }

    $fields = implode(', ', $arr_fields);
    $values = implode(', ', $arr_values);

    $sql = "INSERT INTO " . $table . " (" . $fields . ") VALUES (" . $values . ")";

    return $sql;
  }

  /**
   * 
   * @param string $tables
   * @param array $items
   * @param string $where
   * @param boolean $alias
   * 
   * @return string
   */
  public function buildUpdate($tables, $items, $where, $alias = false) {

    $arr_update = array();

    $all = explode(' ', trim($tables));

    $table = $all[0];

    foreach ($items as $item) {

      if ($this->isValid($item, 'UPDATE')) {

        $field = $item['id'];
        if ($alias) {
          $field = $table . '.' . $item['id'];
        }
        $value = $this->renderer($item, $item['value'], 'UPDATE');

        $arr_update[] = $field . " = " . $value;
      }
    }
    $update = implode(', ', $arr_update);

    $sql = "UPDATE " . $tables . " SET " . $update . " WHERE " . $where;

    return $sql;
  }

  /**
   * 
   * @param string $table
   * @param string $where
   * 
   * @return string
   */
  public function buildDelete($table, $where) {

    $sql = "DELETE FROM " . $table . " WHERE " . $where;

    return $sql;
  }

  /**
   * 
   * @param $items
   */
  private function clearItems($items) {

   $unsets = array("external", "hidden", "title", "description", "type_content", "action", "style", "validate", "fast", "grid", "grid_width", "form", "form_width", "readonly", "default_view", "default_sql", "select", "update", "insert", "line", "order");
    foreach ($items as $key => $item) {
      foreach ($unsets as $unset) {
        unset($item[$unset]);
        $items[$key] = $item;
      }
    }

    return $items;
  }

  /**
   * 
   * @param array $items
   * @param resource $result
   * 
   * @return array
   */
  public function populateItems($items, $result) {

    $items = $this->clearItems($items);

    $rows = null;

    if ($result != null) {

      $rows = array();

      while ($row = $this->fetchRow($result)) {

        $line = array();

        foreach ($items as $item) {

          $key = $item['id'];
          $line[$key] = $item;

          if (isset($row[$key])) {

            $line[$key]['value'] = $this->renderer($item, $row[$key], 'SELECT');

            if (isset($item['foreign'])) {
              $reference = '_' . $item['foreign']['description'];
              if (isset($row[$reference])) {
                $line[$key]['reference'] = $row[$reference];
              }

            }

          }

        }

        $rows[] = $line;
      }

    }

    return $rows;
  }

}
