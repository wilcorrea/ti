<?php

/**
 *
 * @class Javascript
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Javascript {

  /**
   * @var string
   */
  private $root;

  /**
   * @var string
   */
  private $module;

  /**
   * @var string
   */
  private $entity;

  /**
   * @var object
   */
  private $controller;

  /**
   * @var object
   */
  private $properties;

  /**
   * @var object
   */
  private $operations;

  /**
   * @var array
   */
  private $items;

  /**
   * @param string $root
   * @param string $module
   * @param string $entity
   */
  public function Javascript($root, $module, $entity) {

    $this->root = $root;
    $this->module = $module;
    $this->entity = $entity;

    System::import('c', $module, $entity, $root);
    
    $entityCtrl = $entity . 'Ctrl';

    $getProperties = 'getProperties' . $entityCtrl;
    $getItems = 'getItems' . $entityCtrl;


    $this->controller = new $entityCtrl();


    $this->properties = $this->controller->$getProperties();
    $this->operations = $this->properties['operations'];
    
    unset($this->properties['operations']);

    $this->items = $this->controller->$getItems('');

    foreach ($this->items as $key => $item) {

      $item['component'] = Application::getComponent($item['type']);
      $item['options'] = Application::text($this->options($item), null, false);

      $item['label'] = Application::text($item['description'], null, false);
      $item['title'] = Application::text($item['title'], null, false);
      $item['placeholder'] = Application::text((isset($item['placeholder']) ? $item['placeholder'] : $item['title']), null, false);

      if ($item['pk'] || $item['type'] === 'pk') {
        $item['readonly'] = true;
      }

      if ($item['type'] === 'subitem' && isset($item['foreign'])) {

        $module = isset($item['foreign']['module']) ? $item['foreign']['module'] : (isset($item['foreign']['modulo']) ? $item['foreign']['modulo'] : '');
        $entity = $item['foreign']['entity'];

        $entityCtrl = $entity . 'Ctrl';
        $getItemsCtrl = 'getItems' . $entityCtrl;
        $getPropertiesCtrl = 'getProperties' . $entityCtrl;

        System::import('c', $module, $entity, 'src');

        $objectCtrl = new $entityCtrl();

        $item['subitem']['items'] = [];
        
        $_items = $objectCtrl->$getItemsCtrl("");
        
        foreach ($_items as $_item) {

          if (isset($_item['foreign']) && $_item['foreign']['key'] == $this->properties['reference']) {
            $_item['form'] = 0;
          }

          if ($_item['pk'] || $_item['type'] === 'pk') {
            $_item['readonly'] = true;
          }

          $_item['component'] = Application::getComponent($_item['type']);
          $_item['options'] = Application::text($this->options($_item), null, false);
    
          $_item['label'] = Application::text($_item['description'], null, false);
          $_item['title'] = Application::text($_item['title'], null, false);
          $_item['placeholder'] = Application::text((isset($_item['placeholder']) ? $_item['placeholder'] : $_item['title']), null, false);

          if ($_item['form'] === 1 && !$_item['pk']) {
            
            $item['subitem']['items'][$_item['id']] = $_item;
          }
        }
      }

      $this->items[$key] = $item;
    }
  }
  
  /**
   * 
   * @param object $item
   */
  private function options($item) {

    $_options = [];

    if ($item['type'] === 'yes/no' && !$item['type_content']) {

      $item['type_content'] = '1,Sim|0,Não';
    }

    if (String::position($item['type_content'], '|') !== false) {

      $data = explode('|', $item['type_content']);
      foreach ($data as $_data) {

        $d = explode(',', $_data);

        $value = $d[0];
        $label = isset($d[1]) ? $d[1] : $d[0];
        
        $_options[] = (object) ["value" => $value, "label" => $label];
      }
    }

    return $_options;
  }

  /**
   * 
   * @param string $filename
   * 
   * @return string path file
   */
  private function path($filename) {

    $path = File::path(APP_PATH, 'root', 'source', $this->module, $this->entity, $filename);

    if (!File::exists($path)) {

      $path = File::path(APP_PATH, 'root', 'assets', 'api', 'class', $filename);
    }

    return $path;
  }

  /**
   * 
   * render a javascript object model
   */
  public function model() {

    $filename = $this->path('model.js');
    $model = '';

    if (File::exists($filename)) {

      $model = File::openFile($filename);
    }

    $replaces = [
        '{{entity}}' => $this->entity
      , '{{module}}' => $this->module
      , '{{rotule}}' => $this->properties['rotule']
      , 'properties = {/*{{properties}}*/}' => 'properties = ' . Json::encode($this->properties)
      , 'items = {/*{{items}}*/}' => 'items = ' . Json::encode($this->items)
      , 'operations = {/*{{operations}}*/}' => 'operations = ' . Json::encode($this->operations)
    ];

    foreach ($replaces as $key => $value) {

      $model = String::replace($model, $key, $value);
    }

    return $model;
  }
  
  /**
   * 
   * render a javascript object controller
   */
  public function controller() {

    $filename = $this->path('controller.js');
    $controller = '';

    if (File::exists($filename)) {

      $controller = File::openFile($filename);
    }

    $replaces = [
        '{{entity}}' => $this->entity
      , '{{module}}' => $this->module
      , '{{rotule}}' => $this->properties['rotule']
      , '{{entityCtrl}}' => $this->entity . 'Ctrl'
      , 'layouts = {/*{{layouts}}*/}' => 
        'layouts = {' .
          'manager: {' .
            'operations: {' .
              '"search": function(items) {' . 
                'items["' . $this->properties['reference'] . '"].readonly = false;' .
                'return items;' .
              '}' . 
            '}' .
          '},' .
          'list: {' .
            'operations: {' .
            '}' .
          '}' .
        '}'
    ];
    /**
     * layouts: {
     *
     *   manager: {
     *
     *     operations: {
     *
     *       'add': function(items) {
     *
     *         items['men_codigo'].form = 0;
     *
     *         return items;
     *       },
     *       'set': function(items) {
     *        
     *        return layouts.manager.operations['add'](items);
     *       }
     *     }
     *   },
     *
     *   list: {
     * 
     *     operations: {
     *
     *     }
     *   } 
     * }
     * 
     */

    foreach ($replaces as $key => $value) {

      $controller = String::replace($controller, $key, $value);
    }

    return $controller;
  }

}
