<?php

class Controller {

  /**
   * 
   * @return type
   */
  private static function getResponse() {
    return array(
        'success' => false
        , 'value' => null
    );
  }

  /**
   * @param $item
   * 
   * @return type
   */
  private static function willUpdate($item) {

    $update = !isset($item['update']) || $item['update'];

    $relationship_types = array('select-multi', 'tag');
    $relationship = in_array($item['type'], $relationship_types, true);

    return ($update || $relationship);
  }

  /**
   * 
   * @param type $prefix
   * @return type
   */
  private static function getFileDir($prefix, $type) {
    return implode(File::getSeparator(), array('files', $prefix, $type, ''));
  }

  /**
   * 
   * @param type $pk
   * @param type $item
   * @param type $prefix
   * @param type $key
   */
  private static function processBeforeParent($pk, $item, $prefix, $key) {

    $response = self::getResponse();

    if (isset($item['parent'])) {

      $parent = $item['parent'];

      $class = $parent['entity'];
      $classCtrl = $class."Ctrl";

      System::import('m', $parent['modulo'], $class, 'src', true);
      System::import('c', $parent['modulo'], $class, 'src', true);

      $obj = new $class();
      $objCtrl = new $classCtrl(APP_PATH);

      $p_prefix =  $parent['prefix'];

      $get_items_parent = "get_" . $p_prefix . "_items";
      $set_value_parent = "set_" . $p_prefix . "_value";
      $get_value_parent = "get_" . $p_prefix . "_value";

      $p_items = $obj->$get_items_parent();
      foreach ($p_items as $p_key => $p_item) {

        $value = $object->get_ust_value($p_key);
        $obj->$set_value_parent($p_key, $value);
        $object->set_ust_value($p_key, null);
      }

      $operationObjectCtrl = "add" . $class . "Ctrl";

      $value = $objCtrl->$operationObjectCtrl($obj);
      if ($param === 'add') {

        $object->set_ust_value($item['id'], $value);
      } else {

        $object->set_ust_value($item['id'], $obj->$getValue($item['parent']['key']));
      }
    }

    return $response;
  }

  /**
   * 
   * @param type $pk
   * @param type $items
   * @param type $prefix
   * @return type
   */
  public static function before($pk, $items, $prefix, $object = null) {

    $response = array();

    foreach ($items as $key => $item) {

      $processed = self::getResponse();

      switch ($item['type_behavior']) {
        case 'parent':
          break;
      }

      if (isset($item['update']) && $item['update']) {
        switch ($item['type']) {
          default:
            break;
        }
      }

      if ($processed['success'] === true && !is_null($processed['value'])) {
        $response[$key] = $processed['value'];
      }
    }

    return $response;
  }

  /**
   * 
   * @param type $pk
   * @param type $item
   * @param type $prefix
   * @param type $key
   */
  private static function processAfterUpload($pk, $item, $prefix, $key) {

    System::import('class', 'image', 'Html5ImageUpload', 'upload');

    $response = self::getResponse();

    $type = $item['type'];
    $value = $item['value'];

    if ($value) {

      $source_path = APP_PATH . Html5ImageUpload::getFileDir();
      $source_file = $value;
      $source = $source_path . '' . $source_file;
      //var_dump("$source_path, $source_file, $source");

      $destin_path = APP_PATH . self::getFileDir($prefix, $type);
      $destin_name = md5($key) . '-' . md5($pk) . '.' . File::getExtension($source);
      $destin = $destin_path . '' . $destin_name;

      if ($destin !== $source) {

        if (!is_dir($destin_path)) {
          mkdir($destin_path, 0755, true);
        }

        if (is_writable($destin_path)) {
          if (file_exists($source)) {
            if (file_exists($destin)) {
              unlink($destin);
            }

            $copy = copy($source, $destin);
            if ($copy) {

              $response['success'] = true;
              $response['value'] = "'" . String::replace($destin, $source_path, "") . "'";
            } else {
              System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
            }
          }
        } else {
          System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
        }
      }
    }

    return $response;
  }

  /**
   * 
   * @param type $pk
   * @param type $item
   * @param type $prefix
   * @param type $key
   */
  private static function processAfterRelationship($pk, $item, $prefix, $key) {

    System::import('class', 'pattern', 'ControllerRelationship', 'core');

    //$item
    $root = 'src';
    $module = isset($item['foreign']['module']) ? $item['foreign']['module'] : (isset($item['foreign']['modulo']) ? $item['foreign']['modulo'] : '');
    $entity = isset($item['foreign']['entity']) ? $item['foreign']['entity'] : '';
    $source = $item['id'];
    $target = isset($item['foreign']['key']) ? $item['foreign']['key'] : '';
    $collection = $item['value'];

    $saved = ControllerRelationship::save($root, $module, $entity, $source, $target, $pk, $collection);

    $response = self::getResponse();

    if ($saved) {
      $response['success'] = true;
      $response['value'] = null;
    }

    return $response;
  }

  /**
   * 
   * @param type $pk
   * @param type $item
   * @param type $prefix
   * @param type $key
   */
  private static function processAfterCalculated($pk, $item, $prefix, $key) {

    $response = self::getResponse();

    if ($item['type_content']) {
      $response['success'] = true;
      $response['value'] = "(" . $item['type_content'] . ")";
    }

    return $response;
  }

  /**
   * 
   * @param type $pk
   * @param type $items
   * @param type $prefix
   * @return type
   */
  public static function after($pk, $items, $prefix, $object = null) {

    $response = array();

    foreach ($items as $key => $item) {

      $processed = self::getResponse();

      if (self::willUpdate($item)) {
        switch ($item['type']) {
          case 'file':
          case 'image':
            $processed = self::processAfterUpload($pk, $item, $prefix, $key);
            break;
          case 'select-multi':
          case 'tag':
          case 'subitem':
            $processed = self::processAfterRelationship($pk, $item, $prefix, $key);
            break;
          case 'calculated':
            $processed = self::processAfterCalculated($pk, $item, $prefix, $key);
            break;
        }
      }

      if ($processed['success'] === true && !is_null($processed['value'])) {
        $response[$key] = $processed['value'];
      }
    }

    return $response;
  }

  /**
   * 
   * @param type $fields
   * @return type
   */
  public static function makeUpdate($fields) {
    $update = "";

    $u = array();
    if (is_array($fields)) {
      foreach ($fields as $f => $v) {
        $u[] = $f . " = " . $v;
      }
    }
    $update = implode(', ', $u);

    return $update;
  }

  /**
   * 
   * @param type $prefix
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $max
   * @param type $fields
   * 
   * @return type
   */
  private static function cachedPath($prefix, $where, $group, $order, $max, $fields) {

    $path = File::path(APP_PATH, 'files', 'cached', $prefix, '');
    $file = md5($where . $group . $order . $max . serialize($fields)) . '.cache';

    return ($path . $file);
  }

  /**
   * 
   * @param type $module
   * @param type $entity
   * @param type $where
   * @param type $group = null
   * @param type $order = null
   * @param type $max = null
   * @param type $fields = null
   * 
   * @return type
   */
  public static function cachedGet($module, $entity, $where, $group = null, $order = null, $max = null, $fields = null, $validate = true, $debug = false) {

    if (is_null($max)) {
      $max = 20;
    }

    System::import('class', 'resource', 'Cache', 'core');

    System::import('m', $module, $entity, 'src');
    System::import('c', $module, $entity, 'src');

    $class = $entity;
    $classCtrl = $class . "Ctrl";

    $objectCtrl = new $classCtrl();

    $cached = array();

    $getObjectCtrl = "get" . $class . "Ctrl";
    $getPropertiesObjectCtrl = "getProperties" . $class . "Ctrl";

    $properties = $objectCtrl->$getPropertiesObjectCtrl();

    $prefix = $properties['prefix'];

    $cached_full = self::cachedPath($prefix, $where, $group, $order, $max, $fields);

    $cached_path = File::getParent($cached_full);
    $cached_file = File::getBase($cached_full);

    $directory = File::makeDirectory($cached_path);
    if ($directory) {

      $cache = new Cache($cached_path);

      $key = $cached_file;
    
      $cached = $cache->read($key);

      if (is_null($cached) || (is_array($cached) && !count($cached))) {

        $object = new $class();

        $gets = array();

        $get_items_object = "get_" . $prefix . "_items";

        $objects = $objectCtrl->$getObjectCtrl($where, $group, $order, "0", $max, $fields, $validate, $debug);
        if (is_array($objects)) {

          foreach ($objects as $object) {

            $get = new ArrayObject(array(), ArrayObject::ARRAY_AS_PROPS);

            $items = $object->$get_items_object();
            foreach ($items as $key => $item) {

              if (is_null($fields) || in_array($key, $fields, true)) {
                $get[$key] = $item['value'];
              }
            }

            $gets[] = $get;
          }
        }

        $cached = $cache->save($key, $gets);
      }
    } else {

      System::debug('Cannot create ' . $cached_path);
    }

    return $cached;
  }

  /**
   * 
   * @param type $module
   * @param type $entity
   * 
   * @return type
   */
  public static function cachedClear($module, $entity) {

    System::import('c', $module, $entity, 'src');

    $class = $entity;
    $classCtrl = $class . "Ctrl";

    $objectCtrl = new $classCtrl();

    $getObjectCtrl = "get" . $class . "Ctrl";
    $getPropertiesObjectCtrl = "getProperties" . $class . "Ctrl";

    $properties = $objectCtrl->$getPropertiesObjectCtrl();

    $prefix = $properties['prefix'];

    $cache_path = File::getParent(self::cachedPath($prefix, null, null, null, null, null));

    return File::delTree($cache_path);
  }
  
  /**
   * 
   * @param $module
   * @param $entity
   * @param $operation
   * @param $items
   * @param $filter
   */
  public static function getOperations($module, $entity, $operation = null, $items = null, $filter = null) {

    $operations = null;

    $path = System::import('c', $module, $entity, 'src', false);
    if (File::exists($path)) {

      $path = System::import('c', $module, $entity, 'src');

      $class = $entity;
      $classCtrl = $class . "Ctrl";

      $objectCtrl = new $classCtrl();

      $getObjectCtrl = "get" . $class . "Ctrl";
      $getPropertiesObjectCtrl = "getProperties" . $class . "Ctrl";
      $getOperationsObjectCtrl = "getOperations" . $class . "Ctrl";

      $properties = $objectCtrl->$getPropertiesObjectCtrl();

      $operations = $properties['operations'];

      if (method_exists($objectCtrl, $getOperationsObjectCtrl)) {

        $operations = $objectCtrl->$getOperationsObjectCtrl($operations, $operation, $items, $filter);
      }
    }
  
    return $operations;
  }

}
