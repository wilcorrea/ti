<?php


class ControllerRelationship {
 
 /**
  * 
  * @param string $root
  * @param string $module
  * @param string $entity
  * @param string $source
  * @param string $target
  * @param mixed $pk
  * @param array collection
  */
  public static function save($root, $module, $entity, $source, $target, $pk, $collection) {
    
    System::import('c', $module, $entity, $root);
    
    $entityCtrl = $entity . 'Ctrl';
    $removeEntityCtrl = 'execute' . $entityCtrl;
    $operationEntityCtrl = 'operation' . $entityCtrl;
    $getItemsEntityCtrl = 'getItems' . $entityCtrl;

    $objectCtrl = new $entityCtrl();
    
    $keep = [];

    if (is_array($collection)) {

      foreach ($collection as $row) {

        $items = $objectCtrl->$getItemsEntityCtrl('');

        $items[$target]['value'] = $pk;

        foreach ($row as $key => $value) {

          if (isset($items[$key])) {

            $items[$key]['value'] = $value;

            if ($items[$key]['type'] === 'pk') {

              $keep[] = "`" . $key . "` <> '" . $value . "'";
            }
          }
        }

        $objectCtrl->$operationEntityCtrl('save', $items);
      }
    }

    $keeped = '';
    if (count($keep)) {

      $keeped = " AND " . "(" . implode(' AND ', $keep) . ")";
    }

    $objectCtrl->$removeEntityCtrl("`" . $target . "` = '" . ((int) $pk) . "'" . $keeped, "");

    return true;
  }
}