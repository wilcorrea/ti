<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Database
 *
 * @author william
 */
abstract class Database {

  protected $database;
  protected $path;
  protected $auto_increment;
  
  protected $server;
  protected $port;
  protected $db;
  protected $user;
  protected $password;
  
  protected $connection;
  protected $connected;
  
  protected $time;
  
  protected $transaction;

  protected $code;
  protected $code_error;
  protected $name;
  protected $name_error;
  protected $address;
  protected $address_error;
  protected $commands;
  
  public $debug;

  /**
   * 
   * @param type $database
   * @param type $path
   * 
   * @return \Database
   */
  public function Database($database, $path, $auto_increment) {

    $database = $database ? $database : DEFAULT_DATABASE;
    $path = $path ? $path : APP_PATH_ROOT;
    $auto_increment = $auto_increment === null ? true : $auto_increment;
    
    $this->path = $path;
    $this->database = $database;
    $this->auto_increment = $auto_increment;

    $this->connection = null;
    $this->connected = false;
    $this->debug = false;

    $this->code_error = "Sessão não identificada";
    $this->name_error = "Usuário da sessão não identificada";
    $this->address_error = "Localização não identificada";

    $this->commands = array("rollback" => "ROLLBACK", "commit" => "COMMIT", "start" => "START TRANSACTION");

    $this->restore();

    return $this;
  }

  /**
   * 
   * @param type $database
   */
  public function setConfig($database) {

    $configuracao = array();

    require File::path(APP_PATH_ROOT, 'config', 'database.php');

    if (isset($configuracao[$database])) {
      
      $config = $configuracao[$database];

      if (is_array($config)) {
        $this->server = $config['server'];
        $this->port = $config['port'];
        $this->db = $config['db'];
        $this->user = $config['user'];
        $this->password = $config['password'];
      } else {
        $this->server = $config->server;
        $this->port = $config->port;
        $this->db = $config->db;
        $this->user = $config->user;
        $this->password = $config->password;
      }

    } else {
      
      Console::log("Database not found: " . $database);

    }
    
  }

  /**
   * 
   * @param type $database
   */
  public abstract function connect();

  /**
   * 
   * @param type $database
   */
  public abstract function disconnect();

  /**
   * 
   * @param type $sql
   * 
   * @return resource
   */
  protected abstract function query($sql);
  
  /**
   * 
   * @param type $sql
   * @param type $history
   * @param type $validate
   * @param type $id
   * 
   * @return int
   */
  public abstract function execute($sql, $history = true, $validate = true, $id = "", $transaction = null);
  
  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public abstract function fetchRow($result);
  
  /**
   * 
   * @param resource $result
   * 
   * @return array
   */
  public abstract function fetchObject($result);

  /**
   * 
   * @param type $sql
   * @param type $save
   * @param type $validate
   * @param type $deactivate
   * 
   * @return int (auto increment id)
   */
  public abstract function insert($sql, $save = true, $validate = true, $deactivate = false);

  /**
   * 
   * @param type $sql
   * @param type $validate
   * @param type $database
   * @param type $error
   * 
   * @return resouce
   */
  public function select($sql, $validate = true, $database = "", $error = true) {

    if ($validate) {
      //$this->validate($pSql);
    }
    $this->connect($database);
    $query = $this->query($sql);
    if ($error) {
      $this->error($sql, true);
    }
    $this->disconnect();

    return $query;
  }

  /**
   * 
   * @param type $sql
   * @param type $save
   * 
   * @return boolean
   */
  protected abstract function error($sql, $save = true);
  
  /**
   *
   */
  public function startTransactionSQL() {
    $this->execute($this->commands['start'], false, false, "", true);
  }

  /**
   *
   */
  public function commitTransactionSQL() {
    $this->execute($this->commands['commit'], false, false, "", false);
  }

  /**
   *
   */
  public function rollbackTransactionSQL() {
    $this->execute($this->commands['rollback'], false, false, "", false);
  }

  /**
   * 
   * @param type $sql
   * 
   * @return boolean
   */
  protected function validate($sql) {

    $validate = true;

    if (!Security::isSecured()) {
      
      $erro = false;
      $err_description = "";
      if (empty($this->code)) {
        $err_description .= ";" . $this->code_error;
      }
      if (empty($this->address)) {
        $err_description .= ";" . $this->address_error;
      }
      if (empty($this->name)) {
        $erro = true;
        $err_description .= ";" . $this->name_error;
      }
  
      if ($erro) {
        $mensagem = "Acesso inv&aacute;lido. ";
        $err_number = "403";
        if ($err_description) {
          $err_description = substr($err_description, 1);
        }
  
        core_err($err_number, $mensagem . $err_description, null, null, $sql, "MYSQL");
        $validate = false;
      }
    }

    return $validate;
  }

  /**
   * 
   * @param type $hst_comando
   * @param type $hst_backup
   * @param type $hst_info
   * 
   */
  protected function history($hst_comando, $hst_backup, $hst_info) {

    $modulo = System::import('m', 'manager', 'Historico', 'src', false, '');

    if (file_exists($modulo)) {

      System::import('c', 'manager', 'Historico', 'src', true, '');
      System::import('m', 'manager', 'Historico', 'src', true, '');

      $hst_tipo = $this->getOperationType($hst_comando);
      $hst_tabela = $this->getOperationTable($hst_comando, $hst_tipo);

      $historico = new Historico();
      $historico->set_hst_value('hst_tipo', $hst_tipo);
      $historico->set_hst_value('hst_tabela', $hst_tabela);
      $historico->set_hst_value('hst_info', $hst_info);
      $historico->set_hst_value('hst_comando', $hst_comando);
      $historico->set_hst_value('hst_backup', $hst_backup);
      $historico->set_hst_value('hst_ip', System::address(true));

      $historicoCtrl = new HistoricoCtrl();
      $historicoCtrl->addHistoricoCtrl($historico);
    } else {
      print "M&oacute;dulo de <b>HIST&Oacute;RICO</b> desativado";
    }
  }

  /**
   * 
   * @param type $sql
   * 
   * @return string
   */
  protected function getOperationType($sql) {
    $type = "INVALIDO";

    $sql = trim($sql);

    $comando = substr($sql, 0, 6);

    if ($comando == 'INSERT') {
      $type = "INSERT";
    }
    if ($comando == 'DELETE') {
      $type = "DELETE";
    }
    if ($comando == 'UPDATE') {
      $type = "UPDATE";
    }

    return $type;
  }

  /**
   * 
   * @param type $sql
   * @param type $type
   * 
   * @return string
   */
  protected function getOperationTable($sql, $type) {
    $index = 0;

    $s = trim($sql);
    $sql = preg_replace('/\s+/', ' ', $s);

    if ($type == "UPDATE") {
      $index = 1;
    } else if ($type == "DELETE") {
      $index = 2;
    } else if ($type == "INSERT") {
      $index = 2;
    } else {
      return "";
    }
    $explode = explode(" ", $sql);
    $table = $explode[$index];
    if (strpos("(", $table)) {
      $t = $explode("(", $table);
      $table = $t[0];
    }

    return $table;
  }

  /**
   * 
   */
  protected function restore() {

    $code = "";
    $name = Application::get();
    $address = Application::get('address');

    $this->code = $code;
    $this->name = $name;
    $this->address = $address;
  }

  /**
   * 
   * @return type
   */
  public function getTransaction() {
    return $this->transaction;
  }

  /**
   * 
   * @param type $transaction
   * @return \Database
   */
  public function setTransaction($transaction) {
    $this->transaction = $transaction;
  }

}