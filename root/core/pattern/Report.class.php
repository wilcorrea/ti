<?php

/**
 *
 * @copyright array software
 *
 * @author Pedro - 20/02/2013 14:46
 * Modulo..........: Geral
 *
 * Responsavel.....: Pedro
 * Alteracao.......: 20/02/2013 14:46
 */
class Report {

  public $debug = false;
  public $orientation = "landscape";
  public $width_label = "";

  public static $DEFAULT = "DEFAULT";

  public static $TYPE_REP = "rep";
  public static $TYPE_DOC = "doc";
  public static $TYPE_TAB = "tab";
  
  protected $tab_get_last_data = false;

  protected static $settings = array("DEFAULT" => array("header" => true, "footer" => true));

  protected $module = "";
  protected $id = "";
  protected $ext = "";
  public $type = "rep";
  protected $uniq = "";
  protected $current = 1;
  
  protected $name = "";
  protected $description = "";
  protected $xml = "";

  protected $items = array();

  /**
   * @var string
   */
  protected $field = "";
  /**
   * @var string
   */
  protected $table = "";
  /**
   * @var array()
   */
  protected $fields = array();
  /**
   * @var array()
   */
  protected $tables = array();

  protected $where = "";
  protected $order = "FALSE";
  protected $group = "";
  protected $limit = "10000";

  protected $sql = "";

  /**
   * 
   * @param type $settings
   */
  public function Report($settings = false) {
    $this->debug = $settings;

    $this->id = String::lower(get_class($this));
    $this->ext = DEFAULT_EXTENSION;

    $this->type = self::$TYPE_REP;

    self::$settings = array(
      self::$DEFAULT => array("header" => true, "footer" => true),
      self::$TYPE_TAB => array("header" => false, "footer" => true),
    );
  }

  /**
   *
   * @return type
   */
  public function get_name() {
    return $this->name;
  }

  /**
   *
   * @param type $name
   */
  public function set_name($name) {
    $this->name = $name;
  }

  /**
   * 
   * @return type
   */
  public function get_description() {
    return $this->description;
  }

  /**
   * 
   * @param type $description
   */
  public function set_description($description) {
    $this->description = $description;
  }

  /**
   *
   * @return type
   */
  public function get_module() {
    return $this->module;
  }

  /**
   *
   * @param type $module
   */
  public function set_module($module) {
    $this->module = $module;
  }

  /**
   *
   * @return type
   */
  public function get_id() {
    return $this->id;
  }

  /**
   *
   * @param type $id
   */
  public function set_id($id) {
    $this->id = $id;
  }

  /**
   *
   * @return type
   */
  public function get_uniq() {
    return $this->uniq;
  }

  /**
   *
   * @param type $uniq
   */
  public function set_uniq($uniq) {
    $this->uniq = $uniq;
  }

  /**
   *
   * @return type
   */
  public function get_items() {
    if ($this->type === self::$TYPE_TAB) {
      $this->items['tab_get_last_data'] = $this->generateItem("tab_get_last_data", "Comportamento", "option", "", 99, "0", "Define se os últimos dados coletados com o mesmo filtro serão recuperados", "0,Padrão|1,Recuperar última requisição|2,Reprocessar dados");
    }

    return $this->items;
  }

  /**
   *
   * @param type $items
   */
  public function set_items($items) {
    $this->items = $items;
  }

  /**
   *
   * @return type
   */
  public function get_field() {
    return $this->field;
  }

  /**
   *
   * @return type
   */
  public function get_fields() {
    return $this->fields;
  }

  /**
   *
   * @param type $field
   */
  public function set_field($field) {
    $this->field = $field;
  }

  /**
   *
   * @return type
   */
  public function get_table() {
    return $this->table;
  }

  /**
   *
   * @return type
   */
  public function get_tables() {
    return $this->tables;
  }

  /**
   *
   * @param type $table
   */
  public function set_table($table) {
    $this->table = $table;
  }

  /**
   *
   * @return type
   */
  public function get_where() {
    return $this->where;
  }

  /**
   *
   * @param type $where
   */
  public function set_where($where) {
    $this->where = $where;
  }

  /**
   *
   * @return type
   */
  public function get_order() {
    return $this->order;
  }

  /**
   *
   * @param type $order
   */
  public function set_order($order) {
    $this->order = $order;
  }

  /**
   *
   * @return type
   */
  public function get_group() {
    return $this->group;
  }

  /**
   *
   * @param type $group
   */
  public function set_group($group) {
    $this->group = $group;
  }

  /**
   *
   * @return type
   */
  public function get_limit() {
    return $this->limit;
  }

  /**
   *
   * @param type $limit
   */
  public function set_limit($limit) {
    $this->limit = $limit;
  }

  /**
   *
   * @return type
   */
  public function getType() {
    return $this->type;
  }

  /**
   * 
   * @return type
   */
  public function getSql() {
    return $this->sql;
  }

  /**
   * 
   * @param type $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * 
   * @param type $sql
   */
  public function setSql($sql) {
    $this->sql = $sql;
  }

  /**
   *
   * @param type $data
   */
  public function add_item($data) {
    $this->items[] = $data;
  }

  /**
   *
   * @return type
   */
  public function get_ext() {
    return $this->ext;
  }

  /**
   *
   * @param type $ext
   */
  public function set_ext($ext) {
    $this->ext = $ext;
  }

  /**
   *
   * @param type $id
   */
  public function remove_item($id) {
    unset($this->items[$id]);
  }
  
  public function getSettings() {
    return isset(self::$settings[$this->type]) ? self::$settings[$this->type] : self::$settings[self::$DEFAULT];
  }

  /**
   *
   * @param type $field
   * @param type $type
   * @param type $as
   * @return type
   */
  public function format($field, $type, $as = "") {
    if ($as == "") {
      $as = $field;
    }

    $format = "";

    switch ($type) {
      case 'data' :
        $format = "DATE_FORMAT(" . $field . ",'%d/%m/%Y')";
        break;

      case 'dia' :
        $format = "DATE_FORMAT(" . $field . ",'%w')";
        break;
    }

    return $format . " AS " . $as;
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printHtmlFormReport($target, $level) {
    ?>
    <!--<?php print $target; ?>-->
    <!--<?php print $level; ?>-->
    <?php
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printJavascriptFormReport($target, $level) {
    ?>
      <script type="text/javascript">/*<?php print $target; ?>*//*<?php print $level; ?>*/</script>
    <?php
  }

  /**
   * 
   * @param type $target
   * @param type $level
   */
  public function printCssFormReport($target, $level) {
    ?>
      <style type="text/css">/*<?php print $target; ?>*//*<?php print $level; ?>*/</style>
    <?php
  }

  /**
   *
   * @param type $acesso
   * @param type $target
   * @param type $level
   * @param string $modulo
   * @param type $items
   */
  public function printToolbarReport($acesso, $target, $level, $modulo, $items = '{}', $container = false) {
    $modulo = 'src/' . $modulo . '/view';
    ?>
    <table width="100%">
      <td align="left" width="105">
        <input value="Pesquisar" id="searchit-<?php print $target; ?>" onclick="system.report.search('<?php print $level; ?>', '<?php print $target; ?>', <?php print $items; ?>);" class="form-buttom" type="button">
        <input value="Voltar" id="backit-<?php print $target; ?>" onclick="system.report.back('<?php print $target; ?>', true);" class="form-buttom" type="button" style="display: none;">
      </td>
      <?php
        if ($container) {
        ?>
          <td align="left" width="105">
                <input value="Relat&oacute;rios" id="backcontainerit-<?php print $target; ?>" onclick="system.report.back('<?php print $target; ?>', true, true);" class="form-buttom" type="button" style="display: block;">
          </td>
        <?php
        }
      ?>
      <td align="left" width="105">
        <input id="printit-<?php print $target; ?>" value="Imprimir" onclick="system.report.print('<?php print $target; ?>');" class="form-buttom" type="button" style="display: none;">
      </td>
      <td align="left" width="105">
        <input id="downloadit-<?php print $target; ?>" value="Download" onclick="system.report.download('<?php print $this->type; ?>', '<?php print $this->module; ?>', '<?php print $this->id; ?>', '<?php print $target; ?>', '<?php print md5(System::getUser(false, 'user')); ?>');" class="form-buttom" type="button" style="display: none;">
        <input id="saveit-<?php print $target; ?>" value="Salvar" onclick="system.report.save('<?php print $this->module; ?>', '<?php print $this->id; ?>', '<?php print $target; ?>', '<?php print md5(System::getUser(false, 'user')); ?>');" class="form-buttom" type="button" style="display: none;">
      </td>
      <td align="left" valign="middle">
        <div id="message_validate_<?php print $target; ?>"></div>
      </td>
    </table>
    <?
  }

  /**
   * 
   * @param type $title
   * @param type $orientation
   * @param type $name
   * @param type $notification
   */
  public function printTopReport($title = "", $orientation = "landscape", $name = "", $notification = "") {

    $settings = $this->getSettings();

    ?>
      <style>
        .page-top {
          background: url('<?php print PAGE_IMAGE;?>logo/logo.png') no-repeat right !important;
        }
        
        .page-header p {
          text-align: left !important;
          background: transparent !important;
        }
        
        .page-top {
          height: 50px;
          background-size: contain !important;
          border-bottom: 5px solid #02913C;
          padding: 5px 0 5px 0;
        }
        
        .page-title {
          padding: 30px 0 20px 0;
          border-width: 5px 0 1px 0;
          border-color: #DEDEDC;
          border-style: solid;
        }
      </style>
      <div class="<?php print $orientation; ?> page-header-container">
        <div class="page-header page-top">
          <p>
            <b><?php print mb_strtoupper(COMPANY_NAME); ?></b>
          </p>
          <p>
            <?php print String::upper(COMPANY_RAZAO_SOCIAL); ?>
          </p>
          <p>
            <?php print String::upper(COMPANY_Z); ?>
          </p>
        </div>
        <div class="page-header page-title">
          <p>
            <b><?php print mb_strtoupper($name); ?> [<?php print $this->module; ?>/<?php print $this->id; ?>]</b>
          </p>
          <span style="float: right;"><?php print mb_strtoupper(System::getUser(true)); ?> [<?php print date('d/m/Y'); ?>]</span>
        </div>
        <br>
      </div>
    <?php

    if ($this->type === self::$TYPE_DOC) {
      ?>
        <div class="<?php print $orientation; ?>">
          <p align="justify">
            <?php
              print $this->description;
            ?>
          </p>
        </div>
        <br>
      <?php
    }

  }

  /**
   * 
   * @param type $orientation
   */
  public function printHeaderReport($orientation = "landscape") {
    
    $orientation = $this->orientation ? $this->orientation : $orientation;
    
    ?>
    <table align="center" class="<?php print $orientation; ?>">
      <tbody>
        <?php
  }

  /**
   * 
   * @param type $w_description
   * @param type $g_description
   * @param type $o_description
   * @param type $orientation
   */
  public function printFooterReport($w_description = "", $g_description = "", $o_description = "", $orientation = "landscape") {
    
    $orientation = $this->orientation ? $this->orientation : $orientation;
    
          ?>
        </tbody>
      </table>
      <br>
      <table align="center" class="<?php print $orientation; ?> filter">
        <tbody>
          <?php
            if ($w_description) {
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Filtros: 
                  </td>
                  <td>
                    <?php print $w_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
          <?php
            if ($g_description) {
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Grupos: 
                  </td>
                  <td>
                    <?php print $g_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
          <?php
            if ($o_description) {
              ?>
                <tr>
                  <td width="50" class="page-header">
                    Ordena&ccedil;&atilde;o: 
                  </td>
                  <td>
                    <?php print $o_description;?>
                  </td>
                </tr>
              <?php
            }
          ?>
        </tbody>
      </table>
      <br>
    <?php
  }
  
  /**
   * 
   * @param type $items
   * @param type $header
   */
  public function printRowReport($items, $header = false){
    ?><?php

      $header = $header ? "header" : "";    
      if (is_array($items)) {
        ?>
          <tr>
            <?php
              foreach ($items as $item) {
                $title = isset($item['title']) ? $item['title'] : "";
                ?>
                  <td colspan="<?php print $item['colspan'];?>" align="<?php print $item['align'];?>" width="<?php print $item['width'];?>" class="<?php print $header;?> <?php print $item['class'];?>" title="<?php print $title;?>">
                    <?php print $item['value'];?>
                  </td>
                <?php
              }
            ?>
          </tr>
        <?php
      }
  }

  /**
   *
   * @param type $sql
   * @param type $index
   * 
   * @return type
   */
  public function getValue($sql, $index) {

    $value = null;

    $query = $this->getData($sql);
    if ($query != NULL) {
      while ($row = $this->fetchData($query)) {
        $value = $row[$index];
      }
    }

    return $value;
  }

  /**
   * 
   * @param type $data
   * 
   * @return type
   */
  public function fetchData($result) {

    require System::desire('file', '', 'header', APP_PATH . 'core', false);
    System::desire('class', 'base', 'DAO', APP_PATH . 'core');

    $dao = new DAO();
    $row = $dao->fetchRow($result);

    return $row;
  }

  /**
   *
   * @param type $sql
   * @return type
   */
  public function getData($sql) {

    require System::desire('file', '', 'header', APP_PATH . 'core', false);
    System::desire('class', 'base', 'DAO', APP_PATH . 'core');

    $dao = new DAO();
    $query = $dao->selectSQL($sql);

    return $query;
  }

  /**
   * 
   * @param type $where
   * @param type $group
   * @param type $order
   * @param type $items
   * @param type $fast
   * 
   * @return type
   */
  public function makeWhere($where, $group, $order, $items, $fast = "") {

    if (isset($items['tab_get_last_data'])) {
      $this->tab_get_last_data = System::request('tab_get_last_data');
      unset($items['tab_get_last_data']);
    }
    $recovered = System::recover($items, $where, $group, $order, $fast);

    return $recovered;
  }

  /**
   * 
   * @param type $recovered
   * 
   * @return string
   */
  public function makeSql($recovered) {

    $_field = $this->field;
    $_table = $this->table;
    $_where = $this->where;
    $_group = $this->group;
    $_order = $this->order;
    $_limit = $this->limit;

    $where = $recovered['w'];
    if ($where != "") {
      if ($_where != "") {
        $_where = "(" . $_where . ") AND (" . $where . ")";
      } else {
        $_where = $where;
      }
    }

    $group = $recovered['g'];
    if ($group != "") {
      if ($group != "") {
        $_group = $_group . ", " . $group;
      } else {
        $_group = $group;
      }
    }

    $order = $recovered['o'];
    if ($order != "") {
      if ($order != "") {
        $_order = $_order . ", " . $order;
      } else {
        $_order = $order;
      }
    }

    if (strlen($_field) == 0) {
      $f = array();
      foreach ($this->fields as $key => $field) {

        if (!isset($field['as']) || strlen($field['as']) == 0) {
          $field['as'] = $field['field'];
        }
        $this->fields[$key]['as'] = $field['as'];

        $f[] = $field['field'] . " AS " . $field['as'];
      }
      $_field = implode(', ', $f);
    }

    if (strlen($_table) == 0) {
      $_table = implode(' ', $this->tables);
    }

    if ($_where) {
      $_where = " WHERE " . $_where;
    }

    if ($_group) {
      $_group = " GROUP BY " . $_group;
    }

    if ($_order) {
      $_order = " ORDER BY " . $_order;
    }

    if ($_limit) {
      $_limit = " LIMIT " . $_limit;
    }

    $this->sql = "SELECT " . $_field . " FROM " . $_table . " " . $_where . " " . $_group . " " . $_order . " " . $_limit;
    
    return $this->sql;
  }

  /**
   * 
   * @param type $module
   * @param type $id
   * @param type $recovered
   */
  public function run($module, $id, $recovered) {
    
    set_time_limit(0);

    $error = $this->verify($recovered);

    $this->id = String::lower(get_class($this));

    $dirname = PATH_REPORT . md5(System::getUser(false, 'user')) . '/' . $this->module . '/' . $this->id;
    if (!file_exists($dirname)) {
      mkdir($dirname, 0755, true);
    }
    $filename = $dirname . '/' . "last.xls";

    $title = COMPANY . ' :: ' . TITLE;
    $orientation = $this->orientation ? $this->orientation : "landscape";
    $name = $this->name;
    $notification = "";

    $this->printTopReport($title, $orientation, $name, $notification);

    if (count($error)) {

      /**
       * $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
       */ 
      if (is_array($error)) {

        ?>
          <div style="text-align: left; color: #CE7F17; border: 1px solid #ccc; line-height: 150%;" class="<?php print $orientation; ?>">
            <div style="padding: 10px; background: #fafafa !important;">
              <span style="font-size: 12px; line-height: 150%;">
                Problemas de <b>validação</b> encontrados durante a execução do relatório:
              </span>
              <ul>
                <?php
                  foreach ($error as $err) {
                    ?>
                      <li>
                        <?php
                          print ' - ' . $err['message'];
                        ?>
                      </li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
        <?php
      }

    } else {

      ob_start();

      $sql = $this->makeSql($recovered);
      if (($this->debug === true && MODO_TESTE === 'true') || ($this->debug === 'force')) {
        print $sql;
      }

      if ($this->type === self::$TYPE_TAB) {
        $target = "pivot-" . uniqid();

        ?>

          <script type="text/javascript" src="<?php print APP_PAGE; ?>lib/?id=fagoc.br&r=js&jquery=1&v=<?php print VERSION; ?>&file=jquery.js"></script>
          <link rel="stylesheet" type="text/css" media="all" href="<?php print APP_PAGE; ?>lib/?id=fagoc.br&r=css&jquery=1&v=<?php print VERSION; ?>&file=jquery.css"/>
          <script type="text/javascript" src="https://www.google.com/jsapi"></script>
          <style>
            @media print {
              center {
                display: none;
              }
              body > table.pvtTable {
                /*width: 100%;*/
                box-shadow: none;
              }
              body {
                overflow: auto !important;
              }
            }

            body {
              cursor: auto;
              padding: 0px;
              overflow: hidden;
              height: <?php print (WINDOW_HEIGHT + 26); ?>px;
            }
            .pivot-container {
              overflow: auto;
              margin: 5px;
              width: <?php print (WINDOW_WIDTH - 138); ?>px;
              height: <?php print (WINDOW_HEIGHT - 86); ?>px;
            }
            .pvtHorizList {
              width: <?php print (WINDOW_WIDTH - 140); ?>px;
              height: 50px;
              overflow-y: auto;
            }
            .pvtRendererArea {
              box-shadow: 1px 1px 5px 1px rgba(0,0,0,0.5) inset;
              padding: 0;
            }
            select.pvtRenderer, .pvtVals button.form-buttom {
              width: 100%;
            }
            .pvtVals button.form-buttom {
              padding: 4px 10px;
            }
            .page-header-container {
              width: 100%;
            }
            .modal-dialog {
              display: none;
            }


            @media screen {
              .page-header-container {
                display: none;
              }
            }
          </style>

          <div class="pivot-mask" style="width: 100%; height: 100%; background: url('<?php print PAGE_IMAGE; ?>default/progress/engine.gif') 50% 100px no-repeat scroll #fff !important;">
            <div class="pivot-progress"></div>
          </div>

          <div id="<?php print $target; ?>" class="pivot-main"></div>

          <script type="text/javascript">
            google.load("visualization", "1", {packages:["corechart", "charteditor", "gauge"]});

            /**
             *
             */
            var system = {

              dynamicTable: {

                  derivers: $.pivotUtilities.derivers
                , target: '<?php print $target; ?>'
                , module: '<?php print $module; ?>'
                , config: '<?php print $module; ?>-<?php print $id; ?>'
                , id: '<?php print $id; ?>'
                , sql: '<?php print base64_encode($sql); ?>'
                , key: '<?php print md5(base64_encode($recovered['w'])); ?>'
                , url: '<?php print APP_PAGE; ?>core/interface/report.view.php?action=tab-json&target=' + '<?php print $target; ?>'
                , $progress: jQuery(".pivot-progress").progress().create()
                , tab_get_last_data: '<?php print Number::parseInteger($this->tab_get_last_data); ?>'
                
                , $cube: {
                  
                }
                , print: function() {
                  jQuery('#pvtTable').battatech_excelexport({
                    containerid: "pvtTable"
                   , datatype: 'table'
                   , encoding: "UTF-8"
                  });
                }
                , getLastConfig: function(index) {
                  var response = 0
                    , config = JSON.parse(localStorage.getItem(system.dynamicTable.config))
                  ;

                  if (config) {
                    response = config[index];
                  }

                  return response;
                }
              }
            };
            /**
             *
             */
            jQuery(function() {

              var $target = jQuery("#" + system.dynamicTable.target);

              jQuery('.page-header-container').appendTo(jQuery('body'));

              jQuery('body').click(function(e) {

                var c = e.target.className;

                if (c && c !== 'form-buttom' && c !== 'pvtAttrDropdown' && c !== 'pvtAggregator' && c !== 'pivot-functions') {
                  jQuery('div.pivot-functions:visible').hide('fast');
                }
                $filter = jQuery('div.pvtFilterBox:visible');
                if ($filter.length) {
                  if (c && c !== 'pvtTriangle' && c !== 'pvtFilter' && c !== 'pvtSearch' && c !== 'pvtFilterBox') {
                    $filter.hide('fast');
                  } else {
                    if (c === 'pvtTriangle') {
                      if ($filter.length === 2) {
                        var $parent = jQuery(e.target).parent().parent().next()[0];
                        $filter.each(function(i) {
                          if (this !== $parent) {
                            jQuery(this).hide('fast');
                          }
                        });
                      }
                    }
                  }
                }
              });

              system.dynamicTable.$progress.to(1);
              jQuery.ajax({
                  type: 'POST',
                  dataType: 'json',
                  url: system.dynamicTable.url,
                  cache: false,
                  data: {
                    m: system.dynamicTable.module
                    , i: system.dynamicTable.id
                    , key: system.dynamicTable.key
                    , sql: system.dynamicTable.sql
                    , last: system.dynamicTable.tab_get_last_data
                  },
                  error: function (xhr, ajaxOptions, thrownError) {
                    system.dynamicTable.$progress.to(100);
                    system.dynamicTable.$progress.stylize(null, {background: '#DA746F'});
                  },
                  xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    //Download progress
                    xhr.addEventListener("progress", function (evt) {
                      if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;

                        system.dynamicTable.$progress.to(Math.round(percentComplete * 100));
                      }
                    }, false);
                    return xhr;
                  },
                  success: function (json) {

                    jQuery('.pivot-mask').hide();
                    system.dynamicTable.$cube = $target.pivotUI(
                      json
                      , {
                        derivedAttributes: {}
                      , renderers: jQuery.extend(jQuery.pivotUtilities.renderers, jQuery.pivotUtilities.gchart_renderers)
                      , menuLimit: 200
                      , aggregatorName: system.dynamicTable.getLastConfig('aggregatorName')
                      , rows: system.dynamicTable.getLastConfig('rows')
                      , cols: system.dynamicTable.getLastConfig('cols')
                      , vals: system.dynamicTable.getLastConfig('vals')
                      , exclusions: system.dynamicTable.getLastConfig('exclusions')
                      , rendererName: system.dynamicTable.getLastConfig('rendererName')

                      , onRefresh: function(c) {

                        var config_copy = JSON.parse(JSON.stringify(c));

                        //delete some values which are functions
                        delete config_copy["aggregators"];
                        delete config_copy["renderers"];
                        delete config_copy["derivedAttributes"];
                        //delete some bulky default values
                        delete config_copy["rendererOptions"];
                        delete config_copy["localeStrings"];

                        localStorage.setItem(system.dynamicTable.config, JSON.stringify(config_copy, undefined, 2));

                        jQuery('body > table.pvtTable').remove();
                        jQuery('body').append(jQuery('div.pivot-container').html());
                      }
                    }
                    , false
                    , 'en');

                  }
              });
            });
          </script>
        <?php

      } else {

        ini_set("include_path", ini_get("include_path") . ":" . APP_PATH . "lib/phpreports/");

        include "PHPReportMaker.php";

        $reportMaker = new PHPReportMaker();

        $this->xml = $this->xml ? $this->xml : APP_PATH . "src/" . $this->module . "/report/" . $this->id . ".xml";

        $reportMaker->putEnvObj("report", $this);
        $reportMaker->setXML($this->xml);
        $reportMaker->setSQL($sql);
        $reportMaker->run();

      }

      $page = ob_get_contents();

      ob_end_clean();
  
      File::saveFile($filename, $page, 'w', false);
  
      print $page;

    }

    $this->printFooterReport($recovered['wd'], $recovered['gd'], $recovered['od']);

  }

  /**
   * 
   * @param String $id
   * @param String $description
   * @param String $type
   * @param String $validate
   * @param String $line
   * @param String $value
   * @param String $title
   * @param String $type_content
   * @param String $type_behavior
   * @param String $action
   * @param String $style
   * @param Array $fk
   * 
   * @return Array
   */
  public function generateItem($id, $description, $type, $validate, $line, $value = "", $title = "", $type_content = "", $type_behavior = "", $action = "", $style = "", $fk = null, $onchange = null) {

    $item = array("id" => $id, "description" => $description, "title" => $title, "type" => $type, "value" => $value, "line" => $line, "validate" => $validate, "type_content" => $type_content, "type_behavior" => $type_behavior, "action" => $action, "style" => $style, "fast" => 0, "grid" => 1, "grid_width" => "", "form" => 1, "form_width" => "", "readonly" => 0, "default_view" => "", "default_sql" => "", "update" => 1, "insert" => 1);

    if (!is_null($fk)) {
      $foreign = array("modulo" => $fk['module'], "entity" => $fk['entity'], "table" => $fk['table'], "tag" => $fk['tag'], "key" => $fk['key'], "description" => $fk['description'], "form" => $fk['form'], "width" => $fk['width'], "target" => $fk['target'], "onchange" => $fk['onchange'], "encode" => $fk['encode'], "where" => $fk['where'], "order" => $fk['order']);
      $item['foreign'] = $foreign;
    }
    if (!is_null($onchange)) {
      $item['onchange'] = $onchange;
    }

    return $item;
  }

  /**
   * 
   * @param String $module
   * @param String $entity
   * @param String $table
   * @param String $key
   * @param String $description
   * @param String $target
   * @param Boolean $encode
   * @param String $onchange
   * @param String $where
   * @param String $order
   * @param Integer $width
   * 
   * @return Array
   */
  public function generateForeign($module, $entity, $table, $key, $description, $target = null, $encode = null, $onchange = null, $where = null, $order = null, $width = 400) {

    $tag = String::lower($entity);
    
    $entity = String::clearExtension($entity);

    return array(
        "module" => $module
        , "entity" => $entity
        , "tag" => $tag
        , "table" => $table
        , "key" => $key
        , "description" => $description
        , "form" => "form"
        , "width" => $width
        , "target" => !is_null($target) ? $target : "div-" . uniqid()
        , "encode" => !is_null($encode) ? $encode : true
        , "onchange" => !is_null($onchange) ? ($encode ? base64_encode($onchange) : $onchange) : ("")
        , "where" => !is_null($where) ? ($encode ? Encode::encrypt($where) : $where) : ("")
        , "order" => !is_null($order) ? ($encode ? Encode::encrypt($order) : $order) : ("")
    );
  }

  /**
   * 
   * @param $id
   * @param $property
   * @param $where
   * @param $message
   * @param $order
   * @param $group
   */
  public function generateOnchange($id, $property, $where, $message = 'Selecione um item acima', $order = '', $group = '') {
    return 
      array(
          "id" => $id
        , "property" => $property
        , "message" => $message
        , "where" => $where
        , "group" => $group
        , "order" => $order
     );
  }

  /**
   * 
   * @param type $reset
   * 
   * @return type
   */
  public function getCurrentRow($reset = false) {

    if ($reset) {
      $this->current = 1;
    }

    return $this->current++;
  }

  /**
   * 
   * @param type $type
   * @param type $value
   * @param type $type_content
   * 
   * @return string
   */
  public function renderValue($type, $value, $type_content = "") {

    $rendered = "";
    switch ($type) {
      case 'yes/no':
        $rendered = Boolean::parse($value) ? "SIM" : "NÃO";
        break;
      case 'list':
      case 'option':
        $rendered = $value;
        if (is_array($type_content)) {
          $module = $type_content['module'];
          $entity = $type_content['entity'];
          $prefix = $type_content['prefix'];
          $id = $type_content['id'];
          System::import('m', $module, $entity, 'src', true);
          $model = new $entity();
          $get = "get_" . $prefix . "_items";
          $items = $model->$get();
          $type_content = $items[$id]['type_content'];
        }
        $type_content = explode("|", $type_content);
        for ($i = 0; $i < count($type_content); $i++) {
          $data = explode(",", $type_content[$i]);
          $_value = $data[0];
          if (isset($data[1])) {
            if ($value == $_value) {
              $rendered = $data[1];
            }
          }
        }
        break;
      default:
        $rendered = $value . " [no-type]";
        break;
    }

    return $rendered;
  }

  /**
   * 
   * @param string $module
   * @param string $id
   * @param string $alias
   * @param array $filters
   * @param array $array
   * 
   * @return string
   */ 
  public function generateLink($module, $id, $alias, $filters, $array = false) {

    $class = ucfirst($id);
  
    System::import('r', $module, $class, 'src');
    
    $report = new $class();
    $label = utf8_encode($report->name);
    $type = $report->type;

    $fs = array();
    if (is_array($filters)) {
      foreach ($filters as $_id=>$_item) {
        $_value = $_item['value'];
        $_type = $_item['type'];
        if ($_type === 'f') {
          $_value = "', " . $_value . ", '";
        }
        $_f = addslashes("{'id':'" . $_id . "','value':'{value}'}");
        $_fs = String::replace($_f, '{value}', $_value);
        $fs[] = $_fs;
      }
    }
    $f = implode(',', $fs);
    //', emp_codigo, '

    $link = array('field' => "CONCAT('" . addslashes("report:{'module':'" . $module . "','id':'" . $id . "','label':'" . $label . "','type':'" . $type . "','fields':[{filters}]}") . "')",  'as' => $alias);
    $link['field'] = String::replace($link['field'], '{filters}', $f);
    if (!$array) {
      $link = implode(' AS ', $link);
    }

    return $link;
  }

  /**
   * 
   * @param array $recovered
   * 
   * @return array
   */ 
  public function verify($recovered) {
    /**
     * $validate = array("id" => $item['id'], "description" => $item['description'], "type" => $item['type'], "value" => $item['value'], "message" => "Favor informar um valor v&aacute;lido");
     */ 
    return array();
  }
  
  /**
   * 
   * @param $key
   * @param $sql
   * @param $behavior
   */ 
  public function getJson($key, $sql, $behavior = null) {
    $response = "";

    $behavior = Number::parseInteger($behavior);

    $file_path = APP_PATH . 'files/pivot/' . $this->module . '/' . $this->id . '/' . date("Y-m-d") . '-' . $key . '.json';
    $last_path = APP_PATH . 'files/pivot/' . $this->module . '/' . $this->id . '/last-' . $key . '.json';

    if ($behavior === 1 && file_exists($last_path)) {

      $response = $last_path;

    } else if ($behavior === 0 && file_exists($file_path)) {

      $response = $file_path;

    }

    if (strlen($response) === 0) {
      $first = true;
      
      if (!file_exists(dirname($file_path))) {
        mkdir(dirname($file_path), 0777, true);
      }
      
      $file = fopen($file_path, "w");
      fwrite($file, '[');

      //$lines = array();
      $fields = $this->get_fields();

      $result = $this->getData($sql);
      while($row = $this->fetchData($result)) {
        $line = array();
        foreach ($fields as $field) {
          if (!isset($field['as']) || strlen($field['as']) == 0) {
            $field['as'] = $field['field'];
          }
          $as = $field['as'];
          if (!isset($field['description']) || strlen($field['description']) == 0) {
            $field['description'] = $field['as'];
          }
          $description = Json::encodeValue($field['description']);

          $line[$description] = Json::encodeValue($row[$as]);
        }

        if (!$first) {
          fwrite($file, ',');
        }
        $first = false;
        fwrite($file, json_encode($line));
      }

      fwrite($file, ']');

      fclose($file);
      File::copyFile($file_path, $last_path);
      
      $response = $file_path;
    }

    return $response;
  }

}
