<?php

class ReportContainer {
  
  protected $module;
  protected $id;
  protected $name;
  protected $ext;

  private $reports;

  public function ReportContainer() {
    $this->reports = array();
  }
  
  public function get_module() {
    return $this->module;
  }

  public function set_module($module) {
    $this->module = $module;
  }

  public function get_id() {
    return $this->id;
  }

  public function set_id($id) {
    $this->id = $id;
  }

  public function get_name() {
    return $this->name;
  }

  public function set_name($name) {
    $this->name = $name;
  }

  public function get_ext() {
    return $this->ext;
  }

  public function set_ext($ext) {
    $this->ext = $ext;
  }
  
  public function get_reports() {
    return $this->reports;
  }
  
  public function set_reports($reports) {
    if (!is_array($reports)) {
      $this->setReports(array());
      core_err(0, "Deve ser parrado um array no formato array(array('module'=>'modulo', 'id'=>'001'))");
      return false;
    }

    $this->reports = $reports;
    return true;
  }

}