<?php
/**
 *
 * @copyright dracones.io
 *
 * @author Pedro - 20/02/2013 14:46
 * Modulo..........: Geral
 *
 * Responsavel.....: Pedro Mázala
 * Alteracao.......: 27/04/2015
 */

class Upload {

  protected $module;
  protected $id;
  protected $ext;

  protected $name;
  protected $items;
  protected $field;
  protected $table;
  protected $where;
  protected $order;
  protected $group;
  protected $limit;

  public function Upload() {

    $this->name = "";

    $this->field = "";
    $this->table = "";
    $this->where = "";
    $this->order = "";
    $this->group = "";
    $this->limit = "";

    $this->items['module'] = array("id" => "module", "description" => "", "hidden" => 1, "title" => "", "type" => "string", "type_content" => "", "type_behavior" => "", "value" => $this->module, "action" => "", "style" => "", "validate" => "", "fast" => 0, "grid" => 0, "grid_width" => "", "form" => 0, "form_width" => "", "readonly" => 1, "default_view" => "0", "default_sql" => "", "update" => 0, "insert" => 0, "line" => 1);
    $this->items['id'] = array("id" => "id", "description" => "", "hidden" => 1, "title" => "", "type" => "string", "type_content" => "", "type_behavior" => "", "value" => $this->id, "action" => "", "style" => "", "validate" => "", "fast" => 0, "grid" => 0, "grid_width" => "", "form" => 0, "form_width" => "", "readonly" => 1, "default_view" => "0", "default_sql" => "", "update" => 0, "insert" => 0, "line" => 1);
    $this->items["return"] = array("id" => "return", "description" => "Resposta", "title" => "", "type" => "html", "type_content" => "", "type_behavior" => "", "value" => "", "action" => "", "style" => "", "validate" => "", "fast" => 0, "grid" => 0, "grid_width" => "", "form" => 0, "form_width" => "", "readonly" => 0, "default_view" => "", "default_sql" => "", "update" => 0, "insert" => 0, "line" => 1);
  }

  /**
   *
   * @return type
   */
  public function get_name() {
    return $this->name;
  }

  /**
   *
   * @param type $name
   */
  public function set_name($name) {
    $this->name = $name;
  }

  /**
   *
   * @return type
   */
  public function get_module() {
    return $this->module;
  }

  /**
   *
   * @param type $module
   */
  public function set_module($module) {
    $this->module = $module;
  }

  /**
   *
   * @return type
   */
  public function get_id() {
    return $this->id;
  }

  /**
   *
   * @param type $id
   */
  public function set_id($id) {
    $this->id = $id;
  }

  /**
   *
   * @return type
   */
  public function get_items() {
    return $this->items;
  }

  /**
   *
   * @param type $items
   */
  public function set_items($items) {
    $this->items = $items;
  }

  /**
   *
   * @return type
   */
  public function get_field() {
    return $this->field;
  }

  /**
   *
   * @param type $field
   */
  public function set_field($field) {
    $this->field = $field;
  }

  /**
   *
   * @return type
   */
  public function get_ext(){
    return $this->ext;
  }

  /**
   *
   * @param type $ext
   */
  public function set_ext($ext){
    $this->ext = $ext;
  }

  /**
   *
   * @return type
   */
  public function get_table() {
    return $this->table;
  }

  /**
   *
   * @param type $table
   */
  public function set_table($table) {
    $this->table = $table;
  }

  /**
   *
   * @return type
   */
  public function get_where() {
    return $this->where;
  }

  /**
   *
   * @param type $where
   */
  public function set_where($where) {
    $this->where = $where;
  }

  /**
   *
   * @return type
   */
  public function get_order() {
    return $this->order;
  }

  /**
   *
   * @param type $order
   */
  public function set_order($order) {
    $this->order = $order;
  }

  /**
   *
   * @return type
   */
  public function get_group() {
    return $this->group;
  }

  /**
   *
   * @param type $group
   */
  public function set_group($group) {
    $this->group = $group;
  }

  /**
   *
   * @return type
   */
  public function get_limit() {
    return $this->limit;
  }

  /**
   *
   * @param type $limit
   */
  public function set_limit($limit) {
    $this->limit = $limit;
  }

  /**
   *
   * @param type $data
   */
  public function add_item($data) {
    $this->items[] = $data;
  }

  /**
   *
   * @param type $id
   */
  public function remove_item($id) {
    unset($this->items[$id]);
  }

  /**
   *
   * @param type $recovered
   */
  public function processFile($recovered){
    print_r($recovered);

    return false;
  }

  /**
   *
   * @param type $sql
   * @return type
   */
  public function getData($sql){
    require System::desire('file', '', 'header', APP_PATH.'core', false);
    System::desire('class', 'base', 'DAO', APP_PATH.'core');

    $dao = new DAO();
    $query = $dao->selectSQL($sql);

    return $query;
  }

  /**
   *
   * @param type $sql
   * @param type $index
   * @return type
   */
  public function getValue($sql, $index){

    $value = null;

    $query = $this->getData($sql);
    if($query != NULL) {
      while($row = mysql_fetch_array($query)) {
        $value = $row[$index];
      }
    }

    return $value;
  }

  /**
   *
   * @param type $type
   * @param type $items
   * @param type $target
   * @param type $level
   * 
   * @return type
   */
  public function printContent($type, $items, $target, $level) {

    System::import('class', 'resource', 'Screen', 'core');

    $screen = new Screen(DEFAULT_EXTENSION);

    $properties = array();
    $properties['reference'] = 0;
    $properties['module'] = $this->get_module();
    $properties['entity'] = ucfirst($this->get_id());
    $properties['tag'] = $this->get_id();

    $properties['lines'] = 0;
    foreach ($items as $item) {

      $properties['lines'] = max($properties['lines'], $item['line']);
    }

    $properties["layout"] = 'manager';

    $properties['operations'] = array(
      'form' => new Object(array("action"=>'form', "label"=>"Voltar", "title"=>$this->name, "layout"=>"manager", "position"=>"toolbar", "type"=>"upload-back", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("process"=>"btn-primary"))),
      'process' => new Object(array("action"=>'process', "label"=>"Importar", "title"=>$this->name, "layout"=>"manager", "position"=>"toolbar", "type"=>"upload-import", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("form"=>"btn-primary"), 'properties'=>['module'=>$this->module, 'id'=>$this->id])),
    );

    $items['module']['value'] = $this->module;
    $items['id']['value'] = $this->id;

    $screen->printContent($target, 'manager', $type, $level, null, $items, null, null, $properties, null);
  }

}