<?php

/**
 * 
 * @class Download
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Download {

  /**
   * 
   * @param $uri
   */
  public static function render($uri) {

    $error = true;

    $code = "404";
    $title = "Not Found";
    $message = "The requested URL was not found on this server";
    
    $begining = 9;
    $start = 11;

    $behavior = String::substring($uri, $begining, 1);
    $filename = File::path(APP_PATH, 'storage', 'files', String::replace(String::replace(String::substring($uri, $start), '../', ''), '..\\', ''));

    switch ($behavior) {

      case 'f':

        if (File::exists($filename)) {

          $name = File::getName($filename);

          header("Content-Type: application/force-download");
          header("Content-Type: application/octet-stream");
          header("Content-Type: application/download");
          header("Content-Type: application/save");
          header("Content-Description: File Transfer");
          header("Content-Transfer-Encoding: binary");
          header("Content-Disposition: attachment; filename=" . urlencode($name) . "");
          header("Content-Length:" . filesize($filename));
          header("Expires: 0");
          header("Pragma: no-cache");

          readfile($filename);

          $error = false;

        }

        break;

      case 'i':

        if (File::exists($filename)) {

          $type = Application::getContentType($filename);

          header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
          header("Cache-Control: post-check=0, pre-check=0", false);
          header("Pragma: no-cache");

          header("Content-type: " . $type);
          header("Content-Length:" . filesize($filename));
          readfile($filename);

          $error = false;

        }

        break;

      case 'a':

        System::import('c', 'site', 'Midia', 'src');

        $midiaCtrl = new MidiaCtrl();

        $mdi_slug = addslashes(String::substring($uri, $start));

        $mid_arquivo = $midiaCtrl->getColumnMidiaCtrl("mid_arquivo", "mid_slug = '" . $mdi_slug . "'");

        $filename = $mid_arquivo ? File::path(APP_PATH, 'storage', 'files', $mid_arquivo) : '';

        if (File::exists($filename)) {

          $type = File::getContentType($filename);

          header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
          header("Cache-Control: post-check=0, pre-check=0", false);
          header("Pragma: no-cache");

          header("Content-type: " . $type);
          header("Content-Length:" . filesize($filename));
          readfile($filename);

          $error = false;

        }

        break;

      default:

        $error = true;
        break;
    }

    if ($error) {

      self::error($code, $title, $message);
    }
  }
  
  /**
   * 
   * @param $code
   * @param $title
   * @param $message
   */
  public static function error($code, $title, $message) {
    ?>
      <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
      <html>
        <head>
          <title><?php print $code; ?> <?php print $title; ?></title>
        </head>
        <body>
          <h1><?php print $title; ?></h1>
          <p><?php print $message; ?>.</p>
          <hr>
          <address><?php print APP_NAME; ?>/<?php print APP_VERSION; ?> (<?php print APP_HOST; ?>)</address>
        </body>
      </html>
    <?php
  }

}


