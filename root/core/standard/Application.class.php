<?php

/**
 *
 * @class Application
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Application {

  /**
   * 
   * @var cookies names
   */
  public static $cookies = ['token' => '_token', 'session' => '_session', 'remember' => '_remember'];

  /**
   * 
   * @var credencials inputs
   */
  public static $credencials = ['login' => 'login', 'password' => 'password', 'remember' => 'remember'];

  /**
   * 
   * @var theme name
   */
  public static $theme = 'theme';

  /**
   * 
   * @var app uri
   */
  public static $admin = 'admin';

  /**
   * 
   * @var language package
   */
  public static $language = 'pt-BR';

  /**
   * 
   * @var protocol default
   */
  public static $protocol = 'http';

  /**
   * 
   * @var services handlers
   */
  public static $services = [

    /* cms services  */
      'download' => ['handler' => 'download', 'logged' => false]
    , 'static' => ['handler' => 'direct', 'logged' => false]
    , 'assets' => ['handler' => 'assets', 'logged' => false]

    /* api services  */
    , 'api' => ['handler' => 'api', 'logged' => false]

    /* app services  */
    , 'logout' => ['handler' => 'logout', 'logged' => false]
    , 'upload' => ['handler' => 'upload', 'logged' => true]
    , 'app' => ['handler' => 'app', 'logged' => true]
  ];

  /**
   * 
   * @var uri current
   */
  public static $uri = '';

  /**
   * 
   * @var user current
   */  
  public static $user = null;

  /**
   * @var array
   */
  public static $behaviors = [

    'pk'=>["type"=>"numeric","component"=>"input[number]"],
    'fk'=>["type"=>"numeric","component"=>"autocomplete"],
    'child'=>["type"=>"child","component"=>"items"],

    'int'=>["type"=>"numeric","component"=>"input[number]"],
    'double'=>["type"=>"numeric","component"=>"input[number]"],

    'money'=>["type"=>"numeric","component"=>"input[text]"],
    'percent'=>["type"=>"numeric","component"=>"input[text]"],
    'min/max'=>["type"=>"numeric","component"=>"input[text]"],

    'color'=>["type"=>"color","component"=>"input[color]"],

    'boolean'=>["type"=>"boolean","component"=>"input[checkbox]"],

    'upper'=>["type"=>"string","component"=>"input[text]"],

    'hash'=>["type"=>"string","component"=>"input[text]"],

    'string'=>["type"=>"string","component"=>"input[text]"],
    'source'=>["type"=>"string","component"=>"input[text]"],
    'cpf'=>["type"=>"string","component"=>"input[text]"],
    'cnpj'=>["type"=>"string","component"=>"input[text]"],
    'telefone'=>["type"=>"string","component"=>"input[text]"],
    'cep'=>["type"=>"string","component"=>"input[text]"],
    'base64'=>["type"=>"string","component"=>"input[text]"],
    'email'=>["type"=>"string","component"=>"input[text]"],

    'text'=>["type"=>"string","component"=>"textarea"],
    'rich'=>["type"=>"string","component"=>"rich-text"],
    'rich-text'=>["type"=>"string","component"=>"rich-text"],
    'longtext'=>["type"=>"string","component"=>"rich-text"],

    'source-code'=>["type"=>"string","component"=>"source-code"],

    'datetime'=>["type"=>"date","component"=>"datetimepicker"],
    'date'=>["type"=>"date","component"=>"datetimepicker"],
    'time'=>["type"=>"date","component"=>"datetimepicker"],
    'second'=>["type"=>"date","component"=>"input[text]"],
    'timestamp'=>["type"=>"date","component"=>"input[text]"],
    'dateshort'=>["type"=>"date","component"=>"input[text]"],

    'yes/no'=>["type"=>"numeric","component"=>"input[radio]"],
    'option'=>["type"=>"string","component"=>"input[radio]"],

    'select'=>["type"=>"string","component"=>"select"],
    'select-multi'=>["type"=>"string","component"=>"select"],
    'tag'=>["type"=>"tag","component"=>"select"],
    'list'=>["type"=>"string","component"=>"select"],
    'array'=>["type"=>"string","component"=>"select"],

    'image'=>["type"=>"string","component"=>"input[image]"],
    'file'=>["type"=>"string","component"=>"input[file]"],

    'html'=>["type"=>"string","component"=>"html"],
    'calculated'=>["type"=>"string","component"=>"html"],

    'between'=>["type"=>"date","component"=>"datetimepicker"],
    'range'=>["type"=>"numeric","component"=>"input[number]"],

    'subquery'=>["type"=>"string","component"=>"html"],
    'subquery-radio'=>["type"=>"string","component"=>"html"],
    'mixed'=>["type"=>"string","component"=>"html"],

    'subitem'=>["type"=>"collection","component"=>"subitem"],
    'object'=>["type"=>"string","component"=>"html"]
  ];



  /**
   * 
   * @methods define routes and services
   */
  // 

  /**
   *
   * lets go boy
   */
  public static function start() {

    $uri = App::request('uri');

    if (String::substring($uri, 0, 1) === '/') {
      $uri = String::substring($uri, 1);
    }
    $request = explode("/", $uri);

    $service = $request[0];

    $return = App::request('r', null);

    $redirected = false;

    if ($return) {

      if (self::login()) {

        $redirected = self::redirect($return);
      }
    }
    
    if (!$redirected) {
      
      define('APP_SERVICE_LOGOUT', 'logout');
  
      define('APP_SERVICE_ADMIN', self::$admin);

      if (isset(self::$services[$service])) {

        define('APP_SERVICE', $service);

        $handler = self::$services[$service]['handler'];
        
        $user = '';
        $message = '';
        
        if (self::$services[$service]['logged']) {

          self::login();
        }

        App::$handler($uri, $return);
  
      } else {

        self::login();

        $admin = self::isAdmin($service);

        define('APP_ADMIN', $admin);

        define('APP_SERVICE', ($service === self::$admin) ? 'admin' : 'site');

        System::import('theme', DEFAULT_THEME, 'Theme', '', true);

        Theme::init($uri, $return);
      }

    }

  }

  /**
   * 
   * @return boolean
   */
  public static function login() {

    $connected = true;

    if (self::$user === null) {

      $connected = false;

      self::$user = App::login(
          App::request(self::$credencials['login'], null)
        , App::request(self::$credencials['password'], null)
        , App::request(self::$credencials['remember'], null)
      );

      if (self::$user->connected) {
  
        $connected = true;
      }
    }

    define('APP_CONECTED', $connected);

    return $connected;
  }

  /**
   * 
   * @param $url
   */
  public static function redirect($url) {
    
    header('Location: ' . $url);
    
    exit;

    return false;
  }

  /**
   *
   * @param type $location
   *
   * @return type
   */
  public static function language($location) {

    $root = defined('APP_PATH') ? APP_PATH : '.' . APP_FS . '';

    $filename = App::path($root, 'root', 'config', 'language.' . $location . '.xml');
    $language = $location;

    if (App::exists($filename)) {
      $language = simplexml_load_file($filename);
    }

    self::$language = $language;
  }

  /**
   *
   * @param string $label
   * @param boolean $print
   * @param boolean $description
   *
   * @return string
   */
  public static function lang($label, $print = false, $description = false) {

    $text = "";
    $function = "";

    if (gettype(self::$language) === 'string') {
      self::language(self::$language);
    }

    if (isset(self::$language->$label)) {

      $text = self::$language->$label;
      if (String::trim(UTF8_LANG)) {
        $function = 'utf8_' . UTF8_LANG;
      }
    }

    if ($function) {
      $text = $function($text);
    }

    if ($print) {
      self::text($text);
    }
    return $text;
  }

  /**
   *
   * @param type $user
   * @return string
   */
  public static function manutencao($user) {

    $redirect = "";
    if (($user->usu_administrador < 10) && !App::request('su')) {
      if (self::$uri !== String::replace(PAGE_MANUTENCAO, APP_PAGE, "")) {
        $redirect = PAGE_MANUTENCAO;
      }
    }

    return $redirect;
  }

  /**
   *
   * @param type $service
   * @return boolean
   */
  public static function isAdmin($service) {

    $admin = false;
    if (APP_CONECTED) {

      if ($service === self::$admin) {
        $admin = true;
      }

    }
    return $admin;
  }

  /**
   *
   * @param string $path
   * @param boolean $print
   * @param boolean $back
   *
   * @return string
   */
  public static function link($path = "", $print = true, $back = false) {

    $r = "";

    if ($back) {

      $local = "b=" . self::getLocation();
      $r = String::position($path, "?") ? "&" . $local : "?" . $local ;
    }

    $link = APP_PAGE . "/" . $path . $r;

    if ($print) {

      print $link;
    }

    return $link;
  }

  /**
   *
   * @return string
   */
  public static function getLocation() {

    return ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  }

  /**
   *
   * @param string $text
   * @param boolean $recovered
   * @param boolean $print
   *
   * @return string
   */
  public static function text($text, $recovered = null, $print = true) {

    $sufix = '';
    if ($recovered === true) {
      $sufix = UTF8_RECOVERED;
    } else if ($recovered === false) {
      $sufix = UTF8_NOT_RECOVERED;
    }
    
    if (String::trim($sufix)) {
      $function = 'utf8_' . $sufix;
      //print '[' . $function . ']';
      $text = $function($text);
    }

    if ($print) {
      print $text;
    }
    return $text;
  }

  /**
   *
   * @param type $index
   * @return type
   */
  public static function get($index = 'name') {

    $value = "";

    $options = array(
      "code" => new Object(array("property" => "usu_codigo", "decrypt" => false)),
      "name" => new Object(array("property" => "usu_nome", "decrypt" => true)),
      "type" => new Object(array("property" => "usu_tipo", "decrypt" => true)),
      "admin" => new Object(array("property" => "usu_admin", "decrypt" => true)),
      "email" => new Object(array("property" => "usu_email", "decrypt" => true)),
      "avatar" => new Object(array("property" => "usu_foto", "decrypt" => true)),
      "login" => new Object(array("property" => "usu_login", "decrypt" => true))
    );

    if (isset($options[$index])) {

      $option = $options[$index];

      $user = null;
      $session = self::$user;
      if (!is_object($session)) {

        $user = json_decode(stripslashes($session));
      }

      if ($user) {

        $property = $option->property;
        if (property_exists($user, $property)) {
          $value = $user->$property;
          if ($option->decrypt) {
            $value = App::decrypt($value);
          }
        }

      }

    }

    return $value;
  }

  /**
   *
   * @return string
   */
  public static function getHost() {

    $host = $_SERVER["HTTP_HOST"];
    
    if (defined('APP_PAGE')) {

      $pieces = explode('/', APP_PAGE);
      
      // remove protocol
      array_shift($pieces);
      //remove empty space between /
      array_shift($pieces);
      //remove host
      array_shift($pieces);
      //remove last /
      array_pop($pieces);
      
      if ($pieces) {
        $host = $host . "/" . implode('/', $pieces);
      }
    }

    return $host;
  }



  /**
   * 
   * @methods resolve routes and services
   */
   // 

  /**
   * 
   * @param $uri
   * @param $user
   * @param $message
   * @param $return
   */
  public static function assets($uri, $return) {

    System::import('theme', DEFAULT_THEME, 'Theme', '', true);

    Theme::asset($uri, $return);
  }

  /**
   * 
   * @param $uri
   */
  public static function app($uri) {

    $request = explode('/', $uri);

    if (isset($request[1])) {

      $type = $request[1];
      $paths = [];
      $_file = '';

      $comment = '';
      $compact = false;

      switch ($type) {

        case 'style':

          header('Content-Type: text/css; charset=UTF-8');

          $comment = '/* CSS __FILE__ {file} */';

          $paths = [
              'vendor/ionic/css/ionic.css'
            , 'vendor/ionic-material/dist/ionic.material.min.css'
            , 'vendor/angular-ui-grid/ui-grid.min.css'
            , 'vendor/autocomplete/style/autocomplete.css'
          ];
          $compact = true;
          break;

        case 'javascript':

          header('Content-Type: text/javascript; charset=UTF-8');

          $comment = '/* JS  __FILE__ {file} */';

          $paths = [
              'vendor/ionic/js/ionic.bundle.js'
            , 'vendor/ionic-material/dist/ionic.material.min.js'
            //, 'vendor/angular-ui-grid/ui-grid.min.js'
            , 'vendor/ngcordova/dist/ng-cordova.js'
            , 'vendor/autocomplete/script/autocomplete.js'
            , 'public/lib/localstorage/js/localstorage.min.js'
            , 'public/lib/smoothie/standalone/require.js'
          ];
          $compact = true;
          break;

        case 'css':

          header('Content-Type: text/css; charset=UTF-8');

          $comment = '/* CSS __FILE__ {file} */';

          $paths = [
              'public/css/style.css'
            , 'public/css/animation.css'
            , 'public/css/timeline.css'
          ];
          $compact = true;
          break;

        case 'js':

          header('Content-Type: text/javascript; charset=UTF-8');

          $comment = '/* JS  __FILE__ {file} */';

          $paths = [
              'api/core/app.js'
            , 'api/core/app.main.js'
            , 'api/core/app.factory.js'
            , 'api/core/app.service.js'
            , 'api/core/app.controller.js'
            , 'api/core/app.directive.js'
            , 'api/core/app.constant.js'

            , 'api/core/class.model.js'
            , 'api/core/class.controller.js'
          ];
          $compact = true;
          break;

        case 'html':

          header('Content-Type: text/html; charset=UTF-8');

          $comment = '<!-- HTML  __FILE__ {file} -->';

          $paths = [
            'public/templates/' . (isset($request[2]) ? $request[2] : '') . '.html'
          ];
          $_file = File::path(APP_PATH, 'root', 'assets', String::replace('public/templates/404.html', '/', APP_FS));
          break;

        case 'fonts':

          header('Content-Description: Font File');
          header('Content-Type: application/x-font-TrueType');

          $paths = [
            'vendor/ionic/fonts/' . (isset($request[2]) ? $request[2] : '')
          ];

          break;

        case 'api':

          $paths = [
            String::substring($uri, 4)
          ];
          break;

        case 'model':

          header('Content-Type: text/javascript; charset=UTF-8');

          $root = 'src';
          $module = $request[2];
          $entity = String::replace($request[3], '.class.js', '');

          System::import('class', 'controller', 'Javascript', 'core');

          $javascript = new Javascript($root, $module, $entity);

          print $javascript->model();

          break;

        case 'controller':

          header('Content-Type: text/javascript; charset=UTF-8');

          $root = 'src';
          $module = $request[2];
          $entity = String::replace($request[3], '.ctrl.js', '');

          System::import('class', 'controller', 'Javascript', 'core');

          $javascript = new Javascript($root, $module, $entity);

          print $javascript->controller();

          break;

        case 'view':

          $root = 'src';
          $module = $request[2];
          $entity = $request[3];
          $operation =  String::replace($request[4], '.html', '');
          $filter = isset($request[5]) ? App::decrypt($request[5]) : '';
          $custom =  isset($request[6]) ? App::decrypt($request[6]) : '';

          self::view($root, $module, $entity, $operation, $filter, $custom);
          break;
      }


      foreach ($paths as $path) {

        $file = File::path(APP_PATH, 'root', 'assets', String::replace($path, '/', APP_FS));

        if ($comment) {

          print PHP_EOL;
          print String::replace($comment, '{file}', $path);
          print PHP_EOL;
        }

        if (File::exists($file)) {

          if (!$comment) {

            header('Content-Length: ' . filesize($file));
            header('Content-Disposition: attachement; filename="' . File::getBase($file) . '"');
          }


          if ($compact) {

            $buffer = file_get_contents($file);

            // Remove comments
            //$buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

            // 
            //$buffer = str_replace('//', '', $buffer);
            
            // Remove space after colons
            //$buffer = str_replace(': ', ':', $buffer);
            // Remove whitespace
            //$buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

            // Write everything out
            echo($buffer);
          } else {

            readfile($file);
          }

        } else if ($_file) {
          
          include $_file;
        }

        if ($comment) {

          print PHP_EOL;
        }
      }
    }
  }

  /**
   * 
   * @param $uri
   */
  public static function api($uri) {

    System::import('class', 'app', 'Api', '');

    Api::resolve(String::substring($uri, 4));
  }

  /**
   *
   * @param type $root
   * @param type $module
   * @param type $entity
   * @param type $operation
   * @param type $filter
   * @param type $custom
   */
  public static function view($root, $module, $entity, $operation, $filter = null, $custom = null) {

    System::import('class', 'base', 'Auth', 'core');

    System::import('c', $module, $entity, $root);

    System::import('theme', DEFAULT_THEME, 'Theme', '', true);

    if (Auth::isAuthorized(self::$user->token, $module, $entity, $operation)) {

      $classCtrl = $entity . 'Ctrl';
      $getPropertiesCtrl = 'getProperties' . $classCtrl;

      $entityCtrl = new $classCtrl(APP_PATH);

      $properties = $entityCtrl->$getPropertiesCtrl();

      $operations = $properties['operations'];

      if (isset($operations[$operation])) {

        $_operation = $operations[$operation];

        //Theme::template($_operation->layout);
        include File::path(APP_PATH, 'root', 'assets', 'public', 'templates', 'layout', $_operation->layout . '.html');
      }

    }

  }

  /**
   * 
   * @param string $behavior
   * 
   * @return string
   */
  public static function getComponent($behavior) {

    return isset(self::$behaviors[$behavior]) ? self::$behaviors[$behavior]['component'] : null;
  }

  /**
   * 
   * @param string $behavior
   * 
   * @return string
   */
  public static function getType($behavior) {

    return isset(self::$behaviors[$behavior]) ? self::$behaviors[$behavior]['type'] : null;
  }

}


