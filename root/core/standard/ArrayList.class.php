<?php

/**
 * Description of List
 *
 * @author Analista
 */
class ArrayList implements Iterator{

  private $position = 0;
  private $collection;
  
  /**
   * 
   * @param string $collection
   * @param string $delimiter
   * @param string $indexer
   * @return \ArrayList
   */
  public function ArrayList($collection = null, $delimiter = ",", $indexer = null) {
    
    if ($collection === null) {
      $this->collection = array();
    } else {
      $this->push($collection, $delimiter, $indexer);
    }
    
    return $this;
  }
  
  /**
   * 
   * @param mixed $item
   * @param int $index
   * @param string $id
   * @return Object
   */
  public function add($item, $index = null, $id = "") {

    if ($index === null) {
      $this->collection[] = array("value" => $item, "id" => $id);
    } else {
      $index = Number::parseInteger($index);
      if ($id === null || $id === "") {
        $id = $index;
      }
      if (!isset($this->collection[$index])) {
        if ($index > $this->size()) {
          for ($i = $this->size(); $i < $index; $i++) {
            $this->collection[$i] = array("value" => null, "id" => $i);
          }
        }
      }
      $this->collection[$index] = array("value" => $item, "id" => $id);
    }

    return $this;
  }
  
  /**
   * 
   * @param mixed $item
   * @param int $index
   * @return Object
   */
  public function set($item, $index) {
    return $this->add($item, $index);
  }
  
  /**
   * 
   * @param mixed $index
   * @param boolean $indexed
   * @return mixed
   */
  public function get($index, $indexed = false) {

    $item = null;
    if (isset($this->collection[$index])) {
      $item = $this->collection[$index]['value'];
      if ($indexed) {
        $item = array($this->collection[$index]['id'] => $this->collection[$index]['value']);
      }
    } else {
      foreach ($this as $id => $element) {
        if ($id === $index) {
          $item = $element;
        }
      }
    }

    return $item;
  }
  
  /**
   * 
   * @return array
   */
  public function getCollection() {
    $collection = array();
    foreach ($this as $element) {
      $collection[] = $element;
    }
    return $collection;
  }
  
  /**
   * 
   * @return int
   */
  public function size() {
    return count($this->collection);
  }
  
  /**
   * 
   * @param string $delimiter
   * @return string
   */
  public function pull($delimiter = ",") {
    $collection = array();
    foreach ($this as $value) {
      if (is_object($value)) {
        $collection[] = "[Object]";
      } else {
        $collection[] = $value;
      }
    }
    return implode($delimiter, $collection);
  }
  
  /**
   * 
   * @param string $collection
   * @param string $delimiter
   * @param string $indexer
   * @return array
   */
  public function push($collection, $delimiter = ",", $indexer = null) {
    $array = explode($delimiter, $collection);
    foreach ($array as $key => $value) {
      $id = null;
      if ($indexer) {
        $element = explode($indexer, $value);
        $value = $element[0];
        if (isset($element[1])) {
          $id = $element[0];
          $value = $element[1];
        }
      }
      $this->add($value, $key, $id);
    }
  }

  /**
   * 
   * @return mixed
   */
  function rewind() {
    $this->position = 0;
  }

  /**
   * 
   * @return mixed
   */
  function current() {
    return $this->collection[$this->position]['value'];
  }

  /**
   * 
   * @return mixed
   */
  function key() {
    return $this->collection[$this->position]['id'];
  }

  /**
   * 
   * @return mixed
   */
  function next() {
    ++$this->position;
  }

  /**
   * 
   * @return mixed
   */
  function valid() {
    return isset($this->collection[$this->position]);
  }

  /**
   * 
   */
  function clear() {

    $this->position = 0;
    $this->collection = array();
    
    $clear = 
    (
      ($this->position === 0)
        && 
      (count($this->collection) === 0)
    );

    return $clear;
  }

}
