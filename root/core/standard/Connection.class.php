<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author william
 */
class Connection {
  
  /**
   *
   * @var 
   *    Array (
   *      Database
   *    )
   */
  private static $database = array();
  
  /**
   * 
   * @param type $plataforma
   * @param type $path
   * @param type $database
   * 
   * @return type
   */
  public static function getDatabase($plataforma, $path, $database, $auto_increment = true) {
    
    $plataforma = ucfirst($plataforma);
    $database = $database ? $database : DEFAULT_DATABASE;

    if (!isset(self::$database[$database])) {
      
      System::import('class', 'base', $plataforma, 'core');
      
      self::$database[$database] = new $plataforma($database, $path, $auto_increment);

    }

    self::$database[$database]->setConfig($database);
    
    return self::$database[$database];
  }
  
  /**
   * 
   * @param type $plataforma
   * @param type $database
   * @param type $path
   * 
   * @return type
   */
  public static function startTransaction($plataforma = "", $database = "", $path = "", $debug = false) {
    
    $plataforma = $plataforma ? $plataforma : 'mysql';
    $path = $path ? $path : APP_PATH;
    $database = $database ? $database : DEFAULT_DATABASE;
    
    $transaction = false;
    self::getDatabase($plataforma, $path, $database);

    if (isset(self::$database[$database])) {

      self::$database[$database]->debug = $debug;

      self::$database[$database]->startTransactionSQL();
      $transaction = self::$database[$database]->getTransaction();

    }
    
    ignore_user_abort(true);

    return $transaction;
  }

  /**
   * 
   * @param type $database
   * 
   * @return boolean
   */
  public static function commitTransaction($database = "") {

    $database = $database ? $database : DEFAULT_DATABASE;

    $commit = false;

    if (isset(self::$database[$database])) {
      
      self::$database[$database]->setConfig($database);
      
      if (self::$database[$database]->getTransaction()) {

        self::$database[$database]->commitTransactionSQL();
        $commit = true;

      }

    }

    self::$database[$database]->debug = false;

    ignore_user_abort(false);
    
    return $commit;
  }

  /**
   * 
   * @param type $database
   * 
   * @return boolean
   */
  public static function rollbackTransaction($database = "") {

    $database = $database ? $database : DEFAULT_DATABASE;

    $rollback = false;

    if (isset(self::$database[$database])) {
      
      self::$database[$database]->setConfig($database);
      
      if (self::$database[$database]->getTransaction()) {

        self::$database[$database]->rollbackTransactionSQL();
        $rollback = true;

      }

    }

    self::$database[$database]->debug = false;

    ignore_user_abort(false);
    
    return $rollback;
  }
  
  /**
   * 
   * @return type
   */
  public static function getPersonalDatabase() {
    
    $identificador = "";
    if (defined('DEFAULT_DATABASE_IDENTIFICATOR')) {
      $identificador = DEFAULT_DATABASE_IDENTIFICATOR;
    } else {
      $identificador = Application::getSubDatabase();
      define('DEFAULT_DATABASE_IDENTIFICATOR', $identificador);
    }
    
    return DEFAULT_DATABASE . "_" . $identificador;
  }

}