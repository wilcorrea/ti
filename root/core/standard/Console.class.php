<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Util
 *
 * @author William
 */
class Console{

  private static $location = "log";
  private static $activated = false;

  private static $messages = array();

  public static $STATUS_FAIL = "fail";
  public static $STATUS_WARNING = "warning";
  public static $STATUS_SUCCESS = "success";

  private static $status = null;

  /**
   * 
   */
  private static function start() {
    self::$status = self::$STATUS_SUCCESS;
  }

  private static function changeStatus($s) {
    $changed = false;

    if ($s === self::$STATUS_FAIL) {
      self::$status = self::$STATUS_FAIL;
      $changed = true;
    } else if ($s === self::$STATUS_WARNING && self::status() === self::$STATUS_SUCCESS) {
      self::$status = self::$STATUS_WARNING;
      $changed = true;
    }

    return $changed;
  }

  /**
   * 
   * @return type
   */
  private static function getLocation() {

    return File::path(APP_PATH, 'storage', self::$location);
  }

  /**
   * 
   * @param type $message
   * @param type $file
   * @param type $clear
   * 
   * @return type
   */
  public static function log($message, $file = "file.txt", $clear = null) {
    
    $log = false;
    $dir = self::getLocation();
    
    $exist = is_dir($dir);
    if (!$exist) {
      $exist = mkdir($dir, 0777, true);
    }
    
    if ($exist) {
      
      $filename = $dir . APP_FS . $file;
      
      if (!self::$activated) {
        $clear = $clear === null ? true : $clear;
        self::$activated = true;
      }
      
      if ($clear) {
        self::clear($filename);
      }
      
      $time = date('d/m/Y H:i:s');
      $contents = "[($time) " .  $message . "\r\n"."-\r\n";

      $log = File::saveFile($filename, $contents, "a+");
    }

    return $log;
  }
  
  /**
   * 
   * @param type $filename
   * 
   * @return type
   */
  public static function clear($filename = "") {

    $dir = self::getLocation();
    $clear = false;

    $exist = is_dir($dir);
    if ($exist) {

      if ($filename === "") {
        $clear = File::delTree($dir);
      } else {
        if (file_exists($filename)) {
          unlink($filename);
        }
      }
    }

    self::$status = null;

    return $clear;
  }

  /**
   * 
   * @param type $clear
   * @param type $translates
   */
  public static function json($clear, $translates) {
    return json_decode(self::get($clear, $translates));
  }

  /**
   * 
   * @param type $message
   * @param type $status
   * 
   * @return type
   */
  public static function add($message, $status = null, $key = null, $description = null) {

    $status = ($status === null) ? Console::$STATUS_FAIL : $status;

    self::changeStatus($status);

    $message = Util::isUTF8($message) ? $message : Json::encodeValue($message);
    $description = Util::isUTF8($description) ? $description : Json::encodeValue($description);

    return self::$messages[] = array("m"=>$message, "s"=>$status, "k" => $key, "d" => $description);
  }

  /**
   * 
   * @param type $clear
   * @param type $translates
   */
  public static function get($translates = null, $clear = null) {
    if (is_null($clear)) {
      $clear = true;
    }

    $messages = array();
    foreach (self::$messages as $message) {
      $messages[] = array(
        "m" => System::replaceMarkup($message["m"], $translates), 
        "s" => $message["s"],
        "k" => $message["k"],
        "d" => $message["d"],
      );
    }

    if ($clear) {
      self::$messages = array();
    }

    return $messages;
  }

  /**
   * 
   * @return type
   */
  public static function status() {
    if (is_null(self::$status)) {
      self::start();
    }

    return self::$status;
  }

  /**
   * 
   * @param $class
   * @param $print
   * 
   * @return string
   */
  public static function methods($class, $print = true) {

    $output = [];
    $class = new ReflectionClass($class);
    $methods = $class->getMethods();
    foreach ($methods as $method) {
      $output[] = ($method->isPublic() ? '+' : '-') . ($method->isStatic() ? '_' : '') . $method->getName() . ' {' . ($method->getNumberOfParameters()) . '}' ;
    }

    $result = '[' . (implode(';', $output)) . ']';

    if ($print) {
      print $result;
    }
    
    return $result;
  }
  
  /**
   * 
   * @param $class
   * @param $method
   * @param $print
   * 
   * @return string
   */
  public static function parameters($class, $method, $print = true) {

    $output = [];
    $reflection = new ReflectionMethod($class, $method);
    $parameters = $reflection->getParameters();
    foreach ($parameters as $parameter) {
      $output[] = $parameter->getName() . ($parameter->isOptional() ? ' [' . ($parameter->getDefaultValue() ? $parameter->getDefaultValue() : 'null') . ']' : '') ;
    }

    $result = '{' . (implode(',', $output)) . '}';

    if ($print) {
      print $result;
    }
    
    return $result;
  }

}

