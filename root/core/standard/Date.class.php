<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Date
 *
 * @author William
 */
class Date {
  //put your code here

  /**
   * converte uma data no padrao mysql para o formato brasileiro
   *
   * @param type $date
   * @param type $hour
   * @param type $all
   *
   * @return string
   */
  public static function mysqlbr($date, $hour = false, $all = true) {
    $brDate = "";
    if (strlen($date) >= 10) {
      $all = $all ? $all : ($date !== '0000-00-00' and $date !== '0000-00-00 00:00:00');
      if ($all) {
        $da = str_replace("/", "-", $date);
        $dat = substr($da, 0, 10); //0000-00-00
        $brDate = explode("-", $dat);
        $brDate = $brDate[2] . "/" . $brDate[1] . "/" . $brDate[0];
        if ($hour) {
          if (strlen($date) >= 16) {
            $brDate = $brDate . "" . substr($date, 10, 6);
          }
        }
      }
    }
    return $brDate;
  }

  /**
   * converte uma data no formato brasileiro para o formato padrao do mysql
   *
   * @param type $date
   * @param type $hour
   * @param type $all
   *
   * @return string
   */
  public static function brmysql($date, $hour = false, $all = true) {
    $mysqlDate = "";
    if (strlen($date) >= 10) {
      $all = $all ? $all : ($date !== '0000-00-00' and $date !== '0000-00-00 00:00:00');
      if ($all) {
        $dt = str_replace("-", "/", $date);
        $dat = substr($dt, 0, 10);
        $mysqlDate = explode("/", $dat);
        $mysqlDate = $mysqlDate[2] . "-" . $mysqlDate[1] . "-" . $mysqlDate[0];
        if ($hour) {
          if (strlen($date) >= 16) {
            $mysqlDate = $mysqlDate . " " . substr($date, 10, 6);
          }
        }
      }
    }

    return $mysqlDate;
  }

  /**
   *
   * @param <type> $date
   * @param <type> $days
   * @param <type> $months
   * @param <type> $years
   *
   * @return <type>
   */
  public static function addDays($date, $days, $months = 0, $years = 0) {
    // dd/mm/YYYY
    $thisyear = substr($date, 6, 4);
    $thismonth = substr($date, 3, 2);
    $thisday = substr($date, 0, 2);
    $nextdate = mktime(0, 0, 0, $thismonth + $months, $thisday + $days, $thisyear + $years);
    return strftime("%d/%m/%Y", $nextdate);
  }

  /**
   * Subtrai dias de uma data<br>
   * Espera receber a data no formato <b>BR</b> (dd/mm/YYYY)<br>
   * Retorna uma data no mesmo formato
   *
   * @param brdate $date
   * @param int $days
   *
   * @return brdate
   */
  public static function subDays($date, $days) {
    // dd/mm/YYYY
    $thisyear = substr($date, 6, 4);
    $thismonth = substr($date, 3, 2);
    $thisday = substr($date, 0, 2);
    $nextdate = mktime(0, 0, 0, $thismonth, $thisday - $days, $thisyear);
    return strftime("%d/%m/%Y", $nextdate);
  }

  /**
   * 
   * @param type $date_1
   * @param type $date_2
   * @param type $type
   * @param type $separator
   * 
   * @return type
   */
  public static function diff($date_1, $date_2, $type = 'D', $separator = '/') {
    
    $diff = null;
    
    if ($separator === '-') {
      $date_1 = self::mysqlbr($date_1);
      $date_2 = self::mysqlbr($date_2);
    }

    if (Util::isDate($date_1) && Util::isDate($date_2)) {

      $date_1 = self::brmysql($date_1);
      $date_2 = self::brmysql($date_2);

      switch ($type) {
        case 'A':
          $X = 31536000;
          break;
        case 'M':
          $X = 2592000;
          break;
        case 'D':
          $X = 86400;
          break;
        case 'H':
          $X = 3600;
          break;
        case 'MI':
          $X = 60;
          break;
        default:
          $X = 1;
      }

      $data_1 = strtotime($date_1);
      $data_2 = strtotime($date_2);
      
      $diff = floor(($data_1 - $data_2) / $X);
    }

    return $diff;
  }

  /**
   * 
   * @param type $date
   * @param type $start
   * @param type $end
   * 
   * @return boolean
   */
  public static function between ($date, $start, $end) {

    $between = false;

    $date = strtotime(Date::brmysql($date));
    $start = strtotime(Date::brmysql($start));
    $end = strtotime(Date::brmysql($end));

    if ( ($date >= $start) && ($date <= $end) ) {
      $between = true;
    }
    
    return $between;
  }
  
  /**
   *
   * @param <type> $mes
   * @param <type> $ano
   * @return <type>
   */
  public static function lastDay($mes, $ano) {
    $mes_ = $mes + 1;

    if (strlen($mes_) == 1) {
      $mes_ = "0" . $mes_;
    } else {
      $mes_ = $mes_;
    }

    $new_data = date("d/m/Y", mktime(0, 0, 0, (int) $mes_, 0, (int) $ano));
    $arr_new_data = explode("/", $new_data);
    return $arr_new_data[0];
  }

  /**
   *
   * @param <type> $data dd/mm/YYYY
   * @return <type> STRING
   *
   */
  public static function clear($data) {
    $data = str_replace("/", "", $data);
    $data = substr($data, 0, 4) . substr($data, 6, 8);
    return $data;
  }

  /**
   * 
   * Método que realiza comparações lógicas entre valores de datas
   * 
   * @param type $date1
   * @param type $date2
   * @param type $operator
   * @param type $days
   * 
   * @return boolean
   */
  public static function compare($date1, $date2, $operator = '==', $days = 0) {
    $date1 = self::brmysql($date1);
    $date2 = self::brmysql($date2);

    $diff = self::diff($date1, $date2);
    $return = false;

    eval("\$return = $diff $operator $days;");

    return $return;
  }

}

?>
