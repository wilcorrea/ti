<?php
/**
 * Description of File
 *
 * @author William
 */
class File {
  //put your code here

  /**
   *
   * Modo Acesso
   * 'r' - Abre somente para leitura; coloca o ponteiro no comeoo do arquivo.
   * 'r+' - Abre para leitura e gravaooo; coloca o ponteiro no comeoo do arquivo.
   * 'w' - Abre somente para gravaooo; coloca o ponteiro no comeoo do arquivo e apaga o conteodo que jo foi escrito. Se o arquivo noo existir, tentar crio-lo.
   * 'w+' - Abre para leitura e escrita; coloca o ponteiro no inocio do arquivo e apaga o conteodo que jo foi escrito. Se o arquivo noo existir, tentar crio-lo.
   * 'a' - Abre o arquivo somente para escrita; coloca o ponteiro no fim do arquivo. Se o arquivo noo existir, tentar crio-lo.
   * 'a+' - Abre o arquivo para leitura e gravaooo; coloca o ponteiro no fim do arquivo. Se o arquivo noo existir, tentar crio-lo.
   *
   * @param <type> $temp_path
   * @param <type> $temp_url
   * @param <type> $nome_arq
   * @param <type> $txt
   * @param <type> $modoAcesso
   */
  public static function saveLinkFile($temp_path, $temp_url, $nome_arq, $txt, $modoAcesso) {

    $filepath = $temp_path . $nome_arq;
    try {
      if (file_exists($filepath)) {
        unlink($filepath);
      }
    } catch (Excpetion $exception) {
      //print $exception;
    }
    $file = fopen($filepath, $modoAcesso); // abre o arquivo
    fwrite($file, $txt); // escreve no arquivo
    fclose($file); // fecha o arquivo
    $url = $temp_path . $nome_arq;
    ?>
    &nbsp;&nbsp;<a href="<?php print APP_PAGE; ?>/download/?file=<?php print $url; ?>" target="_blank"><b><i>Download - <?php print $nome_arq; ?></i></b></a>
    <?php
  }

  /**
   *
   * @param type $filename
   * @param type $contents
   * @param type $mode
   * @param type $encode
   *
   * @return type
   */
  public static function saveFile($filename, $contents, $mode = 'w', $encode = true) {
    $saved = false;

    if (!file_exists(dirname($filename))) {
      mkdir(dirname($filename), 0777, true);
    }

    if (file_exists(dirname($filename))) {

      if (is_writable(dirname($filename))) {
        /**
         *
         * UTF8
         * fwrite($handle, pack("CCC",0xEF,0xBB,0xBF));
         * UTF8
         * fprintf($handle, chr(0xEF).chr(0xBB).chr(0xBF));
         * BIG ENDIAN
         * fprintf($handle, chr(0xFE).chr(0xFF).chr(0xEF).chr(0xBB).chr(0xBF));
         */
        $handle = fopen($filename, $mode);
        if ($handle !== false) {
          if ($encode) {
            $contents = utf8_encode($contents);
          }
          $save = fwrite($handle, $contents);
          if ($save !== false) {
            $saved = true;
          }
          fclose($handle);
        }
      }
    }

    return $saved;
  }

  /**
   * 
   * @param type $id
   * @param type $server
   * @param type $file_local
   * @param type $file_remote
   */
  public static function saveRemoteFile($id, $server, $file_local, $file_remote) {

    $uri = $server . "/api/soap/";
    $location = $uri . "server.php";
    $trace = 1;

    $client = new SoapClient(null, array('location' => $location, 'uri' => $uri, 'trace' => $trace));
    $file_content = file_get_contents($file_local);
    $result = $client->saveFile($id, $file_remote, base64_encode($file_content));
    if (is_soap_fault($result)) {
      $result = $result->faultcode . " - " . $result->faultstring;
    }

    return $result;
  }

  /**
   *
   * @param type $dir
   * @return type
   */
  public static function delTree($dir) {
    $scan = array();
    if (is_dir($dir)) {
      $scan = scandir($dir);
    }

    $files = array_diff($scan, array('.', '..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? self::delTree("$dir/$file") : unlink("$dir/$file");
    }

    // Se o diretorio existir, remove
    if ($scan) {
      return rmdir($dir);
    }

    return false;
  }

  /**
   *
   * @param type $type_content
   * @return type
   */
  public static function infoContent($type_content) {
    $f = explode("|", $type_content);
    $file = array();
    $file['path'] = isset($f[0]) ? $f[0] : PATH_TEMP;
    $file['name'] = isset($f[1]) ? $f[1] : '';
    $file['extensions'] = isset($f[2]) ? $f[2] : '[]';
    $file['width'] = isset($f[3]) ? $f[3] : '100';
    $file['height'] = isset($f[4]) ? $f[4] : '100';
    $file['size'] = isset($f[5]) ? $f[5] : 'null';

    return $file;
  }

  /**
   *
   * @param type $file
   * @return type
   */
  public static function getExtension($file) {
    return pathinfo($file, PATHINFO_EXTENSION);
  }

  /**
   * 
   * @param type $plataform
   * @return string
   */
  public static function getSeparator($plataform = null) {

    if ($plataform === null) {
      $plataform = PHP_OS;
    }

    $separator = ($plataform == "Windows" || $plataform == "WINNT") ? "\\" : "/";

    return $separator;
  }

  /**
   * 
   * @param type $filename
   * @return type
   */
  public static function getParent($filename) {
    return dirname($filename);
  }

  /**
   * 
   * @param type $filename
   * 
   * @return type
   */
  public static function getBase($filename) {
    return basename($filename);
  }

  /**
   * 
   * @param type $filename
   */
  public static function openFile($filename) {
    return file_get_contents($filename);
  }

  /**
   * 
   * @param type $filename
   * @return type
   */
  public static function getName($filename) {
    return pathinfo($filename, PATHINFO_FILENAME);
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function exists($filename) {
    $exists = false;
    if (file_exists($filename)) {
      if (!self::isDirectory($filename)) {
        $exists = true;
      }
    }
    return $exists;
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function isDirectory($filename) {
    return is_dir($filename);
  }

  /**
   * 
   * @return string
   */
  public static function path() {

    $args = func_get_args();
    /*if (count($args)) {
      if ($args[0] === APP_PATH) {
        $args[0] = String::substring(APP_PATH, 0, String::length(APP_PATH) - 1);
      }
    }*/
    $path = implode(APP_FS, $args);
    $path = String::replace($path, APP_FS . APP_FS, APP_FS);

    return $path;
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function removeFile($filename) {

    return self::remove($filename);
  }

  /**
   * 
   * @param string $filename
   * @return boolean
   */
  public static function remove($filename) {

    $remove = false;
    if (self::exists($filename)) {
      $remove = unlink($filename);
    }

    return $remove;
  }

  /**
   * 
   * @param $dir_name
   * @param $path
   */ 
  public static function getFiles($dir_name, $path = "") {
    $files = array();
    $current_path = $path . $dir_name . "/";

    $current_openned = opendir($current_path);
    while (false !== ($item = readdir($current_openned))) {
      if ($item != "." && $item != "..") {

        if (is_dir($current_path . $item)) {
          $f = self::getFiles($item, $current_path);

          $files = array_merge($files, $f);
        } else {
          $files[] = $current_path . $item;
        }
      }
    }

    return $files;
  }

  /**
   * 
   * @param $source
   * @param $dest
   * @return bool
   */ 
  public static function copyFile($source, $dest) {
    return copy($source, $dest);
  }

  /**
   * 
   * @param $directory
   * @param $chmod
   * @param $recursive
   * 
   * @return bool
   */ 
  public static function makeDirectory($directory, $chmod = 0755, $recursive = true) {

    $mk = true;
    if (!self::isDirectory($directory)) {
      $mk = mkdir($directory, $chmod, $recursive);
    }
    return $mk;
  }
  
  /**
   * 
   * @param $filename
   */
  public static function getType($filename) {

    $mime_types = array(
      //text
      'txt' => 'text',
      'htm' => 'text',
      'html' => 'text',
      'php' => 'text',
      'css' => 'text',
      'js' => 'text',
      'json' => 'text',
      'xml' => 'text',
      //flash
      'swf' => 'flash',
      'flv' => 'flash',
      // images
      'png' => 'image',
      'jpe' => 'image',
      'jpeg' => 'image',
      'jpg' => 'image',
      'gif' => 'image',
      'bmp' => 'image',
      'ico' => 'image',
      'tiff' => 'image',
      'tif' => 'image',
      'svg' => 'image',
      'svgz' => 'image',
      // archives
      'zip' => 'compressed',
      'rar' => 'compressed',
      'exe' => 'compressed',
      'msi' => 'compressed',
      'cab' => 'compressed',
      'app' => 'compressed',
      'apk' => 'compressed',
      // audio
      'mp3' => 'audio',
      'ogg' => 'audio',
      //video
      'qt' => 'video',
      'mp4' => 'video',
      'mkv' => 'video',
      'avi' => 'video',
      'mov' => 'video',
      // pdf
      'pdf' => 'pdf',
      // adobe
      'psd' => 'adobe',
      'ai' => 'adobe',
      'eps' => 'adobe',
      'ps' => 'adobe',
      // ms office
      'doc' => 'office',
      'rtf' => 'office',
      'xls' => 'office',
      'ppt' => 'office',
      // open office
      'odt' => 'office',
      'ods' => 'office',
    );
    
    $type = '';

    $e = pathinfo($filename, PATHINFO_EXTENSION);
    $ext = strtolower($e);
    if (array_key_exists($ext, $mime_types)) {

      $type = $mime_types[$ext];
    }

    return $type;
  }

  /**
   *
   * @param type $filename
   * @return string
   */
  public static function getContentType($filename) {

    $mime_types = array(
        'txt' => 'text/plain',
        'htm' => 'text/html',
        'html' => 'text/html',
        'php' => 'text/html',
        'css' => 'text/css',
        'js' => 'application/javascript',
        'json' => 'application/json',
        'xml' => 'application/xml',
        'swf' => 'application/x-shockwave-flash',
        'flv' => 'video/x-flv',
        // images
        'png' => 'image/png',
        'jpe' => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg' => 'image/jpeg',
        'gif' => 'image/gif',
        'bmp' => 'image/bmp',
        'ico' => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif' => 'image/tiff',
        'svg' => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip' => 'application/zip',
        'rar' => 'application/x-rar-compressed',
        'exe' => 'application/x-msdownload',
        'msi' => 'application/x-msdownload',
        'cab' => 'application/vnd.ms-cab-compressed',
        // audio/video
        'mp3' => 'audio/mpeg',
        'qt' => 'video/quicktime',
        'mov' => 'video/quicktime',
        // adobe
        'pdf' => 'application/pdf',
        'psd' => 'image/vnd.adobe.photoshop',
        'ai' => 'application/postscript',
        'eps' => 'application/postscript',
        'ps' => 'application/postscript',
        // ms office
        'doc' => 'application/msword',
        'rtf' => 'application/rtf',
        'xls' => 'application/vnd.ms-excel',
        'ppt' => 'application/vnd.ms-powerpoint',
        // open office
        'odt' => 'application/vnd.oasis.opendocument.text',
        'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
    );

    $e = pathinfo($filename, PATHINFO_EXTENSION);
    $ext = strtolower($e);
    if (array_key_exists($ext, $mime_types)) {
      $return = $mime_types[$ext];
    } elseif (function_exists('finfo_open')) {
      $finfo = finfo_open(FILEINFO_MIME);
      $mimetype = finfo_file($finfo, $filename);
      finfo_close($finfo);
      $return = $mimetype;
    } else {
      $return = 'application/octet-stream';
    }
    
    return $return;
  }
  
}
