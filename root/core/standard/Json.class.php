<?php

/**
 * Description of Json
 *
 * @author William
 */
class Json {

  //put your code here

  /**
   * 
   * @param type $item
   * @param type $encode
   * @return type
   */
  public static function parseValue($item, $encode = true) {

    $type = $item['type'];
    $value = $item['value'];
    $reference = '';

    if ($type == 'option' || $type == 'list') {

      $encode = false;

      $t = $item['type_content'];
      $type_content = explode("|", $t);

      for ($i = 0; $i < count($type_content); $i++) {

        $data = explode(",", $type_content[$i]);

        $_value = $data[0];

        if (isset($data[1])) {
          if ($value == $_value) {
            $value = $data[1];
          }
        }

      }

    } else if ($item['fk']) {

      if (isset($item['reference'])) {
        $reference = $item['reference'];
      }

    }

    if ($encode) {

      $function = '';
      // if (UTF8_SYSTEM === 'encode') {
      //   $function = 'utf8_encode';
      // } else if (UTF8_SYSTEM === 'decode') {
      //   $function = 'utf8_decode';
      // }
      
      if ($function) {
        $value = $function($value);
        $reference = $function($reference);
      }
    }

    $item['value'] = $value;

    if ($reference) {
      $item['reference'] = $reference;
    }

    return $item;
  }

  /**
   *
   * @param type $encoded
   * @return type
   */
  public static function decodeValue($encoded) {
    $decoded = utf8_decode($encoded);
    return $decoded;
  }

  /**
   *
   * @param type $decoded
   * @return type
   */
  public static function encodeValue($decoded) {
    $v = strip_tags($decoded);
    $encoded = utf8_encode($v);
    return $encoded;
  }

  /**
   *
   * @param type $action
   * @return type
   */
  public static function encodeAction($action) {
    $v = str_replace("###", "&nbsp;", $action);
    $value = utf8_encode($v);
    return $value;
  }

  /**
   * 
   * @param type $email
   * @return type
   */
  public static function validateEmail($email) {
    $validate = false;

    $username = EMAIL_VALIDATE_LOGIN;//coordti@fagoc.br
    $password = EMAIL_VALIDATE_PASSWORD;//F4g0k1

    $api_url = EMAIL_VALIDATE_URL . '?';//api.verify-email.org/api.php
    $url = 'http' . '://' . $api_url . 'usr=' . $username . '&pwd=' . $password . '&check=' . $email;

    if (function_exists('curl_init')) {
      $validate = self::getContentsCurl($url);
    } else {
      $validate = file_get_contents($url);
    }

    return $validate;
  }

  /**
   * 
   * @param type $url
   * @return type
   */
  private static function getContentsCurl($url) {
    // Initiate the curl session
    $ch = curl_init();

    // Set the URL
    curl_setopt($ch, CURLOPT_URL, $url);

    // Removes the headers from the output
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // Return the output instead of displaying it directly
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // Execute the curl session
    $output = curl_exec($ch);

    // Close the curl session
    curl_close($ch);

    // Return the output as a variable
    return $output;
  }
    
  /**
   * 
   * @param type $actions
   * @return type
   */
  public static function parseProperties($actions) {
    $properties = "{}";
    
    if (is_array($actions)) {
      $properties = json_encode($actions);
    }
    
    return $properties;
  }

  /**
   * 
   * @param type $value
   * 
   * @return type
   */
  public static function encode($value) {
    return json_encode($value);
  }
  
  /**
   * 
   * @param type $json
   * @param type $array
   * 
   * @return type
   */
  public static function decode($json, $array = false) {

    $type = gettype($json);

    if (in_array($type, ['object', 'array'], true)) {

      $return = $json;
    } else /*if ($type === 'string')*/ {

      $return = json_decode($json, $array);
    }

    return $return;
  }
  
  /**
   * 
   * @param type $reference
   * @param type $message
   * @param type $execute
   * 
   * @return type
   */
  public static function response($reference, $message = "", $execute = "") {
    return self::encode(
      array(
        'reference' => $reference,
        'message' => Util::isUTF8($message) ? $message : self::encodeValue($message),
        'execute' => $execute,
        'status' => Console::status(),
        'log' => Console::get()
      )
    );
  }

}
