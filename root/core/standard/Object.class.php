<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Object
 *
 * @author Analista
 */
class Object {

  private $data = array();

  /**
   * 
   * @param json $elements
   */
  public function Object($elements = null) {

    if (!is_null($elements)) {

      if (is_array($elements)) {
        $elements = Json::encode($elements);
      }

      $data = Json::decode($elements);

      if (is_object($data)) {
        foreach ($data as $key => $value) {
          if (!is_null($key) && !is_null($value)) {
            $this->$key = $value;
          }
        }
      }

    }
    
    return $this;
  }
  
  /**
   * 
   * @param type $name
   * @param type $value
   */
  public function __set($name, $value) {
    $this->data[$name] = $value;
  }

  /**
   * 
   * @param type $name
   * @return type
   */
  public function __get($name) {

    $value = null;
    if (array_key_exists($name, $this->data)) {
      $value = $this->data[$name];
    }

    return $value;
  }

  /**
   * 
   * @param type $name
   * @return type
   */
  public function __isset($name) {
    return isset($this->data[$name]);
  }

  /**
   * 
   * @param type $name
   */
  public function __unset($name) {
    unset($this->data[$name]);
  }

  /**
   * 
   */
  public function toString() {
    return Json::encode($this->data);
  }

}
