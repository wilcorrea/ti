<?php

class Response {

  /**
   * Define se a resposta é valida ou não
   * @var boolean
   */
  public $result;

  /**
   * String com os valores das chaves primárias separadas por vírgula
   * @var boolean
   */
  public $reference;

  public function Response($result, $reference) {
    $this->result = $result;
    $this->reference = $reference;
  }

  /**
   * 
   * @param type $name
   * @param type $value
   */
  public function __set($name, $value) {
    Console::add("Não é possível alterar o valor destas propriedades");

    return false;
  }

}