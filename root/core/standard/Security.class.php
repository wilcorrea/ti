<?php

/**
 *
 * @class Security
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Security {
  
  private static $METHOD = "AES-256-CBC";
  private static $IV = '0df1e73b-812f-9bcf-b07c-e8250e73e748';

  public static $SECURE = false;

  public static $NONCE_FREE = 'FREE';
  public static $NONCE_LOCKED = 'LOCKED';
  public static $NONCE_EXPIRED = 'EXPIRED';


  /**
   * 
   * @param string $string
   * @param string $security
   * 
   * @return string
   */
  public static function decrypt($string, $security = null) {

    $security = $security ? $security : APP_SECURITY;

    $key = hash('sha256', $security);

    $iv = substr(hash('sha256', self::$IV), 0, 16);

    $output = openssl_decrypt(base64_decode($string), self::$METHOD, $key, 0, $iv);

    return $output;
  }

  
  /**
   * 
   * @param string $string
   * @param string $security
   * 
   * @return string
   */
  public static function encrypt($string, $security = null) {

    $security = $security ? $security : APP_SECURITY;

    $key = hash('sha256', $security);

    $iv = substr(hash('sha256', self::$IV), 0, 16);

    $output = openssl_encrypt($string, self::$METHOD, $key, 0, $iv);
    $output = base64_encode($output);

    return $output;
  }
  
  /**
   * 
   * @param string $nonce
   * @param boolean $token
   * 
   * @return string
   */
  public static function createNonce($reference, $token = null) {

    $nonce = String::createGuid();

    $token = $token ? $token : Application::$user->token;

    File::saveFile(self::local($nonce), $reference . ':' . Security::$NONCE_FREE);

    return $nonce;
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return array
   */
  private static function nonce($nonce) {

    $content = '';

    $filename = self::local($nonce);
    if (File::exists($filename)) {

      $content = File::openFile($filename);
    }
    
    return explode(':', $content);
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return boolean
   */
  public static function isNonce($nonce) {
    
    return File::exists(self::local($nonce));
  }

  /**
   * 
   * @param string $nonce
   * @param string $status
   * 
   * @return boolean
   */
  public static function setNonce($nonce, $status = null) {

    $set = false;

    $content = Security::nonce($nonce);

    if ($content) {

      if ($status === null) {

        $status = Security::$NONCE_EXPIRED;
      } else if (is_bool($status)) {

        $status = Security::$NONCE_FREE;
        if ($status === false) {

          $status = Security::$NONCE_LOCKED;
        }
      }
      
      $set = File::saveFile(self::local($nonce), $content[0] . ':' . $status);
    }

    return $set;
  }
  
  /**
   * 
   * @param string $nonce
   * 
   * @return string
   */
  private static function local($nonce) {
    
    return File::path(APP_PATH, 'storage', 'nonce', $nonce);
  }

  /**
   * 
   * @param string $nonce
   */
  public static function setFree($nonce) {

    return Security::setNonce($nonce, Security::$NONCE_FREE);
  }

  /**
   * 
   * @param string $nonce
   */
  public static function setLocked($nonce) {

    return Security::setNonce($nonce, Security::$NONCE_LOCKED);
  }

  /**
   * 
   * @param string $nonce
   */
  public static function setExpired($nonce) {

    return Security::setNonce($nonce, Security::$NONCE_EXPIRED);
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return string
   */
  public static function getNonce($nonce) {

    $content = Security::nonce($nonce);

    return isset($content[1]) ? $content[1] : null;
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return boolean
   */
  public static function isFree($nonce) {

    $status = Security::getNonce($nonce) === Security::$NONCE_FREE;

    if ($status) {
      Security::$SECURE = true;
    }

    return $status;
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return boolean
   */
  public static function isExpired($nonce) {

    $status = Security::getNonce($nonce) === Security::$NONCE_EXPIRED;

    if ($status) {
      Security::$SECURE = true;
    }

    return $status;
  }

  /**
   * 
   * @param string $nonce
   * 
   * @return boolean
   */
  public static function isLocked($nonce) {

    $status = Security::getNonce($nonce) === Security::$NONCE_LOCKED;

    if ($status) {
      Security::$SECURE = true;
    }

    return $status;
  }

  /**
   * 
   * @return boolean
   */
  public static function isSecured() {

    return Security::$SECURE;
  }

}
