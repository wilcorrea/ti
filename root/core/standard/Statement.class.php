<?php
/**
 * @author Pedro Mázala <pedroh.mazala@gmail.com>
 */
class Statement {
  /**
   *
   * @var \Object 
   */
  private $object;
  /**
   *
   * @var string
   */
  private $delimiter;
  /**
   * 
   * @param string $index O índice no qual o statement está salvo
   * @param array $replaces Os dados do replace das marcações
   * @param string $delimiter Define o que deverá ser substituído
   */
  public function Statement($index, $replaces = array(), $delimiter = '?') {
    if (!is_array($replaces)) {
      $replaces = array($replaces);
    }
    $elements = array('index' => $index, 'replaces' => $replaces);
    $this->object = new Object($elements);
    $this->delimiter = $delimiter;
  }
  /**
   * 
   * @param array $statements
   */
  public function replaceMarkup($statements) {
    $index = $this->object->index;
    $where = "";
    if (isset($statements[$index])) {
      $statement = $statements[$index];
      $replaces = $this->object->replaces;
      
      $last = 0;
      $replace_i = 0;
      for ($i = 0; $i < String::length($statement); $i++) {
        if ($statement{$i} === $this->delimiter) {
          if (isset($replaces[$replace_i])) {
            $length = $i - $last;
            $where .= String::substring($statement, $last, $length) . $replaces[$replace_i];
            $last = ($i + 1);
            $replace_i++;
          }
        }
      }

      $where .= String::substring($statement, $last, String::length($statement));

    } else {
      core_err(0, "O índice " . $index . " não foi encontrado na lista de statements oferecida");
    }
    return $where;
  }
  /**
   * 
   * @param mixed $variable
   * @param array $statements
   * 
   * @return string
   */
  public static function parse($variable, $statements) {

    $return = "";

    if (is_a($variable, 'Statement')) {
      $return = $variable->replaceMarkup($statements);
    } else if (Util::isStatement($variable)) {
      $return = self::replace($variable, $statements);
    } else {

      $variable = self::convert($variable);

      if (Util::isStatement($variable)) {
        $return = self::replace($variable, $statements);
      } else {
        $return = $variable;
      }
    }
    return $return;
  }
  /**
   * 
   * @param string $variable
   * 
   * @return string
   */
  public static function convert($statement) {

    $converted = $statement;

    $separator = strrpos($statement, ':');

    if ($separator !== false) {

      $index = substr($statement , 0, $separator);
      $values = substr($statement , ($separator + 1));
      $json = new stdclass();
      $json->$index = explode(',', $values);
      $converted = Json::encode($json);
    }
    return $converted;
  }
  
  /**
   * 
   * @param mixed $where
   * @param string $statements
   * 
   * @return string
   */
  public static function convertWhere($where, $statements) {
    if (!is_array($where)) {
      $where = [$where];
    }
    
    $wheres = [];
    foreach ($where as $w) {
      $wheres[] = Statement::parse($w, $statements);
    }
    
    return implode(' AND ', $wheres);
  }
  
  /**
   * 
   * @param string $variable
   * @param string $statements
   * 
   * @return string
   */
  public static function replace($variable, $statements) {
    
    $return = $variable;
    $std = Json::decode($variable);
    $where = "TRUE";
    if (isset($std->WHERE)) {
      $where = self::convertWhere($std->WHERE, $statements);
      unset($std->WHERE);
    }
    $keys = array_keys((array) $std);
    if (isset($keys[0])) {
      $index = $keys[0];
  
      $replaces = $std->$index;
      $variable = new Statement($index, $replaces);
      $statement = $variable->replaceMarkup($statements);
      $return = ($statement . " AND " . $where);
    }
    return $return;
  }
  /**
   * 
   * @param string $statement
   * @param string $where
   * 
   * @return string
   */
  public static function append($statement, $where) {
    
    if ($where) {
      
      $stt = Statement::convert($statement);
      if (Util::isStatement($stt)) {
        
        $statement = Json::decode($stt);
        $statement->WHERE[] = $where;
      } else {
        
        $statement = ($statement) ? ('(' . $statement . ') AND ' . $where) : ('(TRUE) AND ' . $where);
      }
    }

    return $statement;
  }
}