<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of String
 *
 * @author William
 */
class String {
  //put your code here

  const MAX_SIZE = 32768;

  protected static $self;

  /**
   * Construtor
   *
   */
  public function __construct() {
    mb_internal_encoding('utf-8');
  }

  /**
   * Instancia singleton
   *
   * @version
   *     0.1 18/05/2011 Initial
   *
   * @param string $text Texto a ser verificado
   * @param int $length Tamanho maximo
   */
  public static function instance() {
    if (empty(self::$self)) {
      self::$self = new String;
    }

    return self::$self;
  }

  /**
   * Corta um texto, exibindo somente os $num caracteres do inicio
   *
   * @version
   *     0.1 18/05/2011 Initial
   *
   * @param string $text Texto a ser verificado
   * @param int $length Tamanho maximo
   */
  public static function left($text, $length) {
    return self::substring($text, 0, $length);
  }

  /**
   * Corta um texto, exibindo somente os $num caracteres ao final
   *
   * @version
   *     0.1 18/05/2011 Initial
   *
   * @param string $text Texto a ser verificado
   * @param int $length Tamanho maximo
   */
  public static function right($text, $length) {
    return self::substring($text, strlen($text) - $length, $length);
  }

  /**
   * Corta um texto em um tamanho definido, adicionando reticencias ou outro
   * indicador de continuacao ao final.
   *
   * @version
   *     0.1 Initial
   *     0.2 09/10/2010 Adicionado suporte as funcoes multi-byte.
   *
   * @param string $text Texto a ser verificado
   * @param int $length Tamanho maximo
   * @param string $complement Complemento para o texto cortado. O padrao
   *     e reticencias: "..."
   * @param bool $cut Define se as palavras podem ser cortadas, caso o
   *     tamanho maximo seja atingido no meio de uma delas.
   *
   */
  public function slice($text, $length, $complement = '&hellip;', $cut = false) {
    $newText = self::substringMultiByte($text, 0, $length);

    //Se na string original, o tamanho definido ja representar um espaco,
    //ou seja, o tamanho requerido nao decepara a palavra, ou entao, se
    //e permitido cortar a palavra na metade com $cut = true
    if (self::substringMultiByte($text, $length, 1) == ' ' || $cut === true):
      return $newText . $complement;
    endif;

    if (mb_strlen($text) > $length):
      //O Ultimo espaco encontrado na string
      $lastSpacePos = mb_strrpos($newText, " ");

      //Tem que encontrar pelo menos 1 espaco. Se encontrar, defina o
      //tamanho a ser cortado como sendo a posicao desse espaco
      if ($lastSpacePos !== false):
        $length = $lastSpacePos;
      endif;

      return self::substringMultiByte($newText, 0, $length) . $complement;
    else:
      return $text;
    endif;
  }

  /**
   * Checa se a string passada esta em UTF-8
   *
   * @version
   *     0.1 09/10/2010 Initial
   *
   * @param string $text
   * @return bool TRUE caso a string esteja em utf-8
   */
  public function isUtf8($text) {
    return mb_check_encoding($text, 'utf-8');
  }

  /**
   * 
   * @param type $brackets
   * @return string
   */
  public static function createGuid($brackets = false) {

    mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.

    $charid = self::upper(md5(uniqid(rand(), true)));
    $hyphen = chr(45);
    $uuid = self::substring($charid, 0, 8) . $hyphen . self::substring($charid, 8, 4) . $hyphen . self::substring($charid, 12, 4) . $hyphen . self::substring($charid, 16, 4) . $hyphen . self::substring($charid, 20, 12);
    if ($brackets) {
      $uuid = chr(123) . $uuid . chr(125);
    }

    return $uuid;
  }
  
  /**
   * Checa se a string passada esta em UTF-8
   *
   * @version
   *     0.1 09/10/2010 Initial
   *
   * @param string $text
   * @param mixed $verifyFollowings A lista dos charsets que serao usados
   *     para verificar se a string informada esta em um deles.
   * @return string O nome do charset utilizado na string. A busca e feita
   *     entre os charsets indicados em $verifyFollowings
   */
  public function getCharset($text, $verifyFollowings = array('utf-8', 'iso-8859-1')) {

    //O "x" corrige um bug da funcao "mb_detect_string", que falha caso
    //a Ultima letra da string seja acentuada.
    return mb_detect_encoding($text . 'x', $verifyFollowings);
  }

  /**
   * Converte para utf8 se ainda nao estiver
   *
   * @version
   *     0.1 09/10/2010 Initial
   *
   * @param string $text A string que sera convertida
   * @return String O texto convertido
   */
  public function toUtf8($text) {
    if (!$this->isUtf8($text)):
      return utf8_encode($text);
    endif;

    return $text;
  }

  /**
   * 
   * @param type $string
   * @param type $data
   * 
   * @return type
   */
  public static function insert($string, $data) {
    foreach ($data as $key => $value) {
      $regex = '%(:' . $key . ')%';
      $string = self::replace($string, $regex, $value, true);
    }
    return $string;
  }

  /**
   * 
   * @param type $string
   * 
   * @return array
   */
  public static function extract($string) {
    $extracted = array();
    preg_match_all('%:([a-zA-Z-_]+)%', $string, $extracted);
    return $extracted[1];
  }

  /**
   * 
   * @param type $text
   * @param type $put
   * @param type $at
   * 
   * @return type
   */
  public static function putAt($text, $put, $at) {
    return self::left($text, $at) . $put . self::substringMultiByte($text, $at);
  }

  /**
   * 
   * @param type $text
   * @param type $mask
   * 
   * @return type
   */
  public static function applyMask($text, $mask) {
    $length = self::length($text);
    $buff = '';

    $special = array('/', '.', '-', '_', ' ');

    for ($i = 0, $j = 0; $i < $length; $i++, $j++) {
      if (!isset($text[$i]) || !isset($mask[$j]))
        break;

      if (in_array($mask[$j], $special)) {
        $buff .= $mask[$j];
        $j++;
      }
      $buff .= $text[$i];
    }

    return $buff;
  }

  /**
   * 
   * @param type $needle
   * @param type $haystack
   * 
   * @return type
   */
  public static function highlight($needle, $haystack) {
    $ind = stripos($haystack, $needle);
    $len = self::length($needle);
    if ($ind !== false) {
      return self::substring($haystack, 0, $ind) . '<font style="background-color:yellow">' . self::substring($haystack, $ind, $len) . '</font>' . self::highlight($needle, self::substring($haystack, $ind + $len));
    } else {
      return $haystack;
    }
  }

  /**
   * 
   * @param type $string
   * 
   * @return type
   */
  public static function password($string) {
    $p = sha1($string, true);
    $password = sha1($p);
    return "*" . self::upper($password);
  }

  /**
   * 
   * @param type $prefix
   * 
   * @return string
   */
  public static function captcha($prefix = "") {
    $captcha = "";

    $a = rand(100, 999);
    $b = rand(100, 999);
    $captcha = $prefix . $a . "-" . $b;
    
    Cookie::set('captcha', Encode::encrypt($captcha), 1);
    
    return $captcha;
  }

  /**
   * 
   * @param type $var
   * 
   * @return type
   */
  public static function prevent($var) {
    $v = self::trim($var);
    $var = addslashes($v);

    return $var;
  }
  
  /**
   * 
   * @param string $string
   * @return string
   */
  public static function trim($string) {
    return trim($string);
  }

  /**
   *
   * @param type $text
   * @param type $size
   *
   * @return type
   */
  public static function max($text, $size) {
    $end = ceil($size / 7.5);
    if (self::length($text) >= $end) {
      $text = self::substring($text, 0, $end) . "...";
    }

    return $text;
  }

  /**
   * 
   * @param string $string
   * @return int
   */
  public static function length ($string) {
    return strlen($string);
  }

  /**
   * 
   * @param type $cpf
   * @param type $clear
   * 
   * @return type
   */
  public static function cpf($cpf, $clear = true) {

    if ($clear) {
      $cpf = self::replace($cpf, "/[.-]/", "", true);
    }

    return $cpf;
  }
  
  /**
   * 
   * @param type $cpf
   * 
   * @return boolean
   */
  public static function validCPF($cpf) {
    $d1 = 0;
    $d2 = 0;
    
    $cpf = self::cpf($cpf);
    
    $ignore_list = array(
        '00000000000',
        '01234567890',
        '11111111111',
        '22222222222',
        '33333333333',
        '44444444444',
        '55555555555',
        '66666666666',
        '77777777777',
        '88888888888',
        '99999999999'
    );
    
    if (strlen($cpf) != 11 || in_array($cpf, $ignore_list)) {
      return false;
    } else {
      // inicia o processo para achar o primeiro
      // número verificador usando os primeiros 9 dígitos
      for ($i = 0; $i < 9; $i++) {
        // inicialmente $d1 vale zero e é somando.
        // O loop passa por todos os 9 dígitos iniciais
        $d1 += $cpf[$i] * (10 - $i);
      }
      // acha o resto da divisão da soma acima por 11
      $r1 = $d1 % 11;
      // se $r1 maior que 1 retorna 11 menos $r1 se não
      // retona o valor zero para $d1
      $d1 = ($r1 > 1) ? (11 - $r1) : 0;
      // inicia o processo para achar o segundo
      // número verificador usando os primeiros 9 dígitos
      for ($i = 0; $i < 9; $i++) {
        // inicialmente $d2 vale zero e é somando.
        // O loop passa por todos os 9 dígitos iniciais
        $d2 += $cpf[$i] * (11 - $i);
      }
      // $r2 será o resto da soma do cpf mais $d1 vezes 2
      // dividido por 11
      $r2 = ($d2 + ($d1 * 2)) % 11;
      // se $r2 mair que 1 retorna 11 menos $r2 se não
      // retorna o valor zeroa para $d2
      $d2 = ($r2 > 1) ? (11 - $r2) : 0;
      // retona true se os dois últimos dígitos do cpf
      // forem igual a concatenação de $d1 e $d2 e se não
      // deve retornar false.
      return (substr($cpf, -2) == $d1 . $d2) ? true : false;
    }
  }
  
  public static function validCNPJ($cnpj) {
    //Etapa 1: Cria um array com apenas os digitos numéricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
    $j = 0;
    for ($i = 0; $i < (strlen($cnpj)); $i++) {
      if (is_numeric($cnpj[$i])) {
        $num[$j] = $cnpj[$i];
        $j++;
      }
    }
    //Etapa 2: Conta os dígitos, um Cnpj válido possui 14 dígitos numéricos.
    if (count($num) != 14) {
      $isCnpjValid = false;
    }
    //Etapa 3: O número 00000000000 embora não seja um cnpj real resultaria um cnpj válido após o calculo dos dígitos verificares e por isso precisa ser filtradas nesta etapa.
    if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
      $isCnpjValid = false;
    }
    //Etapa 4: Calcula e compara o primeiro dígito verificador.
    else {
      $j1 = 5;
      for ($i = 0; $i < 4; $i++) {
        $multiplica[$i] = $num[$i] * $j1;
        $j1--;
      }
      #$soma = array_sum($multiplica);
      
      $j2 = 9;
      for ($i = 4; $i < 12; $i++) {
        $multiplica[$i] = $num[$i] * $j2;
        $j2--;
      }
      
      $soma = array_sum($multiplica);
      
      $resto = $soma % 11;
      if ($resto < 2) {
        $dg = 0;
      } else {
        $dg = 11 - $resto;
      }
      if ($dg != $num[12]) {
        $isCnpjValid = false;
      }
    }
    //Etapa 5: Calcula e compara o segundo dígito verificador.
    if (!isset($isCnpjValid)) {
      $j3 = 6;
      for ($i = 0; $i < 5; $i++) {
        $multiplica[$i] = $num[$i] * $j3;
        $j3--;
      }
      #$soma = array_sum($multiplica);
      
      $j4 = 9;
      for ($i = 5; $i < 13; $i++) {
        $multiplica[$i] = $num[$i] * $j4;
        $j4--;
      }
      
      $soma = array_sum($multiplica);
      
      $resto = $soma % 11;
      if ($resto < 2) {
        $dg = 0;
      } else {
        $dg = 11 - $resto;
      }
      if ($dg != $num[13]) {
        $isCnpjValid = false;
      } else {
        $isCnpjValid = true;
      }
    }
    
    return $isCnpjValid;
  }

  /**
   * 
   * @param type $text
   * @param type $all
   * 
   * @return type
   */
  public static function removeSpecial($text, $all = false) {

    $text = Util::isUTF8($text) ? utf8_decode($text) : $text;

    $strings = array();

    $strings[] = array('old'=>"/[àáâäã]/", 'new'=>"a", 'regex'=>true);
    $strings[] = array('old'=>"/[èéêë]/",  'new'=>"e", 'regex'=>true);
    $strings[] = array('old'=>"/[ìíîï]/",  'new'=>"i", 'regex'=>true);
    $strings[] = array('old'=>"/[òóôöõ]/", 'new'=>"o", 'regex'=>true);
    $strings[] = array('old'=>"/[ùúûü]/",  'new'=>"u", 'regex'=>true);
    $strings[] = array('old'=>"/[ÀÁÂÄÃ]/", 'new'=>"A", 'regex'=>true);
    $strings[] = array('old'=>"/[ÈÉÊË]/",  'new'=>"E", 'regex'=>true);
    $strings[] = array('old'=>"/[ÌÍÎÏ]/",  'new'=>"I", 'regex'=>true);
    $strings[] = array('old'=>"/[ÒÓÔÖÕ]/", 'new'=>"O", 'regex'=>true);
    $strings[] = array('old'=>"/[ÙÚÛÜ]/",  'new'=>"U", 'regex'=>true);
    $strings[] = array('old'=>"/[ç]/",     'new'=>"c", 'regex'=>true);
    $strings[] = array('old'=>"/[Ç]/",     'new'=>"C", 'regex'=>true);
    $strings[] = array('old'=>"/[ñ]/",     'new'=>"n", 'regex'=>true);
    $strings[] = array('old'=>"/[Ñ]/",     'new'=>"N", 'regex'=>true);

    $strings[] = array('old'=>"´", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"`", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"^", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"~", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"+", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"|", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"¦", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>"'", 'new'=>"", 'regex'=>false);
    $strings[] = array('old'=>'"', 'new'=>"", 'regex'=>false);

    if ($all) {
      $strings[] = array('old'=>".",  'new'=>"",  'regex'=>false);
      $strings[] = array('old'=>"  ", 'new'=>" ", 'regex'=>false);
      $strings[] = array('old'=>"/",  'new'=>"",  'regex'=>false);
      $strings[] = array('old'=>"(",  'new'=>"",  'regex'=>false);
      $strings[] = array('old'=>")",  'new'=>"",  'regex'=>false);
      $strings[] = array('old'=>"-",  'new'=>"",  'regex'=>false);
    }

    foreach ($strings as $string) {

      $old = Util::isUTF8($string['old']) ? utf8_decode($string['old']) : $string['old'];
      $new = $string['new'];
      $regex = $string['regex'];

      $text = self::replace($text, $old, $new, $regex);
    }

    return $text;
  }
  
  /**
   *
   * @param type $string
   * 
   * @return type
   */
  public static function clear($string) {
    return self::removeSpecial($string, true);
  }

  /**
   * 
   * @param type $text
   * 
   * @return type
   */
  public static function ascii($text) {

    $strings = array();
   
    $strings[] = array('old'=>"Ç", 'new'=>"&#128;", 'regex'=>false);
    $strings[] = array('old'=>"ü", 'new'=>"&#129;", 'regex'=>false);
    $strings[] = array('old'=>"é", 'new'=>"&#130;", 'regex'=>false);
    $strings[] = array('old'=>"â", 'new'=>"&#131;", 'regex'=>false);
    $strings[] = array('old'=>"ä", 'new'=>"&#132;", 'regex'=>false);
    $strings[] = array('old'=>"à", 'new'=>"&#133;", 'regex'=>false);
    $strings[] = array('old'=>"å", 'new'=>"&#134;", 'regex'=>false);
    $strings[] = array('old'=>"ç", 'new'=>"&#135;", 'regex'=>false);
    $strings[] = array('old'=>"ê", 'new'=>"&#136;", 'regex'=>false);
    $strings[] = array('old'=>"ë", 'new'=>"&#137;", 'regex'=>false);
    $strings[] = array('old'=>"è", 'new'=>"&#138;", 'regex'=>false);
    $strings[] = array('old'=>"ï", 'new'=>"&#139;", 'regex'=>false);
    $strings[] = array('old'=>"î", 'new'=>"&#140;", 'regex'=>false);
    $strings[] = array('old'=>"ì", 'new'=>"&#141;", 'regex'=>false);
    $strings[] = array('old'=>"Ä", 'new'=>"&#142;", 'regex'=>false);
    $strings[] = array('old'=>"Å", 'new'=>"&#143;", 'regex'=>false);
    $strings[] = array('old'=>"É", 'new'=>"&#144;", 'regex'=>false);
    $strings[] = array('old'=>"ô", 'new'=>"&#147;", 'regex'=>false);
    $strings[] = array('old'=>"ö", 'new'=>"&#148;", 'regex'=>false);
    $strings[] = array('old'=>"ò", 'new'=>"&#149;", 'regex'=>false);
    $strings[] = array('old'=>"û", 'new'=>"&#150;", 'regex'=>false);
    $strings[] = array('old'=>"ù", 'new'=>"&#151;", 'regex'=>false);
    $strings[] = array('old'=>"ÿ", 'new'=>"&#152;", 'regex'=>false);
    $strings[] = array('old'=>"Ö", 'new'=>"&#153;", 'regex'=>false);
    $strings[] = array('old'=>"Ü", 'new'=>"&#154;", 'regex'=>false);
    $strings[] = array('old'=>"á", 'new'=>"&#160;", 'regex'=>false);
    $strings[] = array('old'=>"í", 'new'=>"&#161;", 'regex'=>false);
    $strings[] = array('old'=>"ó", 'new'=>"&#162;", 'regex'=>false);
    $strings[] = array('old'=>"ú", 'new'=>"&#163;", 'regex'=>false);
    $strings[] = array('old'=>"Á", 'new'=>"&#181;", 'regex'=>false);
    $strings[] = array('old'=>"Â", 'new'=>"&#182;", 'regex'=>false);
    $strings[] = array('old'=>"À", 'new'=>"&#183;", 'regex'=>false);
    $strings[] = array('old'=>"Ó", 'new'=>"&#224;", 'regex'=>false);
    $strings[] = array('old'=>"Ì", 'new'=>"&#222;", 'regex'=>false);
    $strings[] = array('old'=>"Ê", 'new'=>"&#210;", 'regex'=>false);
    $strings[] = array('old'=>"Ë", 'new'=>"&#211;", 'regex'=>false);
    $strings[] = array('old'=>"È", 'new'=>"&#212;", 'regex'=>false);
    $strings[] = array('old'=>"Í", 'new'=>"&#214;", 'regex'=>false);
    $strings[] = array('old'=>"Î", 'new'=>"&#215;", 'regex'=>false);
    $strings[] = array('old'=>"Ï", 'new'=>"&#216;", 'regex'=>false);
    $strings[] = array('old'=>"Ô", 'new'=>"&#226;", 'regex'=>false);
    $strings[] = array('old'=>"Ò", 'new'=>"&#227;", 'regex'=>false);
    $strings[] = array('old'=>"õ", 'new'=>"&#228;", 'regex'=>false);
    $strings[] = array('old'=>"Õ", 'new'=>"&#229;", 'regex'=>false);
    $strings[] = array('old'=>"Ú", 'new'=>"&#233;", 'regex'=>false);
    $strings[] = array('old'=>"Û", 'new'=>"&#234;", 'regex'=>false);
    $strings[] = array('old'=>"Ù", 'new'=>"&#235;", 'regex'=>false);
    $strings[] = array('old'=>"ý", 'new'=>"&#236;", 'regex'=>false);
    $strings[] = array('old'=>"Ý", 'new'=>"&#237;", 'regex'=>false);
    $strings[] = array('old'=>"ã", 'new'=>"&#198;", 'regex'=>false);
    $strings[] = array('old'=>"Ã", 'new'=>"&#199;", 'regex'=>false);

    foreach ($strings as $string) {
      $old = $string['old'];
      $new = $string['new'];
      $regex = $string['regex'];
      $text = self::replace($text, $old, $new, $regex);
    }

    return $text;
  }

  /**
   *
   * @param type $string
   *
   * @return type
   */
  public static function upper($string) {

    $string = strtoupper($string);
    
    $strings = array();

    $strings[] = array('old'=>"ç", 'new'=>"Ç", 'regex'=>false);
    $strings[] = array('old'=>"á", 'new'=>"Á", 'regex'=>false);
    $strings[] = array('old'=>"â", 'new'=>"Â", 'regex'=>false);
    $strings[] = array('old'=>"ã", 'new'=>"Ã", 'regex'=>false);
    $strings[] = array('old'=>"à", 'new'=>"À", 'regex'=>false);
    $strings[] = array('old'=>"é", 'new'=>"É", 'regex'=>false);
    $strings[] = array('old'=>"ê", 'new'=>"Ê", 'regex'=>false);
    $strings[] = array('old'=>"í", 'new'=>"Í", 'regex'=>false);
    $strings[] = array('old'=>"i", 'new'=>"Î", 'regex'=>false);
    $strings[] = array('old'=>"ó", 'new'=>"Ó", 'regex'=>false);
    $strings[] = array('old'=>"ô", 'new'=>"Ô", 'regex'=>false);
    $strings[] = array('old'=>"ú", 'new'=>"Ú", 'regex'=>false);
    $strings[] = array('old'=>"u", 'new'=>"Û", 'regex'=>false);
    $strings[] = array('old'=>"õ", 'new'=>"Õ", 'regex'=>false);

    foreach ($strings as $s) {
      $old = $s['old'];
      $new = $s['new'];
      $regex = $s['regex'];
      $string = self::replace($string, $old, $new, $regex);
    }

    return $string;
  }

  /**
   *
   * @param string $string
   * @return string
   */
  public static function lower($string) {

    $string = strtolower($string);
    
    $strings = array();

    $strings[] = array('old'=>"Ç", 'new'=>"ç", 'regex'=>false);
    $strings[] = array('old'=>"Á", 'new'=>"á", 'regex'=>false);
    $strings[] = array('old'=>"À", 'new'=>"à", 'regex'=>false);
    $strings[] = array('old'=>"Â", 'new'=>"â", 'regex'=>false);
    $strings[] = array('old'=>"Ã", 'new'=>"ã", 'regex'=>false);
    $strings[] = array('old'=>"É", 'new'=>"é", 'regex'=>false);
    $strings[] = array('old'=>"Ê", 'new'=>"ê", 'regex'=>false);
    $strings[] = array('old'=>"Í", 'new'=>"í", 'regex'=>false);
    $strings[] = array('old'=>"Î", 'new'=>"i", 'regex'=>false);
    $strings[] = array('old'=>"Ó", 'new'=>"ó", 'regex'=>false);
    $strings[] = array('old'=>"Ô", 'new'=>"ô", 'regex'=>false);
    $strings[] = array('old'=>"Õ", 'new'=>"õ", 'regex'=>false);
    $strings[] = array('old'=>"Ú", 'new'=>"ú", 'regex'=>false);
    $strings[] = array('old'=>"Û", 'new'=>"u", 'regex'=>false);

    foreach ($strings as $s) {
      $old = $s['old'];
      $new = $s['new'];
      $regex = $s['regex'];
      $string = self::replace($string, $old, $new, $regex);
    }

    return $string;
  }

  /**
   * 
   * @param type $size
   * 
   * @return string
   */
  public static function passwordRandom($size) {
    $caracteres = 'abcdxywz-ABCDZYWZ_0123456789@#$%&*!';
    $max = self::length($caracteres) - 1;
    $password = null;

    for ($i = 0; $i < $size; $i++) {
      $password .= $caracteres{mt_rand(0, $max)};
    }

    return $password;
  }

  /**
   * 
   * @param type $date
   * @param type $week
   * @param type $first
   * 
   * @return string
   */
  public static function dateText($date, $week = true, $first = false) {

    $diasemana = "";
    if ($week) {
      $diasemana = self::dateWeekText(Date::brmysql($date)) . ", ";
    }

    $values = explode("/", $date);
    $text = "";

    if (count($values) > 1) {
      $dia = (int) $values[0];
      if ($first && $dia == 1) {
        $dia = "1°";
      }
      $mes = $values[1];
      $ano = $values[2];
      $text = $diasemana . $dia . " de " . self::monthText($mes) . " de " . $ano;
    } else {
      $text = "data incorreta (" . $date . ")";
    }

    return $text;
  }

  /**
   * 
   * @param type $mes
   * 
   * @return string
   */
  public static function monthText($mes) {
    if ($mes == "01") {
      $mes = "janeiro";
    }
    if ($mes == "02") {
      $mes = "fevereiro";
    }
    if ($mes == "03") {
      $mes = "março";
    }
    if ($mes == "04") {
      $mes = "abril";
    }
    if ($mes == "05") {
      $mes = "maio";
    }
    if ($mes == "06") {
      $mes = "junho";
    }
    if ($mes == "07") {
      $mes = "julho";
    }
    if ($mes == "08") {
      $mes = "agosto";
    }
    if ($mes == "09") {
      $mes = "setembro";
    }
    if ($mes == "10") {
      $mes = "outubro";
    }
    if ($mes == "11") {
      $mes = "novembro";
    }
    if ($mes == "12") {
      $mes = "dezembro";
    }
    return $mes;
  }

  /**
   * 
   * @param type $value
   * 
   * @return string
   */
  public static function booleanText($value) {
    $text = 'false';
    if (Boolean::parse($value)) {
      $text = 'true';
    }
    return $text;
  }

  /**
   * 
   * @param type $valor
   * @param type $maiusculas
   * @param type $tipo
   * 
   * @return type
   */
  public static function moneyText($valor = 0, $maiusculas = false, $tipo = "") {

    if ($tipo == "number") {
      $singular = array("", "", "", "", "", "", "");
      $plural = array("", "", "", "", "", "", "");
    } else if ($tipo == "date") {
      $singular = array("dia", "dia", "dia", "dia", "dia", "dia", "dia");
      $plural = array("dias", "dias", "dias", "dias", "dias", "dias", "dias");
    } else {
      $singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
      $plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões");
    }

    $c = array("", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
    $d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa");
    $d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove");
    $u = array("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove");

    if ($maiusculas) {

      foreach ($singular as $i => $v) {
        $singular[$i] = ucwords($v);
      }

      foreach ($plural as $i => $v) {
        $plural[$i] = ucwords($v);
      }

      foreach ($c as $i => $v) {
        $c[$i] = ucwords($v);
      }

      foreach ($d as $i => $v) {
        $d[$i] = ucwords($v);
      }

      foreach ($d10 as $i => $v) {
        $d10[$i] = ucwords($v);
      }

      foreach ($u as $i => $v) {
        $u[$i] = ucwords($v);
      }
    }

    $z = 0;
    $rt = "";

    $valor = number_format($valor * 1.0, 2, ".", ".");
    $inteiro = explode(".", $valor);
    for ($i = 0; $i < count($inteiro); $i++)
      for ($ii = self::length($inteiro[$i]); $ii < 3; $ii++)
        $inteiro[$i] = "0" . $inteiro[$i];

    $fim = count($inteiro) - ($inteiro[count($inteiro) - 1] > 0 ? 1 : 2);
    for ($i = 0; $i < count($inteiro); $i++) {
      $valor = $inteiro[$i];
      $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
      $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
      $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

      $r = $rc . (($rc && ($rd || $ru)) ? " e " : "") . $rd . (($rd &&
              $ru) ? " e " : "") . $ru;
      $t = count($inteiro) - 1 - $i;
      $r .= $r ? " " . ($valor > 1 ? $plural[$t] : $singular[$t]) : "";
      if ($valor == "000")
        $z++; elseif ($z > 0)
        $z--;
      if (($t == 1) && ($z > 0) && ($inteiro[0] > 0))
        $r .= (($z > 1) ? " de " : "") . $plural[$t];
      if ($r)
        $rt = $rt . ((($i > 0) && ($i <= $fim) &&
                ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
    }

    if (!$maiusculas) {
      return($rt ? $rt : "zero");
    } else {

      //if ($rt)
      //$rt = ereg_replace(" E ", " e ", ucwords($rt));
      //$rt = ucwords($rt);
      return (($rt) ? ($rt) : "Zero");
    }
  }

  /**
   * 
   * @param type $week
   * 
   * @return string
   */
  public static function weekText($week) {
    if ($week == 0) {
      $text = "domingo";
    }
    if ($week == 1) {
      $text = "segunda";
    }
    if ($week == 2) {
      $text = "terça";
    }
    if ($week == 3) {
      $text = "quarta";
    }
    if ($week == 4) {
      $text = "quinta";
    }
    if ($week == 5) {
      $text = "sexta";
    }
    if ($week == 6) {
      $text = "sábado";
    }
    return $text;
  }

  /**
   * 
   * @param type $date
   * @param type $format
   * 
   * @return type
   */
  public static function dateWeekText($date, $format = 'br') {

    $text = "";
    $separator = "/";
    $d = 0;
    $m = 1;
    $y = 2;
    if ($format != 'br') {
      $separator = "-";
      $d = 2;
      $m = 1;
      $y = 0;
    }

    $var = explode($separator, $date);

    $day = "";
    if (isset($var[$d])) {
      $day = $var[$d];
    }
    $month = "";
    if (isset($var[$m])) {
      $month = $var[$m];
    }
    $year = "";
    if (isset($var[$y])) {
      $year = $var[$y];
    }

    if ($month and $day and $year) {
      $week = date("w", mktime(0, 0, 0, $month, $day, $year));
      $text = self::weekText($week);
    }

    return $text;
  }

  /**
   * 
   * @param type $value
   * @param type $size
   * @param type $pad
   * @param type $side
   * 
   * @return type
   */
  public static function fix($value, $size, $pad = '0', $side = 0) {
    if (self::length($value) > $size) {
      $value = self::substring($value, 0, $size);
    }
    $value = str_pad($value, $size, $pad, $side);

    return $value;
  }

  /**
   * 
   * @param string $string
   * @param int $start
   * @param int $length
   * @param string $encoding
   * 
   * @return string
   */
  public static function substringMultiByte($string, $start, $length = null, $encoding = null) {
    return mb_substr($string, $start, $length, $encoding);
  }
  
  /**
   * 
   * @param type $text
   * 
   * @return type
   */
  public static function removeBreakLine($text) {

    $string = self::replace($text, '/\s/', ' ', true);
    $value = self::replace($string, array("<br>", "\n"), "");

    return $value;
  }

  /**
   * Trims text to a space then adds ellipses if desired
   * 
   * @param string $input text to trim
   * @param int $length in characters to trim to
   * @param bool $ellipses if ellipses (...) are to be added
   * @param bool $strip_html if html tags are to be stripped
   * 
   * @return string 
   */
  public static function cutText($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
      $input = strip_tags($input);
    }

    //no need to trim, already shorter than trim length
    if (self::length($input) <= $length) {
      return $input;
    }

    //find last space within length
    $last_space = self::position(self::substring($input, 0, $length), ' ');
    $trimmed_text = self::substring($input, 0, $last_space);

    //add ellipses (...)
    if ($ellipses) {
      $trimmed_text .= ' (...)';
    }

    return $trimmed_text;
  }

  /**
   * 
   * @param string $string
   * @param string $search
   * 
   * @return int
   */
  public static function position ($string, $search) {
    return strpos($string, $search);
  }
  
  /**
   *
   * @param type $val
   * @param type $mask
   * 
   * @return type
   */
  public static function mask($val, $mask) {

    $masked = $val;
    $v = self::clear($val);
    $val = self::replace($v, " ", "");

    $masks = array(
        'cep' => '#####-###',
        'cpf' => '###.###.###-##',
        'cnpj' => '##.###.###/#####-##',
        'tel' => self::substring($val, 0, 4) === '0800' ? '####-###-#####' : (self::length($val) <= 10 ? '(##) ####-####' : '(##) #-####-####'),
        'telefone' => self::substring($val, 0, 4) === '0800' ? '####-###-#####' : (self::length($val) <= 10 ? '(##) ####-####' : '(##) #-####-####')
    );

    if (isset($masks[$mask])) {

      $masked = '';
      $mask = $masks[$mask];

      $k = 0;
      for ($i = 0; $i < self::length($mask); $i++) {
        if ($mask[$i] == '#') {
          if (isset($val[$k]))
            $masked .= $val[$k++];
        } else {
          if (isset($mask[$i]))
            $masked .= $mask[$i];
        }
      }

    }

    return $masked;
  }

  /**
   * 
   * @param type $string
   * @param type $old
   * @param type $new
   * @param type $regex
   * 
   * @return string
   */
  public static function replace($string, $old, $new, $regex = false) {

    if ($regex === false) {
      $replaced = str_replace($old, $new, $string);
    } else {
      $replaced = preg_replace($old, $new, $string);
    }
    
    return $replaced;
  }
  
  /**
   * 
   * @param type $string
   * @param type $start
   * @param type $end
   * 
   * @return string
   */
  public static function substring($string, $start, $length = null) {

    if (is_null($length)) {
      $substring = substr($string, $start);
    } else {
      $substring = substr($string, $start, $length);
    }
    return $substring;
  }

  /**
   * 
   * @param string $boolean
   * 
   * @return string
   */
  public static function stringBoolean($boolean) {

    $string = "NÃO";
    if (Boolean::parse($boolean)) {
      $string = "SIM";
    }

    return $string;
  }

  /**
   * Método para abreviar strings baseado no espaço entre as palavras
   * 
   * @param string $string
   * @param boolean $middle
   * @param string $limit
   * 
   * @return string
   */
  public static function abbreviate($string, $middle = false, $limit = 0) {
    $string = explode(" ", $string);

    $first = array_shift($string);
    $last = array_pop($string);
    $middle_str = " ";
    
    if ($middle) {
      if (count($string)) {
        foreach ($string as $s) {
          $middle_str .= self::substring(self::trim($s), 0, 1) . '. ';
        }
      }
    }
    
    $return = $first . $middle_str .  $last;
    if ($limit > 0) {
      $string = self::fix($return, $limit);
    }
    
    return $return;
  }
  
  /**
   * 
   * @param string $text
   * 
   * @return string $slug
   */
  public static function slugify($text) { 

    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
  
    // trim
    $text = trim($text, '-');
  
    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  
    // lowercase
    $text = strtolower($text);
  
    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);
  
    if (empty($text)) {
      return 'n-a';
    }

    return $text;
  }

}