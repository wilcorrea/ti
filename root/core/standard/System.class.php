<?php

/**
 *
 * ArraySoftware.Net
 * SOMMAR
 * Modulo..........: Geral
 * Funcionalidade..: Registrar constantes para o sistema
 * Pre-requisitos..: Nao possui
 */

class System {

  /**
   *
   * @param type $id
   * @param type $module
   * @param type $entity
   * @param type $path
   * @param type $require
   * @param type $resource
   * @param type $extension
   */
  public static function import($id, $module, $entity, $path = "", $require = true, $resource = "", $extension = 'php') {

    $resource = App::replace($resource, '{project.rsc}', '');

    $layers['c'] = ["root" => "root", "name" => "controller", "sufix" => ".ctrl"];
    $layers['d'] = ["root" => "root", "name" => "dao", "sufix" => ".dao"];
    $layers['m'] = ["root" => "root", "name" => "model", "sufix" => ".class"];
    $layers['p'] = ["root" => "root", "name" => "post", "sufix" => ".post"];
    $layers['v'] = ["root" => "root", "name" => "view", "sufix" => ".view"];
    $layers['j'] = ["root" => "root", "name" => "json", "sufix" => ".json"];
    $layers['s'] = ["root" => "root", "name" => "screen", "sufix" => ".screen"];

    $layers['r'] = ["root" => "root", "name" => "report", "sufix" => ".rep"];
    $layers['f'] = ["root" => "root", "name" => "file", "sufix" => ".fil"];

    $layers['file'] = ["root" => "root", "name" => "", "sufix" => ""];
    $layers['class'] = ["root" => "root", "name" => "", "sufix" => ".class"];

    $layers['theme'] = ["root" => "theme", "name" => "", "sufix" => ".class"];

    $root = $layers[$id]['root'];
    $layer = $layers[$id]['name'];
    $sufix = $layers[$id]['sufix'];

    $file = App::path((App::position($path, APP_PATH) === false ? APP_PATH : ''), $root, $path, $module, $layer, ($entity . $sufix . ($resource ? ('.' . $resource) : ('')) . ('.' . $extension)));

    $include = true;

    if (in_array($id, array("c", "d", "m", "s"))) {
      
      $class = $entity;
      if ($id === "c") {
        $class = $entity . "Ctrl";
      } else if ($id === "d") {
        $class = $entity . "DAO";
      } else if ($id === "s") {
        $class = $entity . "Screen";
      }

      if (class_exists($class)) {
        $include = false;
      }
    }


    if ($include) {
      if ($require) {
        if (App::exists($file)) {
          require_once $file;
        } else {
          App::log("Cannot import '" . $file . "'");
        }
      }
    }

    return $file;
  }

  /**
   *
   * @param <type> $index
   * @param <type> $value
   * @param <type> $encode
   * @return <type>
   */
  public static function request($index, $value = "", $encode = ""){

    if(isset($_GET[$index])){
      $value = $_GET[$index];
    }else if(isset($_POST[$index])){
      $value = $_POST[$index];
    }

    if(!is_null($value) and !empty($value)){

      //$value = self::recursiveCallback($value, 'trim');
      if ($encode) {
        $value = self::recursiveCallback($value, $encode);
      }

      if(gettype($value) == "string"){
        if(UTF8_REQUEST === 'encode'){
          $value = utf8_encode($value);
        }else if(UTF8_REQUEST === 'decode'){
          $value = utf8_decode($value);
        }
      }

      if(HTML_ENTITIES_REQUEST){
        $value = self::recursiveCallback($value, 'htmlentities');
      }

      if(ADD_SLASHES_REQUEST){
        //$value = self::recursiveCallback($value, 'addslashes');
      }

    }

    return $value;
  }
  
  /**
   * captura o ip do cliente
   * @return <type>
   */
  public static function address($all = true) {

    $HTTP_CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : "";

    $HTTP_X_FORWARDED_FOR = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : "";

    $REMOTE_ADDR = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";

    if (PHP_SAPI == 'cli') {

      $address = "localhost";
    } else {

      //check ip from share internet
      if (!empty($HTTP_CLIENT_IP)) {
        $ip = $HTTP_CLIENT_IP;
        //to check ip is pass from proxy
      } else if (!empty($HTTP_X_FORWARDED_FOR)) {
        $ip = $HTTP_X_FORWARDED_FOR;
      } else {
        $ip = $REMOTE_ADDR;
      }
      $address = $ip;
      if ($all) {
        $address = $REMOTE_ADDR . ":" . $ip;
      }
    }

    return $address;
  }

  /**
   * Função recursiva abstrata para utilização em arrays ou strings
   * @param array|string $input
   * @param string $procedure
   * @return array|string
   * @example recursiveCallback($_POST, 'utf8_decode')
   */
  private static function recursiveCallback($input, $procedure) {
    if (!is_array($input)) {
      $output = $procedure($input);
    } else {
      foreach ($input as $key => $value) {
        $output[$key] = $procedure($input[$key]);
      }
    }
    return $output;
  }

  /**
   * 
   * @param type $id
   * 
   * @return string
   */
  public static function getUser($id = 'name') {
    
   return Application::get($id);
  }

}
