
/* global angular, document, window */
'use strict';

var layouts = {/*layout*/};

var onchange = {

  "add": function(operation, model, element, collection, event) {

    switch (element.id) {

      case 'prs_abrange_municipios':

        var field = App.manager.search(operation, 'prs_municipios_abrangidos');
        field.hidden = collection[element.id] === '0' ? true : false;
        break;

      case 'prs_repetir_endereco':

      	var field ;

      	if (collection[element.id] !== 'I') {

      		field = App.manager.search(operation, 'prs_endereco_correspondencia');
      		field.hidden = true;

			field = App.manager.search(operation, 'prs_numero_correspondencia');
      		field.hidden = true;

      	} else {

      		field = App.manager.search(operation, 'prs_endereco_correspondencia');
      		field.hidden = false;

			field = App.manager.search(operation, 'prs_numero_correspondencia');
      		field.hidden = false;
      	}



      	field = App.manager.search(operation, 'prs_numero_correspondencia');


    }
  }, 
  
  "set": function(operation, model, element, collection, event) {

    onchange.add(operation, model, element, collection, event);
  }
};

App
  .registerCtrl('{{entityCtrl}}', new Controller('{{module}}', '{{entity}}', '{{rotule}}', layouts, onchange));
  
  