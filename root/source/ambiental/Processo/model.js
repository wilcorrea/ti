
/* global angular, document, window */
'use strict';

var 

    properties = {/*{{properties}}*/}
  , items = {/*{{items}}*/}
  , operations = {/*{{operations}}*/};

properties.tabs = [];

var separators = [
  {
    label: 'Informações Gerais', 
    items:['prs_codigo', 'prs_descricao' , 'prs_tipo', 'prs_cod_PROJETO']
  },
  {
    label: 'Endereço para envio de correspondência', 
    items:['prs_destinatario', 'prs_destinatario_vinculo', 'prs_repetir_endereco', 'prs_endereco_correspondencia', 'prs_numero_correspondencia', 'prs_cod_CIDADE', 
           'prs_complemento_correspondencia', 'prs_cod_CIDADE', 'prs_uf_correspondencia', 'prs_cep_correspondencia', 'prs_telefone_correspondencia', 'prs_email_correspondencia']
  },
  {
    label: 'Localização do Empreendimento', 
    items:['prs_abrange_municipios', 'prs_municipios_abrangidos', 'prs_abrange_estados', 'prs_estados_abrangidos', 'prs_empreendimento_localizado_uc', 'prs_uc_nome', 
           'prs_empreendimento_localizado_zona_amortecimento', 'prs_zona_amortecimento_nome']
  },
  {
    label: 'Uso de Recurso Hídrico',
    items:['prs_recurso_hidrico', 'prs_recurso_hidrico', 'prs_recurso_hidrico_exclusivo', 'prs_processo_outorga', 'prs_uso_outorgado', 'prs_volume_insignificante', 'prs_recurso_hidrico_coletivo',
           'prs_dac', 'prs_igam', 'prs_outorga', 'prs_renovacao_outorga', 'prs_retificacao_outorga', 'prs_processo_outorga_subitem_OUTORGA', 'prs_uso_outorgado_subitem_RECURSO_HIDRICO_USO', 
           'prs_volume_insignificante_subitem_RECURSO_HIDRICO_USO', 'prs_recurso_hidrico_coletivo_subitem_RECURSO_HIDRICO_USO', 'prs_outorga_subitem_OUTORGA', 'prs_renovacao_outorga_subitem_OUTORGA', 
           'prs_retificacao_outorga_subitem_OUTORGA']
  },
  {
    label: 'Autorização para intervenção ambiental (DAIA) e/ou intervenção em área de preservação permanente (APP) e/ou declaração de colheita e comercialização (DCC)',
    items:['prs_area_rural', 'prs_reserva_legal', 'prs_nova_supressao', 'prs_supressao_vegetacao', 'prs_supressao_vegetacao_tipo', 'prs_vinculo_legal', 'prs_supressao_area_preservacao_permanente', 
           'prs_processo_intervencao_subitem_INTERVENCAO', 'prs_autorizacao_intervencao_subitem_INTERVENCAO']
  },
  {
    label: 'Dados da(s) atividades do empreendimento',
    items:['prs_empreendimento_atividade_fim', 'prs_requerimento_fase', 'prs_inicio_instalacao', 'prs_inicio_operacao', 'prs_apresentacao_requerimento_concomitantemente', 'prs_licenca_ambiental',
           'prs_licenca_ambiental_processo', 'prs_licenca_tipo', 'prs_empreendimento_ampliacao_regularizada', 'prs_lo_certificado', 'prs_numero_autorizacao_ambiental', 'prs_ampliacao_fase', 'prs_inicio_ampliacao_instalacao',
           'prs_inicio_ampliacao_operacao', 'prs_licenca_obrigacoes', 'prs_uso_prerrogativa', 'prs_atividades_empreendimento_subitem_ATIVIDADE', 'prs_outras_atividades_empreendimento_subitem_ATIVIDADE',
           'prs_atividades_empreendimento_ampliacao_subitem_ATIVIDADE', 'prs_atividades_regularizadas_empreendimento_ampliacao_subitem_ATIVIDADE']
  },
  {
    label: 'Forma de Pagamento',
    items:['prs_forma_pagamento']
  }
];

separators.forEach(function(separator) {

  var tab = separator.label;

  properties.tabs.push(tab);

  separator.items.forEach(function(item) {
    items[item].tab = tab;
  });
});


module.exports = new Model('{{module}}', '{{entity}}', properties, items, operations);