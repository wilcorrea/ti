
/* global angular, document, window */
'use strict';

var layouts = {

  manager: {

    operations: {

      "search": function (items, collection) {

        items.men_codigo.readonly = false;

        return items;
      }
    }
  },
  list: {

    operations: {
      
    }
  }
};


App
  .registerCtrl('{{entityCtrl}}', new Controller('{{module}}', '{{entity}}', '{{rotule}}', layouts));
