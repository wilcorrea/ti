
/* global angular, document, window */
'use strict';

var layouts = {/*{{layouts}}*/};

var onchange = {

  add: function(operation, model, element, collection, event) {

    switch (element.id) {

      case 'mef_separator':

        var field = App.manager.search(operation, 'mef_codigo');

        field.hidden = collection[element.id] === '0' ? false : true;
        break;
    }
  }, 
  
  set: function(operation, model, element, collection, event) {

    onchange.add(operation, model, element, collection, event);
  }
};

App
  .registerCtrl('{{entityCtrl}}', new Controller('{{module}}', '{{entity}}', '{{rotule}}', layouts, onchange));
  
  