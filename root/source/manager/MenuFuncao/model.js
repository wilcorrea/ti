
/* global angular, document, window */
'use strict';

var 

    properties = {/*{{properties}}*/}
  , items = {/*{{items}}*/}
  , operations = {/*{{operations}}*/};

properties.tabs = [];

var separators = [
  {
    label: 'Informações', 
    items:['mef_codigo', 'mef_cod_MENU', 'mef_descricao', 'mef_separator', 'mef_order', 'mef_icon']
  },
  {
    label: 'Operação', 
    items:['mef_root', 'mef_module', 'mef_entity', 'mef_action', 'mef_filter']
  },
  {
    label: 'Segurança', 
    items:['mef_ativo', 'mef_permissoes']
  }
];

separators.forEach(function(separator) {

  var tab = separator.label;

  properties.tabs.push(tab);

  separator.items.forEach(function(item) {
    if (items[item]) {
      items[item].tab = tab;
    } else {
      App.error(item);
    }
  });
});


module.exports = new Model('{{module}}', '{{entity}}', properties, items, operations);