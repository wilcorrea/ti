<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 10:16:29
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:47:36
 * @category controller
 * @package ambiental
 */


class ProcessoServicoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:35
   */
  public  function ProcessoServicoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');
    
    $entity = new ProcessoServico();
    $properties = $entity->get_psr_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:36
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:36
   */
  public  function getRowsProcessoServicoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    $items = $processoServico->get_psr_items();
    $properties = $processoServico->get_psr_properties();

    $statements = $processoServico->getStatementsProcessoServico();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:38
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:38
   */
  public  function getProcessoServicoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    $processoServicos = null;

    $rows = $this->getRowsProcessoServicoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $processoServicos = array();
      foreach ($rows as $row) {
        $processoServico_set = clone $processoServico;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $processoServico_set->set_psr_value($key, $item['value'], $reference);
        }
        $processoServico_set->clearProcessoServico($not_clear);
        $processoServicos[] = $processoServico_set;
      }
    }

    return $processoServicos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:39
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:39
   */
  public  function getColumnProcessoServicoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    $properties = $processoServico->get_psr_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $processoServico->getStatementsProcessoServico();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:41
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:41
   */
  public  function getCountProcessoServicoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    $properties = $processoServico->get_psr_properties();

    $statements = $processoServico->getStatementsProcessoServico();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnProcessoServicoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:42
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:42
   */
  private  function beforeProcessoServicoCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsProcessoServicoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(PATH_APP);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_psr_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_psr_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_psr_value($item['id'], $value);
            } else {
              $object->set_psr_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsProcessoServicoCtrl("");
      $properties = $this->getPropertiesProcessoServicoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_psr_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnProcessoServicoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(PATH_APP);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:43
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 26/10/2015 09:43:11
   */
  public  function addProcessoServicoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesProcessoServicoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_psr_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesProcessoServicoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_psr_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setProcessoServicoCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:44
   */
  public  function setProcessoServicoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_psr_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesProcessoServicoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_psr_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:45
   */
  public  function removeProcessoServicoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_psr_items();
    $properties = $this->getPropertiesProcessoServicoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_psr_value($action['field'], $action['value']);
        $object->clearProcessoServico(array($action['field']));

        $remove = $this->setProcessoServicoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:45
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:45
   */
  private  function afterProcessoServicoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_psr_value($object->get_psr_reference());
    } else if ($param === 'add') {
      $object->set_psr_value($object->get_psr_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_psr_items();

      $fields = Controller::after($value, $items, 'psr');
      if (count($fields)) {
        $executed = $this->executeProcessoServicoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_psr_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:46
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:46
   */
  public  function executeProcessoServicoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    $properties = $processoServico->get_psr_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $processoServico->getStatementsProcessoServico();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", psr_responsavel = '" . System::getUser(false) . "', psr_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:47
   */
  private  function verifyProcessoServicoCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_psr_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:47
   */
  public  function getObjectProcessoServicoCtrl($value){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $object = new ProcessoServico();

    $properties = $object->get_psr_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $processoServico = null;

    $processoServicos = $this->getProcessoServicoCtrl($where, "", "", "0", "1");
    if (is_array($processoServicos)) {
      $processoServico = $processoServicos[0];
    }

    return $processoServico;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:48
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:48
   */
  public  function getReplacesProcessoServicoCtrl($reference){
		?><?php

		System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

		$processoServico = new ProcessoServico();

    $replaces = array();

    $where = "psr_codigo = '" . $reference . "'";
    $processoServicos = $this->getProcessoServicoCtrl($where, "", "");
    if (is_array($processoServicos)) {
      $processoServico = $processoServicos[0];

      $items = $processoServico->get_psr_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:48
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:48
   */
  public  function configureProcessoServicoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:49
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:49
   */
  public  function getPropertiesProcessoServicoCtrl(){
    ?><?php

      System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

      $processoServico = new ProcessoServico();

      $properties = $processoServico->get_psr_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:49
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:49
   */
  public  function getItemsProcessoServicoCtrl($search){
    ?><?php

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

    $processoServico = new ProcessoServico();

    if ($search) {
      $object = $this->getObjectProcessoServicoCtrl($search);
      if (!is_null($object)) {
        $processoServico = $object;
      }
    }
    
    $items = $processoServico->get_psr_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $processoServico->get_psr_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:50
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:50
   */
  public  function getHistoryProcessoServicoCtrl($items){
    ?><?php
    
    $defaults = array(
      'psr_registro'=>array('id'=>'psr_registro', 'value'=>date('d/m/Y H:i:s')),
      'psr_criador'=>array('id'=>'psr_criador', 'value'=>System::getUser()),
      'psr_alteracao'=>array('id'=>'psr_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'psr_responsavel'=>array('id'=>'psr_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:50
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:50
   */
  public  function operationProcessoServicoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'ambiental', 'ProcessoServico', 'src', true);

    $processoServico = new ProcessoServico();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $processoServico->get_psr_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $processoServico->set_psr_value($id, $item['value']);
    }
    
    $method = $action . "ProcessoServicoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyProcessoServicoCtrl($processoServico, $operation);
      if ($verify) {

        $before = $this->beforeProcessoServicoCtrl($processoServico, $operation);
        if ($before) {

          $response = $this->$method($processoServico);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterProcessoServicoCtrl($processoServico, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'ProcessoServicoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:52
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:52
   */
  public  function copyProcessoServicoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesProcessoServicoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_psr_value($reference, null);
    }

    return $this->addProcessoServicoCtrl($object, $validate, $debug);
  }


}