<?php
/**
 * @copyright array software
 *
 * @author GENESON COSTA - 24/07/2015 19:15:08
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:24:19
 * @category json
 * @package ambiental
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'chamado-j-list') {
    $type = System::request('type');
    $level = System::request('l');
    $filter = System::request('f');
    $where = System::request('w');
    $group = System::request('g');
    $order = System::request('o');
    $begin = System::request('start', "0");
    $end = System::request('limit');
    $query = System::request('query');
    $code = System::request('code');
    $checkbox = System::request('checkbox', false);
    $readonly = System::request('r', false);

    jsonListChamado($type, $level, $filter, $where, $group, $order, $begin, $end, $query, $code, $checkbox, $readonly);
  }
}

  /**
   * Recupera os dados da entidade convertidos em JSON para interoperabilidade das consultas
   * 
   * @param string $type 
   * @param string $level 
   * @param string $filter 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $begin 
   * @param string $end 
   * @param string $query 
   * @param boolean $code 
   * @param boolean $checkbox 
   * @param boolean $readonly 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:48
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:48
   */
  function jsonListChamado($type, $level, $filter, $where, $group, $order, $begin, $end, $query, $code, $checkbox = false, $readonly = false){
    ?><?php

    $acesso_controle = -1;
    
    require System::import('file', '', 'header', 'core', false);
    
    $rows = array();
    $message = "";
    $total = 0;

    if ($acesso_controle >= 0) {

      System::import('c','ambiental', 'Chamado', 'src', true, '{project.rsc}');

      $chamadoCtrl = new ChamadoCtrl(PATH_APP);

      $items = $chamadoCtrl->getItemsChamadoCtrl("");
      $properties = $chamadoCtrl->getPropertiesChamadoCtrl();

      $reference = $properties['reference'];
      $operations = $properties['operations'];
      $readonly = is_bool($readonly) ? $readonly : $properties['readonly'];
      $saveonly = $properties['saveonly'];

      if ($code) {

        $chamados = $chamadoCtrl->getObjectChamadoCtrl($code);

        $total = count($chamados);
        
        $message = "code: " . $code;

      } else {

        if ($filter) {

          $filter = Encode::decrypt($filter);
          $ws = array();
          $elements = explode(",", $filter);
          foreach ($elements as $fs) {
            $_f = explode(":", $fs);
            $ws[] = $_f[0] . " = '" . $_f[1] . "'";
          }
          $w = implode(" AND ", $ws);
          $where = Encode::decrypt($where);
          $where = ($w && $where) ? ("(" . $w . ") AND (" . $where . ")") : ($where ? $where : $w);
          $where = Encode::encrypt($where);

        }

        $sort = System::request('sort');
        if (is_array($sort)) {

          $os = array();
          foreach ($sort as $k => $fs) {
            $os[] = $k . " " . String::upper($fs);
          }
          $o = implode(",", $os);
          $order = Encode::decrypt($order);
          $order = ($o && $order) ? ($o . "," . $order) : ($order ? $order : $o);
          $order = Encode::encrypt($order);

        }

        $recovereds = array('w'=>Encode::decrypt($where), 'g'=>Encode::decrypt($group), 'o'=>Encode::decrypt($order));
        $recovered = System::recover($items, true, true, true, $query);

        foreach ($recovereds as $r => $value) {
          if ($recovered[$r]) {
            $conector = ($r === 'w') ? "AND" : ",";
            $recovereds[$r] = ($value) ? "(" . $value . ") " . $conector . " (" . $recovered[$r] . ")" : $recovered[$r];
          }
        }

        $total = $chamadoCtrl->getCountChamadoCtrl($recovereds['w']);

        $message = "where: " . $recovereds['w'] . "/" . $query;

        $chamados = $chamadoCtrl->getChamadoCtrl($recovereds['w'], $recovereds['g'], $recovereds['o'], $begin, $end);

      }

      for ($i = 0; $i < count($chamados); $i++) {

        $chamado = $chamados[$i];

        $counter = ($begin + $i) + 1;

        $json['counter'] = $counter;

        if ($checkbox) {

          $show = true;
          if (is_array($checkbox)) {
            $show = $chamado->get_chm_value($checkbox['field']) == $checkbox['value'];
          }

          if ($show) {
            $json['checkbox'] = $chamado->get_chm_value($reference);
          }

        }

        $r = array();
        $references = explode(",", $reference);
        foreach ($references as $rs) {
          $r[] = Encode::encrypt($chamado->get_chm_value($rs));
        }
        $referenced = implode(",", $r);

        $elements = $chamado->get_chm_items();

        $json['commands'] = Json::parseAction($acesso_controle, $type, $elements, $operations, $reference, $referenced, $readonly, $saveonly);

        foreach ($elements as $id => $element) {

          $exibir = $element['grid'];

          if ($exibir) {
            $element = Json::parseValue($element);
            $json[$id] = $element['value'];
            if (isset($element['reference'])) {
              $json[$element['foreign']['description']] = $element['reference'];
            }
          }

        }

        $rows[] = $json;

      }

    } else {
      
      $message = Encode::encrypt(MESSAGE_FORBIDDEN);

    }
    
    print json_encode(
      array(
        "rows" => $rows,
        "total" => $total,
        "begin" => $begin,
        "end" => $end,
        "message" => $message
      )
    );
}

