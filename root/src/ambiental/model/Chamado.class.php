<?php
/**
 * @copyright array software
 *
 * @author GENESON COSTA - 24/07/2015 19:15:08
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:24:19
 * @category model
 * @package ambiental
 */


class Chamado
{
  private  $chm_items = array();
  private  $chm_properties = array();
  private  $chm_parents = array();
  private  $chm_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:26
   */
  public  function Chamado(){
    ?><?php

    $this->chm_items = array();
    
    // Atributos
    $this->chm_items["chm_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"chm_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->chm_items["chm_nome"] = array("pk"=>0, "fk"=>0, "id"=>"chm_nome", "description"=>"Nome / Razão Social", "title"=>"Nome ou Razão Social dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>3, "order"=>3, );
    $this->chm_items["chm_apelido"] = array("pk"=>0, "fk"=>0, "id"=>"chm_apelido", "description"=>"Apelido / Nome Fantasia", "title"=>"Apelido ou Nome Fantasia dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>4, "order"=>4, );
    $this->chm_items["chm_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"chm_tipo", "description"=>"Tipo", "title"=>"", "type"=>"option", "type_content"=>"F,Pessoa Física|J,Jurídica", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>6, "order"=>6, );
    $this->chm_items["chm_cnpj"] = array("pk"=>0, "fk"=>0, "id"=>"chm_cnpj", "description"=>"CNPJ", "title"=>"Utilizado para Pessoa Física", "type"=>"cnpj", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CNP", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>7, "order"=>7, );
    $this->chm_items["chm_cpf"] = array("pk"=>0, "fk"=>0, "id"=>"chm_cpf", "description"=>"CPF", "title"=>"Utilizado para Pessoa Física", "type"=>"cpf", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CPF", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>7, "order"=>7, );
    $this->chm_items["chm_inscricao_estadual"] = array("pk"=>0, "fk"=>0, "id"=>"chm_inscricao_estadual", "description"=>"Inscrição Estadual", "title"=>"Inscrição Estadual", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>7, "order"=>8, );
    $this->chm_items["chm_bairro"] = array("pk"=>0, "fk"=>0, "id"=>"chm_bairro", "description"=>"Bairro / Localidade", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>9, "order"=>9, );
    $this->chm_items["chm_complemento"] = array("pk"=>0, "fk"=>0, "id"=>"chm_complemento", "description"=>"Complemento", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>9, "order"=>9, );
    $this->chm_items["chm_endereco"] = array("pk"=>0, "fk"=>0, "id"=>"chm_endereco", "description"=>"Endereço", "title"=>"Rua, Avenida, Rodovia, etc...", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>8, "order"=>9, );
    $this->chm_items["chm_numero"] = array("pk"=>0, "fk"=>0, "id"=>"chm_numero", "description"=>"Número / km", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>9, "order"=>10, );
    $this->chm_items["chm_cep"] = array("pk"=>0, "fk"=>0, "id"=>"chm_cep", "description"=>"CEP", "title"=>"", "type"=>"cep", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>10, "order"=>10, );
    $this->chm_items["chm_micro_empresa"] = array("pk"=>0, "fk"=>0, "id"=>"chm_micro_empresa", "description"=>"Micro Empresa", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>11, "order"=>11, );
    $this->chm_items["chm_uf"] = array("pk"=>0, "fk"=>0, "id"=>"chm_uf", "description"=>"UF", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>10, "order"=>11, );
    $this->chm_items["chm_telefone"] = array("pk"=>0, "fk"=>0, "id"=>"chm_telefone", "description"=>"Telefone", "title"=>"", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>10, "order"=>12, );
    $this->chm_items["chm_titulo"] = array("pk"=>0, "fk"=>0, "id"=>"chm_titulo", "description"=>"Titulo", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>13, );
    $this->chm_items["chm_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"chm_descricao", "description"=>"Descrição", "title"=>"Descrever os detalhes sobre a ordem de serviço", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>14, );
    $this->chm_items["chm_data_abertura"] = array("pk"=>0, "fk"=>0, "id"=>"chm_data_abertura", "description"=>"Data de abertura", "title"=>"Data da ordem de serviço", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>15, );
    $this->chm_items["chm_data_inicio"] = array("pk"=>0, "fk"=>0, "id"=>"chm_data_inicio", "description"=>"Data de Início", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>16, );
    $this->chm_items["chm_data_finalizacao"] = array("pk"=>0, "fk"=>0, "id"=>"chm_data_finalizacao", "description"=>"Data de Finalização", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>17, );
    $this->chm_items["chm_situacao"] = array("pk"=>0, "fk"=>0, "id"=>"chm_situacao", "description"=>"Situação", "title"=>"", "type"=>"option", "type_content"=>"ABERTO|EM ANALISE|FINALIZADO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"ABERTO", "select"=>1, "update"=>1, "insert"=>1, "line"=>18, "order"=>18, );


    // Atributos FK
    $this->chm_items["chm_cod_CADASTRO_CLIENTE"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_CADASTRO_CLIENTE", "description"=>"Cliente", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cliente", "table"=>"TBL_CADASTRO_CLIENTE", "prefix"=>"cli", "tag"=>"cliente", "key"=>"cli_codigo", "description"=>"cli_descricao", "form"=>"form", "target"=>"div-chm_cod_CADASTRO_CLIENTE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->chm_items["chm_cod_EMPREENDIMENTO"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_EMPREENDIMENTO", "description"=>"Empreendimento", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Empreendimento", "table"=>"TBL_EMPREENDIMENTO", "prefix"=>"emp", "tag"=>"empreendimento", "key"=>"emp_codigo", "description"=>"emp_endereco", "form"=>"form", "target"=>"div-chm_cod_EMPREENDIMENTO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->chm_items["chm_cod_CIDADE"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_CIDADE", "description"=>"Município", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>5, "order"=>5, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cidade", "table"=>"TBL_CIDADE", "prefix"=>"cdd", "tag"=>"cidade", "key"=>"cdd_codigo", "description"=>"cdd_descricao", "form"=>"form", "target"=>"div-chm_cod_CIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->chm_items["chm_cod_CATEGORIA"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_CATEGORIA", "description"=>"Categoria", "title"=>"Categoria do chamado", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>12, "foreign"=>array("modulo"=>"ambiental", "entity"=>"ChamadoCategoria", "table"=>"TBL_CHAMADO_CATEGORIA", "prefix"=>"chc", "tag"=>"chamadocategoria", "key"=>"chc_codigo", "description"=>"chc_descricao", "form"=>"form", "target"=>"div-chm_cod_CATEGORIA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->chm_items['chm_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->chm_items = $this->configureItemsChamado($this->chm_items);

    $join = $this->configureJoinChamado($this->chm_items);

    $lines = 0;
    foreach ($this->chm_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->chm_properties = array(
      'rotule'=>'Chamado',
      'module'=>'ambiental',
      'entity'=>'Chamado',
      'table'=>'TBL_CHAMADO',
      'join'=>$join,
      'tag'=>'chamado',
      'prefix'=>'chm',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'chm_codigo',
      'description'=>'chm_titulo',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>new Object(array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>new Object(array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>new Object(array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"btn-primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>new Object(array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"btn-primary","add"=>"","back"=>""))),
          'find'=>new Object(array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>new Object(array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>new Object(array("action"=>'view', "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"fa-search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>new Object(array("action"=>'set', "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"fa-edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"btn-primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>new Object(array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"fa-trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          'print'=>new Object(array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'refresh'=>new Object(array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>new Object(array("action"=>'list', "label"=>"", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"btn-primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->chm_properties['reference']) {
      foreach ($this->chm_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->chm_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->chm_properties['description']) {
      foreach ($this->chm_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->chm_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsChamado();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:27
   */
  public  function get_chm_properties(){
    ?><?php
    return $this->chm_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:27
   */
  public  function get_chm_items(){
    ?><?php
    return $this->chm_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:27
   */
  public  function get_chm_item($key){
    ?><?php

		$this->validateItemChamado($key);

    return $this->chm_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:27
   */
  public  function get_chm_reference(){
    ?><?php
    $key = $this->chm_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:27
   */
  public  function get_chm_value($key){
    ?><?php

		$this->validateItemChamado($key);

    return $this->chm_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:28
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:28
   */
  public  function set_chm_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemChamado($key);

    $this->chm_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->chm_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:28
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:28
   */
  public  function set_chm_type($key, $type){
    ?><?php

		$this->validateItemChamado($key);

    $this->chm_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:29
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 16/10/2015 10:57:38
   */
  private  function configureJoinChamado($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk'] && $item['select']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:29
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:29
   */
  private  function configureItemsChamado($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->chm_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->chm_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:29
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:29
   */
  public  function clearChamado($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_chm_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_chm_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_chm_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:30
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:30
   */
  private  function validateItemChamado($key){
    ?><?php

    if (!isset($this->chm_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:30
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:30
   */
  public  function setStatementsChamado(){
    ?><?php

    $this->chm_statements["0"] = "chm_codigo = '?'";

    $this->chm_statements["chm_0"] = $this->chm_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:30
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:30
   */
  public  function getStatementsChamado(){
    ?><?php

    return $this->chm_statements;
  }


}