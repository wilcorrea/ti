<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 08/10/2015 11:08:24
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:34:50
 * @category model
 * @package ambiental
 */


class Empreendimento
{
  private  $emp_items = array();
  private  $emp_properties = array();
  private  $emp_parents = array();
  private  $emp_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:01
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 29/10/2015 09:18:14
   */
  public  function Empreendimento(){
    ?><?php

    $this->emp_items = array();
    
    // Atributos
    $this->emp_items["emp_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"emp_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->emp_items["emp_nome"] = array("pk"=>0, "fk"=>0, "id"=>"emp_nome", "description"=>"Nome / Razão Social", "title"=>"Nome ou Razão Social dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->emp_items["emp_apelido"] = array("pk"=>0, "fk"=>0, "id"=>"emp_apelido", "description"=>"Apelido / Nome Fantasia", "title"=>"Apelido ou Nome Fantasia dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->emp_items["emp_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"emp_tipo", "description"=>"Tipo", "title"=>"", "type"=>"option", "type_content"=>"F,Pessoa Física|J,Jurídica", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->emp_items["emp_cnpj"] = array("pk"=>0, "fk"=>0, "id"=>"emp_cnpj", "description"=>"CNPJ", "title"=>"Utilizado para Pessoa Física", "type"=>"cnpj", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CNP", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->emp_items["emp_cpf"] = array("pk"=>0, "fk"=>0, "id"=>"emp_cpf", "description"=>"CPF", "title"=>"Utilizado para Pessoa Física", "type"=>"cpf", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CPF", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->emp_items["emp_inscricao_estadual"] = array("pk"=>0, "fk"=>0, "id"=>"emp_inscricao_estadual", "description"=>"Inscrição Estadual", "title"=>"Inscrição Estadual", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>8, );
    $this->emp_items["emp_bairro"] = array("pk"=>0, "fk"=>0, "id"=>"emp_bairro", "description"=>"Bairro / Localidade", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->emp_items["emp_complemento"] = array("pk"=>0, "fk"=>0, "id"=>"emp_complemento", "description"=>"Complemento", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->emp_items["emp_endereco"] = array("pk"=>0, "fk"=>0, "id"=>"emp_endereco", "description"=>"Endereço", "title"=>"Rua, Avenida, Rodovia, etc...", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>9, );
    $this->emp_items["emp_numero"] = array("pk"=>0, "fk"=>0, "id"=>"emp_numero", "description"=>"Número / km", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>10, );
    $this->emp_items["emp_cep"] = array("pk"=>0, "fk"=>0, "id"=>"emp_cep", "description"=>"CEP", "title"=>"", "type"=>"cep", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>10, );
    $this->emp_items["emp_micro_empresa"] = array("pk"=>0, "fk"=>0, "id"=>"emp_micro_empresa", "description"=>"Micro Empresa", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>11, );
    $this->emp_items["emp_uf"] = array("pk"=>0, "fk"=>0, "id"=>"emp_uf", "description"=>"UF", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>11, );
    $this->emp_items["emp_telefone"] = array("pk"=>0, "fk"=>0, "id"=>"emp_telefone", "description"=>"Telefone", "title"=>"", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>12, );
    $this->emp_items["emp_email"] = array("pk"=>0, "fk"=>0, "id"=>"emp_email", "description"=>"E-mail", "title"=>"", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>13, );


    // Atributos FK
    $this->emp_items["emp_cod_CADASTRO_CLIENTE"] = array("pk"=>0, "fk"=>1, "id"=>"emp_cod_CADASTRO_CLIENTE", "description"=>"Cliente", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cliente", "table"=>"TBL_CADASTRO_CLIENTE", "prefix"=>"cli", "tag"=>"cliente", "key"=>"cli_codigo", "description"=>"cli_descricao", "form"=>"form", "target"=>"div-emp_cod_CADASTRO_CLIENTE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->emp_items["emp_cod_CIDADE"] = array("pk"=>0, "fk"=>1, "id"=>"emp_cod_CIDADE", "description"=>"Município", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cidade", "table"=>"TBL_CIDADE", "prefix"=>"cdd", "tag"=>"cidade", "key"=>"cdd_codigo", "description"=>"cdd_descricao", "form"=>"form", "target"=>"div-emp_cod_CIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->emp_items['emp_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'emp_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emp_items['emp_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'emp_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->emp_items['emp_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'emp_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emp_items['emp_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'emp_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->emp_items = $this->configureItemsEmpreendimento($this->emp_items);

    $join = $this->configureJoinEmpreendimento($this->emp_items);

    $lines = 0;
    foreach ($this->emp_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->emp_properties = array(
      'rotule'=>'Empreendimento',
      'module'=>'ambiental',
      'entity'=>'Empreendimento',
      'table'=>'TBL_EMPREENDIMENTO',
      'join'=>$join,
      'tag'=>'empreendimento',
      'prefix'=>'emp',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'emp_codigo',
      'description'=>'emp_endereco',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object)(array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "icon"=>"", "level"=>1, "popup"=>true, "child"=>false, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object)(array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "icon"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "icon"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "icon"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("get" => 'object', "action"=>'view', "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("get" => 'object', "action"=>'set', "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object)(array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object)(array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object)(array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "icon"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object)(array("get" => 'collection', "action"=>'list', "label"=>"", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "icon"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"danger")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->emp_properties['reference']) {
      foreach ($this->emp_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->emp_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->emp_properties['description']) {
      foreach ($this->emp_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->emp_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsEmpreendimento();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:01
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:01
   */
  public  function get_emp_properties(){
    ?><?php
    return $this->emp_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:01
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:01
   */
  public  function get_emp_items(){
    ?><?php
    return $this->emp_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:01
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:01
   */
  public  function get_emp_item($key){
    ?><?php

		$this->validateItemEmpreendimento($key);

    return $this->emp_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:01
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:01
   */
  public  function get_emp_reference(){
    ?><?php
    $key = $this->emp_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:02
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:02
   */
  public  function get_emp_value($key){
    ?><?php

		$this->validateItemEmpreendimento($key);

    return $this->emp_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:02
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:02
   */
  public  function set_emp_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemEmpreendimento($key);

    $this->emp_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->emp_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:02
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:03
   */
  public  function set_emp_type($key, $type){
    ?><?php

		$this->validateItemEmpreendimento($key);

    $this->emp_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:03
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:03
   */
  private  function configureJoinEmpreendimento($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:03
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:03
   */
  private  function configureItemsEmpreendimento($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->emp_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->emp_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:04
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:04
   */
  public  function clearEmpreendimento($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_emp_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_emp_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_emp_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:04
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:04
   */
  private  function validateItemEmpreendimento($key){
    ?><?php

    if (!isset($this->emp_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:04
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 21/10/2015 10:31:54
   */
  public  function setStatementsEmpreendimento(){
    ?><?php

    $this->emp_statements["0"] = "emp_codigo = '?'";

    $this->emp_statements["emp_0"] = $this->emp_statements["0"];
    //$this->emp_statements["emp_1"] = "emp_codigo = ? AND emp_ativo";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:04
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:05
   */
  public  function getStatementsEmpreendimento(){
    ?><?php

    return $this->emp_statements;
  }


}