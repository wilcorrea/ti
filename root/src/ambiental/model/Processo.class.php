<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 30/10/2015 10:14:56
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:16:46
 * @category model
 * @package ambiental
 */


class Processo
{
  private  $prs_items = array();
  private  $prs_properties = array();
  private  $prs_parents = array();
  private  $prs_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:14
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:14
   */
  public  function Processo(){
    ?><?php

    $this->prs_items = array();
    
    // Atributos
    $this->prs_items["prs_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"prs_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->prs_items["prs_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_descricao", "description"=>"Descrição", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->prs_items["prs_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_tipo", "description"=>"Tipo", "title"=>"Tipo da FCE", "type"=>"option", "type_content"=>"A,Água|I,Atividades Industriais", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->prs_items["prs_repetir_endereco"] = array("pk"=>0, "fk"=>0, "id"=>"prs_repetir_endereco", "description"=>"Endereço para envio de correspondência", "title"=>"Endereço para envio de correspondência", "type"=>"option", "type_content"=>"C,Repetir Endereço Empreendedor|E,Repetir Endereço do Empreendimento|I,Inserir um Endereço", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->prs_items["prs_destinatario"] = array("pk"=>0, "fk"=>0, "id"=>"prs_destinatario", "description"=>"Destinatário", "title"=>"Nome da pessoa que vai receber a correspondência", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->prs_items["prs_destinatario_vinculo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_destinatario_vinculo", "description"=>"Vínculo do Destinatário", "title"=>"Vinculo do destinatário com a empresa", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->prs_items["prs_endereco_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_endereco_correspondencia", "description"=>"Endereço", "title"=>"Rua, Avenida, Etc...", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>8, );
    $this->prs_items["prs_numero_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_numero_correspondencia", "description"=>"Nº / Km", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->prs_items["prs_complemento_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_complemento_correspondencia", "description"=>"Complemento", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>10, );
    $this->prs_items["prs_bairro_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_bairro_correspondencia", "description"=>"Bairro / Localidade", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>11, );
    $this->prs_items["prs_uf_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_uf_correspondencia", "description"=>"UF", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>13, );
    $this->prs_items["prs_cep_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_cep_correspondencia", "description"=>"CEP", "title"=>"", "type"=>"cep", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>14, );
    $this->prs_items["prs_telefone_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_telefone_correspondencia", "description"=>"Telefone", "title"=>"", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>15, );
    $this->prs_items["prs_email_correspondencia"] = array("pk"=>0, "fk"=>0, "id"=>"prs_email_correspondencia", "description"=>"E-mail", "title"=>"", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>16, "order"=>16, );
    $this->prs_items["prs_abrange_municipios"] = array("pk"=>0, "fk"=>0, "id"=>"prs_abrange_municipios", "description"=>"A área do empreendimento abrange outros municípios?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>17, );
    $this->prs_items["prs_municipios_abrangidos"] = array("pk"=>0, "fk"=>0, "id"=>"prs_municipios_abrangidos", "description"=>"Municípios abrangidos", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>18, "order"=>18, );
    $this->prs_items["prs_abrange_estados"] = array("pk"=>0, "fk"=>0, "id"=>"prs_abrange_estados", "description"=>"A área do empreendimento abrange outros estados?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>19, "order"=>19, );
    $this->prs_items["prs_estados_abrangidos"] = array("pk"=>0, "fk"=>0, "id"=>"prs_estados_abrangidos", "description"=>"Estados abrangidos", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>20, "order"=>20, );
    $this->prs_items["prs_empreendimento_localizado_uc"] = array("pk"=>0, "fk"=>0, "id"=>"prs_empreendimento_localizado_uc", "description"=>"O Empreendimento está localizado dentro de Unidade de Conservação (UC) de uso sustentável ou de proteção integral, criada ou implantada, ou em outra área de interesse ambiental legalmente protegida?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>21, "order"=>21, );
    $this->prs_items["prs_uc_nome"] = array("pk"=>0, "fk"=>0, "id"=>"prs_uc_nome", "description"=>"Nome", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>22, "order"=>22, );
    $this->prs_items["prs_empreendimento_localizado_zona_amortecimento"] = array("pk"=>0, "fk"=>0, "id"=>"prs_empreendimento_localizado_zona_amortecimento", "description"=>"O Empreendimento está localizado em sua zona de amortecimento (ou entorno, no raio de 10 Km ao redor da UC), de alguma UC, exceto APA ou RPPN?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>23, "order"=>23, );
    $this->prs_items["prs_zona_amortecimento_nome"] = array("pk"=>0, "fk"=>0, "id"=>"prs_zona_amortecimento_nome", "description"=>"Nome", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>24, "order"=>24, );
    $this->prs_items["prs_recurso_hidrico"] = array("pk"=>0, "fk"=>0, "id"=>"prs_recurso_hidrico", "description"=>"O empreendimento faz uso ou intervenção em recurso hídrico?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>25, "order"=>25, );
    $this->prs_items["prs_recurso_hidrico_exclusivo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_recurso_hidrico_exclusivo", "description"=>"Utilização do Recurso Hídrico é/será exclusiva de Concessionária Local?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>26, "order"=>26, );
    $this->prs_items["prs_processo_outorga"] = array("pk"=>0, "fk"=>0, "id"=>"prs_processo_outorga", "description"=>"Existe Processo de Outorga já solicitado junto ao IGAM (Em análise)", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>27, "order"=>27, );
    $this->prs_items["prs_uso_outorgado"] = array("pk"=>0, "fk"=>0, "id"=>"prs_uso_outorgado", "description"=>"Uso não outorgado (ainda não possui outorga)", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>29, "order"=>29, );
    $this->prs_items["prs_volume_insignificante"] = array("pk"=>0, "fk"=>0, "id"=>"prs_volume_insignificante", "description"=>"Uso de Volume Insignificante?", "title"=>"(Uso de volume Insignificante é definido pela UPGRH em que o empreendimento está localizado.  Informe-se no site do SIAM através DN CERH 09/2004)", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>31, "order"=>31, );
    $this->prs_items["prs_recurso_hidrico_coletivo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_recurso_hidrico_coletivo", "description"=>"Utilização do Recurso Hídrico é ou será Coletiva?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>33, "order"=>33, );
    $this->prs_items["prs_dac"] = array("pk"=>0, "fk"=>0, "id"=>"prs_dac", "description"=>"DAC", "title"=>"(A Declaração de Área de Conflito DAC/IGAM, deverá ser solicitada no IGAM ou  através das  SUPRAM?s)", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>34, "order"=>34, );
    $this->prs_items["prs_igam"] = array("pk"=>0, "fk"=>0, "id"=>"prs_igam", "description"=>"IGAM", "title"=>"(A Declaração de Área de Conflito DAC/IGAM, deverá ser solicitada no IGAM ou  através das  SUPRAM?s)", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>35, "order"=>35, );
    $this->prs_items["prs_outorga"] = array("pk"=>0, "fk"=>0, "id"=>"prs_outorga", "description"=>"Possui Outorga/Certidão de Uso Insignificante? ", "title"=>"(Portaria de Outorga publicada)", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>37, "order"=>37, );
    $this->prs_items["prs_renovacao_outorga"] = array("pk"=>0, "fk"=>0, "id"=>"prs_renovacao_outorga", "description"=>"Trata-se de Revalidação/Renovação de Outorga?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>39, "order"=>39, );
    $this->prs_items["prs_retificacao_outorga"] = array("pk"=>0, "fk"=>0, "id"=>"prs_retificacao_outorga", "description"=>"Trata-se de Retificação de portaria de Outorga?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>41, "order"=>41, );
    $this->prs_items["prs_area_rural"] = array("pk"=>0, "fk"=>0, "id"=>"prs_area_rural", "description"=>"O Empreendimento está localizado em área rural? ", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>45, "order"=>45, );
    $this->prs_items["prs_reserva_legal"] = array("pk"=>0, "fk"=>0, "id"=>"prs_reserva_legal", "description"=>"A propriedade possui regularização de reserva legal (Termo de Compromisso/IEF ou Averbação)?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>46, "order"=>46, );
    $this->prs_items["prs_nova_supressao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_nova_supressao", "description"=>"Haverá necessidade de nova supressão/intervenção neste empreendimento, além dos itens relacionados nas perguntas 6.1 e 6.2 ?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>47, "order"=>47, );
    $this->prs_items["prs_supressao_vegetacao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_supressao_vegetacao", "description"=>"Ocorrerá supressão de vegetação?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>48, "order"=>48, );
    $this->prs_items["prs_supressao_vegetacao_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_supressao_vegetacao_tipo", "description"=>"Informar qual supressão de vegetação ocorrerá", "title"=>"", "type"=>"option", "type_content"=>"N,Nativa|P,Plantada|NP,Nativa e Plantada", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>49, "order"=>49, );
    $this->prs_items["prs_vinculo_legal"] = array("pk"=>0, "fk"=>0, "id"=>"prs_vinculo_legal", "description"=>"É vinculada, legal ou contratualmente, as empresas consumidoras de produtos florestais?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>50, "order"=>50, );
    $this->prs_items["prs_supressao_area_preservacao_permanente"] = array("pk"=>0, "fk"=>0, "id"=>"prs_supressao_area_preservacao_permanente", "description"=>"Ocorrerá supressão/intervenção em Área de Preservação Permanente (APP)? ", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>51, "order"=>51, );
    $this->prs_items["prs_empreendimento_atividade_fim"] = array("pk"=>0, "fk"=>0, "id"=>"prs_empreendimento_atividade_fim", "description"=>"Descreva sucintamente a atividade fim do empreendimento ? atual e futura", "title"=>"*Informar SOMENTE os dados referentes às alterações (ampliação ou modificação) das atividades já licenciadas. Lembrando ainda que as novas atividades desenvolvidas nesta propriedade, e ainda não licenciadas, deverão ser listadas.", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>52, "order"=>52, );
    $this->prs_items["prs_requerimento_fase"] = array("pk"=>0, "fk"=>0, "id"=>"prs_requerimento_fase", "description"=>"Fase do objeto do requerimento", "title"=>"", "type"=>"option", "type_content"=>"P,Projeto|I,Instalação|O,Operação", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>55, "order"=>55, );
    $this->prs_items["prs_inicio_instalacao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_inicio_instalacao", "description"=>"Instalação iniciada em", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>56, "order"=>56, );
    $this->prs_items["prs_inicio_operacao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_inicio_operacao", "description"=>"Operação desde", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>57, "order"=>57, );
    $this->prs_items["prs_apresentacao_requerimento_concomitantemente"] = array("pk"=>0, "fk"=>0, "id"=>"prs_apresentacao_requerimento_concomitantemente", "description"=>"Pretende apresentar requerimento de LP e de LI concomitantemente?", "title"=>"(somente para classes 3 e 4, em fase de projeto)", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>58, "order"=>58, );
    $this->prs_items["prs_licenca_ambiental"] = array("pk"=>0, "fk"=>0, "id"=>"prs_licenca_ambiental", "description"=>"O empreendimento já tem licença ambiental / autorização de funcionamento emitida pelo órgão estadual?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>59, "order"=>59, );
    $this->prs_items["prs_licenca_ambiental_processo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_licenca_ambiental_processo", "description"=>"informe nº do Processo COPAM", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>60, "order"=>60, );
    $this->prs_items["prs_licenca_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"prs_licenca_tipo", "description"=>"Tipo", "title"=>"", "type"=>"option", "type_content"=>"AAF|LP|LP/LI|LI|LIC|LO|LOC|REVLO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>61, "order"=>61, );
    $this->prs_items["prs_empreendimento_ampliacao_regularizada"] = array("pk"=>0, "fk"=>0, "id"=>"prs_empreendimento_ampliacao_regularizada", "description"=>"Ampliação ou modificação de empreendimento já regularizado ambientalmente?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>62, "order"=>62, );
    $this->prs_items["prs_lo_certificado"] = array("pk"=>0, "fk"=>0, "id"=>"prs_lo_certificado", "description"=>"Certificado de LO", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>63, "order"=>63, );
    $this->prs_items["prs_numero_autorizacao_ambiental"] = array("pk"=>0, "fk"=>0, "id"=>"prs_numero_autorizacao_ambiental", "description"=>"Autorização Ambiental de Funcionamento nº", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>64, "order"=>64, );
    $this->prs_items["prs_ampliacao_fase"] = array("pk"=>0, "fk"=>0, "id"=>"prs_ampliacao_fase", "description"=>"Fase atual da ampliação", "title"=>"", "type"=>"option", "type_content"=>"P,Projeto|I,Instalação|O,Operação", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>65, "order"=>65, );
    $this->prs_items["prs_inicio_ampliacao_instalacao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_inicio_ampliacao_instalacao", "description"=>"Instalação iniciada em", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>66, "order"=>66, );
    $this->prs_items["prs_inicio_ampliacao_operacao"] = array("pk"=>0, "fk"=>0, "id"=>"prs_inicio_ampliacao_operacao", "description"=>"Operação desde", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>67, "order"=>67, );
    $this->prs_items["prs_licenca_obrigacoes"] = array("pk"=>0, "fk"=>0, "id"=>"prs_licenca_obrigacoes", "description"=>"Está cumprindo as obrigações inerentes à licença vigente, inclusive suas condicionantes?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>70, "order"=>70, );
    $this->prs_items["prs_uso_prerrogativa"] = array("pk"=>0, "fk"=>0, "id"=>"prs_uso_prerrogativa", "description"=>"Quer fazer uso da prerrogativa do § 2o, art. 8o da DN 74/2004 (redução de 30% no custo de análise)?", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>71, "order"=>71, );
    $this->prs_items["prs_forma_pagamento"] = array("pk"=>0, "fk"=>0, "id"=>"prs_forma_pagamento", "description"=>"Selecione uma opção de Pagamento, tendo como referência a tabela anexa  na RESOLUÇÃO CONJUNTA SEMAD / IEF / FEAM Nº 1919 DE 17 DE SETEMBRO DE 2013.", "title"=>"Nota 1: Ficam sujeitas ao pagamento integral do valor da tabela, as classes I e II referente a Autorização Ambiental de Funcionamento- AAF, não cabendo parcelamento vez que não atingem o valor mínimo de R$ 1.000,00( hum mil reais) exigido para parcelamento.
Nota 2:  Em qualquer das situações acima, ficam o julgamento e a emissão da Licença condicionados à quitação integral dos custos, conforme art. 7º, da DN COPAM n.º 74/2004
Nota 3: Os valores eventualmente pagos a maior em relação ao custo apurado na apresentação da Planilha referente a LP, LI e LO, classes III e IV , na hipótese das opções 8.1 e 8.2,  serão ressarcidos ao empreendedor, desde que esses valores não sejam inferiores a 30% da tabela. 
Nota 4: PAGUE O PRIMEIRO DAE (DE 30 %) SOMENTE APÓS REUNIR TODA A DOCUMENTAÇÃO EXIGIDA, PARA EVITAR TER DE SOLICITAR O RESSARCIMENTO, CASO NÃO FORMALIZE O PROCESSO ATÉ DATA DE VALIDADE DO FOBI.", "type"=>"option", "type_content"=>"1, No ato da Formalização do processo, pagar o valor integral da tabela, e caso os custos apurados na planilha sejam superiores, pagar a diferença antes do julgamento| 2, No ato da Formalização do processo, pagar 30% do valor da tabela e o restante em até 5 (cinco) parcelas mensais e consecutivas, não inferiores a R$ 1.000,00 (hum mil Reais) cada, e caso os custos apurados na planilha sejam superiores, pagar a diferença antes do julgamento obs: incidirá juros de mora de 1% (um por cento) ao mês e multa de 2% (dois por cento) do valor das parcelas pagas após o vencimento| 3, No ato da Formalização do processo, pagar 30% do valor da tabela e o restante de forma integral após a apresentação da planilha de custos", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>72, "order"=>72, );


    // Atributos FK
    $this->prs_items["prs_cod_PROJETO"] = array("pk"=>0, "fk"=>1, "id"=>"prs_cod_PROJETO", "description"=>"Projeto", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Projeto", "table"=>"TBL_PROJETO", "prefix"=>"prj", "tag"=>"projeto", "key"=>"prj_codigo", "description"=>"prj_descricao", "form"=>"form", "target"=>"div-prs_cod_PROJETO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_cod_CIDADE"] = array("pk"=>0, "fk"=>1, "id"=>"prs_cod_CIDADE", "description"=>"Município", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>12, );
    $this->prs_items["prs_processo_outorga_subitem_OUTORGA"] = array("pk"=>0, "fk"=>0, "id"=>"prs_processo_outorga_subitem_OUTORGA", "description"=>"Processo de Outorga", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>28, "order"=>28, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Outorga", "table"=>"TBL_OUTORGA", "prefix"=>"out", "tag"=>"outorga", "key"=>"out_cod_PROCESSO", "description"=>"out_portaria", "form"=>"form", "target"=>"div-prs_processo_outorga_subitem_OUTORGA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_uso_outorgado_subitem_RECURSO_HIDRICO_USO"] = array("pk"=>0, "fk"=>0, "id"=>"prs_uso_outorgado_subitem_RECURSO_HIDRICO_USO", "description"=>"Uso não outorgado", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>30, "order"=>30, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Outorga", "table"=>"TBL_OUTORGA", "prefix"=>"out", "tag"=>"outorga", "key"=>"out_cod_PROCESSO", "description"=>"out_portaria", "form"=>"form", "target"=>"div-prs_uso_outorgado_subitem_RECURSO_HIDRICO_USO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_volume_insignificante_subitem_RECURSO_HIDRICO_USO"] = array("pk"=>0, "fk"=>0, "id"=>"prs_volume_insignificante_subitem_RECURSO_HIDRICO_USO", "description"=>"Uso de Volume Insignificante", "title"=>"(Uso de volume Insignificante é definido pela UPGRH em que o empreendimento está localizado.  Informe-se no site do SIAM através DN CERH 09/2004):", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>32, "order"=>32, "foreign"=>array("modulo"=>"ambiental", "entity"=>"RecursoHidricoUso", "table"=>"TBL_RECURSO_HIDRICO_USO", "prefix"=>"rhu", "tag"=>"recursohidricouso", "key"=>"rhu_cod_PROCESSO", "description"=>"rhu_uso_codigo", "form"=>"form", "target"=>"div-prs_volume_insignificante_subitem_RECURSO_HIDRICO_USO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_recurso_hidrico_coletivo_subitem_RECURSO_HIDRICO_USO"] = array("pk"=>0, "fk"=>0, "id"=>"prs_recurso_hidrico_coletivo_subitem_RECURSO_HIDRICO_USO", "description"=>"Utilização Coletiva do Recurso Hídrico", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>36, "order"=>36, "foreign"=>array("modulo"=>"ambiental", "entity"=>"RecursoHidricoUso", "table"=>"TBL_RECURSO_HIDRICO_USO", "prefix"=>"rhu", "tag"=>"recursohidricouso", "key"=>"rhu_cod_PROCESSO", "description"=>"rhu_uso_codigo", "form"=>"form", "target"=>"div-prs_recurso_hidrico_coletivo_subitem_RECURSO_HIDRICO_USO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_outorga_subitem_OUTORGA"] = array("pk"=>0, "fk"=>0, "id"=>"prs_outorga_subitem_OUTORGA", "description"=>"Outorga / Certidão de Uso", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>38, "order"=>38, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Outorga", "table"=>"TBL_OUTORGA", "prefix"=>"out", "tag"=>"outorga", "key"=>"out_cod_PROCESSO", "description"=>"out_portaria", "form"=>"form", "target"=>"div-prs_outorga_subitem_OUTORGA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_renovacao_outorga_subitem_OUTORGA"] = array("pk"=>0, "fk"=>0, "id"=>"prs_renovacao_outorga_subitem_OUTORGA", "description"=>"Outorga a ser Revalidada / Renovada", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>40, "order"=>40, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Outorga", "table"=>"TBL_OUTORGA", "prefix"=>"out", "tag"=>"outorga", "key"=>"out_cod_PROCESSO", "description"=>"out_portaria", "form"=>"form", "target"=>"div-prs_renovacao_outorga_subitem_OUTORGA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_retificacao_outorga_subitem_OUTORGA"] = array("pk"=>0, "fk"=>0, "id"=>"prs_retificacao_outorga_subitem_OUTORGA", "description"=>"Outorga a ser retificada", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>42, "order"=>42, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Outorga", "table"=>"TBL_OUTORGA", "prefix"=>"out", "tag"=>"outorga", "key"=>"out_cod_PROCESSO", "description"=>"out_portaria", "form"=>"form", "target"=>"div-prs_retificacao_outorga_subitem_OUTORGA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_processo_intervencao_subitem_INTERVENCAO"] = array("pk"=>0, "fk"=>0, "id"=>"prs_processo_intervencao_subitem_INTERVENCAO", "description"=>"Caso já tenha processo de intervenção ambiental ou de intervenção em APP ou pedido de Declaração de Colheita e Comercialização - DCC (protocolados e/ou em análise no IEF) referente a esse empreendimento informar o (s) número (s):", "title"=>"Caso já tenha processo de intervenção ambiental ou de intervenção em APP ou pedido de Declaração de Colheita e Comercialização - DCC (protocolados e/ou em análise no IEF) referente a esse empreendimento informar o (s) número (s):", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>43, "order"=>43, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Intervencao", "table"=>"TBL_INTERVENCAO", "prefix"=>"itv", "tag"=>"intervencao", "key"=>"itv_cod_PROCESSO", "description"=>"itv_numero_autorizacao", "form"=>"form", "target"=>"div-prs_processo_intervencao_subitem_INTERVENCAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_autorizacao_intervencao_subitem_INTERVENCAO"] = array("pk"=>0, "fk"=>0, "id"=>"prs_autorizacao_intervencao_subitem_INTERVENCAO", "description"=>"Caso já tenha Autorização para Intervenção Ambiental - DAIA ou Declaração de Colheita e Comercialização - DCC liberada para esse empreendimento informar o (s) número (s)", "title"=>"Caso já tenha Autorização para Intervenção Ambiental - DAIA ou Declaração de Colheita e Comercialização - DCC liberada para esse empreendimento informar o (s) número (s)", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>44, "order"=>44, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Intervencao", "table"=>"TBL_INTERVENCAO", "prefix"=>"itv", "tag"=>"intervencao", "key"=>"itv_cod_PROCESSO", "description"=>"itv_numero_autorizacao", "form"=>"form", "target"=>"div-prs_autorizacao_intervencao_subitem_INTERVENCAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_atividades_empreendimento_subitem_ATIVIDADE"] = array("pk"=>0, "fk"=>0, "id"=>"prs_atividades_empreendimento_subitem_ATIVIDADE", "description"=>"Obs: Em caso de dúvida sobre o código a ser informado no campo abaixo, não preencher e entrar em contato com o Órgão Ambiental competente, para esclarecimentos. Os códigos das atividades estão listados no anexo 1 da  Deliberação Normativa  -  74/04,  disp", "title"=>"Os códigos das atividades estão listados no anexo 1 da  Deliberação Normativa  -  74/04,  disponível para consulta no site: www.siam.mg.gov.br  ", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>53, "order"=>53, "foreign"=>array("modulo"=>"ambiental", "entity"=>"EmpreendimentoAtividade", "table"=>"TBL_EMPREENDIMENTO_ATIVIDADE", "prefix"=>"ema", "tag"=>"empreendimentoatividade", "key"=>"ema_cod_PROCESSO", "description"=>"ema_codigo_atividade", "form"=>"form", "target"=>"div-prs_atividades_empreendimento_subitem_ATIVIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_outras_atividades_empreendimento_subitem_ATIVIDADE"] = array("pk"=>0, "fk"=>0, "id"=>"prs_outras_atividades_empreendimento_subitem_ATIVIDADE", "description"=>"Outras atividades listadas na DN 74/2004, nesse empreendimento, caso haja, informe:", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>54, "order"=>54, "foreign"=>array("modulo"=>"ambiental", "entity"=>"EmpreendimentoAtividade", "table"=>"TBL_EMPREENDIMENTO_ATIVIDADE", "prefix"=>"ema", "tag"=>"empreendimentoatividade", "key"=>"ema_cod_PROCESSO", "description"=>"ema_codigo_atividade", "form"=>"form", "target"=>"div-prs_outras_atividades_empreendimento_subitem_ATIVIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_atividades_empreendimento_ampliacao_subitem_ATIVIDADE"] = array("pk"=>0, "fk"=>0, "id"=>"prs_atividades_empreendimento_ampliacao_subitem_ATIVIDADE", "description"=>"Dados referentes à ampliação:", "title"=>"", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>68, "order"=>68, "foreign"=>array("modulo"=>"ambiental", "entity"=>"EmpreendimentoAtividade", "table"=>"TBL_EMPREENDIMENTO_ATIVIDADE", "prefix"=>"ema", "tag"=>"empreendimentoatividade", "key"=>"ema_cod_PROCESSO", "description"=>"ema_codigo_atividade", "form"=>"form", "target"=>"div-prs_atividades_empreendimento_ampliacao_subitem_ATIVIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->prs_items["prs_atividades_regularizadas_empreendimento_ampliacao_subitem_ATIVIDADE"] = array("pk"=>0, "fk"=>0, "id"=>"prs_atividades_regularizadas_empreendimento_ampliacao_subitem_ATIVIDADE", "description"=>"Dados da atividade principal do empreendimento já regularizada ambientalmente relacionada à ampliação", "title"=>"*Informar SOMENTE a unidade de medida específica para cada uma da(s) atividade(s), conforme Anexo I da DN COPAM 74/04", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>69, "order"=>69, "foreign"=>array("modulo"=>"ambiental", "entity"=>"EmpreendimentoAtividade", "table"=>"TBL_EMPREENDIMENTO_ATIVIDADE", "prefix"=>"ema", "tag"=>"empreendimentoatividade", "key"=>"ema_cod_PROCESSO", "description"=>"ema_codigo_atividade", "form"=>"form", "target"=>"div-prs_atividades_regularizadas_empreendimento_ampliacao_subitem_ATIVIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->prs_items['prs_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'prs_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->prs_items['prs_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'prs_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->prs_items['prs_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'prs_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->prs_items['prs_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'prs_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->prs_items = $this->configureItemsProcesso($this->prs_items);

    $join = $this->configureJoinProcesso($this->prs_items);

    $lines = 0;
    foreach ($this->prs_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->prs_properties = array(
      'rotule'=>'Processo',
      'module'=>'ambiental',
      'entity'=>'Processo',
      'table'=>'TBL_PROCESSO',
      'join'=>$join,
      'tag'=>'processo',
      'prefix'=>'prs',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'prs_codigo',
      'description'=>'prs_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("id"=>"save", "action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("id"=>"copy", "action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("id"=>"add", "action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("id"=>"search", "action"=>'search', "get"=>"search", "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("id"=>"find", "action"=>'list', "get"=>"search", "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("id"=>"back", "action"=>'list', "get"=>"clear", "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("id"=>"view", "action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("id"=>"set", "action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("id"=>"remove", "action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("id"=>"list", "action"=>'list', "get"=>"collection", "label"=>"Processo", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"primary")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->prs_properties['reference']) {
      foreach ($this->prs_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->prs_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->prs_properties['description']) {
      foreach ($this->prs_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->prs_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsProcesso();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:14
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:14
   */
  public  function get_prs_properties(){
    ?><?php
    return $this->prs_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:14
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:14
   */
  public  function get_prs_items(){
    ?><?php
    return $this->prs_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:14
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:14
   */
  public  function get_prs_item($key){
    ?><?php

		$this->validateItemProcesso($key);

    return $this->prs_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:14
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:15
   */
  public  function get_prs_reference(){
    ?><?php
    $key = $this->prs_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:15
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:15
   */
  public  function get_prs_value($key){
    ?><?php

		$this->validateItemProcesso($key);

    return $this->prs_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:15
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:15
   */
  public  function set_prs_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemProcesso($key);

    $this->prs_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->prs_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:16
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:16
   */
  public  function set_prs_type($key, $type){
    ?><?php

		$this->validateItemProcesso($key);

    $this->prs_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:16
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:16
   */
  private  function configureJoinProcesso($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:17
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:17
   */
  private  function configureItemsProcesso($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->prs_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->prs_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:17
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:17
   */
  public  function clearProcesso($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_prs_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_prs_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_prs_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:17
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:17
   */
  private  function validateItemProcesso($key){
    ?><?php

    if (!isset($this->prs_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:17
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:17
   */
  public  function setStatementsProcesso(){
    ?><?php

    $this->prs_statements["0"] = "prs_codigo = '?'";

    $this->prs_statements["prs_0"] = $this->prs_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:18
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:18
   */
  public  function getStatementsProcesso(){
    ?><?php

    return $this->prs_statements;
  }


}