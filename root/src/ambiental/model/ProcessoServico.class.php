<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 10:16:29
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:47:36
 * @category model
 * @package ambiental
 */


class ProcessoServico
{
  private  $psr_items = array();
  private  $psr_properties = array();
  private  $psr_parents = array();
  private  $psr_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:31
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:31
   */
  public  function ProcessoServico(){
    ?><?php

    $this->psr_items = array();
    
    // Atributos
    $this->psr_items["psr_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"psr_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->psr_items["psr_valor"] = array("pk"=>0, "fk"=>0, "id"=>"psr_valor", "description"=>"Valor", "title"=>"", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->psr_items["psr_observacao"] = array("pk"=>0, "fk"=>0, "id"=>"psr_observacao", "description"=>"Observação", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );


    // Atributos FK
    $this->psr_items["psr_cod_PROCESSO"] = array("pk"=>0, "fk"=>1, "id"=>"psr_cod_PROCESSO", "description"=>"Processo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Processo", "table"=>"TBL_PROCESSO", "prefix"=>"prs", "tag"=>"processo", "key"=>"prs_codigo", "description"=>"prs_descricao", "form"=>"form", "target"=>"div-psr_cod_PROCESSO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->psr_items["psr_cod_SERVICO"] = array("pk"=>0, "fk"=>1, "id"=>"psr_cod_SERVICO", "description"=>"Serviço", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Servico", "table"=>"TBL_SERVICO", "prefix"=>"srv", "tag"=>"servico", "key"=>"srv_codigo", "description"=>"srv_descricao", "form"=>"form", "target"=>"div-psr_cod_SERVICO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->psr_items['psr_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'psr_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->psr_items['psr_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'psr_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->psr_items['psr_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'psr_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->psr_items['psr_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'psr_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->psr_items = $this->configureItemsProcessoServico($this->psr_items);

    $join = $this->configureJoinProcessoServico($this->psr_items);

    $lines = 0;
    foreach ($this->psr_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->psr_properties = array(
      'rotule'=>'Serviço do Processo',
      'module'=>'ambiental',
      'entity'=>'ProcessoServico',
      'table'=>'TBL_PROCESSO_SERVICO',
      'join'=>$join,
      'tag'=>'processoservico',
      'prefix'=>'psr',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'psr_codigo',
      'description'=>'psr_observacao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>new Object(array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>new Object(array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>new Object(array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"btn-primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>new Object(array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"btn-primary","add"=>"","back"=>""))),
          'find'=>new Object(array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>new Object(array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>new Object(array("action"=>'view', "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"fa-search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>new Object(array("action"=>'set', "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"fa-edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"btn-primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>new Object(array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"fa-trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          'print'=>new Object(array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'refresh'=>new Object(array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>new Object(array("action"=>'list', "label"=>"", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"btn-primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->psr_properties['reference']) {
      foreach ($this->psr_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->psr_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->psr_properties['description']) {
      foreach ($this->psr_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->psr_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsProcessoServico();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:31
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:31
   */
  public  function get_psr_properties(){
    ?><?php
    return $this->psr_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:31
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:32
   */
  public  function get_psr_items(){
    ?><?php
    return $this->psr_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:32
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:32
   */
  public  function get_psr_item($key){
    ?><?php

		$this->validateItemProcessoServico($key);

    return $this->psr_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:32
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:32
   */
  public  function get_psr_reference(){
    ?><?php
    $key = $this->psr_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:32
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:32
   */
  public  function get_psr_value($key){
    ?><?php

		$this->validateItemProcessoServico($key);

    return $this->psr_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:32
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:33
   */
  public  function set_psr_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemProcessoServico($key);

    $this->psr_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->psr_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:33
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:33
   */
  public  function set_psr_type($key, $type){
    ?><?php

		$this->validateItemProcessoServico($key);

    $this->psr_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:34
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:34
   */
  private  function configureJoinProcessoServico($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:34
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:34
   */
  private  function configureItemsProcessoServico($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->psr_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->psr_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:34
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:34
   */
  public  function clearProcessoServico($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_psr_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_psr_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_psr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:35
   */
  private  function validateItemProcessoServico($key){
    ?><?php

    if (!isset($this->psr_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:35
   */
  public  function setStatementsProcessoServico(){
    ?><?php

    $this->psr_statements["0"] = "psr_codigo = '?'";

    $this->psr_statements["psr_0"] = $this->psr_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:35
   */
  public  function getStatementsProcessoServico(){
    ?><?php

    return $this->psr_statements;
  }


}