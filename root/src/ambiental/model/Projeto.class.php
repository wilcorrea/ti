<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 09:25:39
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:37:07
 * @category model
 * @package ambiental
 */


class Projeto
{
  private  $prj_items = array();
  private  $prj_properties = array();
  private  $prj_parents = array();
  private  $prj_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:23
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:23
   */
  public  function Projeto(){
    ?><?php

    $this->prj_items = array();
    
    // Atributos
    $this->prj_items["prj_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"prj_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->prj_items["prj_situacao"] = array("pk"=>0, "fk"=>0, "id"=>"prj_situacao", "description"=>"Situacao", "title"=>"", "type"=>"option", "type_content"=>"AGUARDANDO APROVACAO|EM ANDAMENTO|CONCLUIDO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->prj_items["prj_emissao"] = array("pk"=>0, "fk"=>0, "id"=>"prj_emissao", "description"=>"Emissão", "title"=>"Data de emissão do orçamento", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->prj_items["prj_validade"] = array("pk"=>0, "fk"=>0, "id"=>"prj_validade", "description"=>"Validade", "title"=>"Validade do Orçamento", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->prj_items["prj_prazo"] = array("pk"=>0, "fk"=>0, "id"=>"prj_prazo", "description"=>"Prazo", "title"=>"Prazo para conclusão do processo", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>6, );
    $this->prj_items["prj_data_inicio"] = array("pk"=>0, "fk"=>0, "id"=>"prj_data_inicio", "description"=>"Data de Início", "title"=>"Data de início do processo", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>7, );
    $this->prj_items["prj_data_termino"] = array("pk"=>0, "fk"=>0, "id"=>"prj_data_termino", "description"=>"Data de Término", "title"=>"Data de término do processo", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>8, );
    $this->prj_items["prj_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"prj_tipo", "description"=>"Tipo", "title"=>"Identifica se é um processo ou um orçamento.", "type"=>"option", "type_content"=>"O,Orçamento|P,Projeto", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>9, );
    $this->prj_items["prj_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"prj_descricao", "description"=>"Descrição", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>10, );


    // Atributos FK
    $this->prj_items["prj_cod_CHAMADO"] = array("pk"=>0, "fk"=>1, "id"=>"prj_cod_CHAMADO", "description"=>"Chamado", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Chamado", "table"=>"TBL_CHAMADO", "prefix"=>"chm", "tag"=>"chamado", "key"=>"chm_codigo", "description"=>"chm_titulo", "form"=>"form", "target"=>"div-prj_cod_CHAMADO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD
    $this->prj_items["prj_fk_psr_cod_PROJETO_4927"] = array("pk"=>0, "fk"=>0, "id"=>"prj_fk_psr_cod_PROJETO_4927", "description"=>"Itens do Orçamento", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"ambiental", "entity"=>"ProjetoServico", "tag"=>"projetoservico", "prefix"=>"psr", "name"=>"Itens do Orçamento", "key"=>"prj_codigo", "foreign"=>"psr_cod_PROCESSO", "filter"=>"psr_cod_PROCESSO", "source"=>"psr_cod_PROCESSO", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->prj_items['prj_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'prj_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->prj_items['prj_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'prj_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->prj_items['prj_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'prj_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->prj_items['prj_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'prj_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->prj_items = $this->configureItemsProjeto($this->prj_items);

    $join = $this->configureJoinProjeto($this->prj_items);

    $lines = 0;
    foreach ($this->prj_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->prj_properties = array(
      'rotule'=>'Projeto',
      'module'=>'ambiental',
      'entity'=>'Projeto',
      'table'=>'TBL_PROJETO',
      'join'=>$join,
      'tag'=>'projeto',
      'prefix'=>'prj',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'prj_codigo',
      'description'=>'prj_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("id"=>"save", "action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("id"=>"copy", "action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("id"=>"add", "action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("id"=>"search", "action"=>'search', "get"=>"search", "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("id"=>"find", "action"=>'list', "get"=>"search", "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("id"=>"back", "action"=>'list', "get"=>"clear", "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("id"=>"view", "action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("id"=>"set", "action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("id"=>"remove", "action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("id"=>"list", "action"=>'list', "get"=>"collection", "label"=>"Projeto", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"primary")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->prj_properties['reference']) {
      foreach ($this->prj_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->prj_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->prj_properties['description']) {
      foreach ($this->prj_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->prj_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsProjeto();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:23
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:23
   */
  public  function get_prj_properties(){
    ?><?php
    return $this->prj_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:24
   */
  public  function get_prj_items(){
    ?><?php
    return $this->prj_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:24
   */
  public  function get_prj_item($key){
    ?><?php

		$this->validateItemProjeto($key);

    return $this->prj_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:24
   */
  public  function get_prj_reference(){
    ?><?php
    $key = $this->prj_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:24
   */
  public  function get_prj_value($key){
    ?><?php

		$this->validateItemProjeto($key);

    return $this->prj_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:24
   */
  public  function set_prj_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemProjeto($key);

    $this->prj_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->prj_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:25
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:25
   */
  public  function set_prj_type($key, $type){
    ?><?php

		$this->validateItemProjeto($key);

    $this->prj_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:26
   */
  private  function configureJoinProjeto($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:26
   */
  private  function configureItemsProjeto($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->prj_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->prj_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:27
   */
  public  function clearProjeto($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_prj_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_prj_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_prj_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:27
   */
  private  function validateItemProjeto($key){
    ?><?php

    if (!isset($this->prj_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:27
   */
  public  function setStatementsProjeto(){
    ?><?php

    $this->prj_statements["0"] = "prj_codigo = '?'";

    $this->prj_statements["prj_0"] = $this->prj_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:27
   */
  public  function getStatementsProjeto(){
    ?><?php

    return $this->prj_statements;
  }


}