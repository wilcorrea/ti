<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 04/11/2015 10:03:09
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:37
 * @category model
 * @package ambiental
 */


class RecursoHidricoUso
{
  private  $rhu_items = array();
  private  $rhu_properties = array();
  private  $rhu_parents = array();
  private  $rhu_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:44
   */
  public  function RecursoHidricoUso(){
    ?><?php

    $this->rhu_items = array();
    
    // Atributos
    $this->rhu_items["rhu_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"rhu_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->rhu_items["rhu_uso_codigo"] = array("pk"=>0, "fk"=>0, "id"=>"rhu_uso_codigo", "description"=>"Código do Uso", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->rhu_items["rhu_quantidade"] = array("pk"=>0, "fk"=>0, "id"=>"rhu_quantidade", "description"=>"Quantidade", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );


    // Atributos FK
    $this->rhu_items["rhu_cod_PROCESSO"] = array("pk"=>0, "fk"=>1, "id"=>"rhu_cod_PROCESSO", "description"=>"Processo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"ambiental", "entity"=>"Processo", "table"=>"TBL_PROCESSO", "prefix"=>"prs", "tag"=>"processo", "key"=>"prs_codigo", "description"=>"prs_descricao", "form"=>"form", "target"=>"div-rhu_cod_PROCESSO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->rhu_items['rhu_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'rhu_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->rhu_items['rhu_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'rhu_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->rhu_items['rhu_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'rhu_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->rhu_items['rhu_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'rhu_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->rhu_items = $this->configureItemsRecursoHidricoUso($this->rhu_items);

    $join = $this->configureJoinRecursoHidricoUso($this->rhu_items);

    $lines = 0;
    foreach ($this->rhu_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->rhu_properties = array(
      'rotule'=>'Uso de Recurso Hídrico',
      'module'=>'ambiental',
      'entity'=>'RecursoHidricoUso',
      'table'=>'TBL_RECURSO_HIDRICO_USO',
      'join'=>$join,
      'tag'=>'recursohidricouso',
      'prefix'=>'rhu',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'rhu_codigo',
      'description'=>'rhu_uso_codigo',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("id"=>"save", "action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("id"=>"copy", "action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("id"=>"add", "action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("id"=>"search", "action"=>'search', "get"=>"search", "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("id"=>"find", "action"=>'list', "get"=>"search", "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("id"=>"back", "action"=>'list', "get"=>"clear", "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("id"=>"view", "action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("id"=>"set", "action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("id"=>"remove", "action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("id"=>"list", "action"=>'list', "get"=>"collection", "label"=>"Uso de Recurso Hídrico", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"primary")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->rhu_properties['reference']) {
      foreach ($this->rhu_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->rhu_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->rhu_properties['description']) {
      foreach ($this->rhu_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->rhu_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsRecursoHidricoUso();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:44
   */
  public  function get_rhu_properties(){
    ?><?php
    return $this->rhu_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:44
   */
  public  function get_rhu_items(){
    ?><?php
    return $this->rhu_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:44
   */
  public  function get_rhu_item($key){
    ?><?php

		$this->validateItemRecursoHidricoUso($key);

    return $this->rhu_items[$key];
  }

  /**
   * 
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:44
   */
  public  function get_rhu_reference(){
    ?><?php
    $key = $this->rhu_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:45
   */
  public  function get_rhu_value($key){
    ?><?php

		$this->validateItemRecursoHidricoUso($key);

    return $this->rhu_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:45
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:45
   */
  public  function set_rhu_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemRecursoHidricoUso($key);

    $this->rhu_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->rhu_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:45
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:46
   */
  public  function set_rhu_type($key, $type){
    ?><?php

		$this->validateItemRecursoHidricoUso($key);

    $this->rhu_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:46
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:46
   */
  private  function configureJoinRecursoHidricoUso($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:46
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:46
   */
  private  function configureItemsRecursoHidricoUso($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->rhu_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->rhu_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:47
   */
  public  function clearRecursoHidricoUso($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_rhu_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_rhu_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_rhu_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:47
   */
  private  function validateItemRecursoHidricoUso($key){
    ?><?php

    if (!isset($this->rhu_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:47
   */
  public  function setStatementsRecursoHidricoUso(){
    ?><?php

    $this->rhu_statements["0"] = "rhu_codigo = '?'";

    $this->rhu_statements["rhu_0"] = $this->rhu_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:12:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:12:48
   */
  public  function getStatementsRecursoHidricoUso(){
    ?><?php

    return $this->rhu_statements;
  }


}