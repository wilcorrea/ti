<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 08/10/2015 11:08:24
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:34:50
 * @category post
 * @package ambiental
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'empreendimento-p-operation') {
    $operation = System::request('operation');
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postOperationEmpreendimento($operation, $level, $copy, $presave);
  }
}

  /**
   * Método que permite registrar, editar ou copiar uma instância da entidade na base de dados
   * 
   * @param string $operation 
   * @param string $level 
   * @param boolean $copy 
   * @param boolean $presave 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:22
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:22
   */
  function postOperationEmpreendimento($operation, $level, $copy = false, $presave = false){
    ?><?php

    $acesso_controle = -1;

    require System::import('file', '', 'header', 'core', false);

    $status = Console::$STATUS_FAIL;
    $message = "";
    $referenced = "";
    $log = array();
    $execute = "";

    if ($acesso_controle >= 1) {

      System::import('c', 'ambiental', 'Empreendimento', 'src', true, '{project.rsc}');

      $empreendimentoCtrl = new EmpreendimentoCtrl(PATH_APP);

      $properties = $empreendimentoCtrl->getPropertiesEmpreendimentoCtrl();
      $reference = $properties['reference'];
      $operations = $properties['operations'];

      if (isset($operations[$operation])) {

        $op = $operations[$operation];

        $references = explode(",", $reference);
        $search = "";
        if ($op->recover) {
          $s = "";
          $conector = ",";
          foreach ($references as $r) {
            $s .= $conector . Encode::decrypt(System::request($r));
          }
          $search = substr($s, strlen($conector));
        }

        $items = $empreendimentoCtrl->getItemsEmpreendimentoCtrl($search);
        foreach ($items as $id => $item) {

          $value = System::get($item, null);
          if (Number::parseInteger($op->recover) === 2 && !((gettype($value)) === 'object')) {
            $value = Encode::decrypt($value);
          }
          $items[$id]['value'] = $value;

        }

        $referenced = $empreendimentoCtrl->operationEmpreendimentoCtrl($operation, $items, null);

        $status = Console::status();

        if ($op->settings) {
          $message = $op->settings->$status;
        }
        
        if ($status === Console::$STATUS_SUCCESS) {
          $execute = $op->execute;
        }

      } else {

        $message = htmlentities("A operação '" . $operation . "' não corresponde a nenhuma operação desse nível");

      }

    } else {

      $message = htmlentities(MESSAGE_FORBIDDEN);

    }

    print Json::response($referenced, $message, $execute); 
}

