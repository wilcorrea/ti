<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 10:16:29
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:47:36
 * @category post
 * @package ambiental
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'processoservico-p-operation') {
    $operation = System::request('operation');
    $level = System::request('l');
    $copy = System::request('c', false);
    $presave = System::request('p');

    postOperationProcessoServico($operation, $level, $copy, $presave);
  }
}

  /**
   * Método que permite registrar, editar ou copiar uma instância da entidade na base de dados
   * 
   * @param string $operation 
   * @param string $level 
   * @param boolean $copy 
   * @param boolean $presave 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:16:56
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:16:56
   */
  function postOperationProcessoServico($operation, $level, $copy = false, $presave = false){
    ?><?php

    $acesso_controle = -1;

    require System::import('file', '', 'header', 'core', false);

    $status = Console::$STATUS_FAIL;
    $message = "";
    $referenced = "";
    $log = array();
    $execute = "";

    if ($acesso_controle >= 1) {

      System::import('c', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

      $processoServicoCtrl = new ProcessoServicoCtrl(PATH_APP);

      $properties = $processoServicoCtrl->getPropertiesProcessoServicoCtrl();
      $reference = $properties['reference'];
      $operations = $properties['operations'];

      if (isset($operations[$operation])) {

        $op = $operations[$operation];

        $references = explode(",", $reference);
        $search = "";
        if ($op->recover) {
          $s = "";
          $conector = ",";
          foreach ($references as $r) {
            $s .= $conector . Encode::decrypt(System::request($r));
          }
          $search = substr($s, strlen($conector));
        }

        $items = $processoServicoCtrl->getItemsProcessoServicoCtrl($search);
        foreach ($items as $id => $item) {

          $value = System::get($item, null);
          if (Number::parseInteger($op->recover) === 2 && !((gettype($value)) === 'object')) {
            $value = Encode::decrypt($value);
          }
          $items[$id]['value'] = $value;

        }

        $referenced = $processoServicoCtrl->operationProcessoServicoCtrl($operation, $items, null);

        $status = Console::status();

        if ($op->settings) {
          $message = $op->settings->$status;
        }
        
        if ($status === Console::$STATUS_SUCCESS) {
          $execute = $op->execute;
        }

      } else {

        $message = htmlentities("A operação '" . $operation . "' não corresponde a nenhuma operação desse nível");

      }

    } else {

      $message = htmlentities(MESSAGE_FORBIDDEN);

    }

    print Json::response($referenced, $message, $execute); 
}

