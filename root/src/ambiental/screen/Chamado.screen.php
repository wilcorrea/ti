<?php
/**
 * @copyright array software
 *
 * @author GENESON COSTA - 24/07/2015 19:15:08
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:24:19
 * @category screen
 * @package ambiental
 */


class ChamadoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:51
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:52
   */
  public  function managerChamadoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Chamado', 'src', true, '{project.rsc}');

      $chamadoCtrl = new ChamadoCtrl(PATH_APP);

      $properties = $chamadoCtrl->getPropertiesChamadoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $chamadoCtrl->configureChamadoCtrl($operation, $items, $properties, $target, $level);

      $historys = $chamadoCtrl->getHistoryChamadoCtrl($items);
      $history = array(
        'registro' => $historys['chm_registro'],
        'criador' => $historys['chm_criador'],
        'alteracao' => $historys['chm_alteracao'],
        'responsavel' => $historys['chm_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptChamadoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetChamadoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:53
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:53
   */
  public  function listChamadoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Chamado', 'src', true, '{project.rsc}');

      $chamadoCtrl = new ChamadoCtrl(PATH_APP);

      $properties = $chamadoCtrl->getPropertiesChamadoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $chamadoCtrl->configureChamadoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptChamadoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetChamadoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:55
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:55
   */
  private  function printJavascriptChamadoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 08/10/2015 11:01:55
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 08/10/2015 11:01:55
   */
  private  function printStyleSheetChamadoScreen($target, $operation, $filter, $items){

  }


}