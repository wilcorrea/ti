<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 08:50:33
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:33:54
 * @category screen
 * @package ambiental
 */


class ChamadoCategoriaScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 23/11/2015 11:33:08
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:33:08
   */
  public  function managerChamadoCategoriaScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'ChamadoCategoria', 'src', true, '{project.rsc}');

      $chamadoCategoriaCtrl = new ChamadoCategoriaCtrl(PATH_APP);

      $properties = $chamadoCategoriaCtrl->getPropertiesChamadoCategoriaCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $chamadoCategoriaCtrl->configureChamadoCategoriaCtrl($operation, $items, $properties, $target, $level);

      $historys = $chamadoCategoriaCtrl->getHistoryChamadoCategoriaCtrl($items);
      $history = array(
        'registro' => $historys['chc_registro'],
        'criador' => $historys['chc_criador'],
        'alteracao' => $historys['chc_alteracao'],
        'responsavel' => $historys['chc_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptChamadoCategoriaScreen($target, $operation, $filter, $items);

      $this->printStyleSheetChamadoCategoriaScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 23/11/2015 11:33:10
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:33:10
   */
  public  function listChamadoCategoriaScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'ChamadoCategoria', 'src', true, '{project.rsc}');

      $chamadoCategoriaCtrl = new ChamadoCategoriaCtrl(PATH_APP);

      $properties = $chamadoCategoriaCtrl->getPropertiesChamadoCategoriaCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $chamadoCategoriaCtrl->configureChamadoCategoriaCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptChamadoCategoriaScreen($target, $operation, $filter, $items);

      $this->printStyleSheetChamadoCategoriaScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 23/11/2015 11:33:12
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:33:12
   */
  private  function printJavascriptChamadoCategoriaScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 23/11/2015 11:33:13
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:33:13
   */
  private  function printStyleSheetChamadoCategoriaScreen($target, $operation, $filter, $items){

  }


}