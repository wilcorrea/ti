<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 08/10/2015 11:08:24
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:34:50
 * @category screen
 * @package ambiental
 */


class EmpreendimentoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:27
   */
  public  function managerEmpreendimentoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Empreendimento', 'src', true, '{project.rsc}');

      $empreendimentoCtrl = new EmpreendimentoCtrl(PATH_APP);

      $properties = $empreendimentoCtrl->getPropertiesEmpreendimentoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $empreendimentoCtrl->configureEmpreendimentoCtrl($operation, $items, $properties, $target, $level);

      $historys = $empreendimentoCtrl->getHistoryEmpreendimentoCtrl($items);
      $history = array(
        'registro' => $historys['emp_registro'],
        'criador' => $historys['emp_criador'],
        'alteracao' => $historys['emp_alteracao'],
        'responsavel' => $historys['emp_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptEmpreendimentoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetEmpreendimentoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:31
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:31
   */
  public  function listEmpreendimentoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Empreendimento', 'src', true, '{project.rsc}');

      $empreendimentoCtrl = new EmpreendimentoCtrl(PATH_APP);

      $properties = $empreendimentoCtrl->getPropertiesEmpreendimentoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $empreendimentoCtrl->configureEmpreendimentoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptEmpreendimentoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetEmpreendimentoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:35
   */
  private  function printJavascriptEmpreendimentoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:36
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:36
   */
  private  function printStyleSheetEmpreendimentoScreen($target, $operation, $filter, $items){

  }


}