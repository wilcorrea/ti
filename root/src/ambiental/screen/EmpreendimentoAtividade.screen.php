<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 06/11/2015 07:13:40
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 07:14:11
 * @category screen
 * @package ambiental
 */


class EmpreendimentoAtividadeScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 07:14:04
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 07:14:04
   */
  public  function managerEmpreendimentoAtividadeScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'EmpreendimentoAtividade', 'src', true, '{project.rsc}');

      $empreendimentoAtividadeCtrl = new EmpreendimentoAtividadeCtrl(PATH_APP);

      $properties = $empreendimentoAtividadeCtrl->getPropertiesEmpreendimentoAtividadeCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $empreendimentoAtividadeCtrl->configureEmpreendimentoAtividadeCtrl($operation, $items, $properties, $target, $level);

      $historys = $empreendimentoAtividadeCtrl->getHistoryEmpreendimentoAtividadeCtrl($items);
      $history = array(
        'registro' => $historys['ema_registro'],
        'criador' => $historys['ema_criador'],
        'alteracao' => $historys['ema_alteracao'],
        'responsavel' => $historys['ema_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptEmpreendimentoAtividadeScreen($target, $operation, $filter, $items);

      $this->printStyleSheetEmpreendimentoAtividadeScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 07:14:06
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 07:14:06
   */
  public  function listEmpreendimentoAtividadeScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'EmpreendimentoAtividade', 'src', true, '{project.rsc}');

      $empreendimentoAtividadeCtrl = new EmpreendimentoAtividadeCtrl(PATH_APP);

      $properties = $empreendimentoAtividadeCtrl->getPropertiesEmpreendimentoAtividadeCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $empreendimentoAtividadeCtrl->configureEmpreendimentoAtividadeCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptEmpreendimentoAtividadeScreen($target, $operation, $filter, $items);

      $this->printStyleSheetEmpreendimentoAtividadeScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 07:14:08
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 07:14:08
   */
  private  function printJavascriptEmpreendimentoAtividadeScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 07:14:09
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 07:14:09
   */
  private  function printStyleSheetEmpreendimentoAtividadeScreen($target, $operation, $filter, $items){

  }


}