<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 04/11/2015 10:51:58
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:24:55
 * @category screen
 * @package ambiental
 */


class IntervencaoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 06:43:24
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 06:43:24
   */
  public  function managerIntervencaoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Intervencao', 'src', true, '{project.rsc}');

      $intervencaoCtrl = new IntervencaoCtrl(PATH_APP);

      $properties = $intervencaoCtrl->getPropertiesIntervencaoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $intervencaoCtrl->configureIntervencaoCtrl($operation, $items, $properties, $target, $level);

      $historys = $intervencaoCtrl->getHistoryIntervencaoCtrl($items);
      $history = array(
        'registro' => $historys['itv_registro'],
        'criador' => $historys['itv_criador'],
        'alteracao' => $historys['itv_alteracao'],
        'responsavel' => $historys['itv_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptIntervencaoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetIntervencaoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 06:43:26
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 06:43:26
   */
  public  function listIntervencaoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Intervencao', 'src', true, '{project.rsc}');

      $intervencaoCtrl = new IntervencaoCtrl(PATH_APP);

      $properties = $intervencaoCtrl->getPropertiesIntervencaoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $intervencaoCtrl->configureIntervencaoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptIntervencaoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetIntervencaoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 06:43:27
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 06:43:27
   */
  private  function printJavascriptIntervencaoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 06/11/2015 06:43:28
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 06:43:28
   */
  private  function printStyleSheetIntervencaoScreen($target, $operation, $filter, $items){

  }


}