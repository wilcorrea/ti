<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 04/11/2015 09:33:52
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 06/11/2015 06:43:50
 * @category screen
 * @package ambiental
 */


class OutorgaScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 05/11/2015 22:26:33
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 05/11/2015 22:26:33
   */
  public  function managerOutorgaScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Outorga', 'src', true, '{project.rsc}');

      $outorgaCtrl = new OutorgaCtrl(PATH_APP);

      $properties = $outorgaCtrl->getPropertiesOutorgaCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $outorgaCtrl->configureOutorgaCtrl($operation, $items, $properties, $target, $level);

      $historys = $outorgaCtrl->getHistoryOutorgaCtrl($items);
      $history = array(
        'registro' => $historys['out_registro'],
        'criador' => $historys['out_criador'],
        'alteracao' => $historys['out_alteracao'],
        'responsavel' => $historys['out_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptOutorgaScreen($target, $operation, $filter, $items);

      $this->printStyleSheetOutorgaScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 05/11/2015 22:26:35
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 05/11/2015 22:26:35
   */
  public  function listOutorgaScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Outorga', 'src', true, '{project.rsc}');

      $outorgaCtrl = new OutorgaCtrl(PATH_APP);

      $properties = $outorgaCtrl->getPropertiesOutorgaCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $outorgaCtrl->configureOutorgaCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptOutorgaScreen($target, $operation, $filter, $items);

      $this->printStyleSheetOutorgaScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 05/11/2015 22:26:36
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 05/11/2015 22:26:36
   */
  private  function printJavascriptOutorgaScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 05/11/2015 22:26:37
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 05/11/2015 22:26:37
   */
  private  function printStyleSheetOutorgaScreen($target, $operation, $filter, $items){

  }


}