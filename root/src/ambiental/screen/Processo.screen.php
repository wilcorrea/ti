<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 30/10/2015 10:14:56
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:16:46
 * @category screen
 * @package ambiental
 */


class ProcessoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:32
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:32
   */
  public  function managerProcessoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Processo', 'src', true, '{project.rsc}');

      $processoCtrl = new ProcessoCtrl(PATH_APP);

      $properties = $processoCtrl->getPropertiesProcessoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $processoCtrl->configureProcessoCtrl($operation, $items, $properties, $target, $level);

      $historys = $processoCtrl->getHistoryProcessoCtrl($items);
      $history = array(
        'registro' => $historys['prs_registro'],
        'criador' => $historys['prs_criador'],
        'alteracao' => $historys['prs_alteracao'],
        'responsavel' => $historys['prs_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptProcessoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProcessoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:34
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:34
   */
  public  function listProcessoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Processo', 'src', true, '{project.rsc}');

      $processoCtrl = new ProcessoCtrl(PATH_APP);

      $properties = $processoCtrl->getPropertiesProcessoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $processoCtrl->configureProcessoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptProcessoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProcessoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:36
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:36
   */
  private  function printJavascriptProcessoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:32:36
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:32:37
   */
  private  function printStyleSheetProcessoScreen($target, $operation, $filter, $items){

  }


}