<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 10:16:29
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:47:36
 * @category screen
 * @package ambiental
 */


class ProcessoServicoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:17:03
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:17:03
   */
  public  function managerProcessoServicoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

      $processoServicoCtrl = new ProcessoServicoCtrl(PATH_APP);

      $properties = $processoServicoCtrl->getPropertiesProcessoServicoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $processoServicoCtrl->configureProcessoServicoCtrl($operation, $items, $properties, $target, $level);

      $historys = $processoServicoCtrl->getHistoryProcessoServicoCtrl($items);
      $history = array(
        'registro' => $historys['psr_registro'],
        'criador' => $historys['psr_criador'],
        'alteracao' => $historys['psr_alteracao'],
        'responsavel' => $historys['psr_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptProcessoServicoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProcessoServicoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:17:05
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:17:05
   */
  public  function listProcessoServicoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'ProcessoServico', 'src', true, '{project.rsc}');

      $processoServicoCtrl = new ProcessoServicoCtrl(PATH_APP);

      $properties = $processoServicoCtrl->getPropertiesProcessoServicoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $processoServicoCtrl->configureProcessoServicoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptProcessoServicoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProcessoServicoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:17:07
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:17:07
   */
  private  function printJavascriptProcessoServicoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 09/10/2015 10:17:08
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 09/10/2015 10:17:08
   */
  private  function printStyleSheetProcessoServicoScreen($target, $operation, $filter, $items){

  }


}