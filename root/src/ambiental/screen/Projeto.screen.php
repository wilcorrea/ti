<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 09/10/2015 09:25:39
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:37:07
 * @category screen
 * @package ambiental
 */


class ProjetoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:42
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:42
   */
  public  function managerProjetoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Projeto', 'src', true, '{project.rsc}');

      $projetoCtrl = new ProjetoCtrl(PATH_APP);

      $properties = $projetoCtrl->getPropertiesProjetoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $projetoCtrl->configureProjetoCtrl($operation, $items, $properties, $target, $level);

      $historys = $projetoCtrl->getHistoryProjetoCtrl($items);
      $history = array(
        'registro' => $historys['prj_registro'],
        'criador' => $historys['prj_criador'],
        'alteracao' => $historys['prj_alteracao'],
        'responsavel' => $historys['prj_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptProjetoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProjetoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:44
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:44
   */
  public  function listProjetoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'Projeto', 'src', true, '{project.rsc}');

      $projetoCtrl = new ProjetoCtrl(PATH_APP);

      $properties = $projetoCtrl->getPropertiesProjetoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $projetoCtrl->configureProjetoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptProjetoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetProjetoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:45
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:46
   */
  private  function printJavascriptProjetoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 13/11/2015 09:35:47
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 13/11/2015 09:35:47
   */
  private  function printStyleSheetProjetoScreen($target, $operation, $filter, $items){

  }


}