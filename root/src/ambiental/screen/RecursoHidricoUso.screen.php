<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 04/11/2015 10:03:09
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:37
 * @category screen
 * @package ambiental
 */


class RecursoHidricoUsoScreen
{

  /**
   * Cria uma interface para o layout manager da entidade
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:13:02
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:02
   */
  public  function managerRecursoHidricoUsoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

	  $acesso_controle = -1;
		
		require System::import('file', '', 'header',  'core', false);
	  require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'RecursoHidricoUso', 'src', true, '{project.rsc}');

      $recursoHidricoUsoCtrl = new RecursoHidricoUsoCtrl(PATH_APP);

      $properties = $recursoHidricoUsoCtrl->getPropertiesRecursoHidricoUsoCtrl();
      $reference = $properties['reference'];

      $execute = "";
      if ($layout === 'children') {
        $execute = "system.util.copyToForm('" . $reference . "');" . $execute;
      }

      $items = $screen->configureFilter($items, $filter);
      $items = $recursoHidricoUsoCtrl->configureRecursoHidricoUsoCtrl($operation, $items, $properties, $target, $level);

      $historys = $recursoHidricoUsoCtrl->getHistoryRecursoHidricoUsoCtrl($items);
      $history = array(
        'registro' => $historys['rhu_registro'],
        'criador' => $historys['rhu_criador'],
        'alteracao' => $historys['rhu_alteracao'],
        'responsavel' => $historys['rhu_responsavel']
      );

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom, $history, $execute);

      $this->printJavascriptRecursoHidricoUsoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetRecursoHidricoUsoScreen($target, $operation, $filter, $items);
     
    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }
  }

  /**
   * Escreve uma listagem dos registros da entidade com base nos parâmetros informados
   * 
   * @param string $target 
   * @param string $layout 
   * @param string $operation 
   * @param string $level 
   * @param string $filter 
   * @param array $items 
   * @param array $recovereds 
   * @param array $interfaces 
   * @param object $custom 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:13:03
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:03
   */
  public  function listRecursoHidricoUsoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom){
    ?><?php

    $acesso_controle = -1;

		require System::import('file', '', 'header',  'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    if ($acesso_controle >= 0) {

      System::import('c', 'ambiental', 'RecursoHidricoUso', 'src', true, '{project.rsc}');

      $recursoHidricoUsoCtrl = new RecursoHidricoUsoCtrl(PATH_APP);

      $properties = $recursoHidricoUsoCtrl->getPropertiesRecursoHidricoUsoCtrl();

      $items = $screen->configureFilter($items, $filter);
      $items = $recursoHidricoUsoCtrl->configureRecursoHidricoUsoCtrl($operation, $items, $properties, $target, $level);

      $screen->printContent($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $properties, $custom);

      $this->printJavascriptRecursoHidricoUsoScreen($target, $operation, $filter, $items);

      $this->printStyleSheetRecursoHidricoUsoScreen($target, $operation, $filter, $items);

    } else {

      $screen->message->printMessageError(MESSAGE_FORBIDDEN);

    }

  }

  /**
   * Permite que o desenvolvedor crie funções personalizadas para a sua tela
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:13:05
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:05
   */
  private  function printJavascriptRecursoHidricoUsoScreen($target, $operation, $filter, $items){

  }

  /**
   * Permite que o desenvolvedor crie regras personalizadas para a sua interface
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $filter 
   * @param array $items 
   *
   * @author MATHEUS FELIZARDO - 25/11/2015 10:13:06
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 25/11/2015 10:13:06
   */
  private  function printStyleSheetRecursoHidricoUsoScreen($target, $operation, $filter, $items){

  }


}