<?php
/**
 * @copyright array software
 *
 * @author MATHEUS FELIZARDO - 08/10/2015 11:08:24
 * <br><b>Updated by</b> MATHEUS FELIZARDO - 23/11/2015 11:34:50
 * @category view
 * @package ambiental
 */


if (System::request('action')) {
  $action = System::request('action');


 if ($action == 'empreendimento-v-operation') {
    $target = System::request('t');
    $operation = System::request('operation');
    $recover = System::request('r');
    $level = System::request('l');
    $filter = System::request('f');
    $custom = System::request('c');

    viewOperationEmpreendimento($target, $operation, $recover, $level, $filter, $custom);
  }
}

  /**
   * Gerencia qual interface carregar para a operação informada
   * 
   * @param string $target 
   * @param string $operation 
   * @param string $recover 
   * @param string $level 
   * @param string $filter 
   * @param string $custom 
   *
   * @author MATHEUS FELIZARDO - 14/10/2015 10:07:19
   * <br><b>Updated by</b> MATHEUS FELIZARDO - 14/10/2015 10:07:19
   */
  function viewOperationEmpreendimento($target, $operation, $recover, $level, $filter, $custom){
    ?><?php

    $acesso_controle = -1;

    require System::import('file', '', 'header', 'core', false);
    require_once System::import('class', 'resource', 'Screen', 'core', false);
  
    $screen = new Screen('{project.rsc}');
  
    if ($acesso_controle >= 1) {
  
      System::import('c', 'ambiental', 'Empreendimento', 'src', true, '{project.rsc}');
      System::import('s', 'ambiental', 'Empreendimento', 'src', true, '{project.rsc}');
  
      $empreendimentoScreen = new EmpreendimentoScreen();
      $empreendimentoCtrl = new EmpreendimentoCtrl(PATH_APP);
  
      $properties = $empreendimentoCtrl->getPropertiesEmpreendimentoCtrl();
      $reference = $properties['reference'];
      $operations = $properties['operations'];
  
      if (isset($operations[$operation])) {

        $o = $operations[$operation];

        if ($o->type === 'view') {

          $layout = $o->layout;

          $search = "";
          if ($o->recover) {
    
            $references = explode(",", $reference);
            $s = "";
            $conector = ",";
            foreach ($references as $key) {
              $s .= $conector . Encode::decrypt(System::request($key));
            }
            $search = substr($s, strlen($conector));
          }
  
          $items = $empreendimentoCtrl->getItemsEmpreendimentoCtrl($search);
    
          $recovereds = array('w'=>"", 'g'=>"", 'o'=>"", 'wd'=>"", 'gd'=>"", 'od'=>"");
          if ($recover === 'true') {
            $recovered = System::recover($items, true, true, true);
            foreach ($recovereds as $r => $value) {
              $recovereds[$r] = Encode::encrypt($recovered[$r]);
            }
          } else if ($recover === 'clear') {
            foreach ($recovereds as $r => $value) {
              $recovereds[$r] = '';
            }
          } else {
            foreach ($recovereds as $r => $value) {
              $recovereds[$r] = System::request($r);
            }
          }
    
          $interfaces = array('form'=>"", 'rotule'=>"", 'window_width'=>"", 'window_height'=>"", 'width'=>"", 'height'=>"", 'modal'=>"", 'start'=>"", 'child'=>"");
          foreach ($interfaces as $i => $r) {
            $interfaces[$i] = System::request($i);
          }
    
          if ($layout === '' or $layout === 'manager') {
    
            $empreendimentoScreen->managerEmpreendimentoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom);
    
          } else if ($layout === 'list') {
    
            $empreendimentoScreen->listEmpreendimentoScreen($target, $layout, $operation, $level, $filter, $items, $recovereds, $interfaces, $custom);
    
          }

        } else {

          $screen->message->printMessageError("Tipo inválido para carregamento de visualização. A operação deve ser do tipo 'view' e sua solicitação é do tipo '" . $op->type . "'");

        }
  
      }
  
    } else {
  
      $screen->message->printMessageError(MESSAGE_FORBIDDEN);
  
    }
}

