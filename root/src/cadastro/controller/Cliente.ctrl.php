<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:57
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 16:26:16
 * @category controller
 * @package cadastro
 */


class ClienteCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function ClienteCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');
    
    $entity = new Cliente();
    $properties = $entity->get_cli_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function getRowsClienteCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    $items = $cliente->get_cli_items();
    $properties = $cliente->get_cli_properties();

    $statements = $cliente->getStatementsCliente();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function getClienteCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    $clientes = null;

    $rows = $this->getRowsClienteCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $clientes = array();
      foreach ($rows as $row) {
        $cliente_set = clone $cliente;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $cliente_set->set_cli_value($key, $item['value'], $reference);
        }
        $cliente_set->clearCliente($not_clear);
        $clientes[] = $cliente_set;
      }
    }

    return $clientes;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function getColumnClienteCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    $properties = $cliente->get_cli_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $cliente->getStatementsCliente();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function getCountClienteCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    $properties = $cliente->get_cli_properties();

    $statements = $cliente->getStatementsCliente();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnClienteCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/09/2015 12:52:11
   */
  private  function beforeClienteCtrl($object, $param){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $object->set_cli_value('cds_status', trim($object->get_cli_value('cds_status')));

    $cds_codigo = $this->getColumnClienteCtrl('cds_codigo', 'cli_0' . ':' . $object->get_cli_value('cli_codigo'));

    if ($cds_codigo) {

      $object->set_cli_value('cds_codigo', $cds_codigo);
    }

    $before = Controller::before($object, $object->get_cli_items(), 'cli');

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 04/09/2015 19:50:35
   */
  public  function addClienteCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesClienteCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    $where = [];

    foreach ($references as $reference) {
      $value = $object->get_cli_value($reference);
      if ($value !== '' && $value !== null) {
        $where[] = $reference . " = '" . $value . "'";
      }
    }

    $created = $this->getCountClienteCtrl(implode(' AND ', $where));

    if ($created) {
      $setable = true;
    }

    if (!$setable) {

      $properties = $this->getPropertiesClienteCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_cli_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setClienteCtrl($object);
      if ($response->result) {
        $add = $response->reference;
      }

    }
    
    $result = Boolean::parse($add > 0);
    if (!$result) {
      Console::add('INSERT WITHOUT EFFECT ' . '[' . $sql . ']');
    }
    if ($this->dao->getError() !== 'no-error') {
      $result = false;
      Console::add($this->dao->getError());
    }

    return new Response($result, $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 04/09/2015 19:46:44
   */
  public  function setClienteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_cli_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if (($item['pk']) && ($item['id'] != 'cds_codigo')) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }

    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesClienteCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_cli_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    $result = Boolean::parse($set > 0);
    if (!$result) {
      Console::add('UPDATE WITHOUT EFFECT ' . '[' . $sql . ']');
    }
    if ($this->dao->getError() !== 'no-error') {
      $result = false;
      Console::add($this->dao->getError());
    }

    return new Response($result, implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function removeClienteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_cli_items();
    $properties = $this->getPropertiesClienteCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_cli_value($action['field'], $action['value']);
        $object->clearCliente(array($action['field']));

        $remove = $this->setClienteCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/09/2015 18:55:02
   */
  private  function afterClienteCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_cli_value($object->get_cli_reference());
    } else if ($param === 'add') {
      $object->set_cli_value($object->get_cli_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_cli_items();

      $fields = Controller::after($value, $items, 'cli');
      //Console::add(print_r($fields, true));
      if (count($fields)) {
        $executed = $this->executeClienteCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_cli_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function executeClienteCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    $properties = $cliente->get_cli_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $cliente->getStatementsCliente();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", cli_responsavel = '" . System::getUser(false) . "', cli_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  private  function verifyClienteCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_cli_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function getObjectClienteCtrl($value){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $object = new Cliente();

    $properties = $object->get_cli_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $cliente = null;

    $clientes = $this->getClienteCtrl($where, "", "", "0", "1");
    if (is_array($clientes)) {
      $cliente = $clientes[0];
    }

    return $cliente;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function getReplacesClienteCtrl($reference){
		?><?php

		System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

		$cliente = new Cliente();

    $replaces = array();

    $where = "cli_codigo = '" . $reference . "'";
    $clientes = $this->getClienteCtrl($where, "", "");
    if (is_array($clientes)) {
      $cliente = $clientes[0];

      $items = $cliente->get_cli_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 15:58:53
   */
  public  function configureClienteCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    $atributos['cds_cpf']['onchange'] = "Application.cadastro.Cadastro.cds_cpf(this.value);";
    $atributos['cds_cnpj']['onchange'] = "Application.cadastro.Cadastro.cds_cnpj(this.value);";

    if (!$atributos['cds_tipo']['value']) {
      $atributos['cds_tipo']['value'] = 'F';
    }
    $atributos['cds_tipo']['action'] = "Application.cadastro.Cadastro.cds_tipo(this.value);";

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function getPropertiesClienteCtrl(){
    ?><?php

      System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

      $cliente = new Cliente();

      $properties = $cliente->get_cli_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function getItemsClienteCtrl($search){
    ?><?php

    System::import('m', 'cadastro', 'Cliente', 'src', true, '{project.rsc}');

    $cliente = new Cliente();

    if ($search) {
      $object = $this->getObjectClienteCtrl($search);
      if (!is_null($object)) {
        $cliente = $object;
      }
    }
    
    $items = $cliente->get_cli_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $cliente->get_cli_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function getHistoryClienteCtrl($items){
    ?><?php
    
    $defaults = array(
      'cli_registro'=>array('id'=>'cli_registro', 'value'=>date('d/m/Y H:i:s')),
      'cli_criador'=>array('id'=>'cli_criador', 'value'=>System::getUser()),
      'cli_alteracao'=>array('id'=>'cli_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'cli_responsavel'=>array('id'=>'cli_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function operationClienteCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'cadastro', 'Cliente', 'src', true);

    $cliente = new Cliente();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $cliente->get_cli_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $cliente->set_cli_value($id, $item['value']);
    }
    
    $method = $action . "ClienteCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyClienteCtrl($cliente, $operation);
      if ($verify) {

        $before = $this->beforeClienteCtrl($cliente, $operation);
        if ($before) {

          $response = $this->$method($cliente);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterClienteCtrl($cliente, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'ClienteCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:59
   */
  public  function copyClienteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesClienteCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_cli_value($reference, null);
    }

    return $this->addClienteCtrl($object, $validate, $debug);
  }


}