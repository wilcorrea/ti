<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:08
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 16:26:06
 * @category controller
 * @package cadastro
 */


class ProdutoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   */
  public  function ProdutoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');
    
    $entity = new Produto();
    $properties = $entity->get_prd_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   */
  public  function getRowsProdutoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    $items = $produto->get_prd_items();
    $properties = $produto->get_prd_properties();

    $statements = $produto->getStatementsProduto();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   */
  public  function getProdutoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    $produtos = null;

    $rows = $this->getRowsProdutoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $produtos = array();
      foreach ($rows as $row) {
        $produto_set = clone $produto;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $produto_set->set_prd_value($key, $item['value'], $reference);
        }
        $produto_set->clearProduto($not_clear);
        $produtos[] = $produto_set;
      }
    }

    return $produtos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   */
  public  function getColumnProdutoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    $properties = $produto->get_prd_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $produto->getStatementsProduto();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   */
  public  function getCountProdutoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    $properties = $produto->get_prd_properties();

    $statements = $produto->getStatementsProduto();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnProdutoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/09/2015 18:01:41
   */
  private  function beforeProdutoCtrl($object, $param){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $before = Controller::before($object, $object->get_prd_items(), 'prd');

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 16:33:01
   */
  public  function addProdutoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesProdutoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;

    $where = [];

    foreach ($references as $reference) {
      if ($object->get_prd_value($reference)) {
        $where[] = $reference . " = '" . $object->get_prd_value($reference) . "'";
      }
    }

    $created = $this->getCountProdutoCtrl(implode(' AND ', $where));
    if ($created) {
      $setable = true;
    }

    if (!$setable) {

      $properties = $this->getPropertiesProdutoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_prd_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);
      //Console::add($sql);
      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setProdutoCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/09/2015 17:59:09
   */
  public  function setProdutoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_prd_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesProdutoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_prd_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function removeProdutoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_prd_items();
    $properties = $this->getPropertiesProdutoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_prd_value($action['field'], $action['value']);
        $object->clearProduto(array($action['field']));

        $remove = $this->setProdutoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  private  function afterProdutoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_prd_value($object->get_prd_reference());
    } else if ($param === 'add') {
      $object->set_prd_value($object->get_prd_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_prd_items();

      $fields = Controller::after($value, $items, 'prd');
      if (count($fields)) {
        $executed = $this->executeProdutoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_prd_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function executeProdutoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    $properties = $produto->get_prd_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $produto->getStatementsProduto();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", prd_responsavel = '" . System::getUser(false) . "', prd_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  private  function verifyProdutoCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_prd_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function getObjectProdutoCtrl($value){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $object = new Produto();

    $properties = $object->get_prd_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $produto = null;

    $produtos = $this->getProdutoCtrl($where, "", "", "0", "1");
    if (is_array($produtos)) {
      $produto = $produtos[0];
    }

    return $produto;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function getReplacesProdutoCtrl($reference){
		?><?php

		System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

		$produto = new Produto();

    $replaces = array();

    $where = "prd_codigo = '" . $reference . "'";
    $produtos = $this->getProdutoCtrl($where, "", "");
    if (is_array($produtos)) {
      $produto = $produtos[0];

      $items = $produto->get_prd_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function configureProdutoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function getPropertiesProdutoCtrl(){
    ?><?php

      System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

      $produto = new Produto();

      $properties = $produto->get_prd_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function getItemsProdutoCtrl($search){
    ?><?php

    System::import('m', 'cadastro', 'Produto', 'src', true, '{project.rsc}');

    $produto = new Produto();

    if ($search) {
      $object = $this->getObjectProdutoCtrl($search);
      if (!is_null($object)) {
        $produto = $object;
      }
    }
    
    $items = $produto->get_prd_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $produto->get_prd_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function getHistoryProdutoCtrl($items){
    ?><?php
    
    $defaults = array(
      'prd_registro'=>array('id'=>'prd_registro', 'value'=>date('d/m/Y H:i:s')),
      'prd_criador'=>array('id'=>'prd_criador', 'value'=>System::getUser()),
      'prd_alteracao'=>array('id'=>'prd_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'prd_responsavel'=>array('id'=>'prd_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function operationProdutoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'cadastro', 'Produto', 'src', true);

    $produto = new Produto();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $produto->get_prd_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $produto->set_prd_value($id, $item['value']);
    }
    
    $method = $action . "ProdutoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyProdutoCtrl($produto, $operation);
      if ($verify) {

        $before = $this->beforeProdutoCtrl($produto, $operation);
        if ($before) {

          $response = $this->$method($produto);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterProdutoCtrl($produto, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'ProdutoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 02:00:10
   */
  public  function copyProdutoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesProdutoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_prd_value($reference, null);
    }

    return $this->addProdutoCtrl($object, $validate, $debug);
  }


}