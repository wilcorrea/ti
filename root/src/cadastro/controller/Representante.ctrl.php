<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:30:27
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 17:42:22
 * @category controller
 * @package cadastro
 */


class RepresentanteCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function RepresentanteCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');
    
    $entity = new Representante();
    $properties = $entity->get_rpr_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function getRowsRepresentanteCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    $items = $representante->get_rpr_items();
    $properties = $representante->get_rpr_properties();

    $statements = $representante->getStatementsRepresentante();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function getRepresentanteCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    $representantes = null;

    $rows = $this->getRowsRepresentanteCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $representantes = array();
      foreach ($rows as $row) {
        $representante_set = clone $representante;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $representante_set->set_rpr_value($key, $item['value'], $reference);
        }
        $representante_set->clearRepresentante($not_clear);
        $representantes[] = $representante_set;
      }
    }

    return $representantes;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function getColumnRepresentanteCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    $properties = $representante->get_rpr_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $representante->getStatementsRepresentante();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function getCountRepresentanteCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    $properties = $representante->get_rpr_properties();

    $statements = $representante->getStatementsRepresentante();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnRepresentanteCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/09/2015 12:41:41
   */
  private  function beforeRepresentanteCtrl($object, $param){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $object->set_rpr_value('cds_status', trim($object->get_rpr_value('cds_status')));

    $cds_codigo = $this->getColumnRepresentanteCtrl('cds_codigo', 'rpr_0' . ':' . $object->get_rpr_value('rpr_codigo'));

    if ($cds_codigo) {

      $object->set_rpr_value('cds_codigo', $cds_codigo);
    }

    $before = Controller::before($object, $object->get_rpr_items(), 'rpr');

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 21:02:07
   */
  public  function addRepresentanteCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesRepresentanteCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    $where = [];

    foreach ($references as $reference) {
      if ($object->get_rpr_value($reference)) {
        $where[] = $reference . " = '" . $object->get_rpr_value($reference) . "'";
      }
    }

    $created = $this->getCountRepresentanteCtrl(implode(' AND ', $where));
    if ($created) {
      $setable = true;
    }

    if (!$setable) {

      $properties = $this->getPropertiesRepresentanteCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_rpr_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setRepresentanteCtrl($object);
      if ($response->result) {
        $add = $response->reference;
      }

    }
    
    $result = Boolean::parse($add > 0);
    if (!$result) {
      Console::add("INSERT WITHOUT EFFECT");
    }
    if ($this->dao->getError() !== 'no-error') {
      $result = false;
      Console::add($this->dao->getError());
    }

    return new Response($result, $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/09/2015 12:39:08
   */
  public  function setRepresentanteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_rpr_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if (($item['pk']) && ($item['id'] != 'cds_codigo')) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }

    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesRepresentanteCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_rpr_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    $result = Boolean::parse($set > 0);
    if (!$result) {
      Console::add("UPDATE WITHOUT EFFECT");
    }
    if ($this->dao->getError() !== 'no-error') {
      $result = false;
      Console::add($this->dao->getError());
    }

    return new Response($result, implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function removeRepresentanteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_rpr_items();
    $properties = $this->getPropertiesRepresentanteCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_rpr_value($action['field'], $action['value']);
        $object->clearRepresentante(array($action['field']));

        $remove = $this->setRepresentanteCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  private  function afterRepresentanteCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_rpr_value($object->get_rpr_reference());
    } else if ($param === 'add') {
      $object->set_rpr_value($object->get_rpr_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_rpr_items();

      $fields = Controller::after($value, $items, 'rpr');
      if (count($fields)) {
        $executed = $this->executeRepresentanteCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_rpr_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function executeRepresentanteCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    $properties = $representante->get_rpr_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $representante->getStatementsRepresentante();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", rpr_responsavel = '" . System::getUser(false) . "', rpr_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  private  function verifyRepresentanteCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_rpr_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function getObjectRepresentanteCtrl($value){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $object = new Representante();

    $properties = $object->get_rpr_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $representante = null;

    $representantes = $this->getRepresentanteCtrl($where, "", "", "0", "1");
    if (is_array($representantes)) {
      $representante = $representantes[0];
    }

    return $representante;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function getReplacesRepresentanteCtrl($reference){
		?><?php

		System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

		$representante = new Representante();

    $replaces = array();

    $where = "rpr_codigo = '" . $reference . "'";
    $representantes = $this->getRepresentanteCtrl($where, "", "");
    if (is_array($representantes)) {
      $representante = $representantes[0];

      $items = $representante->get_rpr_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function configureRepresentanteCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function getPropertiesRepresentanteCtrl(){
    ?><?php

      System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

      $representante = new Representante();

      $properties = $representante->get_rpr_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function getItemsRepresentanteCtrl($search){
    ?><?php

    System::import('m', 'cadastro', 'Representante', 'src', true, '{project.rsc}');

    $representante = new Representante();

    if ($search) {
      $object = $this->getObjectRepresentanteCtrl($search);
      if (!is_null($object)) {
        $representante = $object;
      }
    }
    
    $items = $representante->get_rpr_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $representante->get_rpr_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function getHistoryRepresentanteCtrl($items){
    ?><?php
    
    $defaults = array(
      'rpr_registro'=>array('id'=>'rpr_registro', 'value'=>date('d/m/Y H:i:s')),
      'rpr_criador'=>array('id'=>'rpr_criador', 'value'=>System::getUser()),
      'rpr_alteracao'=>array('id'=>'rpr_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'rpr_responsavel'=>array('id'=>'rpr_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 20:55:26
   */
  public  function operationRepresentanteCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'cadastro', 'Representante', 'src', true);

    $representante = new Representante();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $representante->get_rpr_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $representante->set_rpr_value($id, $item['value']);
    }
    
    $method = $action . "RepresentanteCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyRepresentanteCtrl($representante, $operation);
      if ($verify) {

        $before = $this->beforeRepresentanteCtrl($representante, $operation);
        if ($before) {

          $response = $this->$method($representante);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterRepresentanteCtrl($representante, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'RepresentanteCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:18
   */
  public  function copyRepresentanteCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesRepresentanteCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_rpr_value($reference, null);
    }

    return $this->addRepresentanteCtrl($object, $validate, $debug);
  }


}