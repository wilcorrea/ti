<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/06/2015 18:28:10
 * @category model
 * @package cadastro
 */


class Cadastro
{
  private  $cds_items = array();
  private  $cds_properties = array();
  private  $cds_parents = array();
  private  $cds_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 15:58:39
   */
  public  function Cadastro(){
    ?><?php

    $this->cds_items = array();
    
    // Atributos
    $this->cds_items["cds_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"cds_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->cds_items["cds_codigo_externo"] = array("pk"=>0, "fk"=>0, "id"=>"cds_codigo_externo", "description"=>"Sincronização", "title"=>"Código utilizado para sincronização", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>2, );
    $this->cds_items["cds_status"] = array("pk"=>0, "fk"=>0, "id"=>"cds_status", "description"=>"Situação", "title"=>"Situação atual do Cadastro", "type"=>"option", "type_content"=>"ATIVO|INATIVO|BLOQUEADO|PROBLEMA", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"ATIVO", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>3, );
    $this->cds_items["cds_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"cds_tipo", "description"=>"Tipo", "title"=>"Pessoa Física ou Jurídica", "type"=>"option", "type_content"=>"F,Pessoa Física|J,Jurídica", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"F", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->cds_items["cds_cnpj_cpf"] = array("pk"=>0, "fk"=>0, "id"=>"cds_cnpj_cpf", "description"=>"CPF / CNPJ", "title"=>"Campo que exibe o CNPJ ou CPF de acordo com o que foi selecionado como Pessoa Física ou Jurídica", "type"=>"calculated", "type_content"=>"IF (cds_tipo = 'F', cds_cpf, cds_cnpj)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>5, );
    $this->cds_items["cds_cnpj"] = array("pk"=>0, "fk"=>0, "id"=>"cds_cnpj", "description"=>"CNPJ", "title"=>"Utilizado para Pessoa Física", "type"=>"cnpj", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CNP", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>6, );
    $this->cds_items["cds_cpf"] = array("pk"=>0, "fk"=>0, "id"=>"cds_cpf", "description"=>"CPF", "title"=>"Utilizado para Pessoa Física", "type"=>"cpf", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[CPF", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>7, );
    $this->cds_items["cds_nome"] = array("pk"=>0, "fk"=>0, "id"=>"cds_nome", "description"=>"Nome / Razão Social", "title"=>"Nome ou Razão Social dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>8, );
    $this->cds_items["cds_apelido"] = array("pk"=>0, "fk"=>0, "id"=>"cds_apelido", "description"=>"Apelido / Nome Fantasia", "title"=>"Apelido ou Nome Fantasia dependendo se é Pessoa Física ou Jurídica", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>9, );
    $this->cds_items["cds_rg"] = array("pk"=>0, "fk"=>0, "id"=>"cds_rg", "description"=>"Registro Geral", "title"=>"Registro Geral", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>10, );
    $this->cds_items["cds_inscricao_municipal"] = array("pk"=>0, "fk"=>0, "id"=>"cds_inscricao_municipal", "description"=>"Inscrição Municipal", "title"=>"Inscrição Municipal", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>11, );
    $this->cds_items["cds_inscricao_estadual"] = array("pk"=>0, "fk"=>0, "id"=>"cds_inscricao_estadual", "description"=>"Inscrição Estadual", "title"=>"Inscrição Estadual", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>12, );
    $this->cds_items["cds_endereco"] = array("pk"=>0, "fk"=>0, "id"=>"cds_endereco", "description"=>"Endereço", "title"=>"Logradouro do registro", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>14, );
    $this->cds_items["cds_complemento"] = array("pk"=>0, "fk"=>0, "id"=>"cds_complemento", "description"=>"Complemento", "title"=>"Apartamento, Vila, ou outro que precise ser informado", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>15, );
    $this->cds_items["cds_bairro"] = array("pk"=>0, "fk"=>0, "id"=>"cds_bairro", "description"=>"Bairro", "title"=>"Bairro do registro", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>16, );
    $this->cds_items["cds_cep"] = array("pk"=>0, "fk"=>0, "id"=>"cds_cep", "description"=>"CEP", "title"=>"Cep do Cadastro relacionado", "type"=>"cep", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>17, );
    $this->cds_items["cds_telefone_1"] = array("pk"=>0, "fk"=>0, "id"=>"cds_telefone_1", "description"=>"Telefone", "title"=>"Telefone principal", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>18, );
    $this->cds_items["cds_celular"] = array("pk"=>0, "fk"=>0, "id"=>"cds_celular", "description"=>"Celular", "title"=>"Celular do registro selecionado", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>19, );
    $this->cds_items["cds_telefone_2"] = array("pk"=>0, "fk"=>0, "id"=>"cds_telefone_2", "description"=>"Telefone Opcional", "title"=>"Telefone opcional para contato", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>20, );
    $this->cds_items["cds_fax"] = array("pk"=>0, "fk"=>0, "id"=>"cds_fax", "description"=>"Fax", "title"=>"Fax do registro selecionado", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>21, );
    $this->cds_items["cds_email"] = array("pk"=>0, "fk"=>0, "id"=>"cds_email", "description"=>"E-mail", "title"=>"Endereço de E-mail do Cadastro", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>22, );
    $this->cds_items["cds_contato"] = array("pk"=>0, "fk"=>0, "id"=>"cds_contato", "description"=>"Contato", "title"=>"Exibido quando Pessoa Jurídica é selecionado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>23, );
    $this->cds_items["cds_observacao"] = array("pk"=>0, "fk"=>0, "id"=>"cds_observacao", "description"=>"Observação", "title"=>"Campo de observação sobre o cadastro", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>24, );


    // Atributos FK
    $this->cds_items["cds_cod_CIDADE"] = array("pk"=>0, "fk"=>1, "id"=>"cds_cod_CIDADE", "description"=>"Cidade", "title"=>"Relacionamento com a cidade", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>13, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cidade", "table"=>"TBL_CIDADE", "prefix"=>"cdd", "tag"=>"cidade", "key"=>"cdd_codigo", "description"=>"cdd_descricao", "form"=>"form", "target"=>"div-cds_cod_CIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->cds_items['cds_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'cds_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cds_items['cds_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'cds_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->cds_items['cds_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'cds_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cds_items['cds_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'cds_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->cds_items = $this->configureItemsCadastro($this->cds_items);

    $join = $this->configureJoinCadastro($this->cds_items);

    $lines = 0;
    foreach ($this->cds_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->cds_properties = array(
      'rotule'=>'Cadastro Unificado',
      'module'=>'cadastro',
      'entity'=>'Cadastro',
      'table'=>'TBL_CADASTRO',
      'join'=>$join,
      'tag'=>'cadastro',
      'prefix'=>'cds',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'cds_codigo',
      'description'=>'cds_nome',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->cds_properties['reference']) {
      foreach ($this->cds_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->cds_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->cds_properties['description']) {
      foreach ($this->cds_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->cds_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsCadastro();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function get_cds_properties(){
    ?><?php
    return $this->cds_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function get_cds_items(){
    ?><?php
    return $this->cds_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function get_cds_item($key){
    ?><?php

		$this->validateItemCadastro($key);

    return $this->cds_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function get_cds_reference(){
    ?><?php
    $key = $this->cds_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function get_cds_value($key){
    ?><?php

		$this->validateItemCadastro($key);

    return $this->cds_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function set_cds_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemCadastro($key);

    $this->cds_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->cds_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function set_cds_type($key, $type){
    ?><?php

		$this->validateItemCadastro($key);

    $this->cds_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  private  function configureJoinCadastro($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  private  function configureItemsCadastro($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->cds_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->cds_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function clearCadastro($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_cds_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_cds_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_cds_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  private  function validateItemCadastro($key){
    ?><?php

    if (!isset($this->cds_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 31/08/2015 22:06:17
   */
  public  function setStatementsCadastro(){
    ?><?php

    $this->cds_statements['0'] = "cds_codigo = '?'";

    $this->cds_statements['cds_0'] = $this->cds_statements['0'];
    $this->cds_statements['cds_1'] = "cds_tipo = '?' AND (cds_cpf = '?' OR cds_cnpj = '?')";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:19:09
   */
  public  function getStatementsCadastro(){
    ?><?php

    return $this->cds_statements;
  }


}