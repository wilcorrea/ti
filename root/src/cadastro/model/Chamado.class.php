<?php
/**
 * @copyright array software
 *
 * @author GENESON COSTA - 24/07/2015 19:15:08
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:33:33
 * @category model
 * @package cadastro
 */


class Chamado
{
  private  $chm_items = array();
  private  $chm_properties = array();
  private  $chm_parents = array();
  private  $chm_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function Chamado(){
    ?><?php

    $this->chm_items = array();
    
    // Atributos
    $this->chm_items["chm_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"chm_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->chm_items["chm_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"chm_descricao", "description"=>"Descrição", "title"=>"Descrever os detalhes sobre a ordem de serviço", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->chm_items["chm_data"] = array("pk"=>0, "fk"=>0, "id"=>"chm_data", "description"=>"Data", "title"=>"Data da ordem de serviço", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[D]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );


    // Atributos FK
    $this->chm_items["chm_cod_CATEGORIA"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_CATEGORIA", "description"=>"Categoria", "title"=>"Categoria do chamado", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Categoria", "table"=>"TBL_CATEGORIA", "prefix"=>"ctg", "tag"=>"categoria", "key"=>"ctg_codigo", "description"=>"ctg_descricao", "form"=>"form", "target"=>"div-chm_cod_CATEGORIA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->chm_items["chm_cod_CONTATO"] = array("pk"=>0, "fk"=>1, "id"=>"chm_cod_CONTATO", "description"=>"Contato", "title"=>"Contato para a ordem de serviço", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Contato", "table"=>"TBL_CONTATO", "prefix"=>"cct", "tag"=>"contato", "key"=>"cct_codigo", "description"=>"cct_descricao", "form"=>"form", "target"=>"div-chm_cod_CONTATO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->chm_items['chm_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->chm_items['chm_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'chm_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->chm_items = $this->configureItemsChamado($this->chm_items);

    $join = $this->configureJoinChamado($this->chm_items);

    $lines = 0;
    foreach ($this->chm_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->chm_properties = array(
      'rotule'=>'Chamado',
      'module'=>'cadastro',
      'entity'=>'Chamado',
      'table'=>'TBL_CHAMADO',
      'join'=>$join,
      'tag'=>'chamado',
      'prefix'=>'chm',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'chm_codigo',
      'description'=>'chm_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->chm_properties['reference']) {
      foreach ($this->chm_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->chm_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->chm_properties['description']) {
      foreach ($this->chm_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->chm_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsChamado();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function get_chm_properties(){
    ?><?php
    return $this->chm_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function get_chm_items(){
    ?><?php
    return $this->chm_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function get_chm_item($key){
    ?><?php

		$this->validateItemChamado($key);

    return $this->chm_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function get_chm_reference(){
    ?><?php
    $key = $this->chm_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function get_chm_value($key){
    ?><?php

		$this->validateItemChamado($key);

    return $this->chm_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function set_chm_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemChamado($key);

    $this->chm_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->chm_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function set_chm_type($key, $type){
    ?><?php

		$this->validateItemChamado($key);

    $this->chm_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  private  function configureJoinChamado($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  private  function configureItemsChamado($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->chm_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->chm_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function clearChamado($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_chm_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_chm_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_chm_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  private  function validateItemChamado($key){
    ?><?php

    if (!isset($this->chm_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function setStatementsChamado(){
    ?><?php

    $this->chm_statements["0"] = "chm_codigo = '?'";

    $this->chm_statements["chm_0"] = $this->chm_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 24/07/2015 19:36:21
   */
  public  function getStatementsChamado(){
    ?><?php

    return $this->chm_statements;
  }


}