<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:57
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 16:26:16
 * @category model
 * @package cadastro
 */


class Cliente
{
  private  $cli_items = array();
  private  $cli_properties = array();
  private  $cli_parents = array();
  private  $cli_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 15:30:12
   */
  public  function Cliente(){
    ?><?php

    $this->cli_items = array();
    
    // Atributos
    $this->cli_items["cli_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"cli_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>1, "order"=>1, );
    $this->cli_items["cli_codigo_externo"] = array("pk"=>0, "fk"=>0, "id"=>"cli_codigo_externo", "description"=>"Sincronização", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->cli_items["cli_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"cli_descricao", "description"=>"Nome", "title"=>"Campo calculado para ser usado em Combobox", "type"=>"calculated", "type_content"=>"(cds_nome)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->cli_items["cli_view"] = array("pk"=>0, "fk"=>0, "id"=>"cli_view", "description"=>"Visualização", "title"=>"", "type"=>"calculated", "type_content"=>"FNC_CLI_VIEW(cds_nome, (SELECT cdd_descricao FROM TBL_CIDADE WHERE cdd_codigo = cds_cod_CIDADE))", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>3, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->cli_items["cli_email_nfe"] = array("pk"=>0, "fk"=>0, "id"=>"cli_email_nfe", "description"=>"E-mail para NF-e", "title"=>"E-mail utilizado para envio da NF-e após o faturamento", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>8, );
    $this->cli_items["cli_email_danfe"] = array("pk"=>0, "fk"=>0, "id"=>"cli_email_danfe", "description"=>"E-mail para Danfe", "title"=>"E-mail utilizado para envio do Danfe após o faturamento", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>9, );
    $this->cli_items["cli_limite_credito"] = array("pk"=>0, "fk"=>0, "id"=>"cli_limite_credito", "description"=>"Limite de Crédito", "title"=>"Limite de crédito que o cliente possui. É utilizado para informar ao operador na hora me que estiver criando uma venda para ele", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>10, );
    $this->cli_items["cli_data_cadastro"] = array("pk"=>0, "fk"=>0, "id"=>"cli_data_cadastro", "description"=>"Data de Cadastro", "title"=>"Data em que o cliente foi incluso na carteira de clientes da empresa", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>11, );
    $this->cli_items["cli_ultima_visita"] = array("pk"=>0, "fk"=>0, "id"=>"cli_ultima_visita", "description"=>"Última Visita", "title"=>"Última visita feita pelo representante ao cliente", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>12, );
    $this->cli_items["cli_ultima_compra"] = array("pk"=>0, "fk"=>0, "id"=>"cli_ultima_compra", "description"=>"Última Compra", "title"=>"Data da Última compra realizada pelo cliente", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>13, );
    $this->cli_items["cli_data_inicio"] = array("pk"=>0, "fk"=>0, "id"=>"cli_data_inicio", "description"=>"Data de Nascimento / Abertura", "title"=>"Data de Nascimento ou criação da pessoa física ou jurídica", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>14, );
    $this->cli_items["cli_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"cli_tipo", "description"=>"Tipo", "title"=>"Determina o tipo de relacionamento que o cliente tem com o seu público Consumidor ou Revenda", "type"=>"option", "type_content"=>"1,Consumidor|2,Revenda", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>15, );
    $this->cli_items["cli_dia_cobranca"] = array("pk"=>0, "fk"=>0, "id"=>"cli_dia_cobranca", "description"=>"Melhor Dia", "title"=>"Dia definido como período bom de pagamento do Cliente", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>16, );
    $this->cli_items["cli_site"] = array("pk"=>0, "fk"=>0, "id"=>"cli_site", "description"=>"Site", "title"=>"Endereço de internet do Cliente", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>17, );
    $this->cli_items["cli_rede_social"] = array("pk"=>0, "fk"=>0, "id"=>"cli_rede_social", "description"=>"Rede Social", "title"=>"Canal de contato com a rede social preferida do cliente", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>18, );


    // Atributos FK
    $this->cli_items["cli_cod_USUARIO"] = array("pk"=>0, "fk"=>1, "id"=>"cli_cod_USUARIO", "description"=>"Usuário", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, "foreign"=>array("modulo"=>"manager", "entity"=>"Usuario", "table"=>"TBL_USUARIO", "prefix"=>"usu", "tag"=>"usuario", "key"=>"usu_codigo", "description"=>"usu_nome", "form"=>"form", "target"=>"div-cli_cod_USUARIO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->cli_items["cli_cod_CADASTRO"] = array("pk"=>0, "fk"=>1, "id"=>"cli_cod_CADASTRO", "description"=>"Cadastro Unificado", "title"=>"Relacionamento com dados principais", "type"=>"fk", "type_content"=>"bottom", "type_behavior"=>"parent", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, "parent"=>array("modulo"=>"cadastro", "entity"=>"Cadastro", "table"=>"TBL_CADASTRO", "prefix"=>"cds", "tag"=>"cadastro", "key"=>"cds_codigo", "description"=>"cds_nome", "form"=>"form", "target"=>"div-cli_cod_CADASTRO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->cli_items["cli_cod_REPRESENTANTE"] = array("pk"=>0, "fk"=>1, "id"=>"cli_cod_REPRESENTANTE", "description"=>"Representante", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>6, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Representante", "table"=>"TBL_CADASTRO_REPRESENTANTE", "prefix"=>"rpr", "tag"=>"representante", "key"=>"rpr_codigo", "description"=>"rpr_descricao", "form"=>"form", "target"=>"div-cli_cod_REPRESENTANTE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->cli_items['cli_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'cli_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cli_items['cli_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'cli_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->cli_items['cli_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'cli_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->cli_items['cli_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'cli_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->cli_items = $this->configureItemsCliente($this->cli_items);

    $join = $this->configureJoinCliente($this->cli_items);

    $lines = 0;
    foreach ($this->cli_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->cli_properties = array(
      'rotule'=>'Cliente',
      'module'=>'cadastro',
      'entity'=>'Cliente',
      'table'=>'TBL_CADASTRO_CLIENTE',
      'join'=>$join,
      'tag'=>'cliente',
      'prefix'=>'cli',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'cli_codigo',
      'description'=>'cli_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro excluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          ////'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          ////'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->cli_properties['reference']) {
      foreach ($this->cli_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->cli_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->cli_properties['description']) {
      foreach ($this->cli_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->cli_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsCliente();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function get_cli_properties(){
    ?><?php
    return $this->cli_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function get_cli_items(){
    ?><?php
    return $this->cli_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function get_cli_item($key){
    ?><?php

		$this->validateItemCliente($key);

    return $this->cli_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function get_cli_reference(){
    ?><?php
    $key = $this->cli_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function get_cli_value($key){
    ?><?php

		$this->validateItemCliente($key);

    return $this->cli_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function set_cli_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemCliente($key);

    $this->cli_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->cli_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function set_cli_type($key, $type){
    ?><?php

		$this->validateItemCliente($key);

    $this->cli_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  private  function configureJoinCliente($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 15:54:34
   */
  private  function configureItemsCliente($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->cli_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->cli_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function clearCliente($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_cli_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_cli_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_cli_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  private  function validateItemCliente($key){
    ?><?php

    if (!isset($this->cli_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function setStatementsCliente(){
    ?><?php

    $this->cli_statements["0"] = "cli_codigo = '?'";

    $this->cli_statements["cli_0"] = $this->cli_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:38:58
   */
  public  function getStatementsCliente(){
    ?><?php

    return $this->cli_statements;
  }


}