<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:30:27
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/09/2015 17:42:22
 * @category model
 * @package cadastro
 */


class Representante
{
  private  $rpr_items = array();
  private  $rpr_properties = array();
  private  $rpr_parents = array();
  private  $rpr_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   */
  public  function Representante(){
    ?><?php

    $this->rpr_items = array();
    
    // Atributos
    $this->rpr_items["rpr_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"rpr_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->rpr_items["rpr_codigo_externo"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_codigo_externo", "description"=>"Sincronização", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->rpr_items["rpr_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_descricao", "description"=>"Descrição", "title"=>"Atributo calculado usado nos combobox", "type"=>"calculated", "type_content"=>"(cds_nome)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->rpr_items["rpr_comissao"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_comissao", "description"=>"Comissão", "title"=>"Comissão padrão do Representante", "type"=>"percent", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->rpr_items["rpr_supervisor"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_supervisor", "description"=>"Supervisor", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->rpr_items["rpr_tipo_comissao"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_tipo_comissao", "description"=>"Tipo de Comissão", "title"=>"Tipo de Comissão utilizado pelo Representante", "type"=>"option", "type_content"=>"1,Fixa|2,Por Produto", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>8, );
    $this->rpr_items["rpr_acerto_comissao"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_acerto_comissao", "description"=>"Forma de Acerto", "title"=>"Forma com a qual é feito o acerto da comissão", "type"=>"list", "type_content"=>"1,Dia de Fechamento|2,Realização da Venda|3,No Recebimento", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->rpr_items["rpr_dia_fechamento"] = array("pk"=>0, "fk"=>0, "id"=>"rpr_dia_fechamento", "description"=>"Dia de Fechamento", "title"=>"Dia do mês em que o acerto da comissão é feito com o Representante", "type"=>"min/max", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>10, );


    // Atributos FK
    $this->rpr_items["rpr_cod_USUARIO"] = array("pk"=>0, "fk"=>1, "id"=>"rpr_cod_USUARIO", "description"=>"Usuário", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, "foreign"=>array("modulo"=>"manager", "entity"=>"Usuario", "table"=>"TBL_USUARIO", "prefix"=>"usu", "tag"=>"usuario", "key"=>"usu_codigo", "description"=>"usu_nome", "form"=>"form", "target"=>"div-rpr_cod_USUARIO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->rpr_items["rpr_cod_CADASTRO"] = array("pk"=>0, "fk"=>1, "id"=>"rpr_cod_CADASTRO", "description"=>"Cadastro Unificado", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"parent", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, "parent"=>array("modulo"=>"cadastro", "entity"=>"Cadastro", "table"=>"TBL_CADASTRO", "prefix"=>"cds", "tag"=>"cadastro", "key"=>"cds_codigo", "description"=>"cds_nome", "form"=>"form", "target"=>"div-rpr_cod_CADASTRO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->rpr_items['rpr_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'rpr_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->rpr_items['rpr_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'rpr_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->rpr_items['rpr_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'rpr_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->rpr_items['rpr_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'rpr_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->rpr_items = $this->configureItemsRepresentante($this->rpr_items);

    $join = $this->configureJoinRepresentante($this->rpr_items);

    $lines = 0;
    foreach ($this->rpr_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->rpr_properties = array(
      'rotule'=>'Representante',
      'module'=>'cadastro',
      'entity'=>'Representante',
      'table'=>'TBL_CADASTRO_REPRESENTANTE',
      'join'=>$join,
      'tag'=>'representante',
      'prefix'=>'rpr',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'rpr_codigo',
      'description'=>'rpr_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->rpr_properties['reference']) {
      foreach ($this->rpr_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->rpr_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->rpr_properties['description']) {
      foreach ($this->rpr_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->rpr_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsRepresentante();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   */
  public  function get_rpr_properties(){
    ?><?php
    return $this->rpr_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   */
  public  function get_rpr_items(){
    ?><?php
    return $this->rpr_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   */
  public  function get_rpr_item($key){
    ?><?php

		$this->validateItemRepresentante($key);

    return $this->rpr_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   */
  public  function get_rpr_reference(){
    ?><?php
    $key = $this->rpr_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function get_rpr_value($key){
    ?><?php

		$this->validateItemRepresentante($key);

    return $this->rpr_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function set_rpr_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemRepresentante($key);

    $this->rpr_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->rpr_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function set_rpr_type($key, $type){
    ?><?php

		$this->validateItemRepresentante($key);

    $this->rpr_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  private  function configureJoinRepresentante($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  private  function configureItemsRepresentante($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->rpr_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->rpr_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function clearRepresentante($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_rpr_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_rpr_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_rpr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  private  function validateItemRepresentante($key){
    ?><?php

    if (!isset($this->rpr_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function setStatementsRepresentante(){
    ?><?php

    $this->rpr_statements["0"] = "rpr_codigo = '?'";

    $this->rpr_statements["rpr_0"] = $this->rpr_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 17:19:17
   */
  public  function getStatementsRepresentante(){
    ?><?php

    return $this->rpr_statements;
  }


}