<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/10/2013 10:56:59
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 27/03/2015 08:34:45
 * @category controller
 * @package manager
 */


class AtualizacaoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   */
  public  function AtualizacaoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');
    
    $entity = new Atualizacao();
    $properties = $entity->get_atl_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   */
  public  function getRowsAtualizacaoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    $items = $atualizacao->get_atl_items();
    $properties = $atualizacao->get_atl_properties();

    $statements = $atualizacao->getStatementsAtualizacao();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   */
  public  function getAtualizacaoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    $atualizacaos = null;

    $rows = $this->getRowsAtualizacaoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $atualizacaos = array();
      foreach ($rows as $row) {
        $atualizacao_set = clone $atualizacao;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $atualizacao_set->set_atl_value($key, $item['value'], $reference);
        }
        $atualizacao_set->clearAtualizacao($not_clear);
        $atualizacaos[] = $atualizacao_set;
      }
    }

    return $atualizacaos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:14
   */
  public  function getColumnAtualizacaoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    $properties = $atualizacao->get_atl_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $atualizacao->getStatementsAtualizacao();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getCountAtualizacaoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    $properties = $atualizacao->get_atl_properties();

    $statements = $atualizacao->getStatementsAtualizacao();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnAtualizacaoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  private  function beforeAtualizacaoCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsAtualizacaoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_atl_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_atl_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_atl_value($item['id'], $value);
            } else {
              $object->set_atl_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsAtualizacaoCtrl("");
      $properties = $this->getPropertiesAtualizacaoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_atl_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnAtualizacaoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function addAtualizacaoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesAtualizacaoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_atl_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesAtualizacaoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_atl_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setAtualizacaoCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function setAtualizacaoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_atl_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesAtualizacaoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_atl_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function removeAtualizacaoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_atl_items();
    $properties = $this->getPropertiesAtualizacaoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_atl_value($action['field'], $action['value']);
        $object->clearAtualizacao(array($action['field']));

        $remove = $this->setAtualizacaoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 27/03/2015 15:23:00
   */
  private  function afterAtualizacaoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    System::import('class', 'resource', 'Update', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_atl_value($object->get_atl_reference());
    } else if ($param === 'add') {
      $object->set_atl_value($object->get_atl_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_atl_items();

      $fields = Controller::after($value, $items, 'atl');
      if (count($fields)) {
        $executed = $this->executeAtualizacaoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $atl_arquivo = APP_PATH . "files/" . $this->getColumnAtualizacaoCtrl('atl_arquivo', '0:' . $value);
    if ($param === 'add') {

      $response = Update::merge($atl_arquivo, VERSION);

      $atl_arquivo = $response['file'];
      $atl_backup = $response['backup'];
      if ($atl_backup) {

        $object->set_atl_value('atl_arquivo', $atl_arquivo);
        $object->set_atl_value('atl_backup', $atl_backup);

        $this->setAtualizacaoCtrl($object);

      } else {
        $after = false;
      }
    }

    $properties = $object->get_atl_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  private  function executeAtualizacaoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    $properties = $atualizacao->get_atl_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $atualizacao->getStatementsAtualizacao();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", atl_responsavel = '" . System::getUser(false) . "', atl_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  private  function verifyAtualizacaoCtrl($object){
		?><?php
	
		$items = $object->get_atl_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getObjectAtualizacaoCtrl($value){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $object = new Atualizacao();

    $properties = $object->get_atl_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $atualizacao = null;

    $atualizacaos = $this->getAtualizacaoCtrl($where, "", "", "0", "1");
    if (is_array($atualizacaos)) {
      $atualizacao = $atualizacaos[0];
    }

    return $atualizacao;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getReplacesAtualizacaoCtrl($reference){
		?><?php

		System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

		$atualizacao = new Atualizacao();

    $replaces = array();

    $where = "atl_codigo = '" . $reference . "'";
    $atualizacaos = $this->getAtualizacaoCtrl($where, "", "");
    if (is_array($atualizacaos)) {
      $atualizacao = $atualizacaos[0];

      $items = $atualizacao->get_atl_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function configureAtualizacaoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getPropertiesAtualizacaoCtrl(){
    ?><?php

      System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

      $atualizacao = new Atualizacao();

      $properties = $atualizacao->get_atl_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getItemsAtualizacaoCtrl($search){
    ?><?php

    System::import('m', 'manager', 'Atualizacao', 'src', true, '{project.rsc}');

    $atualizacao = new Atualizacao();

    if ($search) {
      $object = $this->getObjectAtualizacaoCtrl($search);
      if (!is_null($object)) {
        $atualizacao = $object;
      }
    }
    
    $items = $atualizacao->get_atl_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $atualizacao->get_atl_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function getHistoryAtualizacaoCtrl($items){
    ?><?php
    
    $defaults = array(
      'atl_registro'=>array('id'=>'atl_registro', 'value'=>date('d/m/Y H:i:s')),
      'atl_criador'=>array('id'=>'atl_criador', 'value'=>System::getUser()),
      'atl_alteracao'=>array('id'=>'atl_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'atl_responsavel'=>array('id'=>'atl_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:57:06
   */
  public  function operationAtualizacaoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'Atualizacao', 'src', true);

    $atualizacao = new Atualizacao();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $atualizacao->get_atl_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $atualizacao->set_atl_value($id, $item['value']);
    }
    
    $method = $action . "AtualizacaoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyAtualizacaoCtrl($atualizacao, $operation);
      if ($verify) {

        $before = $this->beforeAtualizacaoCtrl($atualizacao, $operation);
        if ($before) {

          $response = $this->$method($atualizacao);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterAtualizacaoCtrl($atualizacao, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'AtualizacaoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 20:20:15
   */
  public  function copyAtualizacaoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesAtualizacaoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_atl_value($reference, null);
    }

    return $this->addAtualizacaoCtrl($object, $validate, $debug);
  }


}