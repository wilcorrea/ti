<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 09:38:24
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 12/12/2014 17:43:09
 * @category controller
 * @package manager
 */


class AtualizacaoItemCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:25
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:25
   */
  public  function AtualizacaoItemCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:25
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:25
   */
  public  function getAtualizacaoItemCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $atualizacaoItems = $atualizacaoItemDAO->getAtualizacaoItemDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $atualizacaoItems;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function getCountAtualizacaoItemCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $total = $atualizacaoItemDAO->getCountAtualizacaoItemDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function addAtualizacaoItemCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');    
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $this->beforeAtualizacaoItemCtrl($object, "add");
    $add = $atualizacaoItemDAO->addAtualizacaoItemDAO($object, $validate, $debug, $presave, $copy);
    $this->afterAtualizacaoItemCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function removeAtualizacaoItemCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $this->beforeAtualizacaoItemCtrl($object, "remove");
    $remove = $atualizacaoItemDAO->removeAtualizacaoItemDAO($object, $validate, $debug);
    $this->afterAtualizacaoItemCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function setAtualizacaoItemCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $this->beforeAtualizacaoItemCtrl($object, "set");
    $set = $atualizacaoItemDAO->setAtualizacaoItemDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterAtualizacaoItemCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function beforeAtualizacaoItemCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_ati_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_ati_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_ati_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }
    }
    
    if ($param == 'remove') {
      $items = $object->get_ati_items();
      $properties = $object->get_ati_properties();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_ati_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $parentModule = $item['parent']['modulo'];
            $parentEntity = $item['parent']['entity'];
            $parentReference = $item['parent']['key'];
            
            $value = $this->getColumnConteudoModuloCtrl($key, $whereObject);
            
            $whereParent = $parentReference . " = '" . $value . "'";

            System::desire('c', $parentModule, $parentEntity, 'src');
            System::desire('m', $parentModule, $parentEntity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($whereParent, "", "");
            $parent = $parents[0];
            
            $parents = $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:26
   */
  public  function afterAtualizacaoItemCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_ati_value($object->get_ati_reference());
    }

    if($param === 'add'){
      $object->set_ati_value($object->get_ati_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_ati_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

		$properties = $object->get_ati_properties();
    if(isset($properties['notification'])){
      if($properties['notification']){
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   */
  public  function getColumnAtualizacaoItemCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $column = $atualizacaoItemDAO->getColumnAtualizacaoItemDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   */
  public  function executeAtualizacaoItemCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');

    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $executed = $atualizacaoItemDAO->executeAtualizacaoItemDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:27
   */
  public  function verifyAtualizacaoItemCtrl($object){
		?><?php
	
		$items = $object->get_ati_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $usu_codigo 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:18:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2013 13:17:40
   */
  public  function showAtualizacaoItemCtrl($usu_codigo, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'AtualizacaoItem', 'src', true, '');
    
    $atualizacaoItemDAO = new AtualizacaoItemDAO($this->path, $this->database);
    $show = $atualizacaoItemDAO->showAtualizacaoItemDAO($usu_codigo, $validate, $debug);
    
    return $show;
  }


}