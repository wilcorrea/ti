<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 06/05/2013 00:53:52
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:54
 * @category controller
 * @package manager
 */


class EmailEnvioCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public  function EmailEnvioCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:48
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:48
   */
  public  function getEmailEnvioCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $emailEnvios = $emailEnvioDAO->getEmailEnvioDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $emailEnvios;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function getCountEmailEnvioCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $total = $emailEnvioDAO->getCountEmailEnvioDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function addEmailEnvioCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');    
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $this->beforeEmailEnvioCtrl($object, "add");
    $add = $emailEnvioDAO->addEmailEnvioDAO($object, $validate, $debug, $presave, $copy);
    $this->afterEmailEnvioCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function removeEmailEnvioCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $this->beforeEmailEnvioCtrl($object, "remove");
    $remove = $emailEnvioDAO->removeEmailEnvioDAO($object, $validate, $debug);
    $this->afterEmailEnvioCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function setEmailEnvioCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $this->beforeEmailEnvioCtrl($object, "set");
    $set = $emailEnvioDAO->setEmailEnvioDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterEmailEnvioCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function beforeEmailEnvioCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_eme_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_eme_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_eme_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function afterEmailEnvioCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_eme_value($object->get_eme_reference());
    }

    if($param === 'add'){
      $object->set_eme_value($object->get_eme_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_eme_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function getColumnEmailEnvioCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');
    
    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $column = $emailEnvioDAO->getColumnEmailEnvioDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function executeEmailEnvioCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailEnvio', 'src', true, '');

    $emailEnvioDAO = new EmailEnvioDAO($this->path, $this->database);
    $executed = $emailEnvioDAO->executeEmailEnvioDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:49
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:49
   */
  public  function verifyEmailEnvioCtrl($object){
		?><?php
	
		$items = $object->get_eme_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * 
   * 
   * @param string $eme_id 
   * @param string $eme_nome 
   * @param string $eme_email 
   * @param string $eme_assunto 
   * @param string $eme_corpo 
   * @param string $eme_modelo 
   * @param string $eme_copia 
   * @param string $eme_resposta 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 30/06/2014 18:04:30
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 01/07/2014 15:11:12
   */
  public  function createEmailEnvioCtrl($eme_id, $eme_nome, $eme_email, $eme_assunto, $eme_corpo, $eme_modelo = "", $eme_copia = "", $eme_resposta = ""){
    ?><?php

    System::desire('m', 'manager', 'EmailEnvio', 'src', true, '');

    $emailEnvio = new EmailEnvio();

    $emailEnvio->set_eme_value('eme_id', $eme_id);

    $emailEnvio->set_eme_value('eme_nome', $eme_nome);
    $emailEnvio->set_eme_value('eme_email', $eme_email);

    $emailEnvio->set_eme_value('eme_assunto', $eme_assunto);
    $emailEnvio->set_eme_value('eme_corpo', $eme_corpo);

    $emailEnvio->set_eme_value('eme_modelo', $eme_modelo);

    if (is_array($eme_copia)) {
      $eme_copia = serialize($eme_copia);
    }
    $emailEnvio->set_eme_value('eme_copia', $eme_copia);
    $emailEnvio->set_eme_value('eme_resposta', $eme_resposta);

    $eme_codigo = $this->addEmailEnvioCtrl($emailEnvio);

    return $eme_codigo;
  }

  /**
   * 
   * 
   * @param int $eme_codigo 
   * @param boolean $eme_enviado 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 30/06/2014 18:39:37
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/06/2014 18:40:21
   */
  public  function atualizarEmailEnvioCtrl($eme_codigo, $eme_enviado = false){
    ?><?php

    $where_eme = "eme_codigo = '" . $eme_codigo . "' AND eme_enviado = 0";
    $executed = $this->executeEmailEnvioCtrl($where_eme, "eme_enviado = '" . $eme_enviado . "', eme_tentativa = (eme_tentativa + 1)");

    return $executed;
  }


}