<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/05/2013 21:56:15
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:37
 * @category controller
 * @package manager
 */


class EmailModeloCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function EmailModeloCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function getEmailModeloCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $emailModelos = $emailModeloDAO->getEmailModeloDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $emailModelos;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function getCountEmailModeloCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $total = $emailModeloDAO->getCountEmailModeloDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function addEmailModeloCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');    
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $this->beforeEmailModeloCtrl($object, "add");
    $add = $emailModeloDAO->addEmailModeloDAO($object, $validate, $debug, $presave, $copy);
    $this->afterEmailModeloCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function removeEmailModeloCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $this->beforeEmailModeloCtrl($object, "remove");
    $remove = $emailModeloDAO->removeEmailModeloDAO($object, $validate, $debug);
    $this->afterEmailModeloCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function setEmailModeloCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $this->beforeEmailModeloCtrl($object, "set");
    $set = $emailModeloDAO->setEmailModeloDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterEmailModeloCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function beforeEmailModeloCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_emo_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_emo_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_emo_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function afterEmailModeloCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_emo_value($object->get_emo_reference());
    }

    if($param === 'add'){
      $object->set_emo_value($object->get_emo_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_emo_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:26
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:26
   */
  public  function getColumnEmailModeloCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');
    
    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $column = $emailModeloDAO->getColumnEmailModeloDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:26
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:26
   */
  public  function executeEmailModeloCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailModelo', 'src', true, '');

    $emailModeloDAO = new EmailModeloDAO($this->path, $this->database);
    $executed = $emailModeloDAO->executeEmailModeloDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:26
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:26
   */
  public  function verifyEmailModeloCtrl($object){
		?><?php
	
		$items = $object->get_emo_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }


}