<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/09/2013 17:12:50
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 10:19:53
 * @category controller
 * @package manager
 */


class EmailQueueCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   */
  public  function EmailQueueCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   */
  public  function getEmailQueueCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $emailQueues = $emailQueueDAO->getEmailQueueDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $emailQueues;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:05
   */
  public  function getCountEmailQueueCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $total = $emailQueueDAO->getCountEmailQueueDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function addEmailQueueCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');    
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $this->beforeEmailQueueCtrl($object, "add");
    
		$verify = $this->verifyEmailQueueCtrl($object);
		if (count($verify) > 0) {
			
			$add = 0;
		} else {
			
			$add = $emailQueueDAO->addEmailQueueDAO($object, $validate, $debug, $presave, $copy);
    	$this->afterEmailQueueCtrl($object, "add", $add, $copy);
		}
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function removeEmailQueueCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $this->beforeEmailQueueCtrl($object, "remove");
    $remove = $emailQueueDAO->removeEmailQueueDAO($object, $validate, $debug);
    $this->afterEmailQueueCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function setEmailQueueCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $object = $this->beforeEmailQueueCtrl($object, "set");
		
		$verify = $this->verifyEmailQueueCtrl($object);
		if (count($verify) > 0) {
			
			$set = 0;
		} else {
			
			$set = $emailQueueDAO->setEmailQueueDAO($object, $validate, $debug, $trigger);
			if($trigger === true){
				$this->afterEmailQueueCtrl($object, "set");
			}
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function beforeEmailQueueCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_emq_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_emq_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_emq_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }
    }
    
    if ($param == 'remove') {
      $items = $object->get_emq_items();
      $properties = $object->get_emq_properties();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_emq_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $parentModule = $item['parent']['modulo'];
            $parentEntity = $item['parent']['entity'];
            $parentReference = $item['parent']['key'];
            
            $value = $this->getColumnConteudoModuloCtrl($key, $whereObject);
            
            $whereParent = $parentReference . " = '" . $value . "'";

            System::desire('c', $parentModule, $parentEntity, 'src');
            System::desire('m', $parentModule, $parentEntity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($whereParent, "", "");
            $parent = $parents[0];
            
            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function afterEmailQueueCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_emq_value($object->get_emq_reference());
    }

    if($param === 'add'){
      $object->set_emq_value($object->get_emq_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_emq_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

		$properties = $object->get_emq_properties();
    if(isset($properties['notification'])){
      if($properties['notification']){
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function getColumnEmailQueueCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');
    
    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $column = $emailQueueDAO->getColumnEmailQueueDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function executeEmailQueueCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'EmailQueue', 'src', true, '{project.rsc}');

    $emailQueueDAO = new EmailQueueDAO($this->path, $this->database);
    $executed = $emailQueueDAO->executeEmailQueueDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:06
   */
  public  function verifyEmailQueueCtrl($object){
		?><?php
	
		$items = $object->get_emq_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }


}