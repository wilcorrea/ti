<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/04/2013 21:29:44
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 19/05/2014 20:20:26
 * @category controller
 * @package manager
 */


class ErroCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function ErroCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function getErroCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $erros = $erroDAO->getErroDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $erros;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function getCountErroCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $total = $erroDAO->getCountErroDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function addErroCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');    
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "add");
    $add = $erroDAO->addErroDAO($object, $validate, $debug, $presave, $copy);
    $this->afterErroCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function removeErroCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "remove");
    $remove = $erroDAO->removeErroDAO($object, $validate, $debug);
    $this->afterErroCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function setErroCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $this->beforeErroCtrl($object, "set");
    $set = $erroDAO->setErroDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterErroCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function beforeErroCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_err_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_err_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_err_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }


    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 18:47:16
   */
  public  function afterErroCtrl($object, $param, $value = 0, $copy = 0){
?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_err_value($object->get_err_reference());
    }

    if($param === 'add'){
      $object->set_err_value($object->get_err_reference(), $value);
    }

		if($object->get_err_value('err_corrigido')){
      $err_descricao = $this->getColumnErroCtrl("err_descricao", "err_codigo = '".$object->get_err_value('err_codigo')."'");
      if($err_descricao){
        $this->executeErroCtrl("err_descricao = '".addslashes($err_descricao)."'", "err_corrigido = 1");
      }
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_err_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
	
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function getColumnErroCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php
    System::desire("d", 'manager', 'Erro', 'src', true, '');
    
    $erroDAO = new ErroDAO($this->path, $this->database);
    $column = $erroDAO->getColumnErroDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:22
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:22
   */
  public  function executeErroCtrl($where, $update, $validate = true, $debug = false){
  ?><?php
    System::desire('d', 'manager', 'Erro', 'src', true, '');

    $erroDAO = new ErroDAO($this->path, $this->database);
    $executed = $erroDAO->executeErroDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:23
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:23
   */
  public  function verifyErroCtrl($object){
	?><?php

	$items = $object->get_err_items();
	$error_messages = array();

	foreach ($items as $key => $item) {
		if (!is_null($item['value'])) {
			/*
			 * Validation comes here!
			 * If the validation fails, add an item in the array $error_messages
			 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
			 */
		}
	}

	return $error_messages;
  }

  /**
   * Método responsável por criar um novo registro de erro apenas quando sua ocorrência ainda não existir
   * 
   * @param object $erro 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 14:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/08/2013 13:46:28
   */
  public  function registerErroCtrl($erro){
    ?><?php

    System::desire('c', 'manager', 'ErroInvalido', 'src', true, '');
    
    $erroInvalidoCtrl = new ErroInvalidoCtrl(APP_PATH);
    
    System::desire('class', 'resource', 'Browser', 'core', true);
    
    $client = new Browser();
    
    $register = (object) array('protocolo'=>'', 'total'=>'');
    
    $err_comando = addslashes($erro->get_err_value('err_comando'));
    $err_descricao = addslashes($erro->get_err_value('err_descricao'));

    $err_codigo = "";
    $err_total = 0;
    
    if($err_descricao){
      
      $aceitar = $erroInvalidoCtrl->getCountErroInvalidoCtrl("eri_descricao = '".$err_comando."'");

      if(!$aceitar){

        $where = "err_descricao = '".$err_descricao."'";

        if($erro->get_err_value('err_codigo')){
          $where = "err_codigo = '".$erro->get_err_value('err_codigo')."'";
        }

        $erros = $this->getErroCtrl($where, "", "", 0, 1);
        if(is_array($erros)){
          $erro = $erros[0];
          $err_total = $erro->get_err_value('err_total');
          $err_total++;

          $mode = 'set';
        }else{
          $mode = 'add';
        }

        $erro->set_err_value('err_total', $err_total);
        $erro->set_err_value('err_so',$client->platform);
        $erro->set_err_value('err_navegador',$client->browser);
        $erro->set_err_value('err_versao',$client->fullVersion);
        $erro->set_err_value('err_endereco',System::address());


        if($mode == 'add'){

          $err_codigo = $this->addErroCtrl($erro);
        }else{

          $err_codigo = $erro->get_err_value('err_codigo');
          $this->setErroCtrl($erro);
        }

        $register->protocolo = $err_codigo;    
        $register->total = $err_total;

      }

    }
    
    return $register;
  }


}