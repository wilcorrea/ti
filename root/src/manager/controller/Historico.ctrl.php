<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 01/05/2013 09:09:56
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/05/2014 21:19:34
 * @category controller
 * @package manager
 */


class HistoricoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function HistoricoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');
    
    $entity = new Historico();
    $properties = $entity->get_hst_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = false;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function getRowsHistoricoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    $items = $historico->get_hst_items();
    $properties = $historico->get_hst_properties();

    $statements = $historico->getStatementsHistorico();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function getHistoricoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    $historicos = null;

    $rows = $this->getRowsHistoricoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $historicos = array();
      foreach ($rows as $row) {
        $historico_set = clone $historico;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $historico_set->set_hst_value($key, $item['value'], $reference);
        }
        $historico_set->clearHistorico($not_clear);
        $historicos[] = $historico_set;
      }
    }

    return $historicos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function getColumnHistoricoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    $properties = $historico->get_hst_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $historico->getStatementsHistorico();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function getCountHistoricoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    $properties = $historico->get_hst_properties();

    $statements = $historico->getStatementsHistorico();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnHistoricoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function beforeHistoricoCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsHistoricoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_hst_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_hst_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_hst_value($item['id'], $value);
            } else {
              $object->set_hst_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsHistoricoCtrl("");
      $properties = $this->getPropertiesHistoricoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_hst_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnHistoricoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function addHistoricoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesHistoricoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_hst_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyHistoricoCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesHistoricoCtrl();
        $table = $properties['table'];

        $object = $this->beforeHistoricoCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_hst_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterHistoricoCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setHistoricoCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function setHistoricoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyHistoricoCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_hst_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesHistoricoCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeHistoricoCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_hst_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterHistoricoCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function removeHistoricoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_hst_items();
    $properties = $this->getPropertiesHistoricoCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_hst_value($action['field'], $action['value']);
        $object->clearHistorico(array($action['field']));

        $remove = $this->setHistoricoCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

        $object = $this->beforeHistoricoCtrl($object, "remove");

        $sql = $this->dao->buildDelete($table, $where);

        $remove = $this->dao->executeSQL($sql, $this->history, $validate);

        $this->afterHistoricoCtrl($object, "remove");

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function afterHistoricoCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_hst_value($object->get_hst_reference());
    }

    if ($param === 'add') {
      $object->set_hst_value($object->get_hst_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_hst_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_hst_value($key, $items[$key]['type_content']);
            $object->set_hst_type($key, 'execute');
            $update = true;
          }
        } else if ($items[$key]['type'] === 'image') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_hst_value($object->get_hst_reference());
              $source = APP_PATH . $items[$key]['value'];
              $location = $file['path'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (!is_dir(APP_PATH . $file['path'])) {
                mkdir(APP_PATH . $file['path'], 0755, true);
              }

              if (is_writable(APP_PATH . $file['path'])) {
                if (file_exists($source)) {
                  $copy = copy($source, $destin);
                  if ($copy) {
                    $object->set_hst_value($key, $location);
                    $update = true;
                  } else {
                    System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                  }
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        } else if ($items[$key]['type'] === 'file') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_hst_value($object->get_hst_reference());
              $source = $object->get_hst_value($key);

              $location = $file['path'] . $file['name'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (is_writable(APP_PATH . $file['path'])) {
                $copy = copy(APP_PATH . $source, $destin);
                if ($copy) {
                  $object->set_hst_value($key, $location);
                  $update = true;
                } else {
                  System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        }
      }

      if ($update) {
        $this->setHistoricoCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_hst_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function executeHistoricoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    $properties = $historico->get_hst_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $historico->getStatementsHistorico();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", hst_responsavel = '" . System::getUser(false) . "', hst_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function verifyHistoricoCtrl($object){
		?><?php
	
		$items = $object->get_hst_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function getObjectHistoricoCtrl($value){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $object = new Historico();

    $properties = $object->get_hst_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $historico = null;

    $historicos = $this->getHistoricoCtrl($where, "", "", "0", "1");
    if (is_array($historicos)) {
      $historico = $historicos[0];
    }

    return $historico;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function getReplacesHistoricoCtrl($reference){
		?><?php

		System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

		$historico = new Historico();

    $replaces = array();

    $where = "hst_codigo = '" . $reference . "'";
    $historicos = $this->getHistoricoCtrl($where, "", "");
    if (is_array($historicos)) {
      $historico = $historicos[0];

      $items = $historico->get_hst_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function configureHistoricoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "edit":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function getPropertiesHistoricoCtrl(){
    ?><?php

      System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

      $historico = new Historico();

      $properties = $historico->get_hst_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function getItemsHistoricoCtrl($search){
    ?><?php

    System::import('m', 'manager', 'Historico', 'src', true, '{project.rsc}');

    $historico = new Historico();

    if ($search) {
      $object = $this->getObjectHistoricoCtrl($search);
      if (!is_null($object)) {
        $historico = $object;
      }
    }
    
    $items = $historico->get_hst_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $historico->get_hst_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function getHistoryHistoricoCtrl($items){
    ?><?php
    
    $defaults = array(
      'hst_registro'=>array('id'=>'hst_registro', 'value'=>date('d/m/Y H:i:s')),
      'hst_criador'=>array('id'=>'hst_criador', 'value'=>System::getUser()),
      'hst_alteracao'=>array('id'=>'hst_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'hst_responsavel'=>array('id'=>'hst_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:10
   */
  public  function operationHistoricoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'Historico', 'src', true);

    $historico = new Historico();

    foreach ($items as $id => $item) {
      $historico->set_hst_value($id, $item['value']);
    }
    
    $method = $operation . "HistoricoCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($historico);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'HistoricoCtrl'");
    }

    return $result;
  }


}