<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:41:44
 * @category controller
 * @package manager
 */


class MenuFuncaoUsuarioTipoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   */
  public  function MenuFuncaoUsuarioTipoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');
    
    $entity = new MenuFuncaoUsuarioTipo();
    $properties = $entity->get_mft_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:04
   */
  public  function getRowsMenuFuncaoUsuarioTipoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $items = $menuFuncaoUsuarioTipo->get_mft_items();
    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();

    $statements = $menuFuncaoUsuarioTipo->getStatementsMenuFuncaoUsuarioTipo();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:07
   */
  public  function getMenuFuncaoUsuarioTipoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $menuFuncaoUsuarioTipos = null;

    $rows = $this->getRowsMenuFuncaoUsuarioTipoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $menuFuncaoUsuarioTipos = array();
      foreach ($rows as $row) {
        $menuFuncaoUsuarioTipo_set = clone $menuFuncaoUsuarioTipo;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $menuFuncaoUsuarioTipo_set->set_mft_value($key, $item['value'], $reference);
        }
        $menuFuncaoUsuarioTipo_set->clearMenuFuncaoUsuarioTipo($not_clear);
        $menuFuncaoUsuarioTipos[] = $menuFuncaoUsuarioTipo_set;
      }
    }

    return $menuFuncaoUsuarioTipos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:09
   */
  public  function getColumnMenuFuncaoUsuarioTipoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $menuFuncaoUsuarioTipo->getStatementsMenuFuncaoUsuarioTipo();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:11
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:11
   */
  public  function getCountMenuFuncaoUsuarioTipoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();

    $statements = $menuFuncaoUsuarioTipo->getStatementsMenuFuncaoUsuarioTipo();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnMenuFuncaoUsuarioTipoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:12
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:12
   */
  private  function beforeMenuFuncaoUsuarioTipoCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsMenuFuncaoUsuarioTipoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(PATH_APP);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_mft_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_mft_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_mft_value($item['id'], $value);
            } else {
              $object->set_mft_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsMenuFuncaoUsuarioTipoCtrl("");
      $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_mft_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnMenuFuncaoUsuarioTipoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(PATH_APP);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:12
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:12
   */
  public  function saveMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_mft_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_mft_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setMenuFuncaoUsuarioTipoCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:13
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:14
   */
  public  function setMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_mft_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_mft_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:15
   */
  public  function removeMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_mft_items();
    $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings['remove'])) {

      $settings = $operation->settings['remove'];

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_mft_value($action['field'], $action['value']);
        $object->clearMenuFuncaoUsuarioTipo(array($action['field']));

        $remove = $this->setMenuFuncaoUsuarioTipoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:15
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:15
   */
  private  function afterMenuFuncaoUsuarioTipoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_mft_value($object->get_mft_reference());
    } else if ($param === 'add') {
      $object->set_mft_value($object->get_mft_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_mft_items();

      $fields = Controller::after($value, $items, 'mft');
      if (count($fields)) {
        $executed = $this->executeMenuFuncaoUsuarioTipoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_mft_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:17
   */
  public  function executeMenuFuncaoUsuarioTipoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $menuFuncaoUsuarioTipo->getStatementsMenuFuncaoUsuarioTipo();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", mft_responsavel = '" . System::getUser(false) . "', mft_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:18
   */
  private  function verifyMenuFuncaoUsuarioTipoCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_mft_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:19
   */
  public  function getObjectMenuFuncaoUsuarioTipoCtrl($value){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $object = new MenuFuncaoUsuarioTipo();

    $properties = $object->get_mft_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $menuFuncaoUsuarioTipo = null;

    $menuFuncaoUsuarioTipos = $this->getMenuFuncaoUsuarioTipoCtrl($where, "", "", "0", "1");
    if (is_array($menuFuncaoUsuarioTipos)) {
      $menuFuncaoUsuarioTipo = $menuFuncaoUsuarioTipos[0];
    }

    return $menuFuncaoUsuarioTipo;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:20
   */
  public  function getReplacesMenuFuncaoUsuarioTipoCtrl($reference){
		?><?php

		System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

		$menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $replaces = array();

    $where = "mft_codigo = '" . $reference . "'";
    $menuFuncaoUsuarioTipos = $this->getMenuFuncaoUsuarioTipoCtrl($where, "", "");
    if (is_array($menuFuncaoUsuarioTipos)) {
      $menuFuncaoUsuarioTipo = $menuFuncaoUsuarioTipos[0];

      $items = $menuFuncaoUsuarioTipo->get_mft_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:20
   */
  public  function configureMenuFuncaoUsuarioTipoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:22
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:22
   */
  public  function getPropertiesMenuFuncaoUsuarioTipoCtrl(){
    ?><?php

      System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

      $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

      $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:22
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:22
   */
  public  function getItemsMenuFuncaoUsuarioTipoCtrl($search){
    ?><?php

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true, '{project.rsc}');

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    if ($search) {
      $object = $this->getObjectMenuFuncaoUsuarioTipoCtrl($search);
      if (!is_null($object)) {
        $menuFuncaoUsuarioTipo = $object;
      }
    }
    
    $items = $menuFuncaoUsuarioTipo->get_mft_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $menuFuncaoUsuarioTipo->get_mft_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:23
   */
  public  function getHistoryMenuFuncaoUsuarioTipoCtrl($items){
    ?><?php
    
    $defaults = array(
      'mft_registro'=>array('id'=>'mft_registro', 'value'=>date('d/m/Y H:i:s')),
      'mft_criador'=>array('id'=>'mft_criador', 'value'=>System::getUser()),
      'mft_alteracao'=>array('id'=>'mft_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'mft_responsavel'=>array('id'=>'mft_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:23
   */
  public  function operationMenuFuncaoUsuarioTipoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'MenuFuncaoUsuarioTipo', 'src', true);

    $menuFuncaoUsuarioTipo = new MenuFuncaoUsuarioTipo();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $menuFuncaoUsuarioTipo->get_mft_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $menuFuncaoUsuarioTipo->set_mft_value($id, $item['value']);
    }
    
    $method = $action . "MenuFuncaoUsuarioTipoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo, $operation);
      if ($verify) {

        $before = $this->beforeMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo, $operation);
        if ($before) {

          $response = $this->$method($menuFuncaoUsuarioTipo);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterMenuFuncaoUsuarioTipoCtrl($menuFuncaoUsuarioTipo, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'MenuFuncaoUsuarioTipoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:24
   */
  public  function copyMenuFuncaoUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesMenuFuncaoUsuarioTipoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_mft_value($reference, null);
    }

    return $this->saveMenuFuncaoUsuarioTipoCtrl($object, $validate, $debug);
  }


}