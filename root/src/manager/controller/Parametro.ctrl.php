<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 18/01/2013 11:31:53
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/02/2015 01:09:29
 * @category controller
 * @package manager
 */


class ParametroCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:22
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:22
   */
  public  function ParametroCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');
    
    $entity = new Parametro();
    $properties = $entity->get_par_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:22
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:22
   */
  public  function getRowsParametroCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    $items = $parametro->get_par_items();
    $properties = $parametro->get_par_properties();

    $statements = $parametro->getStatementsParametro();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function getParametroCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    $parametros = null;

    $rows = $this->getRowsParametroCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $parametros = array();
      foreach ($rows as $row) {
        $parametro_set = clone $parametro;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $parametro_set->set_par_value($key, $item['value'], $reference);
        }
        $parametro_set->clearParametro($not_clear);
        $parametros[] = $parametro_set;
      }
    }

    return $parametros;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function getColumnParametroCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    $properties = $parametro->get_par_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $parametro->getStatementsParametro();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function getCountParametroCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    $properties = $parametro->get_par_properties();

    $statements = $parametro->getStatementsParametro();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnParametroCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/02/2015 00:02:59
   */
  public  function beforeParametroCtrl($object, $param){
    ?><?php

    if ($param === 'set' || $param === 'add') {
      $par_version = VERSION;
      if ($param === 'set') {
        $par_version = null;
      }

      $object->set_par_value('par_version', $par_version);
      $object->set_par_value('par_variavel', String::clear(String::replace($object->get_par_value('par_variavel'), " ", "_")));
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function addParametroCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesParametroCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_par_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyParametroCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesParametroCtrl();
        $table = $properties['table'];

        $object = $this->beforeParametroCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_par_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterParametroCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setParametroCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function setParametroCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyParametroCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_par_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesParametroCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeParametroCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_par_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterParametroCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:23
   */
  public  function removeParametroCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_par_items();
    $properties = $this->getPropertiesParametroCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_par_value($action['field'], $action['value']);
        $object->clearParametro(array($action['field']));

        $remove = $this->setParametroCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeParametroCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterParametroCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 27/03/2015 16:29:45
   */
  public  function afterParametroCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    //require System::import('file', '', 'header', 'core', false);

    System::import('m', 'manager', 'Parametro', 'src', true, '');

    $parametro = new Parametro();

    $file = "<?php\n" .
      "/**\n" .
      " * " . COMPANY . "\n" .
      " * Projeto " . SHORT . " :: " . COPY_RIGHT . "\n" .
      " * Modulo..........: Core\n" .
      " * Funcionalidade..: Inicializa variaveis utilizadas no sistema\n" .
      " * Pre-requisitos..: Nao possui\n" .
      " * Data............: " . (date('d/m/Y H:i:s')) . "\n" .
      " * Responsavel.....: " . (System::getUser()) . "\n" .
      " */\n" .
      "\n" .
      "if (basename(\$_SERVER[\"PHP_SELF\"]) == \"parameters.php\") {\n" .
      "  die(\"<h1>Not Found</h1><p>The requested URL was not found on this server.</p><hr><address>Apache/2.2.9 (Debian)</address>\");\n" .
      "}\n\n";

    $parametros = $this->getParametroCtrl("", "par_variavel", "par_variavel");
    if (is_array($parametros)) {
      foreach ($parametros as $parametro) {

        $param_var = "\$" . $parametro->get_par_value('par_variavel');
        $param_value = $parametro->get_par_value('par_valor');
        if ($parametro->get_par_value('par_string') == 1) {
          $param_value = '"' . $parametro->get_par_value('par_valor') . '"';
        }

        $file .= $param_var . " = " . $param_value . ";\n";

        $param_type = "String";
        if (is_numeric($param_value)) {
          $param_type = "Numeric";
        } else if ($param_value == 'true' || $param_value == 'false') {
          $param_type = 'Boolean';
        }

        if ($parametro->get_par_value('par_procedimento') != "") {
          $file .= "\n" . $parametro->get_par_value('par_procedimento') . "\n\n";
          $source .= "<br>" . $parametro->get_par_value('par_procedimento') . "<br><br>";

          $param_value = $param_var;
        }

        if (substr($parametro->get_par_value('par_valor'), 0, 6) == "array(") {
          $param_value = "serialize(" . $param_value . ")";
          $param_type = "Array";
        }

        $upper = strtoupper($parametro->get_par_value('par_variavel'));
        $clear = str_replace("[\"", "_", $upper);
        $constant = str_replace("\"]", "", $clear);

        $file .= "if (!defined(\"" . $constant . "\")) {\n"
          . "  /**\n"
          . "   * " . htmlentities($parametro->get_par_value('par_descricao')) . "<br>\n"
          . "   * @name " . $constant . "\n"
          . "   * @var " . $param_type . "\n"
          . "   * @since " . $parametro->get_par_value('par_version') . "\n"
          . "   * @author " . $parametro->get_par_value('par_criador') . " (" . $parametro->get_par_value('par_registro') . ")\n"
          . "   * <br><b>Updated by</b> " . $parametro->get_par_value('par_responsavel') . " (" . $parametro->get_par_value('par_alteracao') . ")\n"
          . "   */\n"
          . "  define(\"" . $constant . "\", " . $param_value . ");\n"
          . "}\n\n";
      }
    }

    $after = File::saveFile(File::path(APP_PATH, 'config', 'parameters.php'), $file, 'w');

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function executeParametroCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    $properties = $parametro->get_par_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $parametro->getStatementsParametro();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", par_responsavel = '" . System::getUser(false) . "', par_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function verifyParametroCtrl($object){
		?><?php
	
		$items = $object->get_par_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function getObjectParametroCtrl($value){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $object = new Parametro();

    $properties = $object->get_par_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $parametro = null;

    $parametros = $this->getParametroCtrl($where, "", "", "0", "1");
    if (is_array($parametros)) {
      $parametro = $parametros[0];
    }

    return $parametro;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function getReplacesParametroCtrl($reference){
		?><?php

		System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

		$parametro = new Parametro();

    $replaces = array();

    $where = "par_codigo = '" . $reference . "'";
    $parametros = $this->getParametroCtrl($where, "", "");
    if (is_array($parametros)) {
      $parametro = $parametros[0];

      $items = $parametro->get_par_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function configureParametroCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function getPropertiesParametroCtrl(){
    ?><?php

      System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

      $parametro = new Parametro();

      $properties = $parametro->get_par_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function getItemsParametroCtrl($search){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '{project.rsc}');

    $parametro = new Parametro();

    if ($search) {
      $object = $this->getObjectParametroCtrl($search);
      if (!is_null($object)) {
        $parametro = $object;
      }
    }
    
    $items = $parametro->get_par_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $parametro->get_par_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function getHistoryParametroCtrl($items){
    ?><?php
    
    $defaults = array(
      'par_registro'=>array('id'=>'par_registro', 'value'=>date('d/m/Y H:i:s')),
      'par_criador'=>array('id'=>'par_criador', 'value'=>System::getUser()),
      'par_alteracao'=>array('id'=>'par_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'par_responsavel'=>array('id'=>'par_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 15/02/2015 23:59:24
   */
  public  function operationParametroCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'Parametro', 'src', true);

    $parametro = new Parametro();

    foreach ($items as $id => $item) {
      $parametro->set_par_value($id, $item['value']);
    }
    
    $method = $operation . "ParametroCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($parametro);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'ParametroCtrl'");
    }

    return $result;
  }

  /**
   * 
   * 
   * @param int $new 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 16/02/2015 01:06:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 27/03/2015 15:24:26
   */
  public  function updateVersionParametroCtrl($new){
    ?><?php

    System::import('m', 'manager', 'Parametro', 'src', true, '');

    $parametro = new Parametro();

    $set = 0;

    $parametros = $this->getParametroCtrl("par_variavel = 'version'", "", "");
    if (is_array($parametros)) {
      $parametro = $parametros[0];

      $parametro->set_par_value('par_valor', $new);

      $set = $this->setParametroCtrl($parametro);
    }

    return $set;
  }


}