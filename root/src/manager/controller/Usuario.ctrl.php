<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/04/2013 12:46:48
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 20:10:49
 * @category controller
 * @package manager
 */


class UsuarioCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function UsuarioCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');
    
    $entity = new Usuario();
    $properties = $entity->get_usu_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function getRowsUsuarioCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    $items = $usuario->get_usu_items();
    $properties = $usuario->get_usu_properties();

    $statements = $usuario->getStatementsUsuario();

    $table = $properties['table'] . $properties['join'];

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);
    $order = Statement::parse($order, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function getUsuarioCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    $usuarios = null;

    $rows = $this->getRowsUsuarioCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $usuarios = array();
      foreach ($rows as $row) {
        $usuario_set = clone $usuario;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $usuario_set->set_usu_value($key, $item['value'], $reference);
        }
        $usuario_set->clearUsuario($not_clear);
        $usuarios[] = $usuario_set;
      }
    }

    return $usuarios;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 17:26:55
   */
  public  function getColumnUsuarioCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    $properties = $usuario->get_usu_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $usuario->getStatementsUsuario();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function getCountUsuarioCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    $properties = $usuario->get_usu_properties();

    $statements = $usuario->getStatementsUsuario();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnUsuarioCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 20:01:02
   */
  public  function beforeUsuarioCtrl($object, $param){
    ?><?php

    System::import('c', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    System::import('c', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $usuarioTipoCtrl = new UsuarioTipoCtrl(APP_PATH);

    $instituicaoCtrl = new InstituicaoCtrl(APP_PATH);

    $before = true;

    $usu_cod_USUARIO_TIPO = $object->get_usu_value('usu_cod_USUARIO_TIPO');
    if (!$usu_cod_USUARIO_TIPO) {
      $ust_codigo = $usuarioTipoCtrl->getColumnUsuarioTipoCtrl('ust_codigo', "ust_padrao");

      $object->set_usu_value('usu_cod_USUARIO_TIPO', $ust_codigo);
    }

    $usu_cod_INSTITUICAO = $object->get_usu_value('usu_cod_INSTITUICAO');
    if (!$usu_cod_INSTITUICAO) {
      $ist_codigo = $instituicaoCtrl->getColumnInstituicaoCtrl('ist_codigo', "ist_identificador = 'default'");

      $object->set_usu_value('usu_cod_INSTITUICAO', $ist_codigo);
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 20:02:21
   */
  public  function addUsuarioCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesUsuarioCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_usu_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesUsuarioCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_usu_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setUsuarioCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 20:03:36
   */
  public  function setUsuarioCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_usu_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesUsuarioCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_usu_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 20:04:37
   */
  public  function removeUsuarioCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_usu_items();
    $properties = $this->getPropertiesUsuarioCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_usu_value($action['field'], $action['value']);
        $object->clearUsuario(array($action['field']));

        $remove = $this->setUsuarioCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 20:05:52
   */
  public  function afterUsuarioCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_usu_value($object->get_usu_reference());
    } else if ($param === 'add') {
      $object->set_usu_value($object->get_usu_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_usu_items();

      $fields = Controller::after($value, $items, 'usu');
      if (count($fields)) {
        $executed = $this->executeUsuarioCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_usu_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function executeUsuarioCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    $properties = $usuario->get_usu_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $usuario->getStatementsUsuario();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", usu_responsavel = '" . System::getUser(false) . "', usu_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 21:12:30
   */
  public  function verifyUsuarioCtrl($object, $operation = null){
    ?><?php
  
    $items = $object->get_usu_items();
    $error_messages = array();
    $passed = true;
  
    $usu_codigo = $object->get_usu_value('usu_codigo');
    foreach ($items as $key => $item) {
      $value = $item['value'];
      if (!is_null($value)) {

        $new_users = array('add', 'new');
        if (in_array($operation, $new_users, true)) {

          $unique = array('usu_login', 'usu_email');
          if (in_array($key, $unique, true)) {

            $count = $this->getCountUsuarioCtrl('stt_usu_unique:' . $key . ',' . $value . ',' . $usu_codigo);
            if ($count > 0) {

              //$error_messages[] = array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"Já existe esta entrada em nosso sistema");
              Console::add("Já existe esta entrada em nosso sistema", null, $key, $item['description']);
              $passed = false;
            }
          }
        }

        $empty_pass = array('new', 'set');
        if (!in_array($operation, $empty_pass, true)) {

          if ($key === 'usu_password') {

            if (String::length($value) < 6 && !$object->get_usu_value('usu_codigo')) {

              //$error_messages[] = array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"A senha contem menos de seis caracteres.");
              Console::add("A senha contém menos de seis caracteres.", null, $key, $item['description']);
              $passed = false;
            }
          }
        }

      }
    }

    return $passed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function getObjectUsuarioCtrl($value){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $object = new Usuario();

    $properties = $object->get_usu_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $usuario = null;

    $usuarios = $this->getUsuarioCtrl($where, "", "", "0", "1");
    if (is_array($usuarios)) {
      $usuario = $usuarios[0];
    }

    return $usuario;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 23/03/2015 22:53:33
   */
  public  function getReplacesUsuarioCtrl($reference){
		?><?php

		System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

		$usuario = new Usuario();

    $replaces = array();

    $where = "usu_codigo = '" . $reference . "'";
    $usuarios = $this->getUsuarioCtrl($where, "", "");
    if (is_array($usuarios)) {
      $usuario = $usuarios[0];

      $items = $usuario->get_usu_items();

      foreach ($items as $item) {
        $replaces[$item['id']] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 17:19:41
   */
  public  function configureUsuarioCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    System::import('m', 'manager', 'Usuario', 'src');

    $screen = new Screen('{project.rsc}');

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "edit":

        if(System::getUser(false, 'user') == USUARIO_TIPO_ADMINISTRADOR) {
          $items['usu_password']['validate'] = '';
        }

        foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;

      case "request":
        $to_show = array('usu_login');

        foreach ($atributos as $key => $atributo) {
          if (!in_array($key, $to_show, true)) {
            $atributo['form'] = 0;
          }

          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }

        $atributos['usu_login']['description'] = "Login ou E-mail";
        $atributos['usu_login']['title'] = "";
        break;

      case "change":

        $usuario = new Usuario();

        $to_clear = array('usu_auth', 'usu_auth_date', 'usu_ativo');
        $to_show = array('usu_html');

        $usuarios = $this->getUsuarioCtrl('usu_0:' . $atributos['usu_auth']['value'], "", "", "0", "1");
        if (is_array($usuarios)) {
          $usuario = $usuarios[0];

          $atributos = $usuario->get_usu_items();

          $to_show = array('usu_nome', 'usu_password');
        } else {

          $atributos['usu_html']['value'] = "A solicitação de alteração não foi encontrada. Isso pode ser causado por um token inexistente ou uma solicitação que foi expirada.";

        }

        foreach ($to_clear as $c) {
          $atributos[$c]['value'] = '';
          $atributos[$c]['hidden'] = 1;
        }
        $atributos['usu_ativo']['value'] = 1;

        foreach ($atributos as $key => $atributo) {
          if (!in_array($key, $to_show, true)) {
            $atributo['form'] = 0;
          } else {
            $atributo['form'] = 1;
          }

          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }

        $atributos['usu_nome']['readonly'] = 1;

        $atributos['usu_password']['value'] = null;

        break;

      case "new":
        $to_hide = array(
          'usu_cod_USUARIO_TIPO',
          'usu_cod_INSTITUICAO',
          'usu_password',
          'usu_admin',
          'usu_ativo',
        );

        foreach ($atributos as $key => $atributo) {
          if (in_array($key, $to_hide, true)) {
            $atributo['form'] = 0;
          }

          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }

        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function getPropertiesUsuarioCtrl(){
    ?><?php

      System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

      $usuario = new Usuario();

      $properties = $usuario->get_usu_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function getItemsUsuarioCtrl($search){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '{project.rsc}');

    $usuario = new Usuario();

    if ($search) {
      $object = $this->getObjectUsuarioCtrl($search);
      if (!is_null($object)) {
        $usuario = $object;
      }
    }
    
    $items = $usuario->get_usu_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $usuario->get_usu_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   */
  public  function getHistoryUsuarioCtrl($items){
    ?><?php
    
    $defaults = array(
      'usu_registro'=>array('id'=>'usu_registro', 'value'=>date('d/m/Y H:i:s')),
      'usu_criador'=>array('id'=>'usu_criador', 'value'=>System::getUser()),
      'usu_alteracao'=>array('id'=>'usu_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'usu_responsavel'=>array('id'=>'usu_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:34
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 19:59:21
   */
  public  function operationUsuarioCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'Usuario', 'src', true);

    $usuario = new Usuario();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $usuario->get_usu_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $usuario->set_usu_value($id, $item['value']);
    }
    
    $method = $action . "UsuarioCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyUsuarioCtrl($usuario, $operation);
      if ($verify) {

        $before = $this->beforeUsuarioCtrl($usuario, $operation);
        if ($before) {

          $response = $this->$method($usuario);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterUsuarioCtrl($usuario, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'UsuarioCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * A partir do código MD5 do usuário são retornados dados de controle do usuário
   * 
   * @param string $usu_codigo 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:47:51
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:47:51
   */
  public  function getInfoUsuarioCtrl($usu_codigo = -1){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src', true, '');

		if ($usu_codigo == -1) {
			$usu_codigo = System::getUser('code');
		}

		$info = null;
		$where = "MD5(usu_codigo) = '" . $usu_codigo . "'";
		
		$usuario = new Usuario();
		
		$usuarios = $this->getUsuarioCtrl($where, "", "");
		if(is_array($usuarios)){
			$usuario = $usuarios[0];
		
			$info = array(
				'usu_codigo' => $usuario->get_usu_value('usu_codigo'),
				'usu_cod_USUARIO_TIPO' => $usuario->get_usu_value('usu_cod_USUARIO_TIPO'),
				'usu_cod_INSTITUICAO' => $usuario->get_usu_value('usu_cod_INSTITUICAO'),
			);
		}

		return $info;
  }

  /**
   * Recupera um usuário utilizando suas credenciais de login e senha
   * 
   * @param string $login 
   * @param string $password 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:59:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 26/02/2015 19:09:36
   */
  public  function login($login, $password){
    ?><?php

    $usuario = null;

    $existe = $this->getCountUsuarioCtrl('{"login":["' . $login . '","' . $login . '"]}', false, true);
    if ($existe) {

      $usuarios = $this->getUsuarioCtrl('{"login_pass":["' . $login . '","' . $login . '","' . $password . '"]}', "", "", "0", "1", null, false, false);
      if (is_array($usuarios)) {

        $usuario = $usuarios[0];
      } else {

        $usuario = '403';
      }

    } else {
      $usuario = '404';
    }

    return $usuario;
  }

  /**
   * 
   * 
   * @param mixed $usuario 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 17/03/2015 13:44:19
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 21:15:15
   */
  public  function requestUsuarioCtrl($usuario){
    ?><?php

    System::import('m', 'manager', 'Usuario', 'src');

    System::import('class', 'base', 'Email', 'core');

    $email = new Email(APP_PATH);

    $requested = 0;

    $login = "";
    $where = "";
    if (is_object($usuario)) {
      $login = $usuario->get_usu_value('usu_login');
      $where = "login:" . $login . "," . $login;
    } else {
      $login = $usuario;
      $where = "usu_1:" . $login . "," . $login;
    }

    $usuarios = $this->getUsuarioCtrl($where, "", "", "0", "1", null, false, false);
    if (is_array($usuarios)) {
      $usuario = $usuarios[0];

      $usuario->set_usu_value('usu_auth_date', date('d/m/Y H:i'));

      $replaces = $this->getReplacesUsuarioCtrl($usuario->get_usu_value('usu_codigo'));
      $replaces['usu_auth_date'] = array("key"=>'usu_auth_date', "value"=>$usuario->get_usu_value('usu_auth_date'), "type"=>"datetime");

      $usu_auth = System::replaceMarkup(Usuario::$USU_PASSWORD_REQUEST, $replaces);
      $usuario->set_usu_value('usu_auth', md5($usu_auth));
      $replaces['usu_auth'] = array("key"=>'usu_auth', "value"=>$usuario->get_usu_value('usu_auth'), "type"=>"string");

      $requested = $this->operationUsuarioCtrl('set', $usuario->get_usu_items());
      if ($requested) {
        $usu_email = $replaces['usu_email']['value'];
        $usu_nome = $replaces['usu_nome']['value'];
    
        $email->sendTemplate($usu_email, $usu_nome, null, 'USU_REQUEST', $replaces, true, true);
      }
    }

    return new Response(Boolean::parse($requested > 0), $requested);
  }


}
