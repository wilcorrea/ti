<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:24
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 09:30:24
 * @category controller
 * @package manager
 */


class UsuarioSetorCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function UsuarioSetorCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function getUsuarioSetorCtrl($where, $group, $order, $start = "", $end = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $usuarioSetors = $usuarioSetorDAO->getUsuarioSetorDAO($where, $group, $order, $start, $end, $validate, $debug);
    
    return $usuarioSetors;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function getCountUsuarioSetorCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $total = $usuarioSetorDAO->getCountUsuarioSetorDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function addUsuarioSetorCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');    
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $this->beforeUsuarioSetorCtrl($object, "add");
    $add = $usuarioSetorDAO->addUsuarioSetorDAO($object, $validate, $debug, $presave, $copy);
    $this->afterUsuarioSetorCtrl($object, "add", $add, $copy);
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function removeUsuarioSetorCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $this->beforeUsuarioSetorCtrl($object, "remove");
    $remove = $usuarioSetorDAO->removeUsuarioSetorDAO($object, $validate, $debug);
    $this->afterUsuarioSetorCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function setUsuarioSetorCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $this->beforeUsuarioSetorCtrl($object, "set");
    $set = $usuarioSetorDAO->setUsuarioSetorDAO($object, $validate, $debug, $trigger);
    if($trigger === true){
			$this->afterUsuarioSetorCtrl($object, "set");
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function beforeUsuarioSetorCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_uss_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_uss_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_uss_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }
    }
    
    if ($param == 'remove') {
      $items = $object->get_uss_items();
      $properties = $object->get_uss_properties();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_uss_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $parentModule = $item['parent']['modulo'];
            $parentEntity = $item['parent']['entity'];
            $parentReference = $item['parent']['key'];
            
            $value = $this->getColumnConteudoModuloCtrl($key, $whereObject);
            
            $whereParent = $parentReference . " = '" . $value . "'";

            System::desire('c', $parentModule, $parentEntity, 'src');
            System::desire('m', $parentModule, $parentEntity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($whereParent, "", "");
            $parent = $parents[0];
            
            $parents = $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:27
   */
  public  function afterUsuarioSetorCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_uss_value($object->get_uss_reference());
    }

    if($param === 'add'){
      $object->set_uss_value($object->get_uss_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_uss_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

		$properties = $object->get_uss_properties();
    if(isset($properties['notification'])){
      if($properties['notification']){
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   */
  public  function getColumnUsuarioSetorCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');
    
    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $column = $usuarioSetorDAO->getColumnUsuarioSetorDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   */
  public  function executeUsuarioSetorCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'manager', 'UsuarioSetor', 'src', true, '');

    $usuarioSetorDAO = new UsuarioSetorDAO($this->path, $this->database);
    $executed = $usuarioSetorDAO->executeUsuarioSetorDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:28
   */
  public  function verifyUsuarioSetorCtrl($object){
		?><?php
	
		$items = $object->get_uss_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }


}