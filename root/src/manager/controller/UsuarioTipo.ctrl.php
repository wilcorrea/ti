<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 21/04/2013 22:45:55
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 24/03/2015 19:44:46
 * @category controller
 * @package manager
 */


class UsuarioTipoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function UsuarioTipoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');
    
    $entity = new UsuarioTipo();
    $properties = $entity->get_ust_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function getRowsUsuarioTipoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    $items = $usuarioTipo->get_ust_items();
    $properties = $usuarioTipo->get_ust_properties();

    $statements = $usuarioTipo->getStatementsUsuarioTipo();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function getUsuarioTipoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    $usuarioTipos = null;

    $rows = $this->getRowsUsuarioTipoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $usuarioTipos = array();
      foreach ($rows as $row) {
        $usuarioTipo_set = clone $usuarioTipo;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $usuarioTipo_set->set_ust_value($key, $item['value'], $reference);
        }
        $usuarioTipo_set->clearUsuarioTipo($not_clear);
        $usuarioTipos[] = $usuarioTipo_set;
      }
    }

    return $usuarioTipos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function getColumnUsuarioTipoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    $properties = $usuarioTipo->get_ust_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $usuarioTipo->getStatementsUsuarioTipo();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function getCountUsuarioTipoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    $properties = $usuarioTipo->get_ust_properties();

    $statements = $usuarioTipo->getStatementsUsuarioTipo();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnUsuarioTipoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  private  function beforeUsuarioTipoCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsUsuarioTipoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_ust_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_ust_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_ust_value($item['id'], $value);
            } else {
              $object->set_ust_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsUsuarioTipoCtrl("");
      $properties = $this->getPropertiesUsuarioTipoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_ust_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnUsuarioTipoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function addUsuarioTipoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesUsuarioTipoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_ust_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesUsuarioTipoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_ust_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setUsuarioTipoCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function setUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_ust_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesUsuarioTipoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_ust_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:42
   */
  public  function removeUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_ust_items();
    $properties = $this->getPropertiesUsuarioTipoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_ust_value($action['field'], $action['value']);
        $object->clearUsuarioTipo(array($action['field']));

        $remove = $this->setUsuarioTipoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  private  function afterUsuarioTipoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_ust_value($object->get_ust_reference());
    } else if ($param === 'add') {
      $object->set_ust_value($object->get_ust_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_ust_items();

      $fields = Controller::after($value, $items, 'ust');
      if (count($fields)) {
        $executed = $this->executeUsuarioTipoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_ust_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  private  function executeUsuarioTipoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    $properties = $usuarioTipo->get_ust_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $usuarioTipo->getStatementsUsuarioTipo();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", ust_responsavel = '" . System::getUser(false) . "', ust_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  private  function verifyUsuarioTipoCtrl($object){
		?><?php
	
		$items = $object->get_ust_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function getObjectUsuarioTipoCtrl($value){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $object = new UsuarioTipo();

    $properties = $object->get_ust_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $usuarioTipo = null;

    $usuarioTipos = $this->getUsuarioTipoCtrl($where, "", "", "0", "1");
    if (is_array($usuarioTipos)) {
      $usuarioTipo = $usuarioTipos[0];
    }

    return $usuarioTipo;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function getReplacesUsuarioTipoCtrl($reference){
		?><?php

		System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

		$usuarioTipo = new UsuarioTipo();

    $replaces = array();

    $where = "ust_codigo = '" . $reference . "'";
    $usuarioTipos = $this->getUsuarioTipoCtrl($where, "", "");
    if (is_array($usuarioTipos)) {
      $usuarioTipo = $usuarioTipos[0];

      $items = $usuarioTipo->get_ust_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function configureUsuarioTipoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function getPropertiesUsuarioTipoCtrl(){
    ?><?php

      System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

      $usuarioTipo = new UsuarioTipo();

      $properties = $usuarioTipo->get_ust_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function getItemsUsuarioTipoCtrl($search){
    ?><?php

    System::import('m', 'manager', 'UsuarioTipo', 'src', true, '{project.rsc}');

    $usuarioTipo = new UsuarioTipo();

    if ($search) {
      $object = $this->getObjectUsuarioTipoCtrl($search);
      if (!is_null($object)) {
        $usuarioTipo = $object;
      }
    }
    
    $items = $usuarioTipo->get_ust_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $usuarioTipo->get_ust_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function getHistoryUsuarioTipoCtrl($items){
    ?><?php
    
    $defaults = array(
      'ust_registro'=>array('id'=>'ust_registro', 'value'=>date('d/m/Y H:i:s')),
      'ust_criador'=>array('id'=>'ust_criador', 'value'=>System::getUser()),
      'ust_alteracao'=>array('id'=>'ust_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'ust_responsavel'=>array('id'=>'ust_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function operationUsuarioTipoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'manager', 'UsuarioTipo', 'src', true);

    $usuarioTipo = new UsuarioTipo();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $usuarioTipo->get_ust_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $usuarioTipo->set_ust_value($id, $item['value']);
    }
    
    $method = $action . "UsuarioTipoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyUsuarioTipoCtrl($usuarioTipo, $operation);
      if ($verify) {

        $before = $this->beforeUsuarioTipoCtrl($usuarioTipo, $operation);
        if ($before) {

          $response = $this->$method($usuarioTipo);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterUsuarioTipoCtrl($usuarioTipo, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'UsuarioTipoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 31/03/2015 11:51:43
   */
  public  function copyUsuarioTipoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesUsuarioTipoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_ust_value($reference, null);
    }

    return $this->addUsuarioTipoCtrl($object, $validate, $debug);
  }


}