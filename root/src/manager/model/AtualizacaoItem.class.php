<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 09:38:24
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 12/12/2014 17:43:09
 * @category model
 * @package manager
 */


class AtualizacaoItem
{
  private  $ati_items = array();
  private  $ati_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   */
  public  function AtualizacaoItem(){
?><?php
    $this->ati_items = array();
    
    // Atributos
    $this->ati_items["ati_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"ati_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->ati_items["ati_setor"] = array("pk"=>0, "fk"=>0, "id"=>"ati_setor", "description"=>"Setor", "title"=>"Setor para o qual a implementação se destina", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->ati_items["ati_categoria"] = array("pk"=>0, "fk"=>0, "id"=>"ati_categoria", "description"=>"Categoria", "title"=>"", "type"=>"option", "type_content"=>"1,CORREÇÃO|2,MANUTENÇÃO|3,IMPLANTAÇÃO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->ati_items["ati_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"ati_descricao", "description"=>"Descrição", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );


    // Atributos FK
    $this->ati_items["ati_cod_ATUALIZACAO"] = array("pk"=>0, "fk"=>1, "id"=>"ati_cod_ATUALIZACAO", "description"=>"Atualização", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"Atualizacao", "table"=>"TBL_ATUALIZACAO", "prefix"=>"atl", "tag"=>"atualizacao", "key"=>"atl_codigo", "description"=>"atl_versao", "form"=>"form", "target"=>"div-ati_cod_ATUALIZACAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->ati_items['ati_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'ati_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ati_items['ati_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'ati_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->ati_items['ati_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'ati_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->ati_items['ati_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'ati_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->ati_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->ati_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->ati_properties = array(
			'rotule'=>'Detalhes da Atualização',
      'module'=>'manager',
      'entity'=>'AtualizacaoItem',
      'table'=>'TBL_ATUALIZACAO_ITEM',
			'join'=>$join,
      'tag'=>'atualizacaoitem',
      'prefix'=>'ati',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'ati_codigo',
      'description'=>'ati_descricao',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->ati_properties['reference']) {
      foreach ($this->ati_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->ati_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->ati_properties['description']) {
      foreach ($this->ati_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->ati_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   */
  public  function get_ati_properties(){
    ?><?php
    return $this->ati_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   */
  public  function get_ati_items(){
    ?><?php
    return $this->ati_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   */
  public  function get_ati_item($key){
    ?><?php
    return $this->ati_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:20
   */
  public  function get_ati_reference(){
    ?><?php
    $key = $this->ati_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   */
  public  function get_ati_value($key){
    ?><?php
    return $this->ati_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   */
  public  function set_ati_value($key, $value){
?><?php
		if (!isset($this->ati_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->ati_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 10:13:21
   */
  public  function set_ati_type($key, $type){
    ?><?php
    $this->ati_items[$key]['type'] = $type;
  }


}