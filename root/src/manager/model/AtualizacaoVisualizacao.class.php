<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 12/12/2014 17:44:49
 * @category model
 * @package manager
 */


class AtualizacaoVisualizacao
{
  private  $atv_items = array();
  private  $atv_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 15/10/2013 16:19:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 15/10/2013 16:49:35
   */
  public  function AtualizacaoVisualizacao(){
?><?php
    $this->atv_items = array();
    
    // Atributos
    $this->atv_items["atv_data_visualizacao"] = array("pk"=>0, "fk"=>0, "id"=>"atv_data_visualizacao", "description"=>"Data", "title"=>"", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );


    // Atributos FK
    $this->atv_items["atv_cod_ATUALIZACAO"] = array("pk"=>0, "fk"=>1, "id"=>"atv_cod_ATUALIZACAO", "description"=>"Atualização", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, "foreign"=>array("modulo"=>"manager", "entity"=>"Atualizacao", "table"=>"TBL_ATUALIZACAO", "prefix"=>"atl", "tag"=>"atualizacao", "key"=>"atl_codigo", "description"=>"atl_versao", "form"=>"form", "target"=>"div-atv_cod_ATUALIZACAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->atv_items["atv_cod_USUARIO"] = array("pk"=>0, "fk"=>1, "id"=>"atv_cod_USUARIO", "description"=>"Usuário", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"Usuario", "table"=>"TBL_USUARIO", "prefix"=>"usu", "tag"=>"usuario", "key"=>"usu_codigo", "description"=>"usu_nome", "form"=>"form", "target"=>"div-atv_cod_USUARIO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->atv_items['atv_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'atv_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->atv_items['atv_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'atv_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->atv_items['atv_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'atv_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->atv_items['atv_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'atv_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->atv_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $this->atv_items["atv_cod_ATUALIZACAO"]['pk'] = 1;
    $this->atv_items["atv_cod_USUARIO"]['pk'] = 1;
    $this->atv_items["atv_cod_USUARIO"]['foreign'] = array("modulo"=>"tecnologia_informacao", "entity"=>"AcessoControle.gen", "table"=>"TBL_ACESSO_CONTROLE", "prefix"=>"acsc", "tag"=>"acessocontrole", "key"=>"acsc_codigo", "description"=>"acsc_login", "form"=>"form", "target"=>"div-atv_cod_USUARIO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>"");

    $j = array();
    foreach($this->atv_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->atv_properties = array(
			'rotule'=>'Visualização do Usuário',
      'module'=>'manager',
      'entity'=>'AtualizacaoVisualizacao',
      'table'=>'TBL_ATUALIZACAO_VISUALIZACAO',
			'join'=>$join,
      'tag'=>'atualizacaovisualizacao',
      'prefix'=>'atv',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>true,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>true,//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'atv_cod_ATUALIZACAO',
      'description'=>'atv_cod_USUARIO',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->atv_properties['reference']) {
      foreach ($this->atv_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->atv_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->atv_properties['description']) {
      foreach ($this->atv_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->atv_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function get_atv_properties(){
    ?><?php
    return $this->atv_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function get_atv_items(){
    ?><?php
    return $this->atv_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function get_atv_item($key){
    ?><?php
    return $this->atv_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function get_atv_reference(){
    ?><?php
    $key = $this->atv_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function get_atv_value($key){
    ?><?php
    return $this->atv_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:36
   */
  public  function set_atv_value($key, $value){
?><?php
		if (!isset($this->atv_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->atv_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:37
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 17:50:37
   */
  public  function set_atv_type($key, $type){
    ?><?php
    $this->atv_items[$key]['type'] = $type;
  }


}