<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/05/2013 21:56:15
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:37
 * @category model
 * @package manager
 */


class EmailModelo
{
  private  $emo_items = array();
  private  $emo_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:24
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:24
   */
  public  function EmailModelo(){
?><?php
    $this->emo_items = array();
    
    // Atributos
    $this->emo_items["emo_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"emo_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>0, "line"=>1, "order"=>1, );
    $this->emo_items["emo_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"emo_descricao", "description"=>"Descrição", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->emo_items["emo_id"] = array("pk"=>0, "fk"=>0, "id"=>"emo_id", "description"=>"Identificador", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->emo_items["emo_resposta"] = array("pk"=>0, "fk"=>0, "id"=>"emo_resposta", "description"=>"Endereço de Resposta", "title"=>"Se não for informado será respondido para o endereço de resposta será o padrão do sistema", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->emo_items["emo_assunto"] = array("pk"=>0, "fk"=>0, "id"=>"emo_assunto", "description"=>"Assunto", "title"=>"Assunto padrão da mensagem", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->emo_items["emo_corpo"] = array("pk"=>0, "fk"=>0, "id"=>"emo_corpo", "description"=>"Mensagem", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->emo_items['emo_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'emo_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emo_items['emo_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'emo_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->emo_items['emo_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'emo_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emo_items['emo_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'emo_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->emo_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->emo_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->emo_properties = array(
			'description'=>'Modelos de E-mail',
      'module'=>'manager',
      'entity'=>'EmailModelo',
      'table'=>'TBL_EMAIL_MODELO',
			'join'=>$join,
      'tag'=>'emailmodelo',
      'prefix'=>'emo',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'emo_codigo',
      'description'=>'emo_descricao',
      'lines'=>$lines
    );
    
    if (!$this->emo_properties['reference']) {
      foreach ($this->emo_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->emo_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->emo_properties['description']) {
      foreach ($this->emo_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->emo_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:24
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:24
   */
  public  function get_emo_properties(){
    ?><?php
    return $this->emo_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function get_emo_items(){
    ?><?php
    return $this->emo_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function get_emo_item($key){
    ?><?php
    return $this->emo_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function get_emo_reference(){
    ?><?php
    $key = $this->emo_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function get_emo_value($key){
    ?><?php
    return $this->emo_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function set_emo_value($key, $value){
?><?php
		if (!isset($this->emo_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->emo_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:07:25
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:25
   */
  public  function set_emo_type($key, $type){
    ?><?php
    $this->emo_items[$key]['type'] = $type;
  }


}