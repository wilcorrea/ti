<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 24/07/2013 10:53:47
 * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:07:03
 * @category model
 * @package manager
 */


class EmailNotificacao
{
  private  $emn_items = array();
  private  $emn_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function EmailNotificacao(){
?><?php
    $this->emn_items = array();
    
    // Atributos
    $this->emn_items["emn_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"emn_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->emn_items["emn_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"emn_descricao", "description"=>"Descrição", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->emn_items["emn_action"] = array("pk"=>0, "fk"=>0, "id"=>"emn_action", "description"=>"Operação", "title"=>"", "type"=>"option", "type_content"=>"all,Todos|add,Inserir|set,Alterar|remove,Remover", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->emn_items["emn_tag"] = array("pk"=>0, "fk"=>0, "id"=>"emn_tag", "description"=>"Função", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->emn_items["emn_email"] = array("pk"=>0, "fk"=>0, "id"=>"emn_email", "description"=>"E-mail", "title"=>"Endereço que irá receber a notificação", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->emn_items["emn_nome"] = array("pk"=>0, "fk"=>0, "id"=>"emn_nome", "description"=>"Nome", "title"=>"Nome de quem irá receber a notificação", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->emn_items["emn_condicional"] = array("pk"=>0, "fk"=>0, "id"=>"emn_condicional", "description"=>"Valor Condicional", "title"=>"Adiciona um valor personalizado para enviar a mensagem", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>8, );


    // Atributos FK
    $this->emn_items["emn_cod_EMAIL_MODELO"] = array("pk"=>0, "fk"=>1, "id"=>"emn_cod_EMAIL_MODELO", "description"=>"Modelo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"EmailModelo", "table"=>"TBL_EMAIL_MODELO", "prefix"=>"emo", "tag"=>"emailmodelo", "key"=>"emo_codigo", "description"=>"emo_descricao", "form"=>"form", "target"=>"div-emn_cod_EMAIL_MODELO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->emn_items['emn_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'emn_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emn_items['emn_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'emn_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->emn_items['emn_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'emn_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emn_items['emn_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'emn_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->emn_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->emn_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->emn_properties = array(
			'description'=>'Notificações por E-mail',
      'module'=>'manager',
      'entity'=>'EmailNotificacao',
      'table'=>'TBL_EMAIL_NOTIFICACAO',
			'join'=>$join,
      'tag'=>'emailnotificacao',
      'prefix'=>'emn',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'emn_codigo',
      'description'=>'emn_descricao',
      'lines'=>$lines
    );
    
    if (!$this->emn_properties['reference']) {
      foreach ($this->emn_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->emn_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->emn_properties['description']) {
      foreach ($this->emn_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->emn_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function get_emn_properties(){
    ?><?php
    return $this->emn_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function get_emn_items(){
    ?><?php
    return $this->emn_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function get_emn_item($key){
    ?><?php
    return $this->emn_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function get_emn_reference(){
    ?><?php
    $key = $this->emn_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function get_emn_value($key){
    ?><?php
    return $this->emn_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:54
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:54
   */
  public  function set_emn_value($key, $value){
?><?php
		if (!isset($this->emn_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->emn_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author ADMINISTRADOR - 24/07/2013 12:05:55
   * <br><b>Updated by</b> ADMINISTRADOR - 24/07/2013 12:05:55
   */
  public  function set_emn_type($key, $type){
    ?><?php
    $this->emn_items[$key]['type'] = $type;
  }


}