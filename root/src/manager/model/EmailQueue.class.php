<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/09/2013 17:12:50
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 10:19:53
 * @category model
 * @package manager
 */


class EmailQueue
{
  private  $emq_items = array();
  private  $emq_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function EmailQueue(){
    ?><?php

    $this->emq_items = array();
    
    // Atributos
    $this->emq_items["emq_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"emq_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->emq_items["emq_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"emq_descricao", "description"=>"Descrição", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"height: 200px;", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->emq_items["emq_destinatario"] = array("pk"=>0, "fk"=>0, "id"=>"emq_destinatario", "description"=>"Destinatários", "title"=>"", "type"=>"html", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>4, "order"=>4, );


    // Atributos FK
    $this->emq_items["emq_cod_EMAIL_TEMPLATE"] = array("pk"=>0, "fk"=>1, "id"=>"emq_cod_EMAIL_TEMPLATE", "description"=>"Modelo", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );


    // Atributos CHILD

    
    // Atributos padrao
    $this->emq_items['emq_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'emq_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emq_items['emq_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'emq_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->emq_items['emq_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'emq_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->emq_items['emq_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'emq_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->emq_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->emq_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->emq_properties = array(
			'rotule'=>'Envio de E-mail em Lote',
      'module'=>'manager',
      'entity'=>'EmailQueue',
      'table'=>'TBL_EMAIL_QUEUE',
			'join'=>$join,
      'tag'=>'emailqueue',
      'prefix'=>'emq',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			//'action'=>array('icon-push'=>array("className"=>'icon-push', "title"=>"Ativar", "level"=>2, "message"=>"", "handler"=>"Ativar", "confirm"=>"Deseja realmente ativar este registro?", "feedback"=>true, "reload"=>true, "condition"=>array("field"=>'fcr_ativo', "value"=>0), "confirm"=>array("active"=>true, "title"=>"Ativação de Facred", "message"=>"Deseja realmente ativar este Facred?"), "operation"=>array("command"=>"save", "update"=>"fcr_ativo=1"))),
      'remove'=>'',
			//false, true or
			//array("field"=>"emq_id", "value"=>"1", "className"=>"icon-trash", "title"=>"Excluir", "message"=>"Deseja realmente remover este registro?", "success"=>"Registro removido com sucesso", "handler"=>"Remover"),
			'database'=>null,
      'reference'=>'emq_codigo',
      'description'=>'emq_descricao',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->emq_properties['reference']) {
      foreach ($this->emq_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->emq_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->emq_properties['description']) {
      foreach ($this->emq_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->emq_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function get_emq_properties(){
    ?><?php
    return $this->emq_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function get_emq_items(){
    ?><?php
    return $this->emq_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function get_emq_item($key){
    ?><?php
    return $this->emq_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function get_emq_reference(){
    ?><?php
    $key = $this->emq_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:03
   */
  public  function get_emq_value($key){
    ?><?php
    return $this->emq_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:04
   */
  public  function set_emq_value($key, $value){
?><?php
		if (!isset($this->emq_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->emq_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 07/10/2013 11:34:04
   */
  public  function set_emq_type($key, $type){
    ?><?php
    $this->emq_items[$key]['type'] = $type;
  }


}