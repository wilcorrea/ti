<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/04/2013 21:29:44
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 19/05/2014 20:20:26
 * @category model
 * @package manager
 */


class Erro
{
  private  $err_items = array();
  private  $err_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 19/05/2014 20:45:31
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 19/05/2014 20:45:31
   */
  public  function Erro(){
?><?php

    $this->err_items = array();
    
    // Atributos
    $this->err_items["err_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"err_codigo", "description"=>"Código", "title"=>"Chave Primária", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->err_items["err_corrigido"] = array("pk"=>0, "fk"=>0, "id"=>"err_corrigido", "description"=>"Corrigido", "title"=>"Define se o erro está corrigido ou não", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->err_items["err_titulo"] = array("pk"=>0, "fk"=>0, "id"=>"err_titulo", "description"=>"Título", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"150", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->err_items["err_url"] = array("pk"=>0, "fk"=>0, "id"=>"err_url", "description"=>"Página", "title"=>"", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->err_items["err_parametros"] = array("pk"=>0, "fk"=>0, "id"=>"err_parametros", "description"=>"Parâmetros", "title"=>"", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->err_items["err_acao"] = array("pk"=>0, "fk"=>0, "id"=>"err_acao", "description"=>"Ação", "title"=>"", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->err_items["err_dados"] = array("pk"=>0, "fk"=>0, "id"=>"err_dados", "description"=>"Dados", "title"=>"", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->err_items["err_plataforma"] = array("pk"=>0, "fk"=>0, "id"=>"err_plataforma", "description"=>"Plataforma", "title"=>"Armazena a plataforma na qual o erro ocorreu [PHP, Javascript, Mysql]", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"110", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>8, );
    $this->err_items["err_referencia"] = array("pk"=>0, "fk"=>0, "id"=>"err_referencia", "description"=>"Referência", "title"=>"Código do erro quando disponível na plataforma de origem", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"100", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->err_items["err_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"err_descricao", "description"=>"Descrição", "title"=>"Descrição breve da mensagem de erro", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"330", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>10, );
    $this->err_items["err_comando"] = array("pk"=>0, "fk"=>0, "id"=>"err_comando", "description"=>"Comando", "title"=>"Comando que gerou a execeção", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>11, );
    $this->err_items["err_contexto"] = array("pk"=>0, "fk"=>0, "id"=>"err_contexto", "description"=>"Contexto", "title"=>"Dados de contexto capturados pela exceção", "type"=>"base64", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>12, );
    $this->err_items["err_navegador"] = array("pk"=>0, "fk"=>0, "id"=>"err_navegador", "description"=>"Navegador", "title"=>"Navegador que estava sendo utilizado quando o erro aconteceu", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>13, );
    $this->err_items["err_so"] = array("pk"=>0, "fk"=>0, "id"=>"err_so", "description"=>"Sistema Operacional", "title"=>"Sistema operacional do cliente", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>14, );
    $this->err_items["err_versao"] = array("pk"=>0, "fk"=>0, "id"=>"err_versao", "description"=>"Versão", "title"=>"Versão do navegador do cliente", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>15, );
    $this->err_items["err_rastreio"] = array("pk"=>0, "fk"=>0, "id"=>"err_rastreio", "description"=>"Rastreio", "title"=>"Um histórico suscinto do caminho percorrido pelo algoritmo até o erro. Exibido quando disponível na plataforma", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>16, "order"=>16, );
    $this->err_items["err_endereco"] = array("pk"=>0, "fk"=>0, "id"=>"err_endereco", "description"=>"IP", "title"=>"Registra o endereço IP do cliente no instante do erro", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>17, );
    $this->err_items["err_total"] = array("pk"=>0, "fk"=>0, "id"=>"err_total", "description"=>"Ocorrências", "title"=>"Armazena o total de erros equivalentes que ocorrem", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[>0]", "fast"=>0, "grid"=>1, "grid_width"=>"100", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>18, "order"=>18, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->err_items['err_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'err_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->err_items['err_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'err_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>1, 'grid_width'=>"120", 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>10);
    $this->err_items['err_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'err_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->err_items['err_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'err_criador', 'description'=>'Usuário', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>true, 'grid'=>0, 'grid_width'=>'', 'form'=>0, 'form_width'=>'0', 'readonly'=>1, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>9);

    $lines = 0;
    foreach($this->err_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->err_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->err_properties = array(
      'module'=>'manager',
      'entity'=>'Erro',
      'table'=>'TBL_ERRO',
			'join'=>$join,
      'tag'=>'erro',
      'prefix'=>'err',
      'order'=>'err_codigo DESC',
      'group'=>'',
      'where'=>'err_corrigido = 0',
      'search'=>'',
      'layout'=>'',
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>true,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>array("field"=>"err_corrigido","value"=>"1","className"=>"icon-trash","title"=>"Corrigir","message"=>"Deseja realmente definir este erro como <b>corrigido</b>?","success"=>"Registro corrigido com sucesso","handler"=>"Remove"),
			'database'=>null,
      'reference'=>'err_codigo',
      'description'=>'err_descricao',
      'lines'=>$lines
    );
    
    if (!$this->err_properties['reference']) {
      foreach ($this->err_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->err_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->err_properties['description']) {
      foreach ($this->err_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->err_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function get_err_properties(){
    ?><?php
    return $this->err_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function get_err_items(){
    ?><?php
    return $this->err_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function get_err_item($key){
    ?><?php
    return $this->err_items[$key];
  }

  /**
   * 
   * 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function get_err_reference(){
    ?><?php
    $key = $this->err_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function get_err_value($key){
    ?><?php
    return $this->err_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function set_err_value($key, $value){
    ?><?php
    $this->err_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author ADMINISTRADOR - 15/06/2013 19:17:21
   * <br><b>Updated by</b> ADMINISTRADOR - 15/06/2013 19:17:21
   */
  public  function set_err_type($key, $type){
    ?><?php
    $this->err_items[$key]['type'] = $type;
  }


}