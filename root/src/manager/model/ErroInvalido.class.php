<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:05
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 09:33:14
 * @category model
 * @package manager
 */


class ErroInvalido
{
  private  $eri_items = array();
  private  $eri_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function ErroInvalido(){
?><?php
    $this->eri_items = array();
    
    // Atributos
    $this->eri_items["eri_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"eri_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->eri_items["eri_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"eri_descricao", "description"=>"Descrição", "title"=>"Texto que será utilizado para consulta pelo Módulo de Erros", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"width: 100%;", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->eri_items["eri_justificativa"] = array("pk"=>0, "fk"=>0, "id"=>"eri_justificativa", "description"=>"Justificativa", "title"=>"Motivo pelo qual o erro deve ser ignorado quando contiver a descrição registrada", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->eri_items['eri_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'eri_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->eri_items['eri_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'eri_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->eri_items['eri_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'eri_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->eri_items['eri_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'eri_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->eri_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->eri_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->eri_properties = array(
			'rotule'=>'Erros Ignorados',
      'module'=>'manager',
      'entity'=>'ErroInvalido',
      'table'=>'TBL_ERRO_INVALIDO',
			'join'=>$join,
      'tag'=>'erroinvalido',
      'prefix'=>'eri',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'eri_codigo',
      'description'=>'eri_descricao',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->eri_properties['reference']) {
      foreach ($this->eri_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->eri_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->eri_properties['description']) {
      foreach ($this->eri_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->eri_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function get_eri_properties(){
    ?><?php
    return $this->eri_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function get_eri_items(){
    ?><?php
    return $this->eri_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function get_eri_item($key){
    ?><?php
    return $this->eri_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function get_eri_reference(){
    ?><?php
    $key = $this->eri_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function get_eri_value($key){
    ?><?php
    return $this->eri_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function set_eri_value($key, $value){
?><?php
		if (!isset($this->eri_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->eri_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/08/2013 17:17:07
   */
  public  function set_eri_type($key, $type){
    ?><?php
    $this->eri_items[$key]['type'] = $type;
  }


}