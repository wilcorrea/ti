<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 01/05/2013 09:09:56
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/05/2014 21:19:34
 * @category model
 * @package manager
 */


class Historico
{
  private  $hst_items = array();
  private  $hst_properties = array();
  private  $hst_parents = array();
  private  $hst_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function Historico(){
    ?><?php

    $this->hst_items = array();
    
    // Atributos
    $this->hst_items["hst_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"hst_codigo", "description"=>"Código", "title"=>"Chave primária", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->hst_items["hst_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"hst_tipo", "description"=>"Tipo", "title"=>"Determina a natureza do comando que será armazanado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->hst_items["hst_tabela"] = array("pk"=>0, "fk"=>0, "id"=>"hst_tabela", "description"=>"Tabela", "title"=>"Especifica em que tabela foi feita a alteração", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->hst_items["hst_comando"] = array("pk"=>0, "fk"=>0, "id"=>"hst_comando", "description"=>"Comando", "title"=>"Comando que foi processado", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->hst_items["hst_info"] = array("pk"=>0, "fk"=>0, "id"=>"hst_info", "description"=>"Detalhes", "title"=>"Informações resultantes do processamento do comando", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->hst_items["hst_backup"] = array("pk"=>0, "fk"=>0, "id"=>"hst_backup", "description"=>"Backup", "title"=>"Um backup é feito quando um comando processa no máximo 10 registros", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->hst_items["hst_ip"] = array("pk"=>0, "fk"=>0, "id"=>"hst_ip", "description"=>"Endereço IP", "title"=>"Armazena o endereço IP do usuário que está fazendo a alteração", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->hst_items['hst_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->hst_items['hst_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'hst_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->hst_items = $this->configureItemsHistorico($this->hst_items);

    $join = $this->configureJoinHistorico($this->hst_items);

    $lines = 0;
    foreach ($this->hst_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->hst_properties = array(
			'rotule'=>'Histórico de Modificações',
      'module'=>'manager',
      'entity'=>'Historico',
      'table'=>'TBL_HISTORICO',
			'join'=>$join,
      'tag'=>'historico',
      'prefix'=>'hst',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'hst_codigo',
      'description'=>'hst_tabela',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->hst_properties['reference']) {
      foreach ($this->hst_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->hst_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->hst_properties['description']) {
      foreach ($this->hst_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->hst_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function get_hst_properties(){
    ?><?php
    return $this->hst_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function get_hst_items(){
    ?><?php
    return $this->hst_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function get_hst_item($key){
    ?><?php

		$this->validateItemHistorico($key);

    return $this->hst_items[$key];
  }

  /**
   * 
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function get_hst_reference(){
    ?><?php
    $key = $this->hst_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function get_hst_value($key){
    ?><?php

		$this->validateItemHistorico($key);

    return $this->hst_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function set_hst_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemHistorico($key);

    $this->hst_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->hst_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function set_hst_type($key, $type){
    ?><?php

		$this->validateItemHistorico($key);

    $this->hst_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  private  function configureJoinHistorico($items){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  private  function configureItemsHistorico($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->hst_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->hst_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function clearHistorico($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_hst_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_hst_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  private  function validateItemHistorico($key){
    ?><?php

    if (!isset($this->hst_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function setStatementsHistorico(){
    ?><?php

    $this->hst_statements[0] = "hst_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   * <br><b>Updated by</b> FELIPE DE FREITAS CARNEIRO - 05/11/2014 16:20:09
   */
  public  function getStatementsHistorico(){
    ?><?php

    return $this->hst_statements;
  }


}