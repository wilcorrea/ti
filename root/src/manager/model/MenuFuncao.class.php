<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 22/04/2013 01:52:19
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 19:52:51
 * @category model
 * @package manager
 */


class MenuFuncao
{
  private  $mef_items = array();
  private  $mef_properties = array();
  private  $mef_parents = array();
  private  $mef_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 06/11/2015 17:33:29
   */
  public  function MenuFuncao(){
    ?><?php

    $this->mef_items = array();
    
    // Atributos
    $this->mef_items["mef_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"mef_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->mef_items["mef_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"mef_descricao", "description"=>"Descrição", "title"=>"Nome da função a ser exibido no menu", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->mef_items["mef_separator"] = array("pk"=>0, "fk"=>0, "id"=>"mef_separator", "description"=>"Separador", "title"=>"Especifica se o item do menu terá ou não um separador sobre ele", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[0]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->mef_items["mef_order"] = array("pk"=>0, "fk"=>0, "id"=>"mef_order", "description"=>"Ordem", "title"=>"Ordem de exibição dos itens de menu dentro de cada menu", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[>0]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->mef_items["mef_icon"] = array("pk"=>0, "fk"=>0, "id"=>"mef_icon", "description"=>"Ícone", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>6, );
    $this->mef_items["mef_root"] = array("pk"=>0, "fk"=>0, "id"=>"mef_root", "description"=>"Raiz", "title"=>"Define qual o caminho inicial da url a ser recuperada para acessar o recurso", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>7, );
    $this->mef_items["mef_module"] = array("pk"=>0, "fk"=>0, "id"=>"mef_module", "description"=>"Módulo", "title"=>"Módulo no qual a função está alocada", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>8, );
    $this->mef_items["mef_entity"] = array("pk"=>0, "fk"=>0, "id"=>"mef_entity", "description"=>"Entidade", "title"=>"Nome da entidade que executa a função", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>9, );
    $this->mef_items["mef_action"] = array("pk"=>0, "fk"=>0, "id"=>"mef_action", "description"=>"Ação", "title"=>"Identificador de ação da função que será executada", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>10, );
    $this->mef_items["mef_filter"] = array("pk"=>0, "fk"=>0, "id"=>"mef_filter", "description"=>"Filtro", "title"=>"Determina o valor do filtro inicial que será aplicado ao comando", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>11, );
    $this->mef_items["mef_ativo"] = array("pk"=>0, "fk"=>0, "id"=>"mef_ativo", "description"=>"Ativo", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>12, );


    // Atributos FK
    $this->mef_items["mef_cod_MENU"] = array("pk"=>0, "fk"=>1, "id"=>"mef_cod_MENU", "description"=>"Menu", "title"=>"Menu no qual a função será exibida", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"Menu", "table"=>"TBL_MENU", "prefix"=>"men", "tag"=>"menu", "key"=>"men_codigo", "description"=>"men_descricao", "form"=>"form", "target"=>"div-mef_cod_MENU-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"200", "where"=>"", "filter"=>""));
    $this->mef_items["mef_permissoes"] = array("pk"=>0, "fk"=>0, "id"=>"mef_permissoes", "description"=>"Permissões", "title"=>"Exibe as permissões de acesso para esta funcionalidade", "type"=>"subitem", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>13, "foreign"=>array("modulo"=>"manager", "entity"=>"MenuFuncaoUsuarioTipo", "table"=>"TBL_MENU_FUNCAO_USUARIO_TIPO", "prefix"=>"mft", "tag"=>"menufuncaousuariotipo", "key"=>"mft_cod_MENU_FUNCAO", "description"=>"mft_cod_USUARIO_TIPO", "form"=>"form", "target"=>"div-mef_permissoes-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->mef_items['mef_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mef_items['mef_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'mef_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->mef_items = $this->configureItemsMenuFuncao($this->mef_items);

    $join = $this->configureJoinMenuFuncao($this->mef_items);

    $lines = 0;
    foreach ($this->mef_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->mef_properties = array(
      'rotule'=>'Função de Menu',
      'module'=>'manager',
      'entity'=>'MenuFuncao',
      'table'=>'TBL_MENU_FUNCAO',
      'join'=>$join,
      'tag'=>'menufuncao',
      'prefix'=>'mef',
      'order'=>'men_ordem, mef_order',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'mef_codigo',
      'description'=>'mef_descricao',
      'emptyCollection'=>'<p><b>Nenhum item cadastrado</b></p><br><p>Para começar preciso cadastrar um <b>Função de Menu</b>. Clique em "Novo" logo acima e em seguida já será possível localizar os items nesta lista</p><br>',
      'emptyFilter'=>'<p><b>Nenhum resultado foi encontrado</b></p><br><p>Parece que você usou filtros que levaram a uma consulta sem resultados.</p><p>Tente selecionar outras opções ou alterar a sua pesquisa.</p><br>',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("id"=>"save", "action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("id"=>"copy", "action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("id"=>"add", "action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("id"=>"search", "action"=>'search', "get"=>"search", "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("id"=>"find", "action"=>'list', "get"=>"find", "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("id"=>"back", "action"=>'list', "get"=>"clear", "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("id"=>"view", "action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("id"=>"set", "action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("id"=>"remove", "action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("id"=>"list", "action"=>'list', "get"=>"collection", "label"=>"Função de Menu", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"primary")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->mef_properties['reference']) {
      foreach ($this->mef_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->mef_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->mef_properties['description']) {
      foreach ($this->mef_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->mef_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsMenuFuncao();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:14
   */
  public  function get_mef_properties(){
    ?><?php
    return $this->mef_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:14
   */
  public  function get_mef_items(){
    ?><?php
    return $this->mef_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   */
  public  function get_mef_item($key){
    ?><?php

		$this->validateItemMenuFuncao($key);

    return $this->mef_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   */
  public  function get_mef_reference(){
    ?><?php
    $key = $this->mef_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:15
   */
  public  function get_mef_value($key){
    ?><?php

		$this->validateItemMenuFuncao($key);

    return $this->mef_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:16
   */
  public  function set_mef_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemMenuFuncao($key);

    $this->mef_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->mef_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:17
   */
  public  function set_mef_type($key, $type){
    ?><?php

		$this->validateItemMenuFuncao($key);

    $this->mef_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:17
   */
  private  function configureJoinMenuFuncao($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:18
   */
  private  function configureItemsMenuFuncao($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->mef_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->mef_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:18
   */
  public  function clearMenuFuncao($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_mef_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_mef_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_mef_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   */
  private  function validateItemMenuFuncao($key){
    ?><?php

    if (!isset($this->mef_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   */
  public  function setStatementsMenuFuncao(){
    ?><?php

    $this->mef_statements["0"] = "mef_codigo = '?'";

    $this->mef_statements["mef_0"] = $this->mef_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/11/2015 10:38:19
   */
  public  function getStatementsMenuFuncao(){
    ?><?php

    return $this->mef_statements;
  }


}