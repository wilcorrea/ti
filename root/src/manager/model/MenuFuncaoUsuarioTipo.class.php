<?php
/**
 * @copyright array software
 *
 * @author ADMINISTRADOR - 16/06/2013 10:06:05
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:41:44
 * @category model
 * @package manager
 */


class MenuFuncaoUsuarioTipo
{
  private  $mft_items = array();
  private  $mft_properties = array();
  private  $mft_parents = array();
  private  $mft_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:57
   */
  public  function MenuFuncaoUsuarioTipo(){
    ?><?php

    $this->mft_items = array();
    
    // Atributos
    $this->mft_items["mft_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"mft_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->mft_items["mft_level"] = array("pk"=>0, "fk"=>0, "id"=>"mft_level", "description"=>"Nível de Acesso", "title"=>"", "type"=>"option", "type_content"=>"0,Baixo|1,Médio|2,Alto", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );


    // Atributos FK
    $this->mft_items["mft_cod_MENU_FUNCAO"] = array("pk"=>0, "fk"=>1, "id"=>"mft_cod_MENU_FUNCAO", "description"=>"Função de Menu", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"MenuFuncao", "table"=>"TBL_MENU_FUNCAO", "prefix"=>"mef", "tag"=>"menufuncao", "key"=>"mef_codigo", "description"=>"mef_descricao", "form"=>"form", "target"=>"div-mft_cod_MENU_FUNCAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->mft_items["mft_cod_USUARIO_TIPO"] = array("pk"=>0, "fk"=>1, "id"=>"mft_cod_USUARIO_TIPO", "description"=>"Tipo de Usuário", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"manager", "entity"=>"UsuarioTipo", "table"=>"TBL_USUARIO_TIPO", "prefix"=>"ust", "tag"=>"usuariotipo", "key"=>"ust_codigo", "description"=>"ust_descricao", "form"=>"form", "target"=>"div-mft_cod_USUARIO_TIPO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->mft_items['mft_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->mft_items['mft_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'mft_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->mft_items = $this->configureItemsMenuFuncaoUsuarioTipo($this->mft_items);

    $join = $this->configureJoinMenuFuncaoUsuarioTipo($this->mft_items);

    $lines = 0;
    foreach ($this->mft_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->mft_properties = array(
      'rotule'=>'Permissões',
      'module'=>'manager',
      'entity'=>'MenuFuncaoUsuarioTipo',
      'table'=>'TBL_MENU_FUNCAO_USUARIO_TIPO',
      'join'=>$join,
      'tag'=>'menufuncaousuariotipo',
      'prefix'=>'mft',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'mft_codigo',
      'description'=>'mft_cod_USUARIO_TIPO',
      'notification'=>false,
      'emptyCollection'=>'<p><b>Nenhum item cadastrado</b></p><br><p>Para começar preciso cadastrar um <b>Permissões</b>. Clique em "Novo" logo acima e em seguida já será possível localizar os items nesta lista</p><br>',
      'emptyFilter'=>'<p><b>Nenhum resultado foi encontrado</b></p><br><p>Parece que você usou filtros que levaram a uma consulta sem resultados.</p><p>Tente selecionar outras opções ou alterar a sua pesquisa.</p><br>',
      'operations'=>array(
        //{
          'save'=>(object)(array("id"=>"save", "action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("id"=>"copy", "action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("id"=>"add", "action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("id"=>"search", "action"=>'search', "get"=>"search", "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("id"=>"find", "action"=>'list', "get"=>"find", "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("id"=>"back", "action"=>'list', "get"=>"clear", "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("id"=>"view", "action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("id"=>"set", "action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("id"=>"remove", "action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("id"=>"list", "action"=>'list', "get"=>"collection", "label"=>"Permissões", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"primary")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->mft_properties['reference']) {
      foreach ($this->mft_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->mft_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->mft_properties['description']) {
      foreach ($this->mft_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->mft_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsMenuFuncaoUsuarioTipo();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:57
   */
  public  function get_mft_properties(){
    ?><?php
    return $this->mft_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   */
  public  function get_mft_items(){
    ?><?php
    return $this->mft_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   */
  public  function get_mft_item($key){
    ?><?php

		$this->validateItemMenuFuncaoUsuarioTipo($key);

    return $this->mft_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   */
  public  function get_mft_reference(){
    ?><?php
    $key = $this->mft_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:59
   */
  public  function get_mft_value($key){
    ?><?php

		$this->validateItemMenuFuncaoUsuarioTipo($key);

    return $this->mft_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:54:59
   */
  public  function set_mft_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemMenuFuncaoUsuarioTipo($key);

    $this->mft_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->mft_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:00
   */
  public  function set_mft_type($key, $type){
    ?><?php

		$this->validateItemMenuFuncaoUsuarioTipo($key);

    $this->mft_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:00
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:00
   */
  private  function configureJoinMenuFuncaoUsuarioTipo($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:01
   */
  private  function configureItemsMenuFuncaoUsuarioTipo($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->mft_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->mft_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:01
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:02
   */
  public  function clearMenuFuncaoUsuarioTipo($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_mft_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_mft_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_mft_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:02
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:02
   */
  private  function validateItemMenuFuncaoUsuarioTipo($key){
    ?><?php

    if (!isset($this->mft_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   */
  public  function setStatementsMenuFuncaoUsuarioTipo(){
    ?><?php

    $this->mft_statements["0"] = "mft_codigo = '?'";

    $this->mft_statements["mft_0"] = $this->mft_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 12/11/2015 14:55:03
   */
  public  function getStatementsMenuFuncaoUsuarioTipo(){
    ?><?php

    return $this->mft_statements;
  }


}