<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 12:41:44
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:19
 * @category model
 * @package manager
 */


class TarefaAgendada
{
  private  $tra_items = array();
  private  $tra_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function TarefaAgendada(){
    ?><?php

    $this->tra_items = array();
    
    // Atributos
    $this->tra_items["tra_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"tra_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->tra_items["tra_arquivo"] = array("pk"=>0, "fk"=>0, "id"=>"tra_arquivo", "description"=>"Arquivo", "title"=>"Arquivo que processa a tarefa", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->tra_items["tra_execucao_inicio"] = array("pk"=>0, "fk"=>0, "id"=>"tra_execucao_inicio", "description"=>"Início da Execução", "title"=>"Armazena o instante em que o arquivo começou a ser processado", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->tra_items["tra_execucao_termino"] = array("pk"=>0, "fk"=>0, "id"=>"tra_execucao_termino", "description"=>"Término da Execução", "title"=>"Armazena o instante em que o arquivo terminou o processamento", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->tra_items["tra_saida"] = array("pk"=>0, "fk"=>0, "id"=>"tra_saida", "description"=>"Saída", "title"=>"Dados informados pela tarefa", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->tra_items['tra_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'tra_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->tra_items['tra_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'tra_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->tra_items['tra_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'tra_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->tra_items['tra_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'tra_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->tra_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->tra_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->tra_properties = array(
			'rotule'=>'Tarefas Agendadas',
      'module'=>'manager',
      'entity'=>'TarefaAgendada',
      'table'=>'TBL_TAREFA_AGENDADA',
			'join'=>$join,
      'tag'=>'tarefaagendada',
      'prefix'=>'tra',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			//'action'=>array('icon-push'=>array("className"=>'icon-push', "title"=>"Ativar", "level"=>2, "message"=>"", "handler"=>"Ativar", "confirm"=>"Deseja realmente ativar este registro?", "feedback"=>true, "reload"=>true, "condition"=>array("field"=>'fcr_ativo', "value"=>0), "confirm"=>array("active"=>true, "title"=>"Ativação de Facred", "message"=>"Deseja realmente ativar este Facred?"), "operation"=>array("command"=>"save", "update"=>"fcr_ativo=1"))),
      'remove'=>'',
			//false, true or
			//array("field"=>"tra_id", "value"=>"1", "className"=>"icon-remove", "title"=>"Excluir", "message"=>"Deseja realmente remover este registro?", "success"=>"Registro removido com sucesso", "handler"=>"Remover"),
			'database'=>null,
      'reference'=>'tra_codigo',
      'description'=>'tra_codigo',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->tra_properties['reference']) {
      foreach ($this->tra_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->tra_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->tra_properties['description']) {
      foreach ($this->tra_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->tra_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function get_tra_properties(){
    ?><?php
    return $this->tra_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function get_tra_items(){
    ?><?php
    return $this->tra_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function get_tra_item($key){
    ?><?php
    return $this->tra_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function get_tra_reference(){
    ?><?php
    $key = $this->tra_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function get_tra_value($key){
    ?><?php
    return $this->tra_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:24
   */
  public  function set_tra_value($key, $value){
?><?php
		if (!isset($this->tra_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->tra_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:25
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/04/2014 17:00:25
   */
  public  function set_tra_type($key, $type){
    ?><?php
    $this->tra_items[$key]['type'] = $type;
  }


}