<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/04/2013 12:46:48
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 20:10:49
 * @category model
 * @package manager
 */


class Usuario
{
  private  $usu_items = array();
  private  $usu_properties = array();
  private  $usu_parents = array();
  private  $usu_statements = array();
  public static $USU_PASSWORD_REQUEST = "{usu_codigo}-password-{usu_auth_date}";

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 16:57:51
   */
  public  function Usuario(){
    ?><?php

    $this->usu_items = array();
    
    // Atributos
    $this->usu_items["usu_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"usu_codigo", "description"=>"Código", "title"=>"Chave primária da entidade", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->usu_items["usu_nome"] = array("pk"=>0, "fk"=>0, "id"=>"usu_nome", "description"=>"Nome", "title"=>"Nome do usuário", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->usu_items["usu_telefone"] = array("pk"=>0, "fk"=>0, "id"=>"usu_telefone", "description"=>"Telefone", "title"=>"", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->usu_items["usu_celular"] = array("pk"=>0, "fk"=>0, "id"=>"usu_celular", "description"=>"Celular", "title"=>"", "type"=>"telefone", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );
    $this->usu_items["usu_login"] = array("pk"=>0, "fk"=>0, "id"=>"usu_login", "description"=>"Login", "title"=>"Nome de usuário", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>8, );
    $this->usu_items["usu_password"] = array("pk"=>0, "fk"=>0, "id"=>"usu_password", "description"=>"Senha", "title"=>"Senha de acesso do usuário (necessário 6 dígitos)", "type"=>"hash", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[P]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );
    $this->usu_items["usu_admin"] = array("pk"=>0, "fk"=>0, "id"=>"usu_admin", "description"=>"Administrador", "title"=>"Define se o usuário tem privilégios de administrador ou não", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>10, );
    $this->usu_items["usu_email"] = array("pk"=>0, "fk"=>0, "id"=>"usu_email", "description"=>"E-mail", "title"=>"Endereço eletrônico do usuário utilizado para notificações e recuperação de senhas", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>11, );
    $this->usu_items["usu_auth"] = array("pk"=>0, "fk"=>0, "id"=>"usu_auth", "description"=>"Autorização", "title"=>"Armazena a chave liberação para edição da conta sem ter acesso a ferramenta", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>0, "line"=>12, "order"=>12, );
    $this->usu_items["usu_auth_date"] = array("pk"=>0, "fk"=>0, "id"=>"usu_auth_date", "description"=>"Data da Autorização", "title"=>"", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>13, );
    $this->usu_items["usu_ativo"] = array("pk"=>0, "fk"=>0, "id"=>"usu_ativo", "description"=>"Ativo", "title"=>"Determina se o usuário está ativo ou não", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>14, );
    $this->usu_items["usu_html"] = array("pk"=>0, "fk"=>0, "id"=>"usu_html", "description"=>"Aviso", "title"=>"", "type"=>"html", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>14, "order"=>15, );


    // Atributos FK
    $this->usu_items["usu_cod_USUARIO_TIPO"] = array("pk"=>0, "fk"=>1, "id"=>"usu_cod_USUARIO_TIPO", "description"=>"Tipo", "title"=>"Determina qual o tipo de acesso do Usuário", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"manager", "entity"=>"UsuarioTipo", "table"=>"TBL_USUARIO_TIPO", "prefix"=>"ust", "tag"=>"usuariotipo", "key"=>"ust_codigo", "description"=>"ust_descricao", "form"=>"form", "target"=>"div-usu_cod_USUARIO_TIPO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"200", "where"=>"", "filter"=>""));
    $this->usu_items["usu_cod_INSTITUICAO"] = array("pk"=>0, "fk"=>1, "id"=>"usu_cod_INSTITUICAO", "description"=>"Organização", "title"=>"Relacionamento com a organização a qual o usuário pertence", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, "foreign"=>array("modulo"=>"organizacao", "entity"=>"Instituicao", "table"=>"TBL_INSTITUICAO", "prefix"=>"ist", "tag"=>"instituicao", "key"=>"ist_codigo", "description"=>"ist_descricao", "form"=>"form", "target"=>"div-usu_cod_INSTITUICAO-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->usu_items['usu_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->usu_items['usu_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'usu_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->usu_items = $this->configureItemsUsuario($this->usu_items);

    $join = $this->configureJoinUsuario($this->usu_items);

    $lines = 0;
    foreach ($this->usu_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->usu_properties = array(
      'rotule'=>'Usuário',
      'module'=>'manager',
      'entity'=>'Usuario',
      'table'=>'TBL_USUARIO',
      'join'=>$join,
      'tag'=>'usuario',
      'prefix'=>'usu',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'usu_codigo',
      'description'=>'usu_nome',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"))),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'r'=>(object) (array("action"=>'request', "label"=>"Solicitar", "layout"=>"", "position"=>"toolbar-bottom", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"", "fail"=>""))),
          'request'=>(object) (array("action"=>'request', "label"=>"Recuperação de Senha", "layout"=>"manager", "position"=>"manager", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>true, "child"=>false, "history"=>false, "operations"=>array("r"=>"primary"), "confirm"=>"", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Solicitação realizada com sucesso! Confira seu e-mail em instantes.", "fail"=>"Não foi possível solicitar a alteração de senha. Usuário ou e-mail não encontrados"), "execute"=>"Application.usuario.execute(data);")),

          'c'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"", "position"=>"toolbar-bottom", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>array("usu_html"=>"")/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"", "fail"=>""))),
          'change'=>(object) (array("action"=>'set', "label"=>"Alteração de Senha", "layout"=>"manager", "position"=>"manager", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>true, "child"=>false, "history"=>false, "operations"=>array("c"=>"primary"), "confirm"=>"", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Senha modificada com sucesso.", "fail"=>"Não foi possível alterar a senha."), "execute"=>"Application.usuario.execute(data);")),

          'new'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"manager", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>true, "child"=>false, "history"=>false, "operations"=>array("save"=>"primary"), "confirm"=>"", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Usuário criado com sucesso! Confira seu e-mail em instantes para a validação.", "fail"=>"Não foi possível criar o usuário."), "execute"=>"Application.usuario.execute(data);")),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->usu_properties['reference']) {
      foreach ($this->usu_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->usu_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->usu_properties['description']) {
      foreach ($this->usu_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->usu_properties['reference'] = $id;
          break;
        }
      }
    }
    
    $this->setStatementsUsuario();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function get_usu_properties(){
    ?><?php
    return $this->usu_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function get_usu_items(){
    ?><?php
    return $this->usu_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function get_usu_item($key){
    ?><?php

		$this->validateItemUsuario($key);

    return $this->usu_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function get_usu_reference(){
    ?><?php
    $key = $this->usu_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function get_usu_value($key){
    ?><?php

		$this->validateItemUsuario($key);

    return $this->usu_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da inst��ncia da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function set_usu_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemUsuario($key);

    $this->usu_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->usu_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function set_usu_type($key, $type){
    ?><?php

		$this->validateItemUsuario($key);

    $this->usu_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  private  function configureJoinUsuario($items){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas caracterí­sticas
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  private  function configureItemsUsuario($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->usu_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->usu_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function clearUsuario($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_usu_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_usu_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  private  function validateItemUsuario($key){
    ?><?php

    if (!isset($this->usu_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 30/03/2015 17:14:32
   */
  public  function setStatementsUsuario(){
    ?><?php

    $this->usu_statements[0] = "usu_codigo = '?'";

    $this->usu_statements['usu_0'] = "usu_auth = '?' AND usu_auth_date >= DATE_SUB(NOW(), INTERVAL 2 HOUR)";

    $this->usu_statements['usu_1'] = "(usu_login = '?' OR usu_email = '?')";
    $this->usu_statements['login'] = $this->usu_statements['usu_1'] . " AND usu_ativo = 1";
    $this->usu_statements['login_pass'] = $this->usu_statements['login'] . " AND usu_password = MD5('?')";

    $this->usu_statements['stt_usu_unique'] = "? = '?' AND usu_codigo <> '?'";


    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 19:44:33
   */
  public  function getStatementsUsuario(){
    ?><?php

    return $this->usu_statements;
  }

  /**
   * Recupera um objeto da entidade formatado
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 20:03:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 28/09/2014 20:08:50
   */
  public  function get_usu_class(){
    ?><?php

    $usuario = array();

    $usuario['usu_codigo'] = md5($this->get_usu_value('usu_codigo'));
    $usuario['usu_login'] = Encode::encrypt($this->get_usu_value('usu_login'));
    $usuario['usu_tipo'] = Encode::encrypt($this->get_usu_value('usu_cod_USUARIO_TIPO'));
    $usuario['usu_nome'] = Encode::encrypt($this->get_usu_value('usu_nome'));
    $usuario['usu_email'] = Encode::encrypt($this->get_usu_value('usu_email'));
    $usuario['usu_admin'] = md5($this->get_usu_value('usu_admin'));

    return $usuario;
  }


}