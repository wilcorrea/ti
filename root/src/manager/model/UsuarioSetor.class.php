<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:24
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 02/09/2013 09:30:24
 * @category model
 * @package manager
 */


class UsuarioSetor
{
  private  $uss_items = array();
  private  $uss_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:24
   */
  public  function UsuarioSetor(){
?><?php
    $this->uss_items = array();
    
    // Atributos
    $this->uss_items["uss_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"uss_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->uss_items["uss_nome"] = array("pk"=>0, "fk"=>0, "id"=>"uss_nome", "description"=>"Nome", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->uss_items['uss_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'uss_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->uss_items['uss_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'uss_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->uss_items['uss_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'uss_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->uss_items['uss_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'uss_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->uss_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->uss_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->uss_properties = array(
			'rotule'=>'Setor do Usuário',
      'module'=>'manager',
      'entity'=>'UsuarioSetor',
      'table'=>'TBL_USUARIO_SETOR',
			'join'=>$join,
      'tag'=>'usuariosetor',
      'prefix'=>'uss',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'remove'=>'',//false or
			 /**
        * array(
        *  "field"=>"prefix_id",
        *  "value"=>"1",
        *  "className"=>"icon-trash",
        *  "title"=>"Excluir",
        *  "message"=>"Deseja realmente remover este registro?",
        *  "success"=>"Registro removido com sucesso"
        * )
        */
			'database'=>null,
      'reference'=>'uss_codigo',
      'description'=>'uss_nome',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->uss_properties['reference']) {
      foreach ($this->uss_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->uss_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->uss_properties['description']) {
      foreach ($this->uss_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->uss_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:25
   */
  public  function get_uss_properties(){
    ?><?php
    return $this->uss_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function get_uss_items(){
    ?><?php
    return $this->uss_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function get_uss_item($key){
    ?><?php
    return $this->uss_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function get_uss_reference(){
    ?><?php
    $key = $this->uss_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function get_uss_value($key){
    ?><?php
    return $this->uss_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function set_uss_value($key, $value){
?><?php
		if (!isset($this->uss_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->uss_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/08/2013 16:24:26
   */
  public  function set_uss_type($key, $type){
    ?><?php
    $this->uss_items[$key]['type'] = $type;
  }


}