<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 08/05/2015 00:58:52
 * @category controller
 * @package movimento
 */


class PedidoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function PedidoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');
    
    $entity = new Pedido();
    $properties = $entity->get_pdd_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getRowsPedidoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    $items = $pedido->get_pdd_items();
    $properties = $pedido->get_pdd_properties();

    $statements = $pedido->getStatementsPedido();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getPedidoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    $pedidos = null;

    $rows = $this->getRowsPedidoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $pedidos = array();
      foreach ($rows as $row) {
        $pedido_set = clone $pedido;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $pedido_set->set_pdd_value($key, $item['value'], $reference);
        }
        $pedido_set->clearPedido($not_clear);
        $pedidos[] = $pedido_set;
      }
    }

    return $pedidos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getColumnPedidoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    $properties = $pedido->get_pdd_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $pedido->getStatementsPedido();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getCountPedidoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    $properties = $pedido->get_pdd_properties();

    $statements = $pedido->getStatementsPedido();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPedidoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  private  function beforePedidoCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsPedidoCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_pdd_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_pdd_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_pdd_value($item['id'], $value);
            } else {
              $object->set_pdd_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsPedidoCtrl("");
      $properties = $this->getPropertiesPedidoCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_pdd_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnPedidoCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function addPedidoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPedidoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pdd_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPedidoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pdd_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setPedidoCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function setPedidoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pdd_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPedidoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pdd_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function removePedidoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pdd_items();
    $properties = $this->getPropertiesPedidoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pdd_value($action['field'], $action['value']);
        $object->clearPedido(array($action['field']));

        $remove = $this->setPedidoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  private  function afterPedidoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {

      $value = $object->get_pdd_value($object->get_pdd_reference());

    } else if ($param === 'add') {

      $object->set_pdd_value($object->get_pdd_reference(), $value);

      System::import('c', 'movimento', 'PedidoItem', 'src');

      $pedidoItemCtrl = new PedidoItemCtrl(APP_PATH);

      $pdd_items = $object->get_pdd_value('pdd_items');

      if (is_array($pdd_items)) {

        //var_dump($pdd_items);

        $pedidoItemCtrl->clearPedidoItemCtrl($value);

        foreach ($pdd_items as $pdd_item) {

          $pedido_item = (array) Json::decode(Encode::decrypt($pdd_item));

          $prd_codigo = $pedido_item['prd_codigo'];
          $pdi_quantidade = $pedido_item['pdi_quantidade'];
          $pdi_valor_bruto = $pedido_item['pdi_valor_bruto'];
          $pdi_valor_desconto = $pedido_item['pdi_valor_desconto'];
          $pdi_valor_liquido = $pedido_item['pdi_valor_liquido'];

          $pdi_codigo = $pedidoItemCtrl->createPedidoItemCtrl($value, $prd_codigo, $pdi_quantidade, $pdi_valor_bruto, $pdi_valor_desconto, $pdi_valor_liquido);
          if (!$pdi_codigo) {
            Console::add('Erro ao tentar add o produto ' . $prd_codigo, null, null, 'Items do Pedido');
          }
        }
      }
    }

    if ($param !== 'remove') {
      $items = $object->get_pdd_items();

      $fields = Controller::after($value, $items, 'pdd');
      if (count($fields)) {
        $executed = $this->executePedidoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pdd_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function executePedidoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    $properties = $pedido->get_pdd_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $pedido->getStatementsPedido();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pdd_responsavel = '" . System::getUser(false) . "', pdd_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  private  function verifyPedidoCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_pdd_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getObjectPedidoCtrl($value){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $object = new Pedido();

    $properties = $object->get_pdd_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $pedido = null;

    $pedidos = $this->getPedidoCtrl($where, "", "", "0", "1");
    if (is_array($pedidos)) {
      $pedido = $pedidos[0];
    }

    return $pedido;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getReplacesPedidoCtrl($reference){
		?><?php

		System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

		$pedido = new Pedido();

    $replaces = array();

    $where = "pdd_codigo = '" . $reference . "'";
    $pedidos = $this->getPedidoCtrl($where, "", "");
    if (is_array($pedidos)) {
      $pedido = $pedidos[0];

      $items = $pedido->get_pdd_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function configurePedidoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

      case "terminal":

        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   */
  public  function getPropertiesPedidoCtrl(){
    ?><?php

      System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

      $pedido = new Pedido();

      $properties = $pedido->get_pdd_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   */
  public  function getItemsPedidoCtrl($search){
    ?><?php

    System::import('m', 'movimento', 'Pedido', 'src', true, '{project.rsc}');

    $pedido = new Pedido();

    if ($search) {
      $object = $this->getObjectPedidoCtrl($search);
      if (!is_null($object)) {
        $pedido = $object;
      }
    }
    
    $items = $pedido->get_pdd_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $pedido->get_pdd_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   */
  public  function getHistoryPedidoCtrl($items){
    ?><?php
    
    $defaults = array(
      'pdd_registro'=>array('id'=>'pdd_registro', 'value'=>date('d/m/Y H:i:s')),
      'pdd_criador'=>array('id'=>'pdd_criador', 'value'=>System::getUser()),
      'pdd_alteracao'=>array('id'=>'pdd_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pdd_responsavel'=>array('id'=>'pdd_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param string $nonce 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   */
  public  function operationPedidoCtrl($operation, $items, $nonce = null, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'movimento', 'Pedido', 'src', true);

    $pedido = new Pedido();

    $reference = null;
    $after = false;

    if (Security::isFree($nonce)) {

      Security::setNonce($nonce, false);

      Connection::startTransaction();
  
      $properties = $pedido->get_pdd_properties();
      $operations = $properties['operations'];
      $o = $operations[$operation];
      $action = $o->action;
  
      foreach ($items as $id => $item) {
        $pedido->set_pdd_value($id, $item['value']);
      }
      
      $method = $action . "PedidoCtrl";
  
      if (method_exists($this, $method)) {
  
        $verify = $this->verifyPedidoCtrl($pedido, $operation);
        if ($verify) {
  
          $before = $this->beforePedidoCtrl($pedido, $operation);
          if ($before) {
  
            $response = $this->$method($pedido);
            if (is_a($response, 'Response')) {
  
              if ($response->result) {
  
                Security::setNonce($nonce, true);

                $reference = $response->reference;
                $after = $this->afterPedidoCtrl($pedido, $operation, $response->reference);
              }
            } else {

              Console::add("A resposta do método [" . $method . "] não é a esperada");
            }
          }
        }
      } else {

        Console::add("Método '" . $method . "' não foi encontrado na classe 'PedidoCtrl'");
      }
  
      if ($after) {
        Connection::commitTransaction();
      } else {
        Connection::rollbackTransaction();
      }

    } else {

      Console::add("Já existe uma solicitação em andamento em nossos servidores [" . $nonce . "]", $status = null, $key = null, 'Controle de Transação');
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:54
   */
  public  function copyPedidoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPedidoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pdd_value($reference, null);
    }

    return $this->addPedidoCtrl($object, $validate, $debug);
  }

  /**
   * 
   * @param $pedido
   * @param $validate
   * @param $debug
   */
  public function terminalPedidoCtrl($pedido, $validate = true, $debug = false) {
    ?><?php

    $pdd_codigo = $pedido->get_pdd_value('pdd_codigo');

    $status = false;

    if (!$pdd_codigo) {

      $pdd_codigo = $this->operationPedidoCtrl('add', $pedido->get_pdd_items());

      if ($pdd_codigo > 0) {

        $status = true;
      }
    }

    return new Response($status, $pdd_codigo);
  }

}