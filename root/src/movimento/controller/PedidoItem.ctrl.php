<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:20
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 08:34:42
 * @category controller
 * @package movimento
 */


class PedidoItemCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function PedidoItemCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');
    
    $entity = new PedidoItem();
    $properties = $entity->get_pdi_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getRowsPedidoItemCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    $items = $pedidoItem->get_pdi_items();
    $properties = $pedidoItem->get_pdi_properties();

    $statements = $pedidoItem->getStatementsPedidoItem();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getPedidoItemCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    $pedidoItems = null;

    $rows = $this->getRowsPedidoItemCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $pedidoItems = array();
      foreach ($rows as $row) {
        $pedidoItem_set = clone $pedidoItem;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $pedidoItem_set->set_pdi_value($key, $item['value'], $reference);
        }
        $pedidoItem_set->clearPedidoItem($not_clear);
        $pedidoItems[] = $pedidoItem_set;
      }
    }

    return $pedidoItems;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getColumnPedidoItemCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    $properties = $pedidoItem->get_pdi_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $pedidoItem->getStatementsPedidoItem();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getCountPedidoItemCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    $properties = $pedidoItem->get_pdi_properties();

    $statements = $pedidoItem->getStatementsPedidoItem();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPedidoItemCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  private  function beforePedidoItemCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsPedidoItemCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_pdi_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_pdi_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_pdi_value($item['id'], $value);
            } else {
              $object->set_pdi_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsPedidoItemCtrl("");
      $properties = $this->getPropertiesPedidoItemCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_pdi_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnPedidoItemCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function addPedidoItemCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPedidoItemCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pdi_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPedidoItemCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pdi_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setPedidoItemCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function setPedidoItemCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pdi_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPedidoItemCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pdi_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function removePedidoItemCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pdi_items();
    $properties = $this->getPropertiesPedidoItemCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pdi_value($action['field'], $action['value']);
        $object->clearPedidoItem(array($action['field']));

        $remove = $this->setPedidoItemCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  private  function afterPedidoItemCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_pdi_value($object->get_pdi_reference());
    } else if ($param === 'add') {
      $object->set_pdi_value($object->get_pdi_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_pdi_items();

      $fields = Controller::after($value, $items, 'pdi');
      if (count($fields)) {
        $executed = $this->executePedidoItemCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pdi_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function executePedidoItemCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    $properties = $pedidoItem->get_pdi_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $pedidoItem->getStatementsPedidoItem();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pdi_responsavel = '" . System::getUser(false) . "', pdi_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  private  function verifyPedidoItemCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_pdi_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getObjectPedidoItemCtrl($value){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $object = new PedidoItem();

    $properties = $object->get_pdi_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $pedidoItem = null;

    $pedidoItems = $this->getPedidoItemCtrl($where, "", "", "0", "1");
    if (is_array($pedidoItems)) {
      $pedidoItem = $pedidoItems[0];
    }

    return $pedidoItem;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getReplacesPedidoItemCtrl($reference){
		?><?php

		System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

		$pedidoItem = new PedidoItem();

    $replaces = array();

    $where = "pdi_codigo = '" . $reference . "'";
    $pedidoItems = $this->getPedidoItemCtrl($where, "", "");
    if (is_array($pedidoItems)) {
      $pedidoItem = $pedidoItems[0];

      $items = $pedidoItem->get_pdi_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function configurePedidoItemCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getPropertiesPedidoItemCtrl(){
    ?><?php

      System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

      $pedidoItem = new PedidoItem();

      $properties = $pedidoItem->get_pdi_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getItemsPedidoItemCtrl($search){
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src', true, '{project.rsc}');

    $pedidoItem = new PedidoItem();

    if ($search) {
      $object = $this->getObjectPedidoItemCtrl($search);
      if (!is_null($object)) {
        $pedidoItem = $object;
      }
    }
    
    $items = $pedidoItem->get_pdi_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $pedidoItem->get_pdi_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:23
   */
  public  function getHistoryPedidoItemCtrl($items){
    ?><?php
    
    $defaults = array(
      'pdi_registro'=>array('id'=>'pdi_registro', 'value'=>date('d/m/Y H:i:s')),
      'pdi_criador'=>array('id'=>'pdi_criador', 'value'=>System::getUser()),
      'pdi_alteracao'=>array('id'=>'pdi_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pdi_responsavel'=>array('id'=>'pdi_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:24
   */
  public  function operationPedidoItemCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'movimento', 'PedidoItem', 'src', true);

    $pedidoItem = new PedidoItem();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $pedidoItem->get_pdi_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $pedidoItem->set_pdi_value($id, $item['value']);
    }
    
    $method = $action . "PedidoItemCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyPedidoItemCtrl($pedidoItem, $operation);
      if ($verify) {

        $before = $this->beforePedidoItemCtrl($pedidoItem, $operation);
        if ($before) {

          $response = $this->$method($pedidoItem);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterPedidoItemCtrl($pedidoItem, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'PedidoItemCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 01:49:24
   */
  public  function copyPedidoItemCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPedidoItemCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pdi_value($reference, null);
    }

    return $this->addPedidoItemCtrl($object, $validate, $debug);
  }

  public function createPedidoItemCtrl($pdd_codigo, $prd_codigo, $pdi_quantidade, $pdi_valor_bruto, $pdi_valor_desconto, $pdi_valor_liquido = null) {
    ?><?php

    System::import('m', 'movimento', 'PedidoItem', 'src');

    $pedidoItem = new PedidoItem();

    if (is_null($pdi_valor_liquido)) {
      $pdi_valor_liquido = $pdi_valor_bruto - $pdi_valor_desconto;
    }

    $pedidoItem->set_pdi_value('pdi_cod_PEDIDO', $pdd_codigo);
    $pedidoItem->set_pdi_value('pdi_cod_PRODUTO', $prd_codigo);

    $pedidoItem->set_pdi_value('pdi_quantidade', $pdi_quantidade);
    $pedidoItem->set_pdi_value('pdi_valor_bruto', $pdi_valor_bruto);
    $pedidoItem->set_pdi_value('pdi_valor_desconto', $pdi_valor_desconto);
    $pedidoItem->set_pdi_value('pdi_valor_liquido', $pdi_valor_liquido);

    $pdi_codigo = $this->operationPedidoItemCtrl('add', $pedidoItem->get_pdi_items());

    return $pdi_codigo;
  }
  
  public function clearPedidoItemCtrl($pdd_codigo) {
    ?><?php

    $where_pdi = "pdi_cod_PEDIDO = '" . $pdd_codigo . "'";
    $deleted = $this->executePedidoItemCtrl($where_pdi, "");

    return $deleted;
  }

}