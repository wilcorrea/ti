<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 08:34:42
 * @category model
 * @package movimento
 */


class Pedido
{
  private  $pdd_items = array();
  private  $pdd_properties = array();
  private  $pdd_parents = array();
  private  $pdd_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function Pedido(){
    ?><?php

    $this->pdd_items = array();
    
    // Atributos
    $this->pdd_items["pdd_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"pdd_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->pdd_items["pdd_codigo_externo"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_codigo_externo", "description"=>"Sincronização", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->pdd_items["pdd_data_pedido"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_data_pedido", "description"=>"Data", "title"=>"Data base na qual o registro do pedido foi incorporado ao cadastro do cliente", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[D]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->pdd_items["pdd_comissao"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_comissao", "description"=>"Comissão", "title"=>"Total da Comissão", "type"=>"percent", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>6, );
    $this->pdd_items["pdd_coluna_preco"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_coluna_preco", "description"=>"Prazo", "title"=>"", "type"=>"list", "type_content"=>"1,30|2,30/60|3,30/60/90|4,30/60/90/120", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>8, );
    $this->pdd_items["pdd_forma_pagamento"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_forma_pagamento", "description"=>"Forma de Pagamento", "title"=>"V,A VISTA|P,A PRAZO|C,CONTRA APRESENTAÇÃO|O,OUTROS", "type"=>"option", "type_content"=>"V,A VISTA|P,A PRAZO|C,CONTRA-APRESENTAÇÃO|O,OUTROS", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>9, );
    $this->pdd_items["pdd_prazo1"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo1", "description"=>"Prazo 1", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>11, );
    $this->pdd_items["pdd_prazo2"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo2", "description"=>"Prazo 2", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>12, );
    $this->pdd_items["pdd_prazo3"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo3", "description"=>"Prazo 3", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>13, );
    $this->pdd_items["pdd_prazo4"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo4", "description"=>"Prazo 4", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>14, );
    $this->pdd_items["pdd_prazo5"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo5", "description"=>"Prazo 5", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>15, );
    $this->pdd_items["pdd_prazo6"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_prazo6", "description"=>"Prazo 6", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"50", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>16, );
    $this->pdd_items["pdd_frete_situacao"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_frete_situacao", "description"=>"Tipo de Frete", "title"=>"Situação Frete: 1- CIF, 2 -FOB", "type"=>"option", "type_content"=>"1,CIF|2,FOB", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>18, );
    $this->pdd_items["pdd_frete_percentual"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_frete_percentual", "description"=>"Percentual do Frete", "title"=>"", "type"=>"percent", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>19, );
    $this->pdd_items["pdd_posicao"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_posicao", "description"=>"Posição", "title"=>"PENDENTE,PENDENTE|FATURADO,FATURADO|CANCELADO,CANCELADO|BLOQUEADO,BLOQUEADO|DEVOLVIDO,DEVOLVIDO", "type"=>"option", "type_content"=>"PENDENTE,PENDENTE|FATURADO,FATURADO|CANCELADO,CANCELADO|BLOQUEADO,BLOQUEADO|DEVOLVIDO,DEVOLVIDO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>20, );
    $this->pdd_items["pdd_data_fatura"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_data_fatura", "description"=>"Faturamento", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>21, );
    $this->pdd_items["pdd_chave_nfe"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_chave_nfe", "description"=>"NFE", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>22, );
    $this->pdd_items["pdd_nota_fiscal"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_nota_fiscal", "description"=>"Nota Fiscal", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>23, );
    $this->pdd_items["pdd_situacao"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_situacao", "description"=>"Situação", "title"=>"", "type"=>"option", "type_content"=>"1,ATIVO|0,INATIVO", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>24, );
    $this->pdd_items["pdd_ajuste"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_ajuste", "description"=>"Ajuste", "title"=>"Porcentagem de cálculo para o Percentual de Faturamento no tipo B", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>25, );
    $this->pdd_items["pdd_data_cadastro"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_data_cadastro", "description"=>"Cadastro", "title"=>"Incluir automático data/hora do Registro", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>26, );
    $this->pdd_items["pdd_valor_bruto"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_valor_bruto", "description"=>"Valor Bruto", "title"=>"", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>27, );
    $this->pdd_items["pdd_valor_liquido"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_valor_liquido", "description"=>"Valor Líquido", "title"=>"Total do Pedido (Mercadorias - Desconto)", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>28, );
    $this->pdd_items["pdd_valor_desconto"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_valor_desconto", "description"=>"Valor de Desconto", "title"=>"", "type"=>"money", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>29, );
    $this->pdd_items["pdd_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_tipo", "description"=>"Tipo", "title"=>"Determina o tipo de Desconto da Tabela de Preço a ser usado", "type"=>"option", "type_content"=>"a,A|b,B|c,C", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>30, );
    $this->pdd_items["pdd_observacao"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_observacao", "description"=>"Observação", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>16, "order"=>31, );
    $this->pdd_items["pdd_items"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_items", "description"=>"Items", "title"=>"Agrupa os Itens do Pedido relacionados ao Pedido que são sincronizados", "type"=>"array", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>17, "order"=>32, );


    // Atributos FK
    $this->pdd_items["pdd_cod_CLIENTE"] = array("pk"=>0, "fk"=>1, "id"=>"pdd_cod_CLIENTE", "description"=>"Cliente", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Cliente", "table"=>"TBL_CADASTRO_CLIENTE", "prefix"=>"cli", "tag"=>"cliente", "key"=>"cli_codigo", "description"=>"cli_descricao", "form"=>"form", "target"=>"div-pdd_cod_CLIENTE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pdd_items["pdd_cod_REPRESENTANTE"] = array("pk"=>0, "fk"=>1, "id"=>"pdd_cod_REPRESENTANTE", "description"=>"Representante", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>5, "foreign"=>array("modulo"=>"cadastro", "entity"=>"Representante", "table"=>"TBL_CADASTRO_REPRESENTANTE", "prefix"=>"rpr", "tag"=>"representante", "key"=>"rpr_codigo", "description"=>"rpr_descricao", "form"=>"form", "target"=>"div-pdd_cod_REPRESENTANTE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD
    $this->pdd_items["pdd_fk_pdi_cod_PEDIDO_4849"] = array("pk"=>0, "fk"=>0, "id"=>"pdd_fk_pdi_cod_PEDIDO_4849", "description"=>"Itens", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"movimento", "entity"=>"PedidoItem", "tag"=>"pedidoitem", "prefix"=>"pdi", "name"=>"Itens", "key"=>"pdd_codigo", "foreign"=>"pdi_cod_PEDIDO", "filter"=>"pdi_cod_PEDIDO", "source"=>"pdi_cod_PEDIDO", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->pdd_items['pdd_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'pdd_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pdd_items['pdd_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'pdd_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->pdd_items['pdd_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'pdd_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pdd_items['pdd_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'pdd_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->pdd_items = $this->configureItemsPedido($this->pdd_items);

    $join = $this->configureJoinPedido($this->pdd_items);

    $lines = 0;
    foreach ($this->pdd_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->pdd_properties = array(
      'rotule'=>'Pedido',
      'module'=>'movimento',
      'entity'=>'Pedido',
      'table'=>'TBL_PEDIDO',
      'join'=>$join,
      'tag'=>'pedido',
      'prefix'=>'pdd',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'pdd_codigo',
      'description'=>'pdd_chave_nfe',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>""))),
        //}
        //{
          'terminal'=>(object) (array("action"=>'terminal', "label"=>"Terminal", "layout"=>"terminal", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "settings"=>array("success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar o registro"), "execute"=>"Application.terminal.pedido.finish(data);"))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->pdd_properties['reference']) {
      foreach ($this->pdd_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->pdd_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->pdd_properties['description']) {
      foreach ($this->pdd_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->pdd_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsPedido();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function get_pdd_properties(){
    ?><?php
    return $this->pdd_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function get_pdd_items(){
    ?><?php
    return $this->pdd_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function get_pdd_item($key){
    ?><?php

		$this->validateItemPedido($key);

    return $this->pdd_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function get_pdd_reference(){
    ?><?php
    $key = $this->pdd_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function get_pdd_value($key){
    ?><?php

		$this->validateItemPedido($key);

    return $this->pdd_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function set_pdd_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemPedido($key);

    $this->pdd_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->pdd_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function set_pdd_type($key, $type){
    ?><?php

		$this->validateItemPedido($key);

    $this->pdd_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  private  function configureJoinPedido($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  private  function configureItemsPedido($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->pdd_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->pdd_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:52
   */
  public  function clearPedido($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_pdd_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_pdd_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_pdd_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  private  function validateItemPedido($key){
    ?><?php

    if (!isset($this->pdd_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function setStatementsPedido(){
    ?><?php

    $this->pdd_statements["0"] = "pdd_codigo = '?'";

    $this->pdd_statements["pdd_0"] = $this->pdd_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/05/2015 00:47:53
   */
  public  function getStatementsPedido(){
    ?><?php

    return $this->pdd_statements;
  }


}