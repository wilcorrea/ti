<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 22/03/2014 16:03:00
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:36:25
 * @category controller
 * @package organizacao
 */


class InstituicaoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function InstituicaoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');
    
    $entity = new Instituicao();
    $properties = $entity->get_ist_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getRowsInstituicaoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    $items = $instituicao->get_ist_items();
    $properties = $instituicao->get_ist_properties();

    $statements = $instituicao->getStatementsInstituicao();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getInstituicaoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    $instituicaos = null;

    $rows = $this->getRowsInstituicaoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $instituicaos = array();
      foreach ($rows as $row) {
        $instituicao_set = clone $instituicao;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $instituicao_set->set_ist_value($key, $item['value'], $reference);
        }
        $instituicao_set->clearInstituicao($not_clear);
        $instituicaos[] = $instituicao_set;
      }
    }

    return $instituicaos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getColumnInstituicaoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    $properties = $instituicao->get_ist_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $instituicao->getStatementsInstituicao();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getCountInstituicaoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    $properties = $instituicao->get_ist_properties();

    $statements = $instituicao->getStatementsInstituicao();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnInstituicaoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:31:56
   */
  public  function beforeInstituicaoCtrl($object, $param){
    ?><?php

    if ($param === 'add') {

      $object->set_ist_value('ist_chave', String::createGuid(false));

    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function addInstituicaoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesInstituicaoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_ist_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyInstituicaoCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesInstituicaoCtrl();
        $table = $properties['table'];

        $object = $this->beforeInstituicaoCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_ist_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterInstituicaoCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setInstituicaoCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function setInstituicaoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyInstituicaoCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_ist_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesInstituicaoCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeInstituicaoCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_ist_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterInstituicaoCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function removeInstituicaoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_ist_items();
    $properties = $this->getPropertiesInstituicaoCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_ist_value($action['field'], $action['value']);
        $object->clearInstituicao(array($action['field']));

        $remove = $this->setInstituicaoCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

        $object = $this->beforeInstituicaoCtrl($object, "remove");

        $sql = $this->dao->buildDelete($table, $where);

        $remove = $this->dao->executeSQL($sql, $this->history, $validate);

        $this->afterInstituicaoCtrl($object, "remove");

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function afterInstituicaoCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_ist_value($object->get_ist_reference());
    }

    if ($param === 'add') {
      $object->set_ist_value($object->get_ist_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_ist_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_ist_value($key, $items[$key]['type_content']);
            $object->set_ist_type($key, 'execute');
            $update = true;
          }
        } else if ($items[$key]['type'] === 'image') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_ist_value($object->get_ist_reference());
              $source = APP_PATH . $items[$key]['value'];
              $location = $file['path'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (!is_dir(APP_PATH . $file['path'])) {
                mkdir(APP_PATH . $file['path'], 0755, true);
              }

              if (is_writable(APP_PATH . $file['path'])) {
                if (file_exists($source)) {
                  $copy = copy($source, $destin);
                  if ($copy) {
                    $object->set_ist_value($key, $location);
                    $update = true;
                  } else {
                    System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                  }
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        } else if ($items[$key]['type'] === 'file') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_ist_value($object->get_ist_reference());
              $source = $object->get_ist_value($key);

              $location = $file['path'] . $file['name'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (is_writable(APP_PATH . $file['path'])) {
                $copy = copy(APP_PATH . $source, $destin);
                if ($copy) {
                  $object->set_ist_value($key, $location);
                  $update = true;
                } else {
                  System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        }
      }

      if ($update) {
        $this->setInstituicaoCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_ist_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function executeInstituicaoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    $properties = $instituicao->get_ist_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $instituicao->getStatementsInstituicao();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", ist_responsavel = '" . System::getUser(false) . "', ist_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function verifyInstituicaoCtrl($object){
		?><?php
	
		$items = $object->get_ist_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getObjectInstituicaoCtrl($value){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $object = new Instituicao();

    $properties = $object->get_ist_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $instituicao = null;

    $instituicaos = $this->getInstituicaoCtrl($where, "", "", "0", "1");
    if (is_array($instituicaos)) {
      $instituicao = $instituicaos[0];
    }

    return $instituicao;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   */
  public  function getReplacesInstituicaoCtrl($reference){
		?><?php

		System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

		$instituicao = new Instituicao();

    $replaces = array();

    $where = "ist_codigo = '" . $reference . "'";
    $instituicaos = $this->getInstituicaoCtrl($where, "", "");
    if (is_array($instituicaos)) {
      $instituicao = $instituicaos[0];

      $items = $instituicao->get_ist_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:53
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   */
  public  function configureInstituicaoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "edit":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   */
  public  function getPropertiesInstituicaoCtrl(){
    ?><?php

      System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

      $instituicao = new Instituicao();

      $properties = $instituicao->get_ist_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   */
  public  function getItemsInstituicaoCtrl($search){
    ?><?php

    System::import('m', 'organizacao', 'Instituicao', 'src', true, '{project.rsc}');

    $instituicao = new Instituicao();

    if ($search) {
      $object = $this->getObjectInstituicaoCtrl($search);
      if (!is_null($object)) {
        $instituicao = $object;
      }
    }
    
    $items = $instituicao->get_ist_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $instituicao->get_ist_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   */
  public  function getHistoryInstituicaoCtrl($items){
    ?><?php
    
    $defaults = array(
      'ist_registro'=>array('id'=>'ist_registro', 'value'=>date('d/m/Y H:i:s')),
      'ist_criador'=>array('id'=>'ist_criador', 'value'=>System::getUser()),
      'ist_alteracao'=>array('id'=>'ist_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'ist_responsavel'=>array('id'=>'ist_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 12:25:54
   */
  public  function operationInstituicaoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'organizacao', 'Instituicao', 'src', true);

    $instituicao = new Instituicao();

    foreach ($items as $id => $item) {
      $instituicao->set_ist_value($id, $item['value']);
    }
    
    $method = $operation . "InstituicaoCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($instituicao);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'InstituicaoCtrl'");
    }

    return $result;
  }


}