<?php
/**
 * @copyright array software
 *
 * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
 * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:44:52
 * @category controller
 * @package organizacao
 */


class InstituicaoCadastroCtrl
{
  private  $path;
  private  $database;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function InstituicaoCadastroCtrl($path, $database = ""){
    ?><?php
    $this->path = $path;
    $this->database = $database;

    return $this;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function getInstituicaoCadastroCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    $instituicaoCadastros = $instituicaoCadastroDAO->getInstituicaoCadastroDAO($where, $group, $order, $start, $end, $fields, $validate, $debug);
    
    return $instituicaoCadastros;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function getCountInstituicaoCadastroCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    $total = $instituicaoCadastroDAO->getCountInstituicaoCadastroDAO($where, $group, $validate, $debug);
    
    return $total;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function addInstituicaoCadastroCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');    
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    
		$verify = $this->verifyInstituicaoCadastroCtrl($object);
		if (count($verify) > 0) {

			$add = 0;
		} else {

      $this->beforeInstituicaoCadastroCtrl($object, "add");
			$add = $instituicaoCadastroDAO->addInstituicaoCadastroDAO($object, $validate, $debug, $presave, $copy);
    	$this->afterInstituicaoCadastroCtrl($object, "add", $add, $copy);
		}
    
    return $add;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function removeInstituicaoCadastroCtrl($object, $validate = true, $debug = false){
    ?><?php
    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    $this->beforeInstituicaoCadastroCtrl($object, "remove");
    $remove = $instituicaoCadastroDAO->removeInstituicaoCadastroDAO($object, $validate, $debug);
    $this->afterInstituicaoCadastroCtrl($object, "remove");
    
    return $remove;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $trigger 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function setInstituicaoCadastroCtrl($object, $validate = true, $debug = false, $trigger = true){
    ?><?php
    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
		
		$verify = $this->verifyInstituicaoCadastroCtrl($object);
		if (count($verify) > 0) {

			$set = 0;
		} else {

      $object = $this->beforeInstituicaoCadastroCtrl($object, "set");
			$set = $instituicaoCadastroDAO->setInstituicaoCadastroDAO($object, $validate, $debug, $trigger);
			if($trigger === true){
				$this->afterInstituicaoCadastroCtrl($object, "set");
			}
		}
    
    return $set;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function beforeInstituicaoCadastroCtrl($object, $param){
    ?><?php

    if($param === 'add' or $param === 'set'){

      $items = $object->get_isc_items();
      
      foreach($items as $item){
        if($item['type_behavior'] == 'parent'){
          if(isset($item['parent'])){
            $class = $item['parent']['entity'];
            System::desire('c', $item['parent']['modulo'], $class, 'src', true);
            $classCtrl = $class."Ctrl";
            $obj = new $classCtrl(APP_PATH);
            $entity = $item['value'];
            if($param === 'add'){
              $action = "add".$class."Ctrl";
            }else{
              $action = "set".$class."Ctrl";
            }
            $value = $obj->$action($entity);
            if($param === 'add'){
              $object->set_isc_value($item['id'], $value);
            }else{
              $get_value = "get_".$item['parent']['prefix']."_value";
              $object->set_isc_value($item['id'], $entity->$get_value($item['parent']['key']));
            }
          }
        }
      }
    }
    
    if ($param == 'remove') {
      $items = $object->get_isc_items();
      $properties = $object->get_isc_properties();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_isc_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $parentModule = $item['parent']['modulo'];
            $parentEntity = $item['parent']['entity'];
            $parentReference = $item['parent']['key'];
            
            $value = $this->getColumnConteudoModuloCtrl($key, $whereObject);
            
            $whereParent = $parentReference . " = '" . $value . "'";

            System::desire('c', $parentModule, $parentEntity, 'src');
            System::desire('m', $parentModule, $parentEntity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($whereParent, "", "");
            $parent = $parents[0];
            
            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function afterInstituicaoCadastroCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php
    
    $add = false;
    
    if($param === 'set'){
      $value = $object->get_isc_value($object->get_isc_reference());
    }

    if($param === 'add'){
      $object->set_isc_value($object->get_isc_reference(), $value);
    }

    if($param === 'set' or $param === 'add'){
			
      $items = $object->get_isc_items();

      $try = 0;
      $added = 0;
      
      foreach ($items as $item) {
        if($item['type'] === 'select-multi'){
          
          if(isset($item['select-multi'])){
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::desire('c', $module, $entity, 'src');
            System::desire('m', $module, $entity, 'src');

            $remove = "execute".$entity."Ctrl";
            $set_value = 'set_'.$prefix.'_value';
            $add = "add".$entity."Ctrl";

            $entityCtrl = $entity.'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign']."='".$value."'","");

            $values = $item['value'];
            foreach ($values as $v) {
							if($v){
								$o = new $entity();
								$o->$set_value($select['foreign'],$value);
								$o->$set_value($select['source'],$v);
								$o->$set_value($prefix.'_responsavel',System::getUser());
								$o->$set_value($prefix.'_criador',System::getUser());
								
								$w = $objectCtrl->$add($o);
								$try++;
								if($w){
									$added++;
								}
							}
            }
          }
        }
      }
      
      $add = $try == $added;
      
      if($copy > 0){
        // input here the code to create the complete copy of instance
      }
      
    }

		$properties = $object->get_isc_properties();
    if(isset($properties['notification'])){
      if($properties['notification']){
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function getColumnInstituicaoCadastroCtrl($column, $where, $table = "", $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');
    
    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    $column = $instituicaoCadastroDAO->getColumnInstituicaoCadastroDAO($column, $where, $table, $validate, $debug);
    
    return $column;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function executeInstituicaoCadastroCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::desire('d', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');

    $instituicaoCadastroDAO = new InstituicaoCadastroDAO($this->path, $this->database);
    $executed = $instituicaoCadastroDAO->executeInstituicaoCadastroDAO($where, $update, $validate, $debug);

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function verifyInstituicaoCadastroCtrl($object){
		?><?php
	
		$items = $object->get_isc_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function getObjectInstituicaoCadastroCtrl($value){
		?><?php

    System::import('m', 'organizacao', 'InstituicaoCadastro', 'src', true, '{project.rsc}');

		$instituicaoCadastro = new InstituicaoCadastro();

		$properties = $instituicaoCadastro->get_isc_properties();
    $reference = $properties['reference'];

		$references = explode(",", $reference);
		$values = explode(",", $value);

		$w = array();
		foreach ($references as $index => $key) {
			$w[] = $key . " = '" . $values[$index] . "'";
		}

		$where = "FALSE";
		if (count($w) > 0) {
			$where = implode(" AND ", $w);
		}

		$instituicaoCadastro = null;

		$instituicaoCadastros = $this->getInstituicaoCadastroCtrl($where, "", "", "0", "1");
		if (is_array($instituicaoCadastros)) {
			$instituicaoCadastro = $instituicaoCadastros[0];
		}

		return $instituicaoCadastro;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param object $object 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:27
   */
  public  function getReplacesInstituicaoCadastroCtrl($object){
		?><?php

    $replaces = array();

		$items = $object->get_isc_items();

		foreach ($items as $item) {
			$replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
		}

		return $replaces;
  }


}