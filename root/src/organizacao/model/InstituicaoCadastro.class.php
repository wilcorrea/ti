<?php
/**
 * @copyright array software
 *
 * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
 * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:44:52
 * @category model
 * @package organizacao
 */


class InstituicaoCadastro
{
  private  $isc_items = array();
  private  $isc_properties = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function InstituicaoCadastro(){
    ?><?php

    $this->isc_items = array();
    
    // Atributos
    $this->isc_items["isc_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"isc_codigo", "description"=>"Código", "title"=>"Código do Cadastro dos Dados Cadastrais da Instituição", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );


    // Atributos FK
    $this->isc_items["isc_cod_INSTITUICAO"] = array("pk"=>0, "fk"=>1, "id"=>"isc_cod_INSTITUICAO", "description"=>"Instituição", "title"=>"Relacionamento com a Instituição", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );


    // Atributos CHILD

    
    // Atributos padrao
    $this->isc_items['isc_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'isc_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->isc_items['isc_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'isc_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->isc_items['isc_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'isc_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->isc_items['isc_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'isc_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $lines = 0;
    foreach($this->isc_items as $item){
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }
     
    $j = array();
    foreach($this->isc_items as $item){
      if($item['fk']){
        if(isset($item['foreign']) or isset($item['parent'])){
          $table = "";
					$key = "";
					if(isset($item['foreign'])){
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					}else if(isset($item['parent'])){
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    $this->isc_properties = array(
			'rotule'=>'Dados Cadastrais',
      'module'=>'organizacao',
      'entity'=>'InstituicaoCadastro',
      'table'=>'TBL_INSTITUICAO_CADASTRO',
			'join'=>$join,
      'tag'=>'instituicaocadastro',
      'prefix'=>'isc',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			//'action'=>array('icon-push'=>array("className"=>'icon-push', "title"=>"Ativar", "level"=>2, "message"=>"", "handler"=>"Ativar", "confirm"=>"Deseja realmente ativar este registro?", "feedback"=>true, "reload"=>true, "condition"=>array("field"=>'fcr_ativo', "value"=>0), "confirm"=>array("active"=>true, "title"=>"Ativação de Facred", "message"=>"Deseja realmente ativar este Facred?"), "operation"=>array("command"=>"save", "update"=>"fcr_ativo=1"))),
      'remove'=>'',
			//false, true or
			//array("field"=>"isc_id", "value"=>"1", "className"=>"icon-remove", "title"=>"Excluir", "message"=>"Deseja realmente remover este registro?", "success"=>"Registro removido com sucesso", "handler"=>"Remover"),
			'database'=>null,
      'reference'=>'isc_codigo',
      'description'=>'isc_',
			'notification'=>false,
      'lines'=>$lines
    );
    
    if (!$this->isc_properties['reference']) {
      foreach ($this->isc_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->isc_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->isc_properties['description']) {
      foreach ($this->isc_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->isc_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function get_isc_properties(){
    ?><?php
    return $this->isc_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function get_isc_items(){
    ?><?php
    return $this->isc_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function get_isc_item($key){
    ?><?php
    return $this->isc_items[$key];
  }

  /**
   * 
   * 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function get_isc_reference(){
    ?><?php
    $key = $this->isc_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function get_isc_value($key){
    ?><?php
    return $this->isc_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param object $value 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function set_isc_value($key, $value){
?><?php
		if (!isset($this->isc_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }
    $this->isc_items[$key]['value'] = $value;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   * <br><b>Updated by</b> LUIZ SERGIO FERREIRA RODRIGUES - 24/07/2014 21:40:26
   */
  public  function set_isc_type($key, $type){
    ?><?php
    $this->isc_items[$key]['type'] = $type;
  }


}