<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 27/06/2014 08:25:53
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:57:17
 * @category model
 * @package organizacao
 */


class Sincronizacao
{
  private  $snc_items = array();
  private  $snc_properties = array();
  private  $snc_parents = array();
  private  $snc_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:01:31
   */
  public  function Sincronizacao(){
    ?><?php

    $this->snc_items = array();
    
    // Atributos
    $this->snc_items["snc_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"snc_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->snc_items["snc_id"] = array("pk"=>0, "fk"=>0, "id"=>"snc_id", "description"=>"Identificador", "title"=>"É o md5 do modulo concatenado com a entidade com a finalidade de servir como identificador", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>2, "order"=>2, );
    $this->snc_items["snc_cliente"] = array("pk"=>0, "fk"=>0, "id"=>"snc_cliente", "description"=>"Cliente", "title"=>"Código GUID", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>3, "order"=>3, );
    $this->snc_items["snc_operacao"] = array("pk"=>0, "fk"=>0, "id"=>"snc_operacao", "description"=>"Operação", "title"=>"", "type"=>"option", "type_content"=>"enviar,Envio|receber,Recebimento", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>4, "order"=>4, );
    $this->snc_items["snc_modulo"] = array("pk"=>0, "fk"=>0, "id"=>"snc_modulo", "description"=>"Módulo", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>5, "order"=>5, );
    $this->snc_items["snc_entidade"] = array("pk"=>0, "fk"=>0, "id"=>"snc_entidade", "description"=>"Entidade", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>6, "order"=>6, );
    $this->snc_items["snc_requisicao"] = array("pk"=>0, "fk"=>0, "id"=>"snc_requisicao", "description"=>"Requisição", "title"=>"", "type"=>"longtext", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>7, "order"=>7, );
    $this->snc_items["snc_data_sincronizacao"] = array("pk"=>0, "fk"=>0, "id"=>"snc_data_sincronizacao", "description"=>"Data de Sincronização", "title"=>"", "type"=>"date", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[D]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>1, "line"=>8, "order"=>8, );
    $this->snc_items["snc_atual"] = array("pk"=>0, "fk"=>0, "id"=>"snc_atual", "description"=>"Atual", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>3, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>9, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->snc_items['snc_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'snc_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->snc_items['snc_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'snc_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->snc_items['snc_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'snc_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->snc_items['snc_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'snc_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->snc_items = $this->configureItemsSincronizacao($this->snc_items);

    $join = $this->configureJoinSincronizacao($this->snc_items);

    $lines = 0;
    foreach ($this->snc_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $database = null;

    $this->snc_properties = array(
			'rotule'=>'Sincronização',
      'module'=>'organizacao',
      'entity'=>'Sincronizacao',
      'table'=>'TBL_SINCRONIZACAO',
			'join'=>$join,
      'tag'=>'sincronizacao',
      'prefix'=>'snc',
      'order'=>'',
      'group'=>'',
      'where'=>'snc_atual = 1',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'snc_codigo',
      'description'=>'snc_id',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->snc_properties['reference']) {
      foreach ($this->snc_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->snc_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->snc_properties['description']) {
      foreach ($this->snc_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->snc_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function get_snc_properties(){
    ?><?php
    return $this->snc_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function get_snc_items(){
    ?><?php
    return $this->snc_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function get_snc_item($key){
    ?><?php

		$this->validateItemSincronizacao($key);

    return $this->snc_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function get_snc_reference(){
    ?><?php
    $key = $this->snc_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function get_snc_value($key){
    ?><?php

		$this->validateItemSincronizacao($key);

    return $this->snc_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function set_snc_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemSincronizacao($key);

    $this->snc_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->snc_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:40
   */
  public  function set_snc_type($key, $type){
    ?><?php

		$this->validateItemSincronizacao($key);

    $this->snc_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  private  function configureJoinSincronizacao($items){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  private  function configureItemsSincronizacao($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->snc_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->snc_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  public  function clearSincronizacao($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_snc_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_snc_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  private  function validateItemSincronizacao($key){
    ?><?php

    if (!isset($this->snc_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  public  function setStatementsSincronizacao(){
    ?><?php

    $this->snc_statements[0] = "snc_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:00:41
   */
  public  function getStatementsSincronizacao(){
    ?><?php

    return $this->snc_statements;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   * @param string $snc_modulo 
   * @param string $snc_entidade 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:33:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 29/10/2014 13:34:23
   */
  public  function generateId($snc_modulo = null, $snc_entidade = null){
    ?><?php

    if (is_null($snc_modulo) || is_null($snc_entidade)) {
      $snc_modulo = $this->get_snc_value('snc_modulo');
      $snc_entidade = $this->get_snc_value('snc_entidade');
    }

    $snc_id = md5($snc_modulo . $snc_entidade);

    return $snc_id;
  }


}