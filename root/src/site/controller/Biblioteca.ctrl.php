<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:29
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 13:23:49
 * @category controller
 * @package site
 */


class BibliotecaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function BibliotecaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');
    
    $entity = new Biblioteca();
    $properties = $entity->get_bbl_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getRowsBibliotecaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    $items = $biblioteca->get_bbl_items();
    $properties = $biblioteca->get_bbl_properties();

    $statements = $biblioteca->getStatementsBiblioteca();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getBibliotecaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    $bibliotecas = null;

    $rows = $this->getRowsBibliotecaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $bibliotecas = array();
      foreach ($rows as $row) {
        $biblioteca_set = clone $biblioteca;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $biblioteca_set->set_bbl_value($key, $item['value'], $reference);
        }
        $biblioteca_set->clearBiblioteca($not_clear);
        $bibliotecas[] = $biblioteca_set;
      }
    }

    return $bibliotecas;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getColumnBibliotecaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    $properties = $biblioteca->get_bbl_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $biblioteca->getStatementsBiblioteca();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getCountBibliotecaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    $properties = $biblioteca->get_bbl_properties();

    $statements = $biblioteca->getStatementsBiblioteca();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnBibliotecaCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function beforeBibliotecaCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsBibliotecaCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_bbl_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_bbl_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_bbl_value($item['id'], $value);
            } else {
              $object->set_bbl_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsBibliotecaCtrl("");
      $properties = $this->getPropertiesBibliotecaCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_bbl_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnBibliotecaCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function addBibliotecaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesBibliotecaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_bbl_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyBibliotecaCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesBibliotecaCtrl();
        $table = $properties['table'];

        $object = $this->beforeBibliotecaCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_bbl_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterBibliotecaCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setBibliotecaCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function setBibliotecaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyBibliotecaCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_bbl_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesBibliotecaCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeBibliotecaCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_bbl_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterBibliotecaCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function removeBibliotecaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_bbl_items();
    $properties = $this->getPropertiesBibliotecaCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_bbl_value($action['field'], $action['value']);
        $object->clearBiblioteca(array($action['field']));

        $remove = $this->setBibliotecaCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeBibliotecaCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterBibliotecaCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function afterBibliotecaCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_bbl_value($object->get_bbl_reference());
    }

    if ($param === 'add') {
      $object->set_bbl_value($object->get_bbl_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_bbl_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_bbl_value($key, $items[$key]['type_content']);
            $object->set_bbl_type($key, 'execute');
            $update = true;
          }
        }
      }

      if ($update) {
        $this->setBibliotecaCtrl($object);
      }

      $response = Controller::after($value, $items, 'bbl');
      if (count($response)) {
        foreach ($response as $key => $new_value) {
          $object->set_bbl_value($key, $new_value);
        }

        $this->setBibliotecaCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_bbl_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function executeBibliotecaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    $properties = $biblioteca->get_bbl_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $biblioteca->getStatementsBiblioteca();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", bbl_responsavel = '" . System::getUser(false) . "', bbl_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function verifyBibliotecaCtrl($object){
		?><?php
	
		$items = $object->get_bbl_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getObjectBibliotecaCtrl($value){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $object = new Biblioteca();

    $properties = $object->get_bbl_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $biblioteca = null;

    $bibliotecas = $this->getBibliotecaCtrl($where, "", "", "0", "1");
    if (is_array($bibliotecas)) {
      $biblioteca = $bibliotecas[0];
    }

    return $biblioteca;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getReplacesBibliotecaCtrl($reference){
		?><?php

		System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

		$biblioteca = new Biblioteca();

    $replaces = array();

    $where = "bbl_codigo = '" . $reference . "'";
    $bibliotecas = $this->getBibliotecaCtrl($where, "", "");
    if (is_array($bibliotecas)) {
      $biblioteca = $bibliotecas[0];

      $items = $biblioteca->get_bbl_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function configureBibliotecaCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getPropertiesBibliotecaCtrl(){
    ?><?php

      System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

      $biblioteca = new Biblioteca();

      $properties = $biblioteca->get_bbl_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getItemsBibliotecaCtrl($search){
    ?><?php

    System::import('m', 'site', 'Biblioteca', 'src', true, '{project.rsc}');

    $biblioteca = new Biblioteca();

    if ($search) {
      $object = $this->getObjectBibliotecaCtrl($search);
      if (!is_null($object)) {
        $biblioteca = $object;
      }
    }
    
    $items = $biblioteca->get_bbl_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $biblioteca->get_bbl_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getHistoryBibliotecaCtrl($items){
    ?><?php
    
    $defaults = array(
      'bbl_registro'=>array('id'=>'bbl_registro', 'value'=>date('d/m/Y H:i:s')),
      'bbl_criador'=>array('id'=>'bbl_criador', 'value'=>System::getUser()),
      'bbl_alteracao'=>array('id'=>'bbl_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'bbl_responsavel'=>array('id'=>'bbl_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function operationBibliotecaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'Biblioteca', 'src', true);

    $biblioteca = new Biblioteca();

    foreach ($items as $id => $item) {
      $biblioteca->set_bbl_value($id, $item['value']);
    }
    
    $method = $operation . "BibliotecaCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($biblioteca);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'BibliotecaCtrl'");
    }

    return $result;
  }


}