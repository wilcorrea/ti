<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:02
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:07
 * @category controller
 * @package site
 */


class BibliotecaItemCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function BibliotecaItemCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');
    
    $entity = new BibliotecaItem();
    $properties = $entity->get_bbi_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function getRowsBibliotecaItemCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    $items = $bibliotecaItem->get_bbi_items();
    $properties = $bibliotecaItem->get_bbi_properties();

    $statements = $bibliotecaItem->getStatementsBibliotecaItem();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function getBibliotecaItemCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    $bibliotecaItems = null;

    $rows = $this->getRowsBibliotecaItemCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $bibliotecaItems = array();
      foreach ($rows as $row) {
        $bibliotecaItem_set = clone $bibliotecaItem;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $bibliotecaItem_set->set_bbi_value($key, $item['value'], $reference);
        }
        $bibliotecaItem_set->clearBibliotecaItem($not_clear);
        $bibliotecaItems[] = $bibliotecaItem_set;
      }
    }

    return $bibliotecaItems;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function getColumnBibliotecaItemCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    $properties = $bibliotecaItem->get_bbi_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $bibliotecaItem->getStatementsBibliotecaItem();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function getCountBibliotecaItemCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    $properties = $bibliotecaItem->get_bbi_properties();

    $statements = $bibliotecaItem->getStatementsBibliotecaItem();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnBibliotecaItemCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function beforeBibliotecaItemCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsBibliotecaItemCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_bbi_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_bbi_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_bbi_value($item['id'], $value);
            } else {
              $object->set_bbi_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsBibliotecaItemCtrl("");
      $properties = $this->getPropertiesBibliotecaItemCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_bbi_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnBibliotecaItemCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function addBibliotecaItemCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesBibliotecaItemCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_bbi_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyBibliotecaItemCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesBibliotecaItemCtrl();
        $table = $properties['table'];

        $object = $this->beforeBibliotecaItemCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_bbi_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterBibliotecaItemCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setBibliotecaItemCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function setBibliotecaItemCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyBibliotecaItemCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_bbi_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesBibliotecaItemCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeBibliotecaItemCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_bbi_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterBibliotecaItemCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function removeBibliotecaItemCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_bbi_items();
    $properties = $this->getPropertiesBibliotecaItemCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_bbi_value($action['field'], $action['value']);
        $object->clearBibliotecaItem(array($action['field']));

        $remove = $this->setBibliotecaItemCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeBibliotecaItemCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterBibliotecaItemCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function afterBibliotecaItemCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_bbi_value($object->get_bbi_reference());
    }

    if ($param === 'add') {
      $object->set_bbi_value($object->get_bbi_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_bbi_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_bbi_value($key, $items[$key]['type_content']);
            $object->set_bbi_type($key, 'execute');
            $update = true;
          }
        }
      }

      if ($update) {
        $this->setBibliotecaItemCtrl($object);
      }

      $response = Controller::after($value, $items, 'bbi');
      if (count($response)) {
        foreach ($response as $key => $new_value) {
          $object->set_bbi_value($key, $new_value);
        }

        $this->setBibliotecaItemCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_bbi_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function executeBibliotecaItemCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    $properties = $bibliotecaItem->get_bbi_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $bibliotecaItem->getStatementsBibliotecaItem();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", bbi_responsavel = '" . System::getUser(false) . "', bbi_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function verifyBibliotecaItemCtrl($object){
		?><?php
	
		$items = $object->get_bbi_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function getObjectBibliotecaItemCtrl($value){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $object = new BibliotecaItem();

    $properties = $object->get_bbi_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $bibliotecaItem = null;

    $bibliotecaItems = $this->getBibliotecaItemCtrl($where, "", "", "0", "1");
    if (is_array($bibliotecaItems)) {
      $bibliotecaItem = $bibliotecaItems[0];
    }

    return $bibliotecaItem;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function getReplacesBibliotecaItemCtrl($reference){
		?><?php

		System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

		$bibliotecaItem = new BibliotecaItem();

    $replaces = array();

    $where = "bbi_codigo = '" . $reference . "'";
    $bibliotecaItems = $this->getBibliotecaItemCtrl($where, "", "");
    if (is_array($bibliotecaItems)) {
      $bibliotecaItem = $bibliotecaItems[0];

      $items = $bibliotecaItem->get_bbi_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function configureBibliotecaItemCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function getPropertiesBibliotecaItemCtrl(){
    ?><?php

      System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

      $bibliotecaItem = new BibliotecaItem();

      $properties = $bibliotecaItem->get_bbi_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function getItemsBibliotecaItemCtrl($search){
    ?><?php

    System::import('m', 'site', 'BibliotecaItem', 'src', true, '{project.rsc}');

    $bibliotecaItem = new BibliotecaItem();

    if ($search) {
      $object = $this->getObjectBibliotecaItemCtrl($search);
      if (!is_null($object)) {
        $bibliotecaItem = $object;
      }
    }
    
    $items = $bibliotecaItem->get_bbi_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $bibliotecaItem->get_bbi_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function getHistoryBibliotecaItemCtrl($items){
    ?><?php
    
    $defaults = array(
      'bbi_registro'=>array('id'=>'bbi_registro', 'value'=>date('d/m/Y H:i:s')),
      'bbi_criador'=>array('id'=>'bbi_criador', 'value'=>System::getUser()),
      'bbi_alteracao'=>array('id'=>'bbi_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'bbi_responsavel'=>array('id'=>'bbi_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:06
   */
  public  function operationBibliotecaItemCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'BibliotecaItem', 'src', true);

    $bibliotecaItem = new BibliotecaItem();

    foreach ($items as $id => $item) {
      $bibliotecaItem->set_bbi_value($id, $item['value']);
    }
    
    $method = $operation . "BibliotecaItemCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($bibliotecaItem);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'BibliotecaItemCtrl'");
    }

    return $result;
  }


}