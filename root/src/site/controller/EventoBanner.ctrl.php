<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:46
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:34:53
 * @category controller
 * @package site
 */


class EventoBannerCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function EventoBannerCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');
    
    $entity = new EventoBanner();
    $properties = $entity->get_evb_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getRowsEventoBannerCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    $items = $eventoBanner->get_evb_items();
    $properties = $eventoBanner->get_evb_properties();

    $statements = $eventoBanner->getStatementsEventoBanner();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getEventoBannerCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    $eventoBanners = null;

    $rows = $this->getRowsEventoBannerCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $eventoBanners = array();
      foreach ($rows as $row) {
        $eventoBanner_set = clone $eventoBanner;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $eventoBanner_set->set_evb_value($key, $item['value'], $reference);
        }
        $eventoBanner_set->clearEventoBanner($not_clear);
        $eventoBanners[] = $eventoBanner_set;
      }
    }

    return $eventoBanners;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getColumnEventoBannerCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    $properties = $eventoBanner->get_evb_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $eventoBanner->getStatementsEventoBanner();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getCountEventoBannerCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    $properties = $eventoBanner->get_evb_properties();

    $statements = $eventoBanner->getStatementsEventoBanner();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnEventoBannerCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function beforeEventoBannerCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsEventoBannerCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_evb_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_evb_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_evb_value($item['id'], $value);
            } else {
              $object->set_evb_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsEventoBannerCtrl("");
      $properties = $this->getPropertiesEventoBannerCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_evb_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnEventoBannerCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function addEventoBannerCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesEventoBannerCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_evb_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyEventoBannerCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesEventoBannerCtrl();
        $table = $properties['table'];

        $object = $this->beforeEventoBannerCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_evb_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterEventoBannerCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setEventoBannerCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function setEventoBannerCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyEventoBannerCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_evb_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesEventoBannerCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeEventoBannerCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_evb_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterEventoBannerCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function removeEventoBannerCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_evb_items();
    $properties = $this->getPropertiesEventoBannerCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_evb_value($action['field'], $action['value']);
        $object->clearEventoBanner(array($action['field']));

        $remove = $this->setEventoBannerCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeEventoBannerCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterEventoBannerCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function afterEventoBannerCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_evb_value($object->get_evb_reference());
    }

    if ($param === 'add') {
      $object->set_evb_value($object->get_evb_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_evb_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_evb_value($key, $items[$key]['type_content']);
            $object->set_evb_type($key, 'execute');
            $update = true;
          }
        }
      }

      if ($update) {
        $this->setEventoBannerCtrl($object);
      }

      $response = Controller::after($value, $items, 'evb');
      if (count($response)) {
        foreach ($response as $key => $new_value) {
          $object->set_evb_value($key, $new_value);
        }

        $this->setEventoBannerCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_evb_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function executeEventoBannerCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    $properties = $eventoBanner->get_evb_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $eventoBanner->getStatementsEventoBanner();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", evb_responsavel = '" . System::getUser(false) . "', evb_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function verifyEventoBannerCtrl($object){
		?><?php
	
		$items = $object->get_evb_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getObjectEventoBannerCtrl($value){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $object = new EventoBanner();

    $properties = $object->get_evb_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $eventoBanner = null;

    $eventoBanners = $this->getEventoBannerCtrl($where, "", "", "0", "1");
    if (is_array($eventoBanners)) {
      $eventoBanner = $eventoBanners[0];
    }

    return $eventoBanner;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   */
  public  function getReplacesEventoBannerCtrl($reference){
		?><?php

		System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

		$eventoBanner = new EventoBanner();

    $replaces = array();

    $where = "evb_codigo = '" . $reference . "'";
    $eventoBanners = $this->getEventoBannerCtrl($where, "", "");
    if (is_array($eventoBanners)) {
      $eventoBanner = $eventoBanners[0];

      $items = $eventoBanner->get_evb_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:47
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 22:19:55
   */
  public  function configureEventoBannerCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    System::import('c', 'site', 'Banner', 'src');

    $screen = new Screen('{project.rsc}');

    $bannerCtrl = new BannerCtrl(APP_PATH);

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }

        $bnnn_codigo = $atributos['evb_cod_BANNER']['value'];
        $where_bnn = "bnn_codigo = '" . $bnn_codigo . "'";
        $bnn_descricao = $bannerCtrl->getColumnBannerCtrl('bnn_descricao', $where_bnn);

        $atributos['evt_descricao']['value'] = 'Perí­odo de exibição do banner ' . $bnn_descricao;
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   */
  public  function getPropertiesEventoBannerCtrl(){
    ?><?php

      System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

      $eventoBanner = new EventoBanner();

      $properties = $eventoBanner->get_evb_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   */
  public  function getItemsEventoBannerCtrl($search){
    ?><?php

    System::import('m', 'site', 'EventoBanner', 'src', true, '{project.rsc}');

    $eventoBanner = new EventoBanner();

    if ($search) {
      $object = $this->getObjectEventoBannerCtrl($search);
      if (!is_null($object)) {
        $eventoBanner = $object;
      }
    }
    
    $items = $eventoBanner->get_evb_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $eventoBanner->get_evb_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   */
  public  function getHistoryEventoBannerCtrl($items){
    ?><?php
    
    $defaults = array(
      'evb_registro'=>array('id'=>'evb_registro', 'value'=>date('d/m/Y H:i:s')),
      'evb_criador'=>array('id'=>'evb_criador', 'value'=>System::getUser()),
      'evb_alteracao'=>array('id'=>'evb_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'evb_responsavel'=>array('id'=>'evb_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:48
   */
  public  function operationEventoBannerCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'EventoBanner', 'src', true);

    $eventoBanner = new EventoBanner();

    foreach ($items as $id => $item) {
      $eventoBanner->set_evb_value($id, $item['value']);
    }
    
    $method = $operation . "EventoBannerCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($eventoBanner);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'EventoBannerCtrl'");
    }

    return $result;
  }


}