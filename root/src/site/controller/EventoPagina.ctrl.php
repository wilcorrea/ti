<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:36
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:32:19
 * @category controller
 * @package site
 */


class EventoPaginaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   */
  public  function EventoPaginaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');
    
    $entity = new EventoPagina();
    $properties = $entity->get_evp_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   */
  public  function getRowsEventoPaginaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    $items = $eventoPagina->get_evp_items();
    $properties = $eventoPagina->get_evp_properties();

    $statements = $eventoPagina->getStatementsEventoPagina();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   */
  public  function getEventoPaginaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    $eventoPaginas = null;

    $rows = $this->getRowsEventoPaginaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $eventoPaginas = array();
      foreach ($rows as $row) {
        $eventoPagina_set = clone $eventoPagina;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $eventoPagina_set->set_evp_value($key, $item['value'], $reference);
        }
        $eventoPagina_set->clearEventoPagina($not_clear);
        $eventoPaginas[] = $eventoPagina_set;
      }
    }

    return $eventoPaginas;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:37
   */
  public  function getColumnEventoPaginaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    $properties = $eventoPagina->get_evp_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $eventoPagina->getStatementsEventoPagina();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getCountEventoPaginaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    $properties = $eventoPagina->get_evp_properties();

    $statements = $eventoPagina->getStatementsEventoPagina();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnEventoPaginaCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/03/2015 14:21:29
   */
  public  function beforeEventoPaginaCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsEventoPaginaCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_evp_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_evp_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_evp_value($item['id'], $value);
            } else {
              $object->set_evp_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsEventoPaginaCtrl("");
      $properties = $this->getPropertiesEventoPaginaCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_evp_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnEventoPaginaCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function addEventoPaginaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesEventoPaginaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_evp_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyEventoPaginaCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesEventoPaginaCtrl();
        $table = $properties['table'];

        $object = $this->beforeEventoPaginaCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_evp_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterEventoPaginaCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setEventoPaginaCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function setEventoPaginaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyEventoPaginaCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_evp_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesEventoPaginaCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeEventoPaginaCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_evp_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterEventoPaginaCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function removeEventoPaginaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_evp_items();
    $properties = $this->getPropertiesEventoPaginaCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_evp_value($action['field'], $action['value']);
        $object->clearEventoPagina(array($action['field']));

        $remove = $this->setEventoPaginaCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeEventoPaginaCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterEventoPaginaCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/03/2015 17:48:10
   */
  public  function afterEventoPaginaCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $executed = true;

    if ($param === 'set') {
      $value = $object->get_evp_value($object->get_evp_reference());
    }

    if ($param === 'add') {
      $object->set_evp_value($object->get_evp_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_evp_items();

      $fields = Controller::after($value, $items, 'evp');
      if (count($fields)) {
        $this->executeEventoPaginaCtrl("0:" . $value, Controller::makeUpdate($fields));
      }
    }

    $properties = $object->get_evp_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $executed;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function executeEventoPaginaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    $properties = $eventoPagina->get_evp_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $eventoPagina->getStatementsEventoPagina();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", evp_responsavel = '" . System::getUser(false) . "', evp_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function verifyEventoPaginaCtrl($object){
		?><?php
	
		$items = $object->get_evp_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getObjectEventoPaginaCtrl($value){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $object = new EventoPagina();

    $properties = $object->get_evp_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $eventoPagina = null;

    $eventoPaginas = $this->getEventoPaginaCtrl($where, "", "", "0", "1");
    if (is_array($eventoPaginas)) {
      $eventoPagina = $eventoPaginas[0];
    }

    return $eventoPagina;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getReplacesEventoPaginaCtrl($reference){
		?><?php

		System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

		$eventoPagina = new EventoPagina();

    $replaces = array();

    $where = "evp_codigo = '" . $reference . "'";
    $eventoPaginas = $this->getEventoPaginaCtrl($where, "", "");
    if (is_array($eventoPaginas)) {
      $eventoPagina = $eventoPaginas[0];

      $items = $eventoPagina->get_evp_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 21:54:40
   */
  public  function configureEventoPaginaCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    System::import('c', 'site', 'Pagina', 'src');

    $screen = new Screen('{project.rsc}');

    $paginaCtrl = new PaginaCtrl(APP_PATH);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }

        $pgn_codigo = $atributos['evp_cod_PAGINA']['value'];
        $where_pgn = "pgn_codigo = '" . $pgn_codigo . "'";
        $pgn_title = $paginaCtrl->getColumnPaginaCtrl('pgn_title', $where_pgn);

        $atributos['evt_descricao']['value'] = 'Perí­odo de exibição da página ' . $pgn_title;

        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getPropertiesEventoPaginaCtrl(){
    ?><?php

      System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

      $eventoPagina = new EventoPagina();

      $properties = $eventoPagina->get_evp_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getItemsEventoPaginaCtrl($search){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src', true, '{project.rsc}');

    $eventoPagina = new EventoPagina();

    if ($search) {
      $object = $this->getObjectEventoPaginaCtrl($search);
      if (!is_null($object)) {
        $eventoPagina = $object;
      }
    }
    
    $items = $eventoPagina->get_evp_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $eventoPagina->get_evp_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function getHistoryEventoPaginaCtrl($items){
    ?><?php
    
    $defaults = array(
      'evp_registro'=>array('id'=>'evp_registro', 'value'=>date('d/m/Y H:i:s')),
      'evp_criador'=>array('id'=>'evp_criador', 'value'=>System::getUser()),
      'evp_alteracao'=>array('id'=>'evp_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'evp_responsavel'=>array('id'=>'evp_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/03/2015 20:25:38
   */
  public  function operationEventoPaginaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'EventoPagina', 'src', true);

    $eventoPagina = new EventoPagina();

    foreach ($items as $id => $item) {
      $eventoPagina->set_evp_value($id, $item['value']);
    }
    
    $method = $operation . "EventoPaginaCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($eventoPagina);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'EventoPaginaCtrl'");
    }

    return $result;
  }

  /**
   * 
   * 
   * @param int $pgn_codigo 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 03/03/2015 18:48:52
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:29:21
   */
  public static function createEventoPaginaCtrl($pgn_codigo){
    ?><?php

    System::import('m', 'site', 'EventoPagina', 'src');
    System::import('c', 'site', 'EventoPagina', 'src');

    System::import('m', 'site', 'Pagina', 'src');
    System::import('c', 'site', 'Pagina', 'src');

    System::import('c', 'organizacao', 'Evento', 'src');

    $eventoPagina = new EventoPagina();
    $eventoPaginaCtrl = new EventoPaginaCtrl(APP_PATH);

    $pagina = new Pagina();
    $paginaCtrl = new PaginaCtrl(APP_PATH);

    $evp_codigo = 0;

    $where_pgn = "pgn_0:" . $pgn_codigo;
    $paginas = $paginaCtrl->getPaginaCtrl($where_pgn, "", "");
    if (is_array($paginas)) {
      $pagina = $paginas[0];

      $where_evp = "evp_cod_PAGINA = '" . $pgn_codigo . "' AND evp_primeiro";
      $evp_codigo = $eventoPaginaCtrl->getColumnEventoPaginaCtrl('evp_codigo', $where_evp);

      $evt_descricao = EventoPagina::$EVP_EVT_DESCRICAO . " " . $pagina->get_pgn_value('pgn_title');

      $evt_inicio_data = null;
      $evt_inicio_hora = null;

      $inicio = explode(' ', $pagina->get_pgn_value('pgn_publish_date'));
      if (count($inicio) > 1) {
        $evt_inicio_data = $inicio[0];
        $evt_inicio_hora = $inicio[1];
      }

      $evt_termino_data = null;
      $evt_termino_hora = null;

      $termino = explode(' ', $pagina->get_pgn_value('pgn_deadline_date'));
      if (count($termino) > 1) {
        $evt_termino_data = $termino[0];
        $evt_termino_hora = $termino[1];
      }

      $evt_dia_inteiro = false;

      $eventoPagina->set_evp_value('evp_cod_PAGINA', $pgn_codigo);

      $eventoPagina->set_evp_value('evt_descricao', $evt_descricao);

      $eventoPagina->set_evp_value('evt_inicio_data', $evt_inicio_data);
      $eventoPagina->set_evp_value('evt_inicio_hora', $evt_inicio_hora);

      $eventoPagina->set_evp_value('evt_termino_data', $evt_termino_data);
      $eventoPagina->set_evp_value('evt_termino_data', $evt_termino_hora);

      $eventoPagina->set_evp_value('evt_dia_inteiro', $evt_dia_inteiro);

      $eventoPagina->set_evp_value('evp_primeiro', 1);

      if (!$evp_codigo) {
        $evp_codigo = $eventoPaginaCtrl->addEventoPaginaCtrl($eventoPagina);
      } else {
        $eventoPagina->set_evp_value('evp_codigo', $evp_codigo);

        $evt_codigo = $eventoPaginaCtrl->getColumnEventoPaginaCtrl('evp_cod_EVENTO', $where_evp);
        $eventoPagina->set_evp_value('evt_codigo', $evt_codigo);

        $eventoPaginaCtrl->setEventoPaginaCtrl($eventoPagina);
      }
    }

    return $evp_codigo;
  }


}