<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 28/02/2015 11:41:06
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:32
 * @category controller
 * @package site
 */


class MidiaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function MidiaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');
    
    $entity = new Midia();
    $properties = $entity->get_mid_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function getRowsMidiaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    $items = $midia->get_mid_items();
    $properties = $midia->get_mid_properties();

    $statements = $midia->getStatementsMidia();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function getMidiaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    $midias = null;

    $rows = $this->getRowsMidiaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $midias = array();
      foreach ($rows as $row) {
        $midia_set = clone $midia;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $midia_set->set_mid_value($key, $item['value'], $reference);
        }
        $midia_set->clearMidia($not_clear);
        $midias[] = $midia_set;
      }
    }

    return $midias;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function getColumnMidiaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    $properties = $midia->get_mid_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $midia->getStatementsMidia();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function getCountMidiaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    $properties = $midia->get_mid_properties();

    $statements = $midia->getStatementsMidia();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnMidiaCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  private  function beforeMidiaCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsMidiaCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_mid_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_mid_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_mid_value($item['id'], $value);
            } else {
              $object->set_mid_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsMidiaCtrl("");
      $properties = $this->getPropertiesMidiaCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_mid_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnMidiaCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function addMidiaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesMidiaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_mid_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesMidiaCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_mid_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setMidiaCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function setMidiaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_mid_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesMidiaCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_mid_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function removeMidiaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_mid_items();
    $properties = $this->getPropertiesMidiaCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_mid_value($action['field'], $action['value']);
        $object->clearMidia(array($action['field']));

        $remove = $this->setMidiaCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  private  function afterMidiaCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_mid_value($object->get_mid_reference());
    } else if ($param === 'add') {
      $object->set_mid_value($object->get_mid_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_mid_items();

      $fields = Controller::after($value, $items, 'mid');
      if (count($fields)) {
        $executed = $this->executeMidiaCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_mid_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  private  function executeMidiaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    $properties = $midia->get_mid_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $midia->getStatementsMidia();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", mid_responsavel = '" . System::getUser(false) . "', mid_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  private  function verifyMidiaCtrl($object, $param){
		?><?php
	
		$verifyed = true;
	
	  $where = "FALSE";

	  switch ($param) {
      case 'add':

	      $where = "mid_1:" . $object->get_mid_value('mid_slug');
	      break;
      case 'set':

	      $where = "mid_2:" . $object->get_mid_value('mid_codigo') . "," . $object->get_mid_value('mid_slug');
	      break;
	  }

    $existe = $this->getColumnMidiaCtrl('mid_codigo', $where);

    if ($existe) {
  		/*
  		 * Validation comes here!
  		 * If the validation fails, add an message using the Console class and set verifyed = false
  		 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
  		 */
  		Console::add("Já existe um registro com esse Identificador (" . $object->get_mid_value('mid_slug') . ")", null, 'mid_slug', 'Identificador');
		  $verifyed = false;
    }
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:15
   */
  public  function getObjectMidiaCtrl($value){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $object = new Midia();

    $properties = $object->get_mid_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $midia = null;

    $midias = $this->getMidiaCtrl($where, "", "", "0", "1");
    if (is_array($midias)) {
      $midia = $midias[0];
    }

    return $midia;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function getReplacesMidiaCtrl($reference){
		?><?php

		System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

		$midia = new Midia();

    $replaces = array();

    $where = "mid_codigo = '" . $reference . "'";
    $midias = $this->getMidiaCtrl($where, "", "");
    if (is_array($midias)) {
      $midia = $midias[0];

      $items = $midia->get_mid_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   * @param string $filter 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function configureMidiaCtrl($operation, $atributos, $properties, $target, $level, $filter = null){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }

        if ($filter) {

          foreach ($atributos as $key => $atributo) {

            if (!in_array($atributo['id'], ['mid_codigo','mid_arquivo','mid_url'])) {
              $atributo['form'] = 0;
              $atributos[$key] = $atributo;
            }
          }
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function getPropertiesMidiaCtrl(){
    ?><?php

      System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

      $midia = new Midia();

      $properties = $midia->get_mid_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function getItemsMidiaCtrl($search){
    ?><?php

    System::import('m', 'site', 'Midia', 'src', true, '{project.rsc}');

    $midia = new Midia();

    if ($search) {
      $object = $this->getObjectMidiaCtrl($search);
      if (!is_null($object)) {
        $midia = $object;
      }
    }
    
    $items = $midia->get_mid_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $midia->get_mid_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function getHistoryMidiaCtrl($items){
    ?><?php
    
    $defaults = array(
      'mid_registro'=>array('id'=>'mid_registro', 'value'=>date('d/m/Y H:i:s')),
      'mid_criador'=>array('id'=>'mid_criador', 'value'=>System::getUser()),
      'mid_alteracao'=>array('id'=>'mid_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'mid_responsavel'=>array('id'=>'mid_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 11/04/2015 21:10:15
   */
  public  function operationMidiaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'Midia', 'src', true);

    $midia = new Midia();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $midia->get_mid_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $midia->set_mid_value($id, $item['value']);
    }
    
    $method = $action . "MidiaCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyMidiaCtrl($midia, $operation);
      if ($verify) {

        $before = $this->beforeMidiaCtrl($midia, $operation);
        if ($before) {

          $response = $this->$method($midia);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterMidiaCtrl($midia, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'MidiaCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 26/03/2015 16:37:16
   */
  public  function copyMidiaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesMidiaCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_mid_value($reference, null);
    }

    return $this->addMidiaCtrl($object, $validate, $debug);
  }

  /**
   * Gerencia as operations em circunstãncias especiais
   * 
   * @param object $operations 
   * @param boolean $operation 
   * @param boolean $items 
   * @param boolean $filter 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:53:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:57:09
   */
  public  function getOperationsMidiaCtrl($operations, $operation, $items, $filter){

    if ($operation === 'set' && $filter) {
      $operations['set']->operations = ["save"=>"btn-primary"];
    }

    return $operations;
  }

}