<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 19:40:05
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/03/2015 21:38:41
 * @category controller
 * @package site
 */


class PaginaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;
  
  private $news = ['pgn_level'=>'1', 'pgn_cod_PAGINA'=>'13', 'pgn_home'=>false, 'pgn_type'=>'0', 'pgn_side_bar'=>'hide', 'pgn_widget'=>'2', 'pgn_menu'=>'0', 'pgn_menu_superior'=>'0', 'pgn_target'=>'_self'];

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function PaginaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');
    
    $entity = new Pagina();
    $properties = $entity->get_pgn_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 11/04/2015 11:42:06
   */
  public  function getRowsPaginaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $items = $pagina->get_pgn_items();
    $properties = $pagina->get_pgn_properties();

    $statements = $pagina->getStatementsPagina();

    $table = $properties['table'] . $properties['join'];

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);
    $order = Statement::parse($order, $statements);

    $where = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $group = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $order = $order ? $order : $properties['order'];


    foreach ($items as $item) {

      $id = $properties['table'] . '.' . $item['id'];
      if (String::position($where, $id) === false) {
        $where = String::replace($where, $item['id'], $id);
      }

    }

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields, false, true);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function getPaginaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $paginas = null;

    $rows = $this->getRowsPaginaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginas = array();
      foreach ($rows as $row) {
        $pagina_set = clone $pagina;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $pagina_set->set_pgn_value($key, $item['value'], $reference);
        }
        $pagina_set->clearPagina($not_clear);
        $paginas[] = $pagina_set;
      }
    }

    return $paginas;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 11/04/2015 13:13:30
   */
  public  function getColumnPaginaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $items = $pagina->get_pgn_items();

    $properties = $pagina->get_pgn_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $pagina->getStatementsPagina();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    foreach ($items as $item) {

      $id = $properties['table'] . '.' . $item['id'];
      if (String::position($w, $id) === false) {
        $w = String::replace($w, $item['id'], $id);
      }

    }

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'select' => 1, 'value' => null);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null, false, true);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:13:46
   */
  public  function getCountPaginaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $properties = $pagina->get_pgn_properties();

    $statements = $pagina->getStatementsPagina();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaCtrl("COUNT(TBL_PAGINA.pgn_codigo)", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 13/04/2015 20:10:24
   */
  public  function beforePaginaCtrl($object, $param){
    ?><?php

    $pgn_home = $object->get_pgn_value('pgn_home');
    if ($pgn_home) {

      $this->executePaginaCtrl("TRUE", "TBL_PAGINA.pgn_home = 0");
    }

    $pgn_type_content = $object->get_pgn_value('pgn_type_content');
    if ($pgn_type_content === 'news') {

      foreach ($this->news as $key => $_news) {

        $object->set_pgn_value($key, $_news);
      }
    }

    if ($param === 'set') {

      $pgn_slug_new = $object->get_pgn_value('pgn_slug');
      $pgn_slug_old = $this->getColumnPaginaCtrl('TBL_PAGINA.pgn_slug', 'pgn_0:' . $object->get_pgn_value('pgn_codigo'));

      $pgn_title_new = $object->get_pgn_value('pgn_title');
      $pgn_title_old = $this->getColumnPaginaCtrl('TBL_PAGINA.pgn_title', 'pgn_0:' . $object->get_pgn_value('pgn_codigo'));

      if (($pgn_slug_new !== $pgn_slug_old) || ($pgn_title_new !== $pgn_title_old)) {

        $kids = $this->getPaginaCtrl("pgn_11:" . $object->get_pgn_value('pgn_codigo'), "", "");

        if (is_array($kids)) {
          foreach ($kids as $kid) {
            $this->setPaginaCtrl($kid);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/03/2015 21:59:51
   */
  public  function addPaginaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pgn_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyPaginaCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesPaginaCtrl();
        $table = $properties['table'];

        $object = $this->beforePaginaCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_pgn_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterPaginaCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setPaginaCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:23:03
   */
  public  function setPaginaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyPaginaCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_pgn_items();
      $statements = $object->getStatementsPagina();
      $properties = $object->get_pgn_properties();

      $conector = ',';
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $item['value'];
        }
      }

      $where = Statement::parse('pgn_0:' . implode($conector, $arr_where), $statements);

      $table = $properties['table'] . $properties['join'];

      $object = $this->beforePaginaCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_pgn_items(), $where, true);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterPaginaCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function removePaginaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pgn_items();
    $properties = $this->getPropertiesPaginaCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pgn_value($action['field'], $action['value']);
        $object->clearPagina(array($action['field']));

        $remove = $this->setPaginaCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'];

          $object = $this->beforePaginaCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterPaginaCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:28:40
   */
  public  function afterPaginaCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    System::import('c', 'site', 'EventoPagina', 'src');

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_pgn_value($object->get_pgn_reference());
    }

    if ($param === 'add') {
      $object->set_pgn_value($object->get_pgn_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_pgn_items();

      $items['pgn_breadcrumb_title']['type'] = 'calculated';
      $items['pgn_breadcrumb_slug']['type'] = 'calculated';

      $fields = Controller::after($value, $items, 'pgn');

      if (!$items['pgn_cod_BANNER']['value']) {
        $fields['pgn_cod_BANNER'] = 'NULL';
      }
      //var_dump($fields);
      if (count($fields)) {
        $executed = $this->executePaginaCtrl('pgn_0:' . $value, Controller::makeUpdate($fields), true, false);
        $after = Boolean::parse($executed);
      }

    }

    EventoPaginaCtrl::createEventoPaginaCtrl($value);

    $properties = $object->get_pgn_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function executePaginaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $properties = $pagina->get_pgn_properties();
    $table = $properties['table']/* . $properties['join']*/;

    $statements = $pagina->getStatementsPagina();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pgn_responsavel = '" . System::getUser(false) . "', pgn_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:17:28
   */
  public  function verifyPaginaCtrl($object){
    ?><?php
  
    $items = $object->get_pgn_items();
    $error_messages = array();
  
    foreach ($items as $key => $item) {
      $value = $item['value'];
      if (!is_null($value)) {
        if ($key === 'pgn_slug') {

          $pgn_title = $this->getColumnPaginaCtrl('TBL_PAGINA.pgn_title', "pgn_8:" . $items['pgn_codigo']['value'] . "");

          if (!is_null($pgn_title)) {
            $error_messages[] = array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>'A página "' . $pgn_title . '" já utiliza este identificador.');
          }
        }
      }
    }
  
    return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 11:35:33
   */
  public  function getObjectPaginaCtrl($value){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $object = new Pagina();

    $properties = $object->get_pgn_properties();

    $values = explode(",", $value);

    $where = "FALSE";
    if (count($values) > 0) {
      $where = "pgn_0:" . $value;
    }

    $pagina = null;

    $paginas = $this->getPaginaCtrl($where, "", "", "0", "1");
    if (is_array($paginas)) {
      $pagina = $paginas[0];
    }

    return $pagina;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   */
  public  function getReplacesPaginaCtrl($reference){
		?><?php

		System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

		$pagina = new Pagina();

    $replaces = array();

    $where = "pgn_codigo = '" . $reference . "'";
    $paginas = $this->getPaginaCtrl($where, "", "");
    if (is_array($paginas)) {
      $pagina = $paginas[0];

      $items = $pagina->get_pgn_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   * @param string $filter
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 09/04/2015 16:51:02
   */
  public  function configurePaginaCtrl($operation, $atributos, $properties, $target, $level, $filter = null){
    ?><?php

    System::import('c', 'site', 'Rodape', 'src');

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $rodapeCtrl = new RodapeCtrl(APP_PATH);

    $screen = new Screen('{project.rsc}');
    
    $edit = false;

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":

        $pgn_type_content = $atributos['pgn_type_content']['value'];
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }

        if ($pgn_type_content === 'news') {

          $atributos['pgn_type_content']['value'] = $pgn_type_content;
        }

        $rdp_value = $rodapeCtrl->getColumnRodapeCtrl("CONCAT(rdp_codigo, '</>', rdp_descricao)", "rdp_padrao");
        $atributos['pgn_cod_RODAPE']['value'] = $rdp_value;
        $atributos['pgn_author']['value'] = utf8_decode(System::getUser());
        
        $edit = true;
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }

        $edit = true;
        break;
        
      case "list":
        break;

    }

    if ($edit) {

      $atributos['pgn_type']['onchange'] = "Application.custom.paginaType(this.value);";
      $atributos['pgn_title']['onkeyup'] = "Application.custom.paginaSlug(this.value);";
      $atributos['pgn_level']['onchange'] = "Application.custom.paginaLevel(this.value);";

      $max_level = $this->getColumnPaginaCtrl("MAX(TBL_PAGINA.pgn_level)", "TRUE");
      if (is_null($max_level)) {
        $max_level = 0;
      }

      $max_level = Number::parseInteger($max_level) + 1;
      $atributos['pgn_level']['type_content'] = "0/" . $max_level;

      $atributos['pgn_cod_PAGINA']['foreign']['where'] = Encode::encrypt('pgn_1:' . ($atributos['pgn_level']['value'] - 1));

    }

    $references = explode(',', $properties['reference']);

    $_filters = explode(',', Encode::decrypt($filter));
    foreach ($_filters as $_filter) {

      $data = explode(':', $_filter);

      if (in_array($data[0], $references)) {

        $hidden = ['pgn_cod_PAGINA','pgn_cod_BANNER','pgn_fk_pgt_cod_PAGINA_4823','pgn_level','pgn_slug','pgn_home','pgn_cod_CATEGORIA','pgn_cod_RODAPE','pgn_type','pgn_side_bar','pgn_featured','pgn_widget','pgn_menu','pgn_publish','pgn_lightbox','pgn_miniature','pgn_background_image','pgn_target','pgn_author','pgn_ordem','pgn_type_content'];

        foreach ($atributos as $key => $atributo) {
          $id = $atributo['id'];
          if (in_array($id, $hidden)) {
            $atributo['form'] = 0;
          }
          $atributos[$key] = $atributo;
        }
      }
    }


    if ($atributos['pgn_type_content']['value'] === 'news') {

      foreach ($atributos as $key => $atributo) {
        $id = $atributo['id'];
        if (isset($this->news[$id])) {
          $atributo['form'] = 0;
        }
        $atributos[$key] = $atributo;
      }
    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   */
  public  function getPropertiesPaginaCtrl(){
    ?><?php

      System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

      $pagina = new Pagina();

      $properties = $pagina->get_pgn_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   */
  public  function getItemsPaginaCtrl($search){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    if ($search) {
      $object = $this->getObjectPaginaCtrl($search);
      if (!is_null($object)) {
        $pagina = $object;
      }
    }
    
    $items = $pagina->get_pgn_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $pagina->get_pgn_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   */
  public  function getHistoryPaginaCtrl($items){
    ?><?php
    
    $defaults = array(
      'pgn_registro'=>array('id'=>'pgn_registro', 'value'=>date('d/m/Y H:i:s')),
      'pgn_criador'=>array('id'=>'pgn_criador', 'value'=>System::getUser()),
      'pgn_alteracao'=>array('id'=>'pgn_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pgn_responsavel'=>array('id'=>'pgn_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:10
   */
  public  function operationPaginaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'Pagina', 'src', true);

    $pagina = new Pagina();

    foreach ($items as $id => $item) {
      $pagina->set_pgn_value($id, $item['value']);
    }
    
    $method = $operation . "PaginaCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($pagina);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'PaginaCtrl'");
    }

    return $result;
  }

  /**
   * Com responsabilidade de copiar um registro, realiza o mapeamento da operação 'copy'
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:53:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:57:09
   */
  public  function copyPaginaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPaginaCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pgn_value($reference, null);
    }

    return $this->addPaginaCtrl($object, $validate, $debug);
  }

  /**
   * Gerencia as operations em circunstãncias especiais
   * 
   * @param object $operations 
   * @param boolean $operation 
   * @param boolean $items 
   * @param boolean $filter 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:53:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/03/2015 14:57:09
   */
  public  function getOperationsPaginaCtrl($operations, $operation, $items, $filter){

    if ($operation === 'set' && $filter && String::position(Encode::decrypt($filter), 'news') === false) {
      $operations['set']->operations = ["save"=>"btn-primary"];
    }
    
    return $operations;
  }

  /**
   * Remove as filhas de uma página 
   * 
   * @param int $pgn_codigo 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function removerFilhasPaginaCtrl($pgn_codigo, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src', true, '{project.rsc}');

    $pagina = new Pagina();

    $properties = $pagina->get_pgn_properties();

    $table = $properties['table'];
    $where = "pgn_cod_PAGINA = '" . (int)($pgn_codigo) . "'";

    $sql = "DELETE FROM " . $table . " WHERE " . $where;

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

}