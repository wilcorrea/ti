<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/09/2015 17:17:00
 * @category controller
 * @package site
 */


class PaginaCursoCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function PaginaCursoCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');
    
    $entity = new PaginaCurso();
    $properties = $entity->get_pgc_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function getRowsPaginaCursoCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    $items = $paginaCurso->get_pgc_items();
    $properties = $paginaCurso->get_pgc_properties();

    $statements = $paginaCurso->getStatementsPaginaCurso();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function getPaginaCursoCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    $paginaCursos = null;

    $rows = $this->getRowsPaginaCursoCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginaCursos = array();
      foreach ($rows as $row) {
        $paginaCurso_set = clone $paginaCurso;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $paginaCurso_set->set_pgc_value($key, $item['value'], $reference);
        }
        $paginaCurso_set->clearPaginaCurso($not_clear);
        $paginaCursos[] = $paginaCurso_set;
      }
    }

    return $paginaCursos;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function getColumnPaginaCursoCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    $properties = $paginaCurso->get_pgc_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaCurso->getStatementsPaginaCurso();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function getCountPaginaCursoCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    $properties = $paginaCurso->get_pgc_properties();

    $statements = $paginaCurso->getStatementsPaginaCurso();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaCursoCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 23/09/2015 19:21:44
   */
  private  function beforePaginaCursoCtrl($object, $param){
    ?><?php

    $before = true;

    System::import('c', 'site', 'Pagina', 'src');
    System::import('c', 'site', 'Categoria', 'src');


    switch ($param) {

      case 'add':
      case 'set':
        //{

        $pgc_slug = String::lower(
          String::removeSpecial(
            String::replace(String::replace(String::replace(String::replace($object->get_pgc_value('pgc_descricao'), ',', ''), '-', ''), '  ', ' '), ' ', '-'), false
          )
        );

        $paginaCtrl = new PaginaCtrl();

        $categoriaCtrl = new CategoriaCtrl();

        $pgn_codigo = $object->get_pgc_value('pgc_cod_PAGINA');
        $ctg_codigo = $object->get_pgc_value('pgc_cod_CATEGORIA');

        if ($param === 'set') {

          $where = "pgc_0:" . $object->get_pgc_value('pgc_codigo');

          if (!$pgc_slug) {

            $pgc_slug = $this->getColumnPaginaCursoCtrl('pgc_slug', $where);
          }

          if (!$pgn_codigo) {

            $pgn_codigo = $this->getColumnPaginaCursoCtrl('pgc_cod_PAGINA', $where);
          }

          if (!$ctg_codigo) {

            $ctg_codigo = $this->getColumnPaginaCursoCtrl('pgc_cod_CATEGORIA', $where);
          }
        }

        $object->set_pgc_value('pgc_slug', $pgc_slug);
    
        $pgc_slugs = [
            'principal' => 'O Curso'
          , 'objetivo-geral' => 'Objetivo Geral'
          , 'publico-alvo' => 'Público Alvo'
          , 'horarios-de-aula' => 'Horários de Aula'
          , 'corpo-docente' => 'Corpo Docente'
          , 'matriz-curricular' => 'Matriz Curricular'
          , 'estagio-supervisionado' => 'Estágio Supervisionado'
          , 'album-de-fotos' => 'Álbum de Fotos'
          , 'arquivos-do-curso' => 'Arquivos'
          , 'matricula' => 'Matrícula'
        ];

        $ctg_menu = $categoriaCtrl->getColumnCategoriaCtrl('ctg_menu', "ctg_0:" . $ctg_codigo);

        if ($ctg_menu === 'simple') {

          $pgc_slugs = [
              'principal' => 'O Curso'
            , 'objetivo-geral' => 'Objetivo Geral'
            , 'publico-alvo' => 'Público Alvo'
            , 'matriz-curricular' => 'Conteúdo'
            , 'contato' => 'Contato'
            , 'matricula' => 'Matrícula'
          ];
        }

        $pgn_cod_PAGINA = $paginaCtrl->getColumnPaginaCtrl('TBL_PAGINA.pgn_codigo', "pgn_10:" . $object->get_pgc_value('pgc_cod_CATEGORIA'));

        $_pgn_cod_PAGINA = $pgn_cod_PAGINA;
        $_pgn_cod_CATEGORIA = $object->get_pgc_value('pgc_cod_CATEGORIA');
        $_pgn_title = $object->get_pgc_value('pgc_descricao');
        $_pgn_miniature = $object->get_pgc_value('pgc_miniatura');
        $_pgn_slug = $pgc_slug;
        $_pgn_level = '1';

        $_pgn_type = '5';
        $_pgn_sidebar = 'full';
        $_pgn_background_image = $object->get_pgc_value('pgc_fundo');

        $pgn_codigo = $this->createPaginaRelacionadaPaginaCursoCtrl($pgn_codigo, $_pgn_cod_PAGINA, $_pgn_cod_CATEGORIA, $_pgn_level, $_pgn_slug, $_pgn_title, $_pgn_type, $_pgn_sidebar, $_pgn_miniature, '', 0);

        $object->set_pgc_value('pgc_cod_PAGINA', $pgn_codigo);

        if ($pgn_codigo) {

          $this->createWidgetPaginaCursoCtrl($pgn_codigo, 'pagina-curso', 'content');
  
          $paginaCtrl->removerFilhasPaginaCtrl($pgn_codigo);

          $_pgn_order = 0;
          foreach ($pgc_slugs as $_pgn_slug => $_pgn_title) {
  
            $_pgn_level = '2';
            $_pgn_sidebar = 'left';
            $_pgn_order++;

            $_pgn_codigo = $this->createPaginaRelacionadaPaginaCursoCtrl(null, $pgn_codigo, $_pgn_cod_CATEGORIA, $_pgn_level, $_pgn_slug, utf8_decode($_pgn_title), $_pgn_type, $_pgn_sidebar, $_pgn_miniature, $_pgn_background_image, $_pgn_order);

            $_widgets = [
                (object) ['wid_id' => 'paginas-relacionadas', 'pgw_local' => 'sidebar', 'pgw_ordem' => 1, 'pgw_filtro' => 'no-main|no-title']
              , (object) ['wid_id' => 'estude-na-fagoc', 'pgw_local' => 'sidebar', 'pgw_ordem' => 2, 'pgw_filtro' => $pgc_slug]
              , (object) ['wid_id' => 'pagina-curso-' . $_pgn_slug, 'pgw_local' => 'content', 'pgw_ordem' => 1, 'pgw_filtro' => 'no-main|no-title']
            ];

            foreach ($_widgets as $_widget) {

              $this->createWidgetPaginaCursoCtrl($_pgn_codigo, $_widget->wid_id, $_widget->pgw_local, $_widget->pgw_ordem, $_widget->pgw_filtro);
            }
          }
        }
        //}
        break;

      case 'remove':
        //{

        $pgc_cod_PAGINA = $object->get_pgc_value('pgc_cod_PAGINA');

        $pgn_codigo = $pgc_cod_PAGINA ? $pgc_cod_PAGINA : $this->getColumnPaginaCursoCtrl('pgc_cod_PAGINA', "pgc_codigo = '" . $object->get_pgc_value('pgc_codigo') . "'");

        $this->removePaginasRelacionadasPaginaCtrl($pgn_codigo);
        //}
        break;
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function addPaginaCursoCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaCursoCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pgc_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPaginaCursoCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pgc_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setPaginaCursoCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function setPaginaCursoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pgc_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPaginaCursoCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pgc_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function removePaginaCursoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pgc_items();
    $properties = $this->getPropertiesPaginaCursoCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pgc_value($action['field'], $action['value']);
        $object->clearPaginaCurso(array($action['field']));

        $remove = $this->setPaginaCursoCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 17/09/2015 23:43:26
   */
  private  function afterPaginaCursoCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_pgc_value($object->get_pgc_reference());
    } else if ($param === 'add') {
      $object->set_pgc_value($object->get_pgc_reference(), $value);
    }

    if ($param !== 'remove') {

      $items = $object->get_pgc_items();

      $fields = Controller::after($value, $items, 'pgc');
      if (count($fields)) {
        $executed = $this->executePaginaCursoCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pgc_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function executePaginaCursoCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    $properties = $paginaCurso->get_pgc_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $paginaCurso->getStatementsPaginaCurso();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pgc_responsavel = '" . System::getUser(false) . "', pgc_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 19:43:53
   */
  private  function verifyPaginaCursoCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_pgc_items();

		$verifyed = true;

    if (in_array($operation, ['add', 'set'])) {

  		$pgc_slug = String::lower(
        String::removeSpecial(
          String::replace($object->get_pgc_value('pgc_descricao'), ' ', '-'), false
        )
      );

      $pgc_codigo = $this->getColumnPaginaCursoCtrl('pgc_codigo', "pgc_4:" . $pgc_slug . "," . $object->get_pgc_value('pgc_codigo') . "");

  		if ($pgc_codigo) {
  			/*
  			 * Validation comes here!
  			 * If the validation fails, add an message using the Console class and set verifyed = false
  			 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
  			 */
  		  Console::add("Já existe um Curso igual a este (" . $items['pgc_slug']['value'] . ") cadastrado!", null, $items['pgc_slug']['id'], $items['pgc_slug']['description']);
  		  $verifyed = false;
  		}
    }
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function getObjectPaginaCursoCtrl($value){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $object = new PaginaCurso();

    $properties = $object->get_pgc_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $paginaCurso = null;

    $paginaCursos = $this->getPaginaCursoCtrl($where, "", "", "0", "1");
    if (is_array($paginaCursos)) {
      $paginaCurso = $paginaCursos[0];
    }

    return $paginaCurso;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function getReplacesPaginaCursoCtrl($reference){
		?><?php

		System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

		$paginaCurso = new PaginaCurso();

    $replaces = array();

    $where = "pgc_codigo = '" . $reference . "'";
    $paginaCursos = $this->getPaginaCursoCtrl($where, "", "");
    if (is_array($paginaCursos)) {
      $paginaCurso = $paginaCursos[0];

      $items = $paginaCurso->get_pgc_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 19:46:08
   */
  public  function configurePaginaCursoCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function getPropertiesPaginaCursoCtrl(){
    ?><?php

      System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

      $paginaCurso = new PaginaCurso();

      $properties = $paginaCurso->get_pgc_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function getItemsPaginaCursoCtrl($search){
    ?><?php

    System::import('m', 'site', 'PaginaCurso', 'src', true, '{project.rsc}');

    $paginaCurso = new PaginaCurso();

    if ($search) {
      $object = $this->getObjectPaginaCursoCtrl($search);
      if (!is_null($object)) {
        $paginaCurso = $object;
      }
    }
    
    $items = $paginaCurso->get_pgc_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $paginaCurso->get_pgc_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function getHistoryPaginaCursoCtrl($items){
    ?><?php
    
    $defaults = array(
      'pgc_registro'=>array('id'=>'pgc_registro', 'value'=>date('d/m/Y H:i:s')),
      'pgc_criador'=>array('id'=>'pgc_criador', 'value'=>System::getUser()),
      'pgc_alteracao'=>array('id'=>'pgc_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pgc_responsavel'=>array('id'=>'pgc_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function operationPaginaCursoCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'PaginaCurso', 'src', true);

    $paginaCurso = new PaginaCurso();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $paginaCurso->get_pgc_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $paginaCurso->set_pgc_value($id, $item['value']);
    }
    
    $method = $action . "PaginaCursoCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyPaginaCursoCtrl($paginaCurso, $operation);
      if ($verify) {

        $before = $this->beforePaginaCursoCtrl($paginaCurso, $operation);
        if ($before) {

          $response = $this->$method($paginaCurso);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterPaginaCursoCtrl($paginaCurso, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'PaginaCursoCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:05
   */
  public  function copyPaginaCursoCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPaginaCursoCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pgc_value($reference, null);
    }

    return $this->addPaginaCursoCtrl($object, $validate, $debug);
  }

  /**
   * Cria um Widget para a página relacionada
   * 
   * @param int $pgn_codigo 
   * @param string $wid_id 
   * @param string $pgw_local 
   * @param string $pgw_ordem 
   * @param string $pgw_filtro 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 17/09/2015 22:53:29
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 20:25:23
   */
  public  function createWidgetPaginaCursoCtrl($pgn_codigo, $wid_id, $pgw_local, $pgw_ordem = 1, $pgw_filtro = null){
    ?><?php

    System::import('c', 'site', 'Widget', 'src');

    System::import('m', 'site', 'PaginaWidget', 'src');
    System::import('c', 'site', 'PaginaWidget', 'src');

    $create = false;
    
    $widgetCtrl = new WidgetCtrl();
  
    $pgw_cod_WIDGET = $widgetCtrl->getColumnWidgetCtrl('wid_codigo', "wid_id = '" . $wid_id . "'");

    if ($pgw_cod_WIDGET) {

      $paginaWidgetCtrl = new PaginaWidgetCtrl();

      $pgw_codigo = $paginaWidgetCtrl->getColumnPaginaWidgetCtrl('pgw_codigo', "pgw_cod_PAGINA = '" . $pgn_codigo . "' AND pgw_cod_WIDGET = '" . $pgw_cod_WIDGET . "'");

      if (!$pgw_codigo) {

        $paginaWidget = new PaginaWidget();

        $paginaWidget->set_pgw_value('pgw_cod_PAGINA', $pgn_codigo);
        $paginaWidget->set_pgw_value('pgw_cod_WIDGET', $pgw_cod_WIDGET);
        $paginaWidget->set_pgw_value('pgw_local', $pgw_local);
        $paginaWidget->set_pgw_value('pgw_ordem', $pgw_ordem);
        $paginaWidget->set_pgw_value('pgw_filtro', $pgw_filtro);
        $paginaWidget->set_pgw_value('pgw_tamanho', '12');
  
        $create = $paginaWidgetCtrl->addPaginaWidgetCtrl($paginaWidget);
      }
    }

    return $create;
    
  }

  /**
   * Cria páginas relacionadas ao Curso
   * 
   * @param int $pgn_codigo 
   * @param int $pgn_cod_PAGINA 
   * @param int $pgn_cod_CATEGORIA 
   * @param string $pgn_level 
   * @param string $pgn_slug 
   * @param string $pgn_title 
   * @param string $pgn_type 
   * @param string $pgn_sidebar 
   * @param string $pgn_miniature 
   * @param string $pgn_background_image 
   * @param int $pgn_ordem 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 10:27:12
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 11:26:23
   */
  public  function createPaginaRelacionadaPaginaCursoCtrl($pgn_codigo, $pgn_cod_PAGINA, $pgn_cod_CATEGORIA, $pgn_level, $pgn_slug, $pgn_title, $pgn_type, $pgn_sidebar, $pgn_miniature, $pgn_background_image, $pgn_ordem){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src');
    System::import('c', 'site', 'Pagina', 'src');


    $paginaCtrl = new PaginaCtrl();

    $where = "pgn_0:" . $pgn_codigo;
    if ($pgn_codigo === null) {
      $where = "pgn_9:" . $pgn_cod_PAGINA . "," . $pgn_slug;
    }
  
    $pgn_codigo = $paginaCtrl->getColumnPaginaCtrl('TBL_PAGINA.pgn_codigo', $where);

    $pagina = new Pagina();

    if ($pgn_codigo) {

      $pagina->set_pgn_value('pgn_codigo', $pgn_codigo);
    } else {

      //die('Página não encontrada: ' . $pgn_slug);
    }

    $pgn_level = (int) $pgn_level;

    $values = [
          'pgn_cod_PAGINA'=>$pgn_cod_PAGINA
        , 'pgn_cod_CATEGORIA'=>$pgn_cod_CATEGORIA
        , 'pgn_level'=>$pgn_level
        , 'pgn_slug'=>$pgn_slug
        , 'pgn_title'=>$pgn_title
        , 'pgn_side_bar'=>$pgn_sidebar
        , 'pgn_type'=>$pgn_type
        , 'pgn_miniature'=>$pgn_miniature
        , 'pgn_background_image'=>$pgn_background_image
        , 'pgn_ordem'=>$pgn_ordem
        , 'pgn_cod_RODAPE'=>1
        , 'pgn_cod_BANNER'=>null
        , 'pgn_menu'=>$pgn_level === 1 ? '2' : '0'
        , 'pgn_home'=>'0'
        , 'pgn_target'=>'_self'
        , 'pgn_type_content'=>'course'
        , 'pgn_menu_superior'=>'0'
        , 'pgn_publish'=>'1'
        , 'pgn_widget'=>'1'
        , 'pgn_featured'=>'0'
      ];

    foreach ($values as $key => $value) {
      $pagina->set_pgn_value($key, $value);
    }

    if ($pgn_codigo) {

      $paginaCtrl->setPaginaCtrl($pagina);
    } else {

      $pgn_codigo = $paginaCtrl->addPaginaCtrl($pagina);
    }
    
    //var_dump("$pgn_codigo, $pgn_cod_PAGINA, $pgn_slug");
    //die();

    return $pgn_codigo;
    
  }

  /**
   * Remove as páginas relacionadas ao Curso
   * 
   * @param int $pgn_codigo 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 12:24:37
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 12:26:06
   */
  public  function removePaginasRelacionadasPaginaCtrl($pgn_codigo){
    ?><?php

    System::import('m', 'site', 'Pagina', 'src');
    System::import('c', 'site', 'Pagina', 'src');

    $pagina = new Pagina();

    $paginaCtrl = new PaginaCtrl();

    $pagina->set_pgn_value('pgn_codigo', $pgn_codigo);
      
    $save = $paginaCtrl->removePaginaCtrl($pagina);

    return $pgn_codigo;
    
  }


}