<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:18
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/09/2015 12:39:01
 * @category controller
 * @package site
 */


class PaginaCursoAreaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   */
  public  function PaginaCursoAreaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');
    
    $entity = new PaginaCursoArea();
    $properties = $entity->get_pca_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   */
  public  function getRowsPaginaCursoAreaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    $items = $paginaCursoArea->get_pca_items();
    $properties = $paginaCursoArea->get_pca_properties();

    $statements = $paginaCursoArea->getStatementsPaginaCursoArea();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   */
  public  function getPaginaCursoAreaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    $paginaCursoAreas = null;

    $rows = $this->getRowsPaginaCursoAreaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginaCursoAreas = array();
      foreach ($rows as $row) {
        $paginaCursoArea_set = clone $paginaCursoArea;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $paginaCursoArea_set->set_pca_value($key, $item['value'], $reference);
        }
        $paginaCursoArea_set->clearPaginaCursoArea($not_clear);
        $paginaCursoAreas[] = $paginaCursoArea_set;
      }
    }

    return $paginaCursoAreas;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   */
  public  function getColumnPaginaCursoAreaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    $properties = $paginaCursoArea->get_pca_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaCursoArea->getStatementsPaginaCursoArea();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   */
  public  function getCountPaginaCursoAreaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    $properties = $paginaCursoArea->get_pca_properties();

    $statements = $paginaCursoArea->getStatementsPaginaCursoArea();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaCursoAreaCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  private  function beforePaginaCursoAreaCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsPaginaCursoAreaCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_pca_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_pca_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_pca_value($item['id'], $value);
            } else {
              $object->set_pca_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsPaginaCursoAreaCtrl("");
      $properties = $this->getPropertiesPaginaCursoAreaCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_pca_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnPaginaCursoAreaCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function addPaginaCursoAreaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaCursoAreaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pca_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPaginaCursoAreaCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pca_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setPaginaCursoAreaCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function setPaginaCursoAreaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pca_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPaginaCursoAreaCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pca_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function removePaginaCursoAreaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pca_items();
    $properties = $this->getPropertiesPaginaCursoAreaCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pca_value($action['field'], $action['value']);
        $object->clearPaginaCursoArea(array($action['field']));

        $remove = $this->setPaginaCursoAreaCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  private  function afterPaginaCursoAreaCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_pca_value($object->get_pca_reference());
    } else if ($param === 'add') {
      $object->set_pca_value($object->get_pca_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_pca_items();

      $fields = Controller::after($value, $items, 'pca');
      if (count($fields)) {
        $executed = $this->executePaginaCursoAreaCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pca_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function executePaginaCursoAreaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    $properties = $paginaCursoArea->get_pca_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $paginaCursoArea->getStatementsPaginaCursoArea();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pca_responsavel = '" . System::getUser(false) . "', pca_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  private  function verifyPaginaCursoAreaCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_pca_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function getObjectPaginaCursoAreaCtrl($value){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $object = new PaginaCursoArea();

    $properties = $object->get_pca_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $paginaCursoArea = null;

    $paginaCursoAreas = $this->getPaginaCursoAreaCtrl($where, "", "", "0", "1");
    if (is_array($paginaCursoAreas)) {
      $paginaCursoArea = $paginaCursoAreas[0];
    }

    return $paginaCursoArea;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function getReplacesPaginaCursoAreaCtrl($reference){
		?><?php

		System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

		$paginaCursoArea = new PaginaCursoArea();

    $replaces = array();

    $where = "pca_codigo = '" . $reference . "'";
    $paginaCursoAreas = $this->getPaginaCursoAreaCtrl($where, "", "");
    if (is_array($paginaCursoAreas)) {
      $paginaCursoArea = $paginaCursoAreas[0];

      $items = $paginaCursoArea->get_pca_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function configurePaginaCursoAreaCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function getPropertiesPaginaCursoAreaCtrl(){
    ?><?php

      System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

      $paginaCursoArea = new PaginaCursoArea();

      $properties = $paginaCursoArea->get_pca_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function getItemsPaginaCursoAreaCtrl($search){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArea', 'src', true, '{project.rsc}');

    $paginaCursoArea = new PaginaCursoArea();

    if ($search) {
      $object = $this->getObjectPaginaCursoAreaCtrl($search);
      if (!is_null($object)) {
        $paginaCursoArea = $object;
      }
    }
    
    $items = $paginaCursoArea->get_pca_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $paginaCursoArea->get_pca_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function getHistoryPaginaCursoAreaCtrl($items){
    ?><?php
    
    $defaults = array(
      'pca_registro'=>array('id'=>'pca_registro', 'value'=>date('d/m/Y H:i:s')),
      'pca_criador'=>array('id'=>'pca_criador', 'value'=>System::getUser()),
      'pca_alteracao'=>array('id'=>'pca_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pca_responsavel'=>array('id'=>'pca_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function operationPaginaCursoAreaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'PaginaCursoArea', 'src', true);

    $paginaCursoArea = new PaginaCursoArea();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $paginaCursoArea->get_pca_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $paginaCursoArea->set_pca_value($id, $item['value']);
    }
    
    $method = $action . "PaginaCursoAreaCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyPaginaCursoAreaCtrl($paginaCursoArea, $operation);
      if ($verify) {

        $before = $this->beforePaginaCursoAreaCtrl($paginaCursoArea, $operation);
        if ($before) {

          $response = $this->$method($paginaCursoArea);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterPaginaCursoAreaCtrl($paginaCursoArea, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'PaginaCursoAreaCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 13/09/2015 21:30:20
   */
  public  function copyPaginaCursoAreaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPaginaCursoAreaCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pca_value($reference, null);
    }

    return $this->addPaginaCursoAreaCtrl($object, $validate, $debug);
  }


}