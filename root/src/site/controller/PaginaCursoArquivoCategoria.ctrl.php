<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:55
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:52:01
 * @category controller
 * @package site
 */


class PaginaCursoArquivoCategoriaCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:56
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:56
   */
  public  function PaginaCursoArquivoCategoriaCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');
    
    $entity = new PaginaCursoArquivoCategoria();
    $properties = $entity->get_pac_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:56
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:56
   */
  public  function getRowsPaginaCursoArquivoCategoriaCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $items = $paginaCursoArquivoCategoria->get_pac_items();
    $properties = $paginaCursoArquivoCategoria->get_pac_properties();

    $statements = $paginaCursoArquivoCategoria->getStatementsPaginaCursoArquivoCategoria();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   */
  public  function getPaginaCursoArquivoCategoriaCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $paginaCursoArquivoCategorias = null;

    $rows = $this->getRowsPaginaCursoArquivoCategoriaCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginaCursoArquivoCategorias = array();
      foreach ($rows as $row) {
        $paginaCursoArquivoCategoria_set = clone $paginaCursoArquivoCategoria;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $paginaCursoArquivoCategoria_set->set_pac_value($key, $item['value'], $reference);
        }
        $paginaCursoArquivoCategoria_set->clearPaginaCursoArquivoCategoria($not_clear);
        $paginaCursoArquivoCategorias[] = $paginaCursoArquivoCategoria_set;
      }
    }

    return $paginaCursoArquivoCategorias;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   */
  public  function getColumnPaginaCursoArquivoCategoriaCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $properties = $paginaCursoArquivoCategoria->get_pac_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaCursoArquivoCategoria->getStatementsPaginaCursoArquivoCategoria();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   */
  public  function getCountPaginaCursoArquivoCategoriaCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $properties = $paginaCursoArquivoCategoria->get_pac_properties();

    $statements = $paginaCursoArquivoCategoria->getStatementsPaginaCursoArquivoCategoria();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaCursoArquivoCategoriaCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   */
  private  function beforePaginaCursoArquivoCategoriaCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsPaginaCursoArquivoCategoriaCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_pac_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_pac_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_pac_value($item['id'], $value);
            } else {
              $object->set_pac_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsPaginaCursoArquivoCategoriaCtrl("");
      $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_pac_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnPaginaCursoArquivoCategoriaCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:57
   */
  public  function addPaginaCursoArquivoCategoriaCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pac_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pac_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $response = $this->setPaginaCursoArquivoCategoriaCtrl($object);
      $add = $response->reference;

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function setPaginaCursoArquivoCategoriaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pac_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pac_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function removePaginaCursoArquivoCategoriaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pac_items();
    $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pac_value($action['field'], $action['value']);
        $object->clearPaginaCursoArquivoCategoria(array($action['field']));

        $remove = $this->setPaginaCursoArquivoCategoriaCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  private  function afterPaginaCursoArquivoCategoriaCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_pac_value($object->get_pac_reference());
    } else if ($param === 'add') {
      $object->set_pac_value($object->get_pac_reference(), $value);
    }

    if ($param !== 'remove') {
      $items = $object->get_pac_items();

      $fields = Controller::after($value, $items, 'pac');
      if (count($fields)) {
        $executed = $this->executePaginaCursoArquivoCategoriaCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pac_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function executePaginaCursoArquivoCategoriaCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $properties = $paginaCursoArquivoCategoria->get_pac_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $paginaCursoArquivoCategoria->getStatementsPaginaCursoArquivoCategoria();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pac_responsavel = '" . System::getUser(false) . "', pac_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   * @param string $operation 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  private  function verifyPaginaCursoArquivoCategoriaCtrl($object, $operation = null){
		?><?php
	
		$items = $object->get_pac_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function getObjectPaginaCursoArquivoCategoriaCtrl($value){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $object = new PaginaCursoArquivoCategoria();

    $properties = $object->get_pac_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $paginaCursoArquivoCategoria = null;

    $paginaCursoArquivoCategorias = $this->getPaginaCursoArquivoCategoriaCtrl($where, "", "", "0", "1");
    if (is_array($paginaCursoArquivoCategorias)) {
      $paginaCursoArquivoCategoria = $paginaCursoArquivoCategorias[0];
    }

    return $paginaCursoArquivoCategoria;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function getReplacesPaginaCursoArquivoCategoriaCtrl($reference){
		?><?php

		System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

		$paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $replaces = array();

    $where = "pac_codigo = '" . $reference . "'";
    $paginaCursoArquivoCategorias = $this->getPaginaCursoArquivoCategoriaCtrl($where, "", "");
    if (is_array($paginaCursoArquivoCategorias)) {
      $paginaCursoArquivoCategoria = $paginaCursoArquivoCategorias[0];

      $items = $paginaCursoArquivoCategoria->get_pac_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function configurePaginaCursoArquivoCategoriaCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

    $atributos = $screen->utilities->configureRelationshipItems($atributos);

    switch ($operation) {

      case "search":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
        break;

      case "add":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
        break;

      case "view":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
        break;

      case "set":
        foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
        break;
        
      case "list":
        break;

    }

    return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function getPropertiesPaginaCursoArquivoCategoriaCtrl(){
    ?><?php

      System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

      $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

      $properties = $paginaCursoArquivoCategoria->get_pac_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function getItemsPaginaCursoArquivoCategoriaCtrl($search){
    ?><?php

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true, '{project.rsc}');

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    if ($search) {
      $object = $this->getObjectPaginaCursoArquivoCategoriaCtrl($search);
      if (!is_null($object)) {
        $paginaCursoArquivoCategoria = $object;
      }
    }
    
    $items = $paginaCursoArquivoCategoria->get_pac_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $paginaCursoArquivoCategoria->get_pac_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function getHistoryPaginaCursoArquivoCategoriaCtrl($items){
    ?><?php
    
    $defaults = array(
      'pac_registro'=>array('id'=>'pac_registro', 'value'=>date('d/m/Y H:i:s')),
      'pac_criador'=>array('id'=>'pac_criador', 'value'=>System::getUser()),
      'pac_alteracao'=>array('id'=>'pac_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pac_responsavel'=>array('id'=>'pac_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:58
   */
  public  function operationPaginaCursoArquivoCategoriaCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'PaginaCursoArquivoCategoria', 'src', true);

    $paginaCursoArquivoCategoria = new PaginaCursoArquivoCategoria();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $paginaCursoArquivoCategoria->get_pac_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $paginaCursoArquivoCategoria->set_pac_value($id, $item['value']);
    }
    
    $method = $action . "PaginaCursoArquivoCategoriaCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyPaginaCursoArquivoCategoriaCtrl($paginaCursoArquivoCategoria, $operation);
      if ($verify) {

        $before = $this->beforePaginaCursoArquivoCategoriaCtrl($paginaCursoArquivoCategoria, $operation);
        if ($before) {

          $response = $this->$method($paginaCursoArquivoCategoria);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterPaginaCursoArquivoCategoriaCtrl($paginaCursoArquivoCategoria, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'PaginaCursoArquivoCategoriaCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:59
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 20/09/2015 23:51:59
   */
  public  function copyPaginaCursoArquivoCategoriaCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPaginaCursoArquivoCategoriaCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pac_value($reference, null);
    }

    return $this->addPaginaCursoArquivoCategoriaCtrl($object, $validate, $debug);
  }


}