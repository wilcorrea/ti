<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:05
 * @category controller
 * @package site
 */


class PaginaTagCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   */
  public  function PaginaTagCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');
    
    $entity = new PaginaTag();
    $properties = $entity->get_pgt_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   */
  public  function getRowsPaginaTagCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    $items = $paginaTag->get_pgt_items();
    $properties = $paginaTag->get_pgt_properties();

    $statements = $paginaTag->getStatementsPaginaTag();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:03
   */
  public  function getPaginaTagCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    $paginaTags = null;

    $rows = $this->getRowsPaginaTagCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginaTags = array();
      foreach ($rows as $row) {
        $paginaTag_set = clone $paginaTag;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $paginaTag_set->set_pgt_value($key, $item['value'], $reference);
        }
        $paginaTag_set->clearPaginaTag($not_clear);
        $paginaTags[] = $paginaTag_set;
      }
    }

    return $paginaTags;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getColumnPaginaTagCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    $properties = $paginaTag->get_pgt_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaTag->getStatementsPaginaTag();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getCountPaginaTagCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    $properties = $paginaTag->get_pgt_properties();

    $statements = $paginaTag->getStatementsPaginaTag();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaTagCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  private  function beforePaginaTagCtrl($object, $param){
    ?><?php

    $before = true;

    if ($param === 'add' or $param === 'set') {

      $items = $this->getItemsPaginaTagCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_pgt_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_pgt_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_pgt_value($item['id'], $value);
            } else {
              $object->set_pgt_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsPaginaTagCtrl("");
      $properties = $this->getPropertiesPaginaTagCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_pgt_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnPaginaTagCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $before;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function addPaginaTagCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaTagCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pgt_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesPaginaTagCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_pgt_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setPaginaTagCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function setPaginaTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_pgt_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesPaginaTagCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_pgt_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function removePaginaTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pgt_items();
    $properties = $this->getPropertiesPaginaTagCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pgt_value($action['field'], $action['value']);
        $object->clearPaginaTag(array($action['field']));

        $remove = $this->setPaginaTagCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  private  function afterPaginaTagCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_pgt_value($object->get_pgt_reference());
    } else if ($param === 'add') {
      $object->set_pgt_value($object->get_pgt_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_pgt_items();

      $fields = Controller::after($value, $items, 'pgt');
      if (count($fields)) {
        $executed = $this->executePaginaTagCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_pgt_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 09/04/2015 17:47:05
   */
  public  function executePaginaTagCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    $properties = $paginaTag->get_pgt_properties();
    $table = $properties['table'];
    $join = $properties['join'];

    $statements = $paginaTag->getStatementsPaginaTag();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " USING " . $table . $join . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pgt_responsavel = '" . System::getUser(false) . "', pgt_alteracao = NOW()";
      $sql = "UPDATE " . $table . $join . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  private  function verifyPaginaTagCtrl($object){
		?><?php
	
		$items = $object->get_pgt_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getObjectPaginaTagCtrl($value){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $object = new PaginaTag();

    $properties = $object->get_pgt_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $paginaTag = null;

    $paginaTags = $this->getPaginaTagCtrl($where, "", "", "0", "1");
    if (is_array($paginaTags)) {
      $paginaTag = $paginaTags[0];
    }

    return $paginaTag;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getReplacesPaginaTagCtrl($reference){
		?><?php

		System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

		$paginaTag = new PaginaTag();

    $replaces = array();

    $where = "pgt_codigo = '" . $reference . "'";
    $paginaTags = $this->getPaginaTagCtrl($where, "", "");
    if (is_array($paginaTags)) {
      $paginaTag = $paginaTags[0];

      $items = $paginaTag->get_pgt_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function configurePaginaTagCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getPropertiesPaginaTagCtrl(){
    ?><?php

      System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

      $paginaTag = new PaginaTag();

      $properties = $paginaTag->get_pgt_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getItemsPaginaTagCtrl($search){
    ?><?php

    System::import('m', 'site', 'PaginaTag', 'src', true, '{project.rsc}');

    $paginaTag = new PaginaTag();

    if ($search) {
      $object = $this->getObjectPaginaTagCtrl($search);
      if (!is_null($object)) {
        $paginaTag = $object;
      }
    }
    
    $items = $paginaTag->get_pgt_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $paginaTag->get_pgt_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function getHistoryPaginaTagCtrl($items){
    ?><?php
    
    $defaults = array(
      'pgt_registro'=>array('id'=>'pgt_registro', 'value'=>date('d/m/Y H:i:s')),
      'pgt_criador'=>array('id'=>'pgt_criador', 'value'=>System::getUser()),
      'pgt_alteracao'=>array('id'=>'pgt_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pgt_responsavel'=>array('id'=>'pgt_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function operationPaginaTagCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'PaginaTag', 'src', true);

    $paginaTag = new PaginaTag();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $paginaTag->get_pgt_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $paginaTag->set_pgt_value($id, $item['value']);
    }
    
    $method = $action . "PaginaTagCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyPaginaTagCtrl($paginaTag, $operation);
      if ($verify) {

        $before = $this->beforePaginaTagCtrl($paginaTag, $operation);
        if ($before) {

          $response = $this->$method($paginaTag);
          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterPaginaTagCtrl($paginaTag, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'PaginaTagCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:08:04
   */
  public  function copyPaginaTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesPaginaTagCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_pgt_value($reference, null);
    }

    return $this->addPaginaTagCtrl($object, $validate, $debug);
  }


}