<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:52
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:02:47
 * @category controller
 * @package site
 */


class PaginaWidgetCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function PaginaWidgetCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');
    
    $entity = new PaginaWidget();
    $properties = $entity->get_pgw_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function getRowsPaginaWidgetCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    $items = $paginaWidget->get_pgw_items();
    $properties = $paginaWidget->get_pgw_properties();

    $statements = $paginaWidget->getStatementsPaginaWidget();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function getPaginaWidgetCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    $paginaWidgets = null;

    $rows = $this->getRowsPaginaWidgetCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $paginaWidgets = array();
      foreach ($rows as $row) {
        $paginaWidget_set = clone $paginaWidget;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $paginaWidget_set->set_pgw_value($key, $item['value'], $reference);
        }
        $paginaWidget_set->clearPaginaWidget($not_clear);
        $paginaWidgets[] = $paginaWidget_set;
      }
    }

    return $paginaWidgets;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getColumnPaginaWidgetCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    $properties = $paginaWidget->get_pgw_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaWidget->getStatementsPaginaWidget();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getCountPaginaWidgetCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    $properties = $paginaWidget->get_pgw_properties();

    $statements = $paginaWidget->getStatementsPaginaWidget();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnPaginaWidgetCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:40:32
   */
  public  function beforePaginaWidgetCtrl($object, $param){
    ?><?php

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function addPaginaWidgetCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesPaginaWidgetCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_pgw_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyPaginaWidgetCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesPaginaWidgetCtrl();
        $table = $properties['table'];

        $object = $this->beforePaginaWidgetCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_pgw_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterPaginaWidgetCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setPaginaWidgetCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function setPaginaWidgetCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyPaginaWidgetCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_pgw_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesPaginaWidgetCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforePaginaWidgetCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_pgw_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterPaginaWidgetCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function removePaginaWidgetCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_pgw_items();
    $properties = $this->getPropertiesPaginaWidgetCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_pgw_value($action['field'], $action['value']);
        $object->clearPaginaWidget(array($action['field']));

        $remove = $this->setPaginaWidgetCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforePaginaWidgetCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterPaginaWidgetCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:40:37
   */
  public  function afterPaginaWidgetCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_pgw_value($object->get_pgw_reference());
    }

    if ($param === 'add') {
      $object->set_pgw_value($object->get_pgw_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {

      $items = $object->get_pgw_items();

      $try = 0;
      $added = 0;

      foreach ($items as $key => $item) {

        if ($item['type'] === 'select-multi') {

          if (isset($item['select-multi'])) {
            $select = $item['select-multi'];

            $module = $select['module'];
            $entity = $select['entity'];
            $prefix = $select['prefix'];

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $remove = "execute" . $entity . "Ctrl";
            $set_value = 'set_' . $prefix . '_value';
            $add = "add" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $objectCtrl = new $entityCtrl(APP_PATH);

            $objectCtrl->$remove($select['foreign'] . "='" . $value . "'", "");

            $values = $item['value'];
            foreach ($values as $v) {
              if ($v) {
                $o = new $entity();
                $o->$set_value($select['foreign'], $value);
                $o->$set_value($select['source'], $v);
                $o->$set_value($prefix . '_responsavel', System::getUser());
                $o->$set_value($prefix . '_criador', System::getUser());

                $w = $objectCtrl->$add($o);
                $try++;
                if ($w) {
                  $added++;
                }
              }
            }
          }
        } else if ($items[$key]['type'] == 'calculated') {

          if ($items[$key]['type_content']) {
            $object->set_pgw_value($key, $items[$key]['type_content']);
            $object->set_pgw_type($key, 'execute');
            $update = true;
          }
        } else if ($items[$key]['type'] === 'image') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_pgw_value($object->get_pgw_reference());
              $source = APP_PATH . $items[$key]['value'];
              $location = $file['path'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (!is_dir(APP_PATH . $file['path'])) {
                mkdir(APP_PATH . $file['path'], 0755, true);
              }

              if (is_writable(APP_PATH . $file['path'])) {
                if (file_exists($source)) {
                  $copy = copy($source, $destin);
                  if ($copy) {
                    $object->set_pgw_value($key, $location);
                    $update = true;
                  } else {
                    System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                  }
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        } else if ($items[$key]['type'] === 'file') {

          if ($items[$key]['value']) {
            $file = File::infoContent($item['type_content']);

            if ($file['path']) {
              $id = $object->get_pgw_value($object->get_pgw_reference());
              $source = $object->get_pgw_value($key);

              $location = $file['path'] . $file['name'] . $id . "." . File::getExtension($source);
              $destin = APP_PATH . $location;

              if (is_writable(APP_PATH . $file['path'])) {
                $copy = copy(APP_PATH . $source, $destin);
                if ($copy) {
                  $object->set_pgw_value($key, $location);
                  $update = true;
                } else {
                  System::showMessage(MESSAGE_FILE_NOT_COPY . " " . $destin);
                }
              } else {
                System::showMessage(MESSAGE_DIR_NOT_WRITABLE . " " . $destin);
              }
            }
          }
        }
      }

      if ($update) {
        $this->setPaginaWidgetCtrl($object);
      }

      $add = $try == $added;

      if ($copy > 0) {
        // input here the code to create the complete copy of instance
      }
    }

    $properties = $object->get_pgw_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function executePaginaWidgetCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    $properties = $paginaWidget->get_pgw_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $paginaWidget->getStatementsPaginaWidget();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", pgw_responsavel = '" . System::getUser(false) . "', pgw_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function verifyPaginaWidgetCtrl($object){
		?><?php
	
		$items = $object->get_pgw_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getObjectPaginaWidgetCtrl($value){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $object = new PaginaWidget();

    $properties = $object->get_pgw_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $paginaWidget = null;

    $paginaWidgets = $this->getPaginaWidgetCtrl($where, "", "", "0", "1");
    if (is_array($paginaWidgets)) {
      $paginaWidget = $paginaWidgets[0];
    }

    return $paginaWidget;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getReplacesPaginaWidgetCtrl($reference){
		?><?php

		System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

		$paginaWidget = new PaginaWidget();

    $replaces = array();

    $where = "pgw_codigo = '" . $reference . "'";
    $paginaWidgets = $this->getPaginaWidgetCtrl($where, "", "");
    if (is_array($paginaWidgets)) {
      $paginaWidget = $paginaWidgets[0];

      $items = $paginaWidget->get_pgw_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:27:30
   */
  public  function configurePaginaWidgetCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getPropertiesPaginaWidgetCtrl(){
    ?><?php

      System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

      $paginaWidget = new PaginaWidget();

      $properties = $paginaWidget->get_pgw_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getItemsPaginaWidgetCtrl($search){
    ?><?php

    System::import('m', 'site', 'PaginaWidget', 'src', true, '{project.rsc}');

    $paginaWidget = new PaginaWidget();

    if ($search) {
      $object = $this->getObjectPaginaWidgetCtrl($search);
      if (!is_null($object)) {
        $paginaWidget = $object;
      }
    }
    
    $items = $paginaWidget->get_pgw_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $paginaWidget->get_pgw_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function getHistoryPaginaWidgetCtrl($items){
    ?><?php
    
    $defaults = array(
      'pgw_registro'=>array('id'=>'pgw_registro', 'value'=>date('d/m/Y H:i:s')),
      'pgw_criador'=>array('id'=>'pgw_criador', 'value'=>System::getUser()),
      'pgw_alteracao'=>array('id'=>'pgw_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'pgw_responsavel'=>array('id'=>'pgw_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:54
   */
  public  function operationPaginaWidgetCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'PaginaWidget', 'src', true);

    $paginaWidget = new PaginaWidget();

    foreach ($items as $id => $item) {
      $paginaWidget->set_pgw_value($id, $item['value']);
    }
    
    $method = $operation . "PaginaWidgetCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($paginaWidget);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'PaginaWidgetCtrl'");
    }

    return $result;
  }


}