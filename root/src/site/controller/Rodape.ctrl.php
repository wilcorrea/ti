<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:25
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:28
 * @category controller
 * @package site
 */


class RodapeCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function RodapeCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');
    
    $entity = new Rodape();
    $properties = $entity->get_rdp_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function getRowsRodapeCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    $items = $rodape->get_rdp_items();
    $properties = $rodape->get_rdp_properties();

    $statements = $rodape->getStatementsRodape();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function getRodapeCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    $rodapes = null;

    $rows = $this->getRowsRodapeCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $rodapes = array();
      foreach ($rows as $row) {
        $rodape_set = clone $rodape;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $rodape_set->set_rdp_value($key, $item['value'], $reference);
        }
        $rodape_set->clearRodape($not_clear);
        $rodapes[] = $rodape_set;
      }
    }

    return $rodapes;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function getColumnRodapeCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    $properties = $rodape->get_rdp_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $rodape->getStatementsRodape();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function getCountRodapeCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    $properties = $rodape->get_rdp_properties();

    $statements = $rodape->getStatementsRodape();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnRodapeCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 14:07:38
   */
  public  function beforeRodapeCtrl($object, $param){
    ?><?php

    if ($param === 'add' or $param === 'set') {
      
      if ($object->get_rdp_value('rdp_padrao')) {
        $this->executeRodapeCtrl('TRUE', "rdp_padrao = '0'");
      }

      $items = $this->getItemsRodapeCtrl("");

      foreach ($items as $item) {

        if ($item['type_behavior'] == 'parent') {

          if (isset($item['parent'])) {

            $class = $item['parent']['entity'];
            $classCtrl = $class."Ctrl";

            System::import('m', $item['parent']['modulo'], $class, 'src', true);
            System::import('c', $item['parent']['modulo'], $class, 'src', true);

            $obj = new $class();
            $objCtrl = new $classCtrl(APP_PATH);

            $p_prefix =  $item['parent']['prefix'];
            $getItems = "get_" . $p_prefix . "_items";
            $setValue = "set_" . $p_prefix . "_value";
            $getValue = "get_" . $p_prefix . "_value";

            $p_items = $obj->$getItems();
            foreach ($p_items as $p_key => $p_item) {
              $value = $object->get_rdp_value($p_key);
              $obj->$setValue($p_key, $value);
              $object->set_rdp_value($p_key, null);
            }

            if ($param === 'add') {
              $action = "add" . $class . "Ctrl";
            } else {
              $action = "set" . $class . "Ctrl";
            }

            $value = $objCtrl->$action($obj);
            if ($param === 'add') {
              $object->set_rdp_value($item['id'], $value);
            } else {
              $object->set_rdp_value($item['id'], $obj->$getValue($item['parent']['key']));
            }
          }
        }

      }
    }

    if ($param == 'remove') {
      $items = $this->getItemsRodapeCtrl("");
      $properties = $this->getPropertiesRodapeCtrl();
      $reference = $properties['reference'];
      $whereObject = $reference . " = '" . $object->get_rdp_value($reference) . "'";

      foreach ($items as $key => $item) {
        if ($item['type_behavior'] === 'parent') {
          if (isset($item['parent'])) {
            $module = $item['parent']['modulo'];
            $entity = $item['parent']['entity'];
            $reference = $item['parent']['key'];

            $value = $this->getColumnRodapeCtrl($key, $whereObject);

            $w = $reference . " = '" . $value . "'";

            System::import('c', $module, $entity, 'src');
            System::import('m', $module, $entity, 'src');

            $getParent = "get" . $entity . "Ctrl";
            $removeParent = "remove" . $entity . "Ctrl";

            $entityCtrl = $entity . 'Ctrl';
            $parentCtrl = new $entityCtrl(APP_PATH);

            $parents = $parentCtrl->$getParent($w, "", "");
            $parent = $parents[0];

            $parentCtrl->$removeParent($parent);
          }
        }
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function addRodapeCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesRodapeCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_rdp_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $verify = $this->verifyRodapeCtrl($object);
      if (count($verify) == 0) {

        $properties = $this->getPropertiesRodapeCtrl();
        $table = $properties['table'];

        $object = $this->beforeRodapeCtrl($object, "add");

        $sql = $this->dao->buildInsert($object->get_rdp_items(), $table);
        $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

        $this->afterRodapeCtrl($object, "add", $add, $copy);

        if ($debug) {
          print $sql;
        }

      }

    } else {

      $add = $this->setRodapeCtrl($object);

    }

    return $add;
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function setRodapeCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;
    $verify = $this->verifyRodapeCtrl($object);
    if (count($verify) == 0) {

      $items = $object->get_rdp_items();

      $conector = " AND ";
      $arr_where = array();
      foreach ($items as $item) {
        if ($item['pk']) {
          $key = $item['id'];
          $arr_where[] = $key . " = '" . $item['value'] . "'";
        }
      }
      $where = implode($conector, $arr_where);

      $properties = $this->getPropertiesRodapeCtrl();
      $table = $properties['table'] . $properties['join'];

      $object = $this->beforeRodapeCtrl($object, "set");

      $sql = $this->dao->buildUpdate($table, $object->get_rdp_items(), $where);
      $set = $this->dao->executeSQL($sql, $this->history, $validate);

      $this->afterRodapeCtrl($object, "set");

      if ($debug) {
        print $sql;
      }

    }

    return $set;
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:26
   */
  public  function removeRodapeCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_rdp_items();
    $properties = $this->getPropertiesRodapeCtrl();

    $remove = false;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_rdp_value($action['field'], $action['value']);
        $object->clearRodape(array($action['field']));

        $remove = $this->setRodapeCtrl($object, $validate, $debug, "remove");

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $object = $this->beforeRodapeCtrl($object, "remove");

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);

          if ($remove) {
            $this->afterRodapeCtrl($object, "remove");
          }

        }

      }

    }

    return $remove;
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   * @param int $copy 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 27/03/2015 16:30:58
   */
  public  function afterRodapeCtrl($object, $param, $value = 0, $copy = 0){
    ?><?php

    $add = false;
    $update = false;

    if ($param === 'set') {
      $value = $object->get_rdp_value($object->get_rdp_reference());
    }

    if ($param === 'add') {
      $object->set_rdp_value($object->get_rdp_reference(), $value);
    }

    $filename = File::path(APP_PATH, 'files', 'footers', $object->get_rdp_value('rdp_codigo') . '.foo.php');
    //print "{" . $filename . "}";

    if ($param === 'set' or $param === 'add') {

      File::saveFile($filename, $object->get_rdp_value('rdp_conteudo'), 'w', true);

    } else if ($param === 'remove') {

      File::removeFile($filename);

    }

    $properties = $object->get_rdp_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $add;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function executeRodapeCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    $properties = $rodape->get_rdp_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $rodape->getStatementsRodape();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", rdp_responsavel = '" . System::getUser(false) . "', rdp_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function verifyRodapeCtrl($object){
		?><?php
	
		$items = $object->get_rdp_items();
		$error_messages = array();
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an item in the array $error_messages
				 * array("id"=>$item['id'],"description"=>$item['description'],"type"=>$item['type'],"value"=>$item['value'],"message"=>"{MESSAGE}");
				 */
			}
		}
	
		return $error_messages;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function getObjectRodapeCtrl($value){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $object = new Rodape();

    $properties = $object->get_rdp_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $rodape = null;

    $rodapes = $this->getRodapeCtrl($where, "", "", "0", "1");
    if (is_array($rodapes)) {
      $rodape = $rodapes[0];
    }

    return $rodape;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function getReplacesRodapeCtrl($reference){
		?><?php

		System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

		$rodape = new Rodape();

    $replaces = array();

    $where = "rdp_codigo = '" . $reference . "'";
    $rodapes = $this->getRodapeCtrl($where, "", "");
    if (is_array($rodapes)) {
      $rodape = $rodapes[0];

      $items = $rodape->get_rdp_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function configureRodapeCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function getPropertiesRodapeCtrl(){
    ?><?php

      System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

      $rodape = new Rodape();

      $properties = $rodape->get_rdp_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function getItemsRodapeCtrl($search){
    ?><?php

    System::import('m', 'site', 'Rodape', 'src', true, '{project.rsc}');

    $rodape = new Rodape();

    if ($search) {
      $object = $this->getObjectRodapeCtrl($search);
      if (!is_null($object)) {
        $rodape = $object;
      }
    }
    
    $items = $rodape->get_rdp_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $rodape->get_rdp_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function getHistoryRodapeCtrl($items){
    ?><?php
    
    $defaults = array(
      'rdp_registro'=>array('id'=>'rdp_registro', 'value'=>date('d/m/Y H:i:s')),
      'rdp_criador'=>array('id'=>'rdp_criador', 'value'=>System::getUser()),
      'rdp_alteracao'=>array('id'=>'rdp_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'rdp_responsavel'=>array('id'=>'rdp_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/02/2015 13:49:27
   */
  public  function operationRodapeCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'Rodape', 'src', true);

    $rodape = new Rodape();

    foreach ($items as $id => $item) {
      $rodape->set_rdp_value($id, $item['value']);
    }
    
    $method = $operation . "RodapeCtrl";

    if (method_exists($this, $method)) {
      $result = $this->$method($rodape);
    } else {
      System::showMessage("Método '" . $method . "' não foi encontrado na classe 'RodapeCtrl'");
    }

    return $result;
  }


}