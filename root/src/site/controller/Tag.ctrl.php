<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:30
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/04/2015 00:01:01
 * @category controller
 * @package site
 */


class TagCtrl
{
  private  $path;
  private  $database;
  private  $dao = null;
  private  $plataform = null;
  private  $history;

  /**
   * Construtor da classe de processamento e interface de acesso a dados da entidade
   * 
   * @param string $path 
   * @param string $database 
   * @param string $plataform 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:30
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:30
   */
  public  function TagCtrl($path = null, $database = null, $plataform = null){
    ?><?php

    System::import('class','base', 'DAO', 'core');

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');
    
    $entity = new Tag();
    $properties = $entity->get_tag_properties();

    $this->path = $path;
    $this->database = $properties['database'] ? $properties['database'] : $database;
    $this->plataform = (isset($properties['plataform']) && $properties['plataform']) ? $properties['plataform'] : $plataform;

    $this->history = true;

    $this->dao = DAO::getDAO($this->path, $this->database, $this->plataform);
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:30
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:30
   */
  public  function getRowsTagCtrl($where, $group, $order, $start = "", $end = "", $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    $items = $tag->get_tag_items();
    $properties = $tag->get_tag_properties();

    $statements = $tag->getStatementsTag();

    $table = $properties['table'] . $properties['join'];

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;
    $o = $properties['order'] ? ($order ? $properties['order'] . "," . $order : $properties['order']) : $order;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);
    $order = Statement::parse($o, $statements);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, $order, $start, $end, $fields);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    return $rows;
  }

  /**
   * Recupera instâncias da entidade na base de dados
   * 
   * @param string $where 
   * @param string $group 
   * @param string $order 
   * @param string $start 
   * @param string $end 
   * @param array $fields 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getTagCtrl($where, $group, $order, $start = null, $end = null, $fields = null, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    $tags = null;

    $rows = $this->getRowsTagCtrl($where, $group, $order, $start, $end, $fields, $validate, $debug);
    if ($rows != null) {
      $tags = array();
      foreach ($rows as $row) {
        $tag_set = clone $tag;
        $not_clear = array();

        foreach ($row as $item) {
          $key = $item['id'];
          $not_clear[] = $key;
          $reference = null;
          if (isset($item['reference'])) {
            $reference = $item['reference'];
          }

          $tag_set->set_tag_value($key, $item['value'], $reference);
        }
        $tag_set->clearTag($not_clear);
        $tags[] = $tag_set;
      }
    }

    return $tags;
  }

  /**
   * Recupera um dado através de uma consulta personalizada
   * 
   * @param string $column 
   * @param string $where 
   * @param string $group 
   * @param string $table 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getColumnTagCtrl($column, $where, $group = "", $table = "", $validate = true, $debug = false){
   ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    $properties = $tag->get_tag_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $tag->getStatementsTag();

    $w = $properties['where'] ? ($where ? "(" . $properties['where'] . ") AND (" . $where . ")" : $properties['where']) : $where;
    $g = $properties['group'] ? ($group ? $properties['group'] . "," . $group : $properties['group']) : $group;

    $where = Statement::parse($w, $statements);
    $group = Statement::parse($g, $statements);

    $items['custom'] = array('pk' => 0, 'fk' => 0, 'id' => "custom", 'type' => "calculated", 'type_content' => $column, 'value' => null, 'select' => 1);

    $sql = $this->dao->buildSelect($items, $table, $where, $group, "", "0", "1", null);
    $result = $this->dao->selectSQL($sql, $validate);
    $rows = $this->dao->populateItems($items, $result);

    if ($debug) {
      print $sql;
    }

    $custom = null;
    if ($rows != null) {
      $row = $rows[0];

      $custom = $row['custom']['value'];
    }

    return $custom;
  }

  /**
   * Recupera um valor inteiro referente ao número de linhas de determina consulta
   * 
   * @param string $where 
   * @param string $group 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getCountTagCtrl($where, $group = "", $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    $properties = $tag->get_tag_properties();

    $statements = $tag->getStatementsTag();

    $where = Statement::parse($where, $statements);
    $group = Statement::parse($group, $statements);

    $total = $this->getColumnTagCtrl("COUNT(" . $properties['reference'] . ")", $where, $group, "", $validate, $debug);

    return $total;
  }

  /**
   * Método de gatilho executado antes de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 15/11/2015 15:53:15
   */
  private  function beforeTagCtrl($object, $param){
    ?><?php

    if ($param === 'add' || $param === 'set') {

      $_tag_descricao = trim($object->get_tag_value('tag_descricao'));

      $tags = $this->getTagCtrl("tag_descricao = '" . addslashes($_tag_descricao) . "'", "", "", 0, 1);

      if (is_array($tags)) {

        $object = $tags[0];

        $tag_descricao = str_replace('\' ', '\'', ucwords(str_replace('\'', '\' ', strtolower($_tag_descricao))));

        var_dump($tag_descricao);

        $object->set_tag_value('tag_descricao', $tag_descricao);
      }
    }

    return $object;
  }

  /**
   * Adiciona um novo registro desta entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   * @param boolean $presave 
   * @param int $copy 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function addTagCtrl($object, $validate = true, $debug = false, $presave = false, $copy = 0){
    ?><?php

    $add = 0;

    $properties = $this->getPropertiesTagCtrl();
    $references = explode(",", $properties['reference']);

    $setable = false;
    
    foreach ($references as $reference) {
      if ($object->get_tag_value($reference)) {
        $setable = true;
      }
    }

    if (!$setable) {

      $properties = $this->getPropertiesTagCtrl();
      $table = $properties['table'];

      $sql = $this->dao->buildInsert($object->get_tag_items(), $table);
      $add = $this->dao->insertSQL($sql, $this->history, $validate, $presave);

      if ($debug) {
        print $sql;
      }

    } else {

      $add = $this->setTagCtrl($object);

    }

    return new Response(Boolean::parse($add > 0), $add);
  }

  /**
   * Atualiza uma instância da entidade na base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function setTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $set = 0;

    $items = $object->get_tag_items();

    $conector = " AND ";
    $arr_where = array();
    $references = array();
    foreach ($items as $item) {
      if ($item['pk']) {
        $key = $item['id'];
        $arr_where[] = $key . " = '" . $item['value'] . "'";
        $references[] = $item['value'];
      }
    }
    $where = implode($conector, $arr_where);

    $properties = $this->getPropertiesTagCtrl();
    $table = $properties['table'] . $properties['join'];

    $sql = $this->dao->buildUpdate($table, $object->get_tag_items(), $where);
    $set = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return new Response(Boolean::parse($set > 0), implode(',', $references));
  }

  /**
   * Remove uma instancia da entidade da base de dados
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function removeTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $items = $object->get_tag_items();
    $properties = $this->getPropertiesTagCtrl();

    $remove = 0;

    $operation = isset($properties['operations']['remove']) ? $properties['operations']['remove'] : array();

    if (isset($operation->settings->remove)) {

      $settings = $operation->settings->remove;

      if (is_array($settings)) {

        $action = $operation['remove'];

        $object->set_tag_value($action['field'], $action['value']);
        $object->clearTag(array($action['field']));

        $remove = $this->setTagCtrl($object, $validate, $debug);

      } else if ($settings) {

        $conector = " AND ";
        $arr_where = array();
        foreach ($items as $item) {
          if ($item['pk']) {
            $key = $item['id'];
            $arr_where[] = $key . " = '" . $item['value'] . "'";
          }
        }
        $where = implode($conector, $arr_where);

        if ($where) {

          $table = $properties['table'] . " USING " . $properties['table'] . $properties['join'];

          $sql = $this->dao->buildDelete($table, $where);

          $remove = $this->dao->executeSQL($sql, $this->history, $validate);
        }

      }

    }

    return new Response(Boolean::parse($remove > 0), $remove);
  }

  /**
   * Método de gatilho executado depois de cada atualização e remoção das instâncias da entidade
   * 
   * @param object $object 
   * @param string $param 
   * @param int $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  private  function afterTagCtrl($object, $param, $value = 0){
    ?><?php

    System::import('class', 'pattern', 'Controller', 'core');

    $after = true;

    if ($param === 'set') {
      $value = $object->get_tag_value($object->get_tag_reference());
    } else if ($param === 'add') {
      $object->set_tag_value($object->get_tag_reference(), $value);
    }

    if ($param === 'set' or $param === 'add') {
      $items = $object->get_tag_items();

      $fields = Controller::after($value, $items, 'tag');
      if (count($fields)) {
        $executed = $this->executeTagCtrl("0:" . $value, Controller::makeUpdate($fields));
        $after = Boolean::parse($executed);
      }
    }

    $properties = $object->get_tag_properties();
    if (isset($properties['notification'])) {
      if ($properties['notification']) {
        System::notification($param, $properties, $object);
      }
    }

    return $after;
  }

  /**
   * Remove um grupo de items ou atualiza uma quantidade relativamente grande 
   * 
   * @param string $where 
   * @param string $update 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  private  function executeTagCtrl($where, $update, $validate = true, $debug = false){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    $properties = $tag->get_tag_properties();
    $table = $properties['table'] . $properties['join'];

    $statements = $tag->getStatementsTag();

    $where = Statement::parse($where, $statements);
    $update = Statement::parse($update, $statements);

    $sql = "DELETE FROM " . $table . " WHERE " . $where;
    if ($update) {
      $update = $update . ", tag_responsavel = '" . System::getUser(false) . "', tag_alteracao = NOW()";
      $sql = "UPDATE " . $table . " SET " . $update . " WHERE " . $where;
    }

    $executed = $this->dao->executeSQL($sql, $this->history, $validate);

    if ($debug) {
      print $sql;
    }

    return $executed;
  }

  /**
   * Método responsável por realizar a validação dos dados recebidos pelo post
   * 
   * @param object $object 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  private  function verifyTagCtrl($object){
		?><?php
	
		$items = $object->get_tag_items();
		$verifyed = true;
	
		foreach ($items as $key => $item) {
			if (!is_null($item['value'])) {
				/*
				 * Validation comes here!
				 * If the validation fails, add an message using the Console class and set verifyed = false
				 * Console::add("{MESSAGE}", null, $item['id'], $item['description']);
				 */
			}
		}
	
		return $verifyed;
  }

  /**
   * Recupera uma única instancia do objeto
   * 
   * @param string $value 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getObjectTagCtrl($value){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $object = new Tag();

    $properties = $object->get_tag_properties();

    $reference = $properties['reference'];

    $references = explode(",", $reference);
    $values = explode(",", $value);

    $w = array();
    foreach ($references as $index => $key) {
      $w[] = $key . " = '" . $values[$index] . "'";
    }

    $where = "FALSE";
    if (count($w) > 0) {
      $where = implode(" AND ", $w);
    }

    $tag = null;

    $tags = $this->getTagCtrl($where, "", "", "0", "1");
    if (is_array($tags)) {
      $tag = $tags[0];
    }

    return $tag;
  }

  /**
   * Recupera um array com os dados de uma instância da entidade
   * 
   * @param int $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getReplacesTagCtrl($reference){
		?><?php

		System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

		$tag = new Tag();

    $replaces = array();

    $where = "tag_codigo = '" . $reference . "'";
    $tags = $this->getTagCtrl($where, "", "");
    if (is_array($tags)) {
      $tag = $tags[0];

      $items = $tag->get_tag_items();

      foreach ($items as $item) {
        $replaces[] = array("key"=>$item['id'], "value"=>$item['value'], "type"=>$item['type']);
      }
    }

		return $replaces;
  }

  /**
   * Modifica os atributos da entidade de acordo com a ação solicitada
   * 
   * @param string $operation 
   * @param array $atributos 
   * @param array $properties 
   * @param string $target 
   * @param string $level 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function configureTagCtrl($operation, $atributos, $properties, $target, $level){
    ?><?php

    require_once System::import('class', 'resource', 'Screen', 'core', false);

    $screen = new Screen('{project.rsc}');

	  switch ($operation) {

	    case "search":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureSearchManager($atributo);
        }
	      break;

	    case "add":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureAddManager($atributo);
        }
	      break;

	    case "view":
	      foreach ($atributos as $key => $atributo) {
          $atributos[$key] = $screen->utilities->configureViewManager($atributo);
        }
	      break;

	    case "set":
	      foreach ($atributos as $key => $atributo) {
          if ($atributo['type'] === 'select-multi') {
            $id = $atributos[$key]['select-multi']['key'];
            $atributos[$key]['select-multi']['filter'] = $atributos[$id]['value'];
          }
          $atributos[$key] = $screen->utilities->configureEditManager($atributo);
        }
	      break;
	      
      case "list":
        break;

	  }

	  return $atributos;
  }

  /**
   * Recupera as propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getPropertiesTagCtrl(){
    ?><?php

      System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

      $tag = new Tag();

      $properties = $tag->get_tag_properties();
      
      return $properties;
  }

  /**
   * Recupera os items da entidade devidamente configurados
   * 
   * @param string $search 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getItemsTagCtrl($search){
    ?><?php

    System::import('m', 'site', 'Tag', 'src', true, '{project.rsc}');

    $tag = new Tag();

    if ($search) {
      $object = $this->getObjectTagCtrl($search);
      if (!is_null($object)) {
        $tag = $object;
      }
    }
    
    $items = $tag->get_tag_items();
    foreach ($items as $key => $item) {
      $items[$key]['value'] = $tag->get_tag_value($key);
    }
    
    return $items;
  }

  /**
   * Recupera o registro da última modificação feita um registro da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function getHistoryTagCtrl($items){
    ?><?php
    
    $defaults = array(
      'tag_registro'=>array('id'=>'tag_registro', 'value'=>date('d/m/Y H:i:s')),
      'tag_criador'=>array('id'=>'tag_criador', 'value'=>System::getUser()),
      'tag_alteracao'=>array('id'=>'tag_alteracao', 'value'=>date('d/m/Y H:i:s')),
      'tag_responsavel'=>array('id'=>'tag_responsavel', 'value'=>System::getUser())
    );

    $history = array();
    foreach ($defaults as $default) {
      $id = $default['id'];
      $value = $default['value'];
      if ($items[$id]['value']) {
        $value = $items[$id]['value'];
      }
      $history[$id] = $value;
    }

    return $history;
  }

  /**
   * Articula o processamento das operações pelo controller
   * 
   * @param string $operation 
   * @param array $items 
   * @param array $resources 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 15/11/2015 15:49:43
   */
  public  function operationTagCtrl($operation, $items, $resources = null){
    ?><?php
   
    $result = new Object();

    System::import('m', 'site', 'Tag', 'src', true);

    $tag = new Tag();

    $reference = null;
    $after = false;

    Connection::startTransaction();

    $properties = $tag->get_tag_properties();
    $operations = $properties['operations'];
    $o = $operations[$operation];
    $action = $o->action;

    foreach ($items as $id => $item) {
      $tag->set_tag_value($id, $item['value']);
    }
    
    $method = $action . "TagCtrl";

    if (method_exists($this, $method)) {

      $verify = $this->verifyTagCtrl($tag, $operation);
      if ($verify) {

        $tag = $this->beforeTagCtrl($tag, $operation);
        if ($tag) {

          $response = $this->$method($tag);

          if (is_a($response, 'Response')) {

            if ($response->result) {

              $reference = $response->reference;
              $after = $this->afterTagCtrl($tag, $operation, $response->reference);
            }
          } else {
            Console::add("A resposta do método [" . $method . "] não é a esperada");
          }
        }
      }
    } else {
      Console::add("Método '" . $method . "' não foi encontrado na classe 'TagCtrl'");
    }

    if ($after) {
      Connection::commitTransaction();
    } else {
      Connection::rollbackTransaction();
    }

    return $reference;
  }

  /**
   * Cria uma cópia do registro sem suas dependências e relacionamentos
   * 
   * @param object $object 
   * @param boolean $validate 
   * @param boolean $debug 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 06/04/2015 21:04:31
   */
  public  function copyTagCtrl($object, $validate = true, $debug = false){
    ?><?php

    $properties = $this->getPropertiesTagCtrl();
    $references = explode(",", $properties['reference']);

    foreach ($references as $reference) {
      $object->set_tag_value($reference, null);
    }

    return $this->addTagCtrl($object, $validate, $debug);
  }


}