<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:10
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:34:03
 * @category model
 * @package site
 */


class FaleConosco
{
  private  $flc_items = array();
  private  $flc_properties = array();
  private  $flc_parents = array();
  private  $flc_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 19:45:08
   */
  public  function FaleConosco(){
    ?><?php

    $this->flc_items = array();
    
    // Atributos
    $this->flc_items["flc_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"flc_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->flc_items["flc_nome"] = array("pk"=>0, "fk"=>0, "id"=>"flc_nome", "description"=>"Nome", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->flc_items["flc_email"] = array("pk"=>0, "fk"=>0, "id"=>"flc_email", "description"=>"E-mail", "title"=>"", "type"=>"email", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->flc_items["flc_assunto"] = array("pk"=>0, "fk"=>0, "id"=>"flc_assunto", "description"=>"Assunto", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->flc_items["flc_mensagem"] = array("pk"=>0, "fk"=>0, "id"=>"flc_mensagem", "description"=>"Mensagem", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );


    // Atributos FK
    $this->flc_items["flc_cod_CIDADE"] = array("pk"=>0, "fk"=>1, "id"=>"flc_cod_CIDADE", "description"=>"Cidade", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, "foreign"=>array("modulo"=>"principal", "entity"=>"Cidade", "table"=>"TBL_CIDADE", "prefix"=>"cid", "tag"=>"cidade", "key"=>"cid_codigo", "description"=>"cid_nome", "form"=>"form", "target"=>"div-flc_cod_CIDADE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->flc_items['flc_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'flc_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->flc_items['flc_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'flc_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->flc_items['flc_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'flc_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->flc_items['flc_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'flc_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->flc_items = $this->configureItemsFaleConosco($this->flc_items);

    $join = $this->configureJoinFaleConosco($this->flc_items);

    $lines = 0;
    foreach ($this->flc_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->flc_properties = array(
      'rotule'=>'Fale Conosco',
      'module'=>'site',
      'entity'=>'FaleConosco',
      'table'=>'TBL_FALE_CONOSCO',
      'join'=>$join,
      'tag'=>'faleconosco',
      'prefix'=>'flc',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'flc_codigo',
      'description'=>'flc_nome',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Sua solicitação foi criada com sucesso! Em breve um de nossos atendentes entrará em contato. Obrigado pela confiança.", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"), "execute"=>"Application.FaleConosco.after(data);")),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->flc_properties['reference']) {
      foreach ($this->flc_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->flc_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->flc_properties['description']) {
      foreach ($this->flc_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->flc_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsFaleConosco();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:14
   */
  public  function get_flc_properties(){
    ?><?php
    return $this->flc_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:14
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:15
   */
  public  function get_flc_items(){
    ?><?php
    return $this->flc_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:15
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:15
   */
  public  function get_flc_item($key){
    ?><?php

		$this->validateItemFaleConosco($key);

    return $this->flc_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:16
   */
  public  function get_flc_reference(){
    ?><?php
    $key = $this->flc_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:16
   */
  public  function get_flc_value($key){
    ?><?php

		$this->validateItemFaleConosco($key);

    return $this->flc_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:16
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:17
   */
  public  function set_flc_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemFaleConosco($key);

    $this->flc_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->flc_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:17
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:18
   */
  public  function set_flc_type($key, $type){
    ?><?php

		$this->validateItemFaleConosco($key);

    $this->flc_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:18
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:18
   */
  private  function configureJoinFaleConosco($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:19
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:19
   */
  private  function configureItemsFaleConosco($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->flc_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->flc_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:20
   */
  public  function clearFaleConosco($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_flc_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_flc_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_flc_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:20
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:21
   */
  private  function validateItemFaleConosco($key){
    ?><?php

    if (!isset($this->flc_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:22
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:22
   */
  public  function setStatementsFaleConosco(){
    ?><?php

    $this->flc_statements["0"] = "flc_codigo = '?'";

    $this->flc_statements["flc_0"] = $this->flc_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:22
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 05/10/2015 09:33:22
   */
  public  function getStatementsFaleConosco(){
    ?><?php

    return $this->flc_statements;
  }


}