<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 19:40:05
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 16/03/2015 21:38:41
 * @category model
 * @package site
 */


class Pagina
{
  private  $pgn_items = array();
  private  $pgn_properties = array();
  private  $pgn_parents = array();
  private  $pgn_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:06:34
   */
  public  function Pagina(){
    ?><?php

    $this->pgn_items = array();
    
    // Atributos
    $this->pgn_items["pgn_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"pgn_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->pgn_items["pgn_level"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_level", "description"=>"Nível", "title"=>"", "type"=>"min/max", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->pgn_items["pgn_title"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_title", "description"=>"Chamada", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>2, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->pgn_items["pgn_breadcrumb_title"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_breadcrumb_title", "description"=>"Títulos", "title"=>"", "type"=>"string", "type_content"=>"PGN_CREATE_BREADCRUMB(TBL_PAGINA.pgn_codigo, 'T')", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>1, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->pgn_items["pgn_slug"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_slug", "description"=>"Identificador", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>3, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>5, );
    $this->pgn_items["pgn_breadcrumb_slug"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_breadcrumb_slug", "description"=>"Identificadores", "title"=>"", "type"=>"string", "type_content"=>"PGN_CREATE_BREADCRUMB(TBL_PAGINA.pgn_codigo, 'S')", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>5, );
    $this->pgn_items["pgn_home"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_home", "description"=>"Principal", "title"=>"Define se a página será a  pagina principal do site", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>6, );
    $this->pgn_items["pgn_type"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_type", "description"=>"Comportamento", "title"=>"Texto - Exibe o conteúdo informado na página
Redirecionamento - Quando uma página será apenas um redirecionamento para outro conteúdo
Galeria de Imagens - Quando uma página será uma galeria de imagens
Galeria de Arquivos - Transforma a página em uma central de downloads
Portifólio - Uma página com imagens e informações sobre elas
Widgets - Permite apenas a adição de Widgets na página criando um layout otimizado para isso
Aplicação - Com esse comportamento o editor se transforma em um editor de código-fonte e é possível usar comandos comuns no desenvolvimento do site", "type"=>"option", "type_content"=>"0,Texto|1,Redirecionamento|2,Galeria de Imagens|3,Galeria de Arquivos|4,Portifólio|5,Widgets|6,Nota Simples|7,Aplicação", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>10, );
    $this->pgn_items["pgn_side_bar"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_side_bar", "description"=>"Barra Lateral", "title"=>"", "type"=>"option", "type_content"=>"left,Esquerda|right,Direita|hide,Ocultar|full,Tela Cheia", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"left", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>11, );

    $this->pgn_items["pgn_widget"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_widget", "description"=>"Widget", "title"=>"Esta opção permite configurar a utilização dos Widgets
Nenhum: desabilita os widgets, usado com a ocultação da Barra Lateral, já que a mesma perderá sua utilidade
Próprio: Permite que sejam informados Widgets para a Página
Página Superior: utiliza os Widgets da Página que está um nível acima
Padrão: usa o padrão do tema e aplica na visualização da Página", "type"=>"option", "type_content"=>"0,Não|1,Próprio|2,Superior|3,Padrão", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>3, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>13, );
    $this->pgn_items["pgn_menu"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_menu", "description"=>"Exibir no Menu", "title"=>"Define como a página será exibida nos menus do tema
Nenhum: nunca é exibida
Superior: nos menus superiores do tema
Inferior: nos menus inferiores
Todos: poderá ser encontrada em todos os menus
Página Superior: será exibida como Sub Menu da Página Superior a ela", "type"=>"option", "type_content"=>"0,Nenhum|1,Superior|2,Inferior|3,Todos|4,Página Superior", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>14, );
    $this->pgn_items["pgn_menu_superior"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_menu_superior", "description"=>"Tipo de Menu", "title"=>"Define de que forma o menu superior irá se comportar.
Padrão: desta forma as páginas que serão exibidas no menu são as que estão definidas para serem exibidas no menu superior
Páginas Vinculadas: exibe no menu superior as páginas filhas desta página", "type"=>"option", "type_content"=>"0,Padrão|1,Páginas Vinculadas", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>15, );
    $this->pgn_items["pgn_publish_date"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_publish_date", "description"=>"Publicação", "title"=>"", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>16, );
    $this->pgn_items["pgn_deadline_date"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_deadline_date", "description"=>"Término", "title"=>"", "type"=>"datetime", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>17, );
    $this->pgn_items["pgn_publish"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_publish", "description"=>"Visibilidade", "title"=>"Determina a visibilidade da página. Uma página Pública pode ser acessada por qualquer pessoa, já a Privada apenas usuários conectados tem acesso", "type"=>"option", "type_content"=>"1,Público|2,Privado|0,Não", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"1", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>18, );
    
    $this->pgn_items["pgn_call"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_call", "description"=>"Título", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>2, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>19, );

    $this->pgn_items["pgn_excerpt"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_excerpt", "description"=>"Resumo", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>20, );

    $this->pgn_items["pgn_content"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_content", "description"=>"Conteúdo", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>21, );

    $this->pgn_items["pgn_source"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_source", "description"=>"Código Fonte", "title"=>"Armazena comandos de código-fonte para serem usados na página", "type"=>"source-code", "type_content"=>'"save":"Application.util.target().find(\'.btn.primary[data-action=\"alias\"]\')[0].click()"', "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>22, );
    
    $this->pgn_items["pgn_width"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_width", "description"=>"Largura", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>23, );
    $this->pgn_items["pgn_lightbox"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_lightbox", "description"=>"LightBox", "title"=>"Define se será utilizado LightBox na visualização das imagens", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>24, );

    $this->pgn_items["pgn_hit"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_hit", "description"=>"Acessos", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>4, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>23, );

    $this->pgn_items["pgn_miniature"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_miniature", "description"=>"Miniatura", "title"=>"", "type"=>"image", "type_content"=>'{"width": 460, "height": 340, "resize": true, "ghost": false, "download": false, "originalsize": false}', "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>24, );
    $this->pgn_items["pgn_miniature_subtitle"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_miniature_subtitle", "description"=>"Legenda", "title"=>"Legenda para a imagem", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>25, );
    $this->pgn_items["pgn_background_image"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_background_image", "description"=>"Fundo", "title"=>"", "type"=>"image", "type_content"=>'{"width": 460, "height": 340, "resize": true, "ghost": false, "download": false, "originalsize": false}', "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>26, );
    $this->pgn_items["pgn_target"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_target", "description"=>"Exibição", "title"=>"Define se a página será aberta na mesma janela ou em uma aba diferente do navegador", "type"=>"option", "type_content"=>"_self,Mesma janela|_blank,Outra aba", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"_self", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>27, );

    $this->pgn_items["pgn_featured"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_featured", "description"=>"Destacar", "title"=>"", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>3, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>18, "order"=>28, );
    $this->pgn_items["pgn_type_content"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_type_content", "description"=>"Tipo de Conteúdo", "title"=>"Tipo de documento que a página processa", "type"=>"option", "type_content"=>"article,Artigo|news,Notícia|post,Postagem|page,Página|course,Curso", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"page", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>18, "order"=>29, );

    $this->pgn_items["pgn_author"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_author", "description"=>"Autor", "title"=>"Nome a ser exibido como sendo o do responsável pelo texto publicado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>19, "order"=>30, );
    $this->pgn_items["pgn_reference"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_reference", "description"=>"Referência", "title"=>"Detalha a origem do conteúdo da página", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>19, "order"=>30, );
    $this->pgn_items["pgn_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_ordem", "description"=>"Ordem", "title"=>"Ordem customizada para exibição das páginas", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>19, "order"=>31, );

    $this->pgn_items["pgn_miniature_body"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_miniature_body", "description"=>"Exibir Miniatura", "title"=>"Exibe ou não a miniatura no corpo da notícia", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>20, "order"=>32, );


    // Atributos FK
    $this->pgn_items["pgn_cod_PAGINA"] = array("pk"=>0, "fk"=>1, "id"=>"pgn_cod_PAGINA", "description"=>"Página Superior", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"Pagina", "table"=>"TBL_PAGINA", "prefix"=>"pgn", "tag"=>"pagina", "key"=>"pgn_codigo", "description"=>"pgn_title", "form"=>"form", "target"=>"div-pgn_cod_PAGINA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgn_items["pgn_cod_CATEGORIA"] = array("pk"=>0, "fk"=>1, "id"=>"pgn_cod_CATEGORIA", "description"=>"Categoria", "title"=>"Categoria a qual a página pertence", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>7, "foreign"=>array("modulo"=>"site", "entity"=>"Categoria", "table"=>"TBL_CATEGORIA", "prefix"=>"ctg", "tag"=>"categoria", "key"=>"ctg_codigo", "description"=>"ctg_descricao", "form"=>"form", "target"=>"div-pgn_cod_CATEGORIA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgn_items["pgn_cod_BANNER"] = array("pk"=>0, "fk"=>1, "id"=>"pgn_cod_BANNER", "description"=>"Banner", "title"=>"Banner da página", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>8, "foreign"=>array("modulo"=>"site", "entity"=>"Banner", "table"=>"TBL_BANNER", "prefix"=>"bnn", "tag"=>"banner", "key"=>"bnn_codigo", "description"=>"bnn_descricao", "form"=>"form", "target"=>"div-pgn_cod_BANNER-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgn_items["pgn_cod_RODAPE"] = array("pk"=>0, "fk"=>1, "id"=>"pgn_cod_RODAPE", "description"=>"Rodapé", "title"=>"Comportamento do rodapé da página", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>9, "foreign"=>array("modulo"=>"site", "entity"=>"Rodape", "table"=>"TBL_RODAPE", "prefix"=>"rdp", "tag"=>"rodape", "key"=>"rdp_codigo", "description"=>"rdp_descricao", "form"=>"form", "target"=>"div-pgn_cod_RODAPE-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD
    $this->pgn_items["pgn_fk_pgw_cod_PAGINA_4798"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_fk_pgw_cod_PAGINA_4798", "description"=>"Widgets", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"site", "entity"=>"PaginaWidget", "tag"=>"paginawidget", "prefix"=>"pgw", "name"=>"Widgets", "key"=>"pgn_codigo", "foreign"=>"pgw_cod_PAGINA", "filter"=>"pgw_cod_PAGINA", "source"=>"pgw_cod_PAGINA", "where"=>"", "group"=>"", "order"=>"pgw_ordem", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));
    $this->pgn_items["pgn_fk_evp_cod_PAGINA_4808"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_fk_evp_cod_PAGINA_4808", "description"=>"Períodos", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"site", "entity"=>"EventoPagina", "tag"=>"eventopagina", "prefix"=>"evp", "name"=>"Períodos", "key"=>"pgn_codigo", "foreign"=>"evp_cod_PAGINA", "filter"=>"evp_cod_PAGINA", "source"=>"evp_cod_PAGINA", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));
    $this->pgn_items["pgn_fk_pgt_cod_PAGINA_4823"] = array("pk"=>0, "fk"=>0, "id"=>"pgn_fk_pgt_cod_PAGINA_4823", "description"=>"Tags", "title"=>"", "type"=>"tag", "type_content"=>"", "type_behavior"=>"tag", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>2, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>16, "order"=>0, "tag"=>array("module"=>"site", "entity"=>"PaginaTag", "tag"=>"paginatag", "prefix"=>"pgt", "name"=>"Tags", "key"=>"pgn_codigo", "foreign"=>"pgt_cod_PAGINA", "filter"=>"pgt_cod_PAGINA", "source"=>"pgt_cod_TAG", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->pgn_items['pgn_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgn_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgn_items['pgn_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgn_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->pgn_items['pgn_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgn_responsavel', 'description'=>'ResponsÃ¡vel', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgn_items['pgn_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgn_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->pgn_items = $this->configureItemsPagina($this->pgn_items);

    $join = $this->configureJoinPagina($this->pgn_items, array());

    $lines = 0;
    foreach ($this->pgn_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->pgn_properties = array(
			'rotule'=>'Página',
      'module'=>'site',
      'entity'=>'Pagina',
      'table'=>'TBL_PAGINA',
			'join'=>$join,
      'tag'=>'pagina',
      'prefix'=>'pgn',
      //'order'=>'TBL_PAGINA.pgn_home DESC, TBL_PAGINA.pgn_breadcrumb_slug, TBL_PAGINA.pgn_ordem',
      'order'=>'TBL_PAGINA.pgn_home DESC, TBL_PAGINA.pgn_publish_date DESC',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'pgn_codigo',
      'description'=>'pgn_title',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			    'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>false, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  ////'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  ////'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->pgn_properties['reference']) {
      foreach ($this->pgn_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->pgn_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->pgn_properties['description']) {
      foreach ($this->pgn_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->pgn_properties['reference'] = $id;
          break;
        }
      }
    }
    
    $this->setStatementsPagina();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function get_pgn_properties(){
    ?><?php
    return $this->pgn_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function get_pgn_items(){
    ?><?php
    return $this->pgn_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function get_pgn_item($key){
    ?><?php

		$this->validateItemPagina($key);

    return $this->pgn_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function get_pgn_reference(){
    ?><?php
    $key = $this->pgn_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function get_pgn_value($key){
    ?><?php

		$this->validateItemPagina($key);

    return $this->pgn_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function set_pgn_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemPagina($key);

    $this->pgn_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->pgn_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function set_pgn_type($key, $type){
    ?><?php

		$this->validateItemPagina($key);

    $this->pgn_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 16/03/2015 13:10:41
   */
  private  function configureJoinPagina($items, $ignore = array()){
    ?><?php

    $joins = array();

    foreach ($items as $item) {

      if ($item['fk']) {

        if (isset($item['foreign']) or isset($item['parent'])) {

          $table = '';
					$key = '';

					if (isset($item['foreign'])) {
						$index = 'foreign';
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$index = 'parent';
					}

					$table = $item[$index]['table'];
					$key = $item[$index]['key'];
					$as = strtoupper($key);

					if (!in_array($table, $ignore, true)) {
            $joins[$table] = ' LEFT JOIN ' . $table . ' AS ' . $as . ' ON (TBL_PAGINA.' . $item['id'] . ' = ' . $as . '.' . $key . ') ';
					}
        }
      }
    }
    $join = ' ' . join(' ', $joins);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas caracterí­sticas
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  private  function configureItemsPagina($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->pgn_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->pgn_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function clearPagina($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_pgn_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_pgn_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  private  function validateItemPagina($key){
    ?><?php

    if (!isset($this->pgn_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 08/04/2015 09:24:23
   */
  public  function setStatementsPagina(){
    ?><?php

    $this->pgn_statements['pgn_0'] = "TBL_PAGINA.pgn_codigo = '?'";
    $this->pgn_statements['pgn_1'] = "TBL_PAGINA.pgn_level = '?'";
    $this->pgn_statements['pgn_2'] = "TBL_PAGINA.pgn_slug = '?'";
    $this->pgn_statements['pgn_3'] = "PGN_CODIGO.pgn_slug = '?'";
    $this->pgn_statements['pgn_4'] = "TBL_PAGINA.pgn_codigo <> '?' AND TBL_PAGINA.pgn_slug = '?'";
    $this->pgn_statements['pgn_5'] = "COALESCE(TBL_PAGINA.pgn_cod_PAGINA, 0) = '?'";
    $this->pgn_statements['pgn_6'] = "TBL_PAGINA.pgn_type_content = '?' AND TBL_PAGINA.pgn_featured = '1' AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_7'] = "TBL_PAGINA.pgn_cod_CATEGORIA = '?' AND TBL_PAGINA.pgn_type_content = '?' AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_8'] = "PGN_CREATE_BREADCRUMB('?', 'S') = pgn_breadcrumb_slug";
    $this->pgn_statements['pgn_9'] = "TBL_PAGINA.pgn_cod_PAGINA = '?' AND TBL_PAGINA.pgn_slug = '?'";
    $this->pgn_statements['pgn_10'] = "TBL_PAGINA.pgn_cod_CATEGORIA = '?' AND TBL_PAGINA.pgn_type_content = 'course' AND TBL_PAGINA.pgn_level = '0'";
    $this->pgn_statements['pgn_11'] = "TBL_PAGINA.pgn_cod_PAGINA = '?'";
    $this->pgn_statements['pgn_12'] = "EXISTS (SELECT pgt_codigo FROM TBL_PAGINA_TAG JOIN TBL_TAG ON (pgt_cod_TAG = tag_codigo) WHERE pgt_cod_PAGINA = TBL_PAGINA.pgn_codigo AND tag_descricao = '?') AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_13'] = "(ctg_cod_CATEGORIA_NATUREZA = '?' OR ? ) AND pgn_ordem IN ('?') AND pgn_type_content = 'news' AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_14'] = "TBL_PAGINA.pgn_type_content = '?' AND DATEDIFF(CURDATE(), DATE(pgn_publish_date)) < 30 AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_15'] = "TBL_PAGINA.pgn_type_content = '?' AND EXISTS (SELECT pgt_codigo FROM TBL_PAGINA_TAG JOIN TBL_TAG ON (pgt_cod_TAG = tag_codigo) WHERE pgt_cod_PAGINA = TBL_PAGINA.pgn_codigo AND tag_descricao = '?') AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";
    $this->pgn_statements['pgn_16'] = "TBL_PAGINA.pgn_cod_CATEGORIA = '?' AND (pgn_menu IN ('2','3') OR EXISTS (SELECT pgt_codigo FROM TBL_PAGINA_TAG JOIN TBL_TAG ON (pgt_cod_TAG = tag_codigo) WHERE pgt_cod_PAGINA = TBL_PAGINA.pgn_codigo AND tag_descricao = '?') AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)) AND PGN_VALIDATE_PUBLISH(TBL_PAGINA.pgn_codigo)";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 02/02/2015 20:27:09
   */
  public  function getStatementsPagina(){
    ?><?php

    return $this->pgn_statements;
  }


}