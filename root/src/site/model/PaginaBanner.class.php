<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:47:59
 * @category model
 * @package site
 */


class PaginaBanner
{
  private  $pgb_items = array();
  private  $pgb_properties = array();
  private  $pgb_parents = array();
  private  $pgb_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function PaginaBanner(){
    ?><?php

    $this->pgb_items = array();
    
    // Atributos
    $this->pgb_items["pgb_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"pgb_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->pgb_items["pgb_titulo"] = array("pk"=>0, "fk"=>0, "id"=>"pgb_titulo", "description"=>"Título", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->pgb_items["pgb_link"] = array("pk"=>0, "fk"=>0, "id"=>"pgb_link", "description"=>"Link", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->pgb_items["pgb_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"pgb_descricao", "description"=>"Descrição", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>5, );
    $this->pgb_items["pgb_imagem"] = array("pk"=>0, "fk"=>0, "id"=>"pgb_imagem", "description"=>"Imagem", "title"=>"", "type"=>"image", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>6, );


    // Atributos FK
    $this->pgb_items["pgb_cod_PAGINA"] = array("pk"=>0, "fk"=>1, "id"=>"pgb_cod_PAGINA", "description"=>"Página", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"Pagina", "table"=>"TBL_PAGINA", "prefix"=>"pgn", "tag"=>"pagina", "key"=>"pgn_codigo", "description"=>"pgn_title", "form"=>"form", "target"=>"div-pgb_cod_PAGINA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->pgb_items['pgb_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgb_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgb_items['pgb_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgb_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->pgb_items['pgb_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgb_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgb_items['pgb_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgb_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->pgb_items = $this->configureItemsPaginaBanner($this->pgb_items);

    $join = $this->configureJoinPaginaBanner($this->pgb_items);

    $lines = 0;
    foreach ($this->pgb_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->pgb_properties = array(
			'rotule'=>'Banners da Página',
      'module'=>'site',
      'entity'=>'PaginaBanner',
      'table'=>'TBL_PAGINA_BANNER',
			'join'=>$join,
      'tag'=>'paginabanner',
      'prefix'=>'pgb',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'pgb_codigo',
      'description'=>'pgb_titulo',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->pgb_properties['reference']) {
      foreach ($this->pgb_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->pgb_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->pgb_properties['description']) {
      foreach ($this->pgb_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->pgb_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function get_pgb_properties(){
    ?><?php
    return $this->pgb_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function get_pgb_items(){
    ?><?php
    return $this->pgb_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function get_pgb_item($key){
    ?><?php

		$this->validateItemPaginaBanner($key);

    return $this->pgb_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function get_pgb_reference(){
    ?><?php
    $key = $this->pgb_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function get_pgb_value($key){
    ?><?php

		$this->validateItemPaginaBanner($key);

    return $this->pgb_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function set_pgb_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemPaginaBanner($key);

    $this->pgb_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->pgb_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  public  function set_pgb_type($key, $type){
    ?><?php

		$this->validateItemPaginaBanner($key);

    $this->pgb_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:24
   */
  private  function configureJoinPaginaBanner($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   */
  private  function configureItemsPaginaBanner($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->pgb_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->pgb_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   */
  public  function clearPaginaBanner($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_pgb_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_pgb_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   */
  private  function validateItemPaginaBanner($key){
    ?><?php

    if (!isset($this->pgb_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   */
  public  function setStatementsPaginaBanner(){
    ?><?php

    $this->pgb_statements[0] = "pgb_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 05/02/2015 10:44:25
   */
  public  function getStatementsPaginaBanner(){
    ?><?php

    return $this->pgb_statements;
  }


}