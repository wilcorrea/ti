<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 14/09/2015 17:17:00
 * @category model
 * @package site
 */


class PaginaCurso
{
  private  $pgc_items = array();
  private  $pgc_properties = array();
  private  $pgc_parents = array();
  private  $pgc_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 15/09/2015 00:24:32
   */
  public  function PaginaCurso(){
    ?><?php

    $this->pgc_items = array();
    
    // Atributos
    $this->pgc_items["pgc_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"pgc_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->pgc_items["pgc_slug"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_slug", "description"=>"Identificador", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->pgc_items["pgc_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_descricao", "description"=>"Nome", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>4, );
    $this->pgc_items["pgc_titulacao"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_titulacao", "description"=>"Titulação", "title"=>"", "type"=>"upper", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>5, );
    $this->pgc_items["pgc_modalidade"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_modalidade", "description"=>"Modalidade", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>6, );
    $this->pgc_items["pgc_regime_letivo"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_regime_letivo", "description"=>"Regime Letivo", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>7, );
    $this->pgc_items["pgc_apresentacao"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_apresentacao", "description"=>"Apresentação", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>8, );
    $this->pgc_items["pgc_duracao"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_duracao", "description"=>"Duração", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>9, );
    $this->pgc_items["pgc_investimento"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_investimento", "description"=>"Investimento", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>10, );
    $this->pgc_items["pgc_carga_horaria"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_carga_horaria", "description"=>"Carga Horária", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>11, );
    $this->pgc_items["pgc_campus"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_campus", "description"=>"Campus", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>12, );
    $this->pgc_items["pgc_video"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_video", "description"=>"Vídeo", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>13, );
    $this->pgc_items["pgc_turno"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_turno", "description"=>"Turno", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>14, );
    $this->pgc_items["pgc_ato_legal"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_ato_legal", "description"=>"Ato Legal", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>8, "order"=>15, );
    $this->pgc_items["pgc_objetivo"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_objetivo", "description"=>"Objetivo Geral", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>9, "order"=>16, );
    $this->pgc_items["pgc_publico_alvo"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_publico_alvo", "description"=>"Público Alvo", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>10, "order"=>17, );
    $this->pgc_items["pgc_horario"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_horario", "description"=>"Horários de Aula", "title"=>"", "type"=>"source-code", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>11, "order"=>18, );
    $this->pgc_items["pgc_corpo_docente"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_corpo_docente", "description"=>"Corpo Docente", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>12, "order"=>19, );
    $this->pgc_items["pgc_matriz"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_matriz", "description"=>"Matriz Curricular", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>13, "order"=>20, );
    $this->pgc_items["pgc_estagio"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_estagio", "description"=>"Estágio Supervisionado", "title"=>"", "type"=>"text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>14, "order"=>21, );
    $this->pgc_items["pgc_miniatura"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_miniatura", "description"=>"Miniatura", "title"=>"Imagem relacionada ao curso", "type"=>"file", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>15, "order"=>22, );
    $this->pgc_items["pgc_fundo"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_fundo", "description"=>"Imagem de Fundo", "title"=>"", "type"=>"file", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>16, "order"=>23, );
    $this->pgc_items["pgc_coordenacao"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_coordenacao", "description"=>"Coordenação", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>24, );
    $this->pgc_items["pgc_diferencial"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_diferencial", "description"=>"Diferencial", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>17, "order"=>24, );
    $this->pgc_items["pgc_breadcrumb_slug"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_breadcrumb_slug", "description"=>"Breadcrumb", "title"=>"", "type"=>"calculated", "type_content"=>"(SELECT pgn_breadcrumb_slug FROM TBL_PAGINA WHERE pgn_codigo = pgc_cod_PAGINA LIMIT 1)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>18, "order"=>25, );
    $this->pgc_items["pgc_menu"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_menu", "description"=>"Tipo de Menu", "title"=>"", "type"=>"calculated", "type_content"=>"(ctg_menu)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>19, "order"=>26, );
    $this->pgc_items["pgc_matricula"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_matricula", "description"=>"Orientações para Matrícula", "title"=>"", "type"=>"rich-text", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>20, "order"=>27, );


    // Atributos FK
    $this->pgc_items["pgc_cod_PAGINA"] = array("pk"=>0, "fk"=>1, "id"=>"pgc_cod_PAGINA", "description"=>"Página", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, "foreign"=>array("modulo"=>"site", "entity"=>"Pagina", "table"=>"TBL_PAGINA", "prefix"=>"pag", "tag"=>"pagina", "key"=>"pag_codigo", "description"=>"pag_nome", "form"=>"form", "target"=>"div-pgc_cod_PAGINA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgc_items["pgc_cod_CATEGORIA"] = array("pk"=>0, "fk"=>1, "id"=>"pgc_cod_CATEGORIA", "description"=>"Categoria", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"Categoria", "table"=>"TBL_CATEGORIA", "prefix"=>"ctg", "tag"=>"categoria", "key"=>"ctg_codigo", "description"=>"ctg_descricao", "form"=>"form", "target"=>"div-pgc_cod_CATEGORIA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgc_items["pgc_cod_PAGINA_CURSO_AREA"] = array("pk"=>0, "fk"=>1, "id"=>"pgc_cod_PAGINA_CURSO_AREA", "description"=>"Área", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"PaginaCursoArea", "table"=>"TBL_PAGINA_CURSO_AREA", "prefix"=>"pca", "tag"=>"paginacursoarea", "key"=>"pca_codigo", "description"=>"pca_descricao", "form"=>"form", "target"=>"div-pgc_cod_PAGINA_CURSO_AREA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD
    $this->pgc_items["pgc_fk_pcf_cod_PAGINA_CURSO_4901"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_fk_pcf_cod_PAGINA_CURSO_4901", "description"=>"Fotos", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"site", "entity"=>"PaginaCursoFoto", "tag"=>"paginacursofoto", "prefix"=>"pcf", "name"=>"Fotos", "key"=>"pgc_codigo", "foreign"=>"pcf_cod_PAGINA_CURSO", "filter"=>"pcf_cod_PAGINA_CURSO", "source"=>"pcf_cod_PAGINA_CURSO", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));
    $this->pgc_items["pgc_fk_prq_cod_PAGINA_CURSO_4902"] = array("pk"=>0, "fk"=>0, "id"=>"pgc_fk_prq_cod_PAGINA_CURSO_4902", "description"=>"Arquivos", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>2, "order"=>0, "child"=>array("module"=>"site", "entity"=>"PaginaCursoArquivo", "tag"=>"paginacursoarquivo", "prefix"=>"prq", "name"=>"Arquivos", "key"=>"pgc_codigo", "foreign"=>"prq_cod_PAGINA_CURSO", "filter"=>"prq_cod_PAGINA_CURSO", "source"=>"prq_cod_PAGINA_CURSO", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->pgc_items['pgc_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgc_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgc_items['pgc_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgc_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->pgc_items['pgc_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgc_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgc_items['pgc_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgc_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    /* DESABILITA A CHAVE COM PAGINA */
    $this->pgc_items["pgc_cod_PAGINA"]['fk'] = 0;
    $this->pgc_items["pgc_cod_PAGINA"]['type'] = 'int';

    $this->pgc_items = $this->configureItemsPaginaCurso($this->pgc_items);

    $join = $this->configureJoinPaginaCurso($this->pgc_items);

    $lines = 0;
    foreach ($this->pgc_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->pgc_properties = array(
      'rotule'=>'Curso',
      'module'=>'site',
      'entity'=>'PaginaCurso',
      'table'=>'TBL_PAGINA_CURSO',
      'join'=>$join,
      'tag'=>'paginacurso',
      'prefix'=>'pgc',
      'order'=>'pgc_descricao',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'pgc_codigo',
      'description'=>'pgc_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'copy'=>(object) (array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "show"=>'child', "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "show"=>'child', "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "show"=>'', "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "show"=>'child', "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

          //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->pgc_properties['reference']) {
      foreach ($this->pgc_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->pgc_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->pgc_properties['description']) {
      foreach ($this->pgc_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->pgc_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsPaginaCurso();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function get_pgc_properties(){
    ?><?php
    return $this->pgc_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function get_pgc_items(){
    ?><?php
    return $this->pgc_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function get_pgc_item($key){
    ?><?php

		$this->validateItemPaginaCurso($key);

    return $this->pgc_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function get_pgc_reference(){
    ?><?php
    $key = $this->pgc_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function get_pgc_value($key){
    ?><?php

		$this->validateItemPaginaCurso($key);

    return $this->pgc_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function set_pgc_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemPaginaCurso($key);

    $this->pgc_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->pgc_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  public  function set_pgc_type($key, $type){
    ?><?php

		$this->validateItemPaginaCurso($key);

    $this->pgc_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:03
   */
  private  function configureJoinPaginaCurso($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  private  function configureItemsPaginaCurso($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->pgc_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->pgc_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function clearPaginaCurso($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_pgc_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_pgc_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_pgc_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  private  function validateItemPaginaCurso($key){
    ?><?php

    if (!isset($this->pgc_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 18/09/2015 19:38:10
   */
  public  function setStatementsPaginaCurso(){
    ?><?php

    $this->pgc_statements["0"] = "pgc_codigo = '?'";

    $this->pgc_statements["pgc_0"] = $this->pgc_statements["0"];
    $this->pgc_statements["pgc_1"] = "pgc_cod_CATEGORIA = '?' AND pgc_cod_PAGINA_CURSO_AREA = '?'";
    $this->pgc_statements["pgc_2"] = "pgc_cod_CATEGORIA = '?'";
    $this->pgc_statements["pgc_3"] = "pgc_cod_PAGINA = '?'";
    $this->pgc_statements["pgc_4"] = "pgc_slug = '?' AND pgc_codigo != '?'";

    return $this;
  
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 03/09/2015 03:34:04
   */
  public  function getStatementsPaginaCurso(){
    ?><?php

    return $this->pgc_statements;
  }


}