<?php
/**
 * @copyright array software
 *
 * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:52
 * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:02:47
 * @category model
 * @package site
 */


class PaginaWidget
{
  private  $pgw_items = array();
  private  $pgw_properties = array();
  private  $pgw_parents = array();
  private  $pgw_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function PaginaWidget(){
    ?><?php

    $this->pgw_items = array();
    
    // Atributos
    $this->pgw_items["pgw_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"pgw_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->pgw_items["pgw_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"pgw_descricao", "description"=>"Descrição", "title"=>"", "type"=>"calculated", "type_content"=>"CONCAT(wid_descricao,' - ', pgw_ordem)", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->pgw_items["pgw_local"] = array("pk"=>0, "fk"=>0, "id"=>"pgw_local", "description"=>"Local", "title"=>"", "type"=>"option", "type_content"=>"sidebar,Barra Lateral|content,Conteúdo", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>5, );
    $this->pgw_items["pgw_tamanho"] = array("pk"=>0, "fk"=>0, "id"=>"pgw_tamanho", "description"=>"Tamanho", "title"=>"Determina a largura do Widget no grid", "type"=>"option", "type_content"=>"0,Original|4,1/3|6,1/2|8,3/4|12,100%", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"default", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>6, );
    $this->pgw_items["pgw_filtro"] = array("pk"=>0, "fk"=>0, "id"=>"pgw_filtro", "description"=>"Filtro", "title"=>"Possibilita a inserção de variáveis em um mesmo widget para que ele possa mudar seu comportamento [id1:value1,value2|id2:value3]", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>7, );
    $this->pgw_items["pgw_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"pgw_ordem", "description"=>"Ordem", "title"=>"", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[>0]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>0, "line"=>5, "order"=>8, );


    // Atributos FK
    $this->pgw_items["pgw_cod_PAGINA"] = array("pk"=>0, "fk"=>1, "id"=>"pgw_cod_PAGINA", "description"=>"Página", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>3, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"Pagina", "table"=>"TBL_PAGINA", "prefix"=>"pgn", "tag"=>"pagina", "key"=>"pgn_codigo", "description"=>"pgn_title", "form"=>"form", "target"=>"div-pgw_cod_PAGINA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));
    $this->pgw_items["pgw_cod_WIDGET"] = array("pk"=>0, "fk"=>1, "id"=>"pgw_cod_WIDGET", "description"=>"Widget", "title"=>"", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[S]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, "foreign"=>array("modulo"=>"site", "entity"=>"Widget", "table"=>"TBL_WIDGET", "prefix"=>"wid", "tag"=>"widget", "key"=>"wid_codigo", "description"=>"wid_descricao", "form"=>"form", "target"=>"div-pgw_cod_WIDGET-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->pgw_items['pgw_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgw_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgw_items['pgw_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgw_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->pgw_items['pgw_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgw_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->pgw_items['pgw_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'pgw_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->pgw_items = $this->configureItemsPaginaWidget($this->pgw_items);

    $join = $this->configureJoinPaginaWidget($this->pgw_items);

    $lines = 0;
    foreach ($this->pgw_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->pgw_properties = array(
			'rotule'=>'Widgets da Página',
      'module'=>'site',
      'entity'=>'PaginaWidget',
      'table'=>'TBL_PAGINA_WIDGET',
			'join'=>$join,
      'tag'=>'paginawidget',
      'prefix'=>'pgw',
      'order'=>'pgw_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'pgw_codigo',
      'description'=>'pgw_descricao',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->pgw_properties['reference']) {
      foreach ($this->pgw_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->pgw_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->pgw_properties['description']) {
      foreach ($this->pgw_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->pgw_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function get_pgw_properties(){
    ?><?php
    return $this->pgw_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function get_pgw_items(){
    ?><?php
    return $this->pgw_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function get_pgw_item($key){
    ?><?php

		$this->validateItemPaginaWidget($key);

    return $this->pgw_items[$key];
  }

  /**
   * 
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function get_pgw_reference(){
    ?><?php
    $key = $this->pgw_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function get_pgw_value($key){
    ?><?php

		$this->validateItemPaginaWidget($key);

    return $this->pgw_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function set_pgw_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemPaginaWidget($key);

    $this->pgw_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->pgw_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function set_pgw_type($key, $type){
    ?><?php

		$this->validateItemPaginaWidget($key);

    $this->pgw_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  private  function configureJoinPaginaWidget($items){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
          $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  private  function configureItemsPaginaWidget($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->pgw_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->pgw_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function clearPaginaWidget($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_pgw_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_pgw_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  private  function validateItemPaginaWidget($key){
    ?><?php

    if (!isset($this->pgw_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function setStatementsPaginaWidget(){
    ?><?php

    $this->pgw_statements[0] = "pgw_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   * <br><b>Updated by</b> PEDRO HENRIQUE MAZALA MACHADO - 04/02/2015 01:00:53
   */
  public  function getStatementsPaginaWidget(){
    ?><?php

    return $this->pgw_statements;
  }


}