<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 23/03/2013 22:31:37
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:16
 * @category model
 * @package site
 */


class Widget
{
  private  $wid_items = array();
  private  $wid_properties = array();
  private  $wid_parents = array();
  private  $wid_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:21
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:22
   */
  public  function Widget(){
    ?><?php

    $this->wid_items = array();
    
    // Atributos
    $this->wid_items["wid_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"wid_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>1, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>0, "insert"=>0, "line"=>1, "order"=>1, );
    $this->wid_items["wid_nome"] = array("pk"=>0, "fk"=>0, "id"=>"wid_nome", "description"=>"Nome", "title"=>"Campo que será usado como ID da div do Widget", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>3, "grid"=>0, "grid_width"=>"", "form"=>0, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>2, );
    $this->wid_items["wid_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"wid_descricao", "description"=>"Descrição", "title"=>"", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>3, );
    $this->wid_items["wid_id"] = array("pk"=>0, "fk"=>0, "id"=>"wid_id", "description"=>"Identificador", "title"=>"Nome que será dado ao arquivo", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>4, );
    $this->wid_items["wid_local"] = array("pk"=>0, "fk"=>0, "id"=>"wid_local", "description"=>"Local", "title"=>"Especifica em qual local o Widget pode ser aplicado", "type"=>"option", "type_content"=>"sidebar,Barra Lateral|content,Conteúdo", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>5, );
    $this->wid_items["wid_class"] = array("pk"=>0, "fk"=>0, "id"=>"wid_class", "description"=>"Tipo", "title"=>"Classe aplicada ao Widget", "type"=>"option", "type_content"=>"default,Normal|portlet,Portlet|sidebar,Barra Lateral", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>6, );
    $this->wid_items["wid_default"] = array("pk"=>0, "fk"=>0, "id"=>"wid_default", "description"=>"Default", "title"=>"Define se é um Widget padrão do sistema ou não", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[R]", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"0", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>7, );
    $this->wid_items["wid_tamanho"] = array("pk"=>0, "fk"=>0, "id"=>"wid_tamanho", "description"=>"Tamanho", "title"=>"Posiciona o elemento dentro do grid que o layout monta", "type"=>"option", "type_content"=>"4,1/3|6,1/2|8,3/4|12,100%", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>8, );
    $this->wid_items["wid_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"wid_ordem", "description"=>"Ordem", "title"=>"Permite definir a sequência em que o Widget será exibido quando o mesmo for utilizado como padrão", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>9, );
    $this->wid_items["wid_conteudo"] = array("pk"=>0, "fk"=>0, "id"=>"wid_conteudo", "description"=>"Conteúdo", "title"=>"", "type"=>"source-code", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>10, );


    // Atributos FK


    // Atributos CHILD

    
    // Atributos padrao
    $this->wid_items['wid_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'wid_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wid_items['wid_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'wid_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->wid_items['wid_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'wid_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->wid_items['wid_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'wid_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->wid_items = $this->configureItemsWidget($this->wid_items);

    $join = $this->configureJoinWidget($this->wid_items);

    $lines = 0;
    foreach ($this->wid_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->wid_properties = array(
      'rotule'=>'Widget',
      'module'=>'site',
      'entity'=>'Widget',
      'table'=>'TBL_WIDGET',
      'join'=>$join,
      'tag'=>'widget',
      'prefix'=>'wid',
      'order'=>'wid_default DESC, wid_local, wid_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
      'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
      'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
      'database'=>$database,
      'reference'=>'wid_codigo',
      'description'=>'wid_descricao',
      'notification'=>false,
      'operations'=>array(
        //{
          'save'=>(object)(array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'copy'=>(object)(array("action"=>'copy', "label"=>"Copiar", "layout"=>"", "position"=>"toolbar", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente copiar este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro copiado com sucesso", "fail"=>"Não foi possível copiar o registro"), "execute"=>"")),
        //}
        //{
          'add'=>(object)(array("action"=>'add', "get"=>"new", "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'search'=>(object)(array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object)(array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object)(array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
        //}
        //{
          'view'=>(object)(array("action"=>'view', "get"=>"object", "label"=>"Visualizar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
          'set'=>(object)(array("action"=>'set', "get"=>"object", "label"=>"Alterar", "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","copy"=>"","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"Não foi possível salvar suas alterações"))),
          'remove'=>(object)(array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exluído com sucesso", "fail"=>"Não foi possível excluir o registro"), "execute"=>"Application.form.reloadGrid();")),
        //}
        //{
          'list'=>(object)(array("action"=>'list', "get"=>"collection", "label"=>"Widget", "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
        //}
      ),
      'lines'=>$lines
    );
    
    if (!$this->wid_properties['reference']) {
      foreach ($this->wid_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->wid_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->wid_properties['description']) {
      foreach ($this->wid_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->wid_properties['reference'] = $id;
          break;
        }
      }
    }

    $this->setStatementsWidget();
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:23
   */
  public  function get_wid_properties(){
    ?><?php
    return $this->wid_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:23
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:24
   */
  public  function get_wid_items(){
    ?><?php
    return $this->wid_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:24
   */
  public  function get_wid_item($key){
    ?><?php

		$this->validateItemWidget($key);

    return $this->wid_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:24
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:24
   */
  public  function get_wid_reference(){
    ?><?php
    $key = $this->wid_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:25
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:25
   */
  public  function get_wid_value($key){
    ?><?php

		$this->validateItemWidget($key);

    return $this->wid_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:25
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:25
   */
  public  function set_wid_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemWidget($key);

    $this->wid_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->wid_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:26
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:26
   */
  public  function set_wid_type($key, $type){
    ?><?php

		$this->validateItemWidget($key);

    $this->wid_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:27
   */
  private  function configureJoinWidget($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:27
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:27
   */
  private  function configureItemsWidget($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->wid_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->wid_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   */
  public  function clearWidget($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_wid_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_wid_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_wid_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   */
  private  function validateItemWidget($key){
    ?><?php

    if (!isset($this->wid_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:28
   */
  public  function setStatementsWidget(){
    ?><?php

    $this->wid_statements["0"] = "wid_codigo = '?'";

    $this->wid_statements["wid_0"] = $this->wid_statements["0"];

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:29
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 30/10/2015 14:05:29
   */
  public  function getStatementsWidget(){
    ?><?php

    return $this->wid_statements;
  }


}