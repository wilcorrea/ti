<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:29
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 13:23:49
 * @category model
 * @package site
 */


class Biblioteca
{
  private  $bbl_items = array();
  private  $bbl_properties = array();
  private  $bbl_parents = array();
  private  $bbl_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 12:45:35
   */
  public  function Biblioteca(){
    ?><?php

    $this->bbl_items = array();
    
    // Atributos
    $this->bbl_items["bbl_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"bbl_codigo", "description"=>"Código", "title"=>"", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->bbl_items["bbl_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_descricao", "description"=>"Descrição", "title"=>"Determina o nome da Biblioteca", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"[E]", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, );
    $this->bbl_items["bbl_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_tipo", "description"=>"Tipo", "title"=>"Local ou externo, determina se o recurso será solicitado de um ambiente externo ou está hospedado no próprio servidor", "type"=>"option", "type_content"=>"local,Local|externo,Externo", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->bbl_items["bbl_escopo"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_escopo", "description"=>"Escopo", "title"=>"Determina em que ocasião os recursos da biblioteca serão incorporadas diretamente ao projeto", "type"=>"option", "type_content"=>"all,Sempre|admin,Painel de Controle|site,Site|ondemand,Sob Demanda", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->bbl_items["bbl_id"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_id", "description"=>"Identificador", "title"=>"String que indexa a biblioteca e é utilizada para pesquisar pelo recurso", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>5, );
    $this->bbl_items["bbl_caminho"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_caminho", "description"=>"Caminho", "title"=>"Especifica o path dentro do domínio principal ou a url absoluta externa onde os recursos da biblioteca estão alocados", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>6, );
    $this->bbl_items["bbl_ordem"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_ordem", "description"=>"Ordem", "title"=>"Ordenação feita para definir a precedência entre as bibliotecas", "type"=>"int", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>7, "order"=>7, );


    // Atributos FK


    // Atributos CHILD
    $this->bbl_items["bbl_fk_bbi_cod_BIBLIOTECA_4807"] = array("pk"=>0, "fk"=>0, "id"=>"bbl_fk_bbi_cod_BIBLIOTECA_4807", "description"=>"Recursos", "title"=>"", "type"=>"child", "type_content"=>"", "type_behavior"=>"child", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"50", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>0, "update"=>0, "insert"=>0, "line"=>0, "order"=>0, "child"=>array("module"=>"site", "entity"=>"BibliotecaItem", "tag"=>"bibliotecaitem", "prefix"=>"bbi", "name"=>"Recursos", "key"=>"bbl_codigo", "foreign"=>"bbi_cod_BIBLIOTECA", "source"=>"bbi_cod_BIBLIOTECA", "filter"=>"bbi_cod_BIBLIOTECA", "where"=>"", "group"=>"", "order"=>"", "columns"=>"400", "params"=>"child=true&width=&height=&rotule=&t=&f=", "target"=>"div-".rand()."-".date("Hisu"), "form"=>"form"));

    
    // Atributos padrao
    $this->bbl_items['bbl_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbl_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->bbl_items['bbl_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbl_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->bbl_items['bbl_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbl_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->bbl_items['bbl_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbl_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->bbl_items = $this->configureItemsBiblioteca($this->bbl_items);

    $join = $this->configureJoinBiblioteca($this->bbl_items);

    $lines = 0;
    foreach ($this->bbl_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->bbl_properties = array(
			'rotule'=>'Biblioteca',
      'module'=>'site',
      'entity'=>'Biblioteca',
      'table'=>'TBL_BIBLIOTECA',
			'join'=>$join,
      'tag'=>'biblioteca',
      'prefix'=>'bbl',
      'order'=>'bbl_ordem',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'children',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'bbl_codigo',
      'description'=>'bbl_descricao',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->bbl_properties['reference']) {
      foreach ($this->bbl_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->bbl_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->bbl_properties['description']) {
      foreach ($this->bbl_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->bbl_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function get_bbl_properties(){
    ?><?php
    return $this->bbl_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function get_bbl_items(){
    ?><?php
    return $this->bbl_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function get_bbl_item($key){
    ?><?php

		$this->validateItemBiblioteca($key);

    return $this->bbl_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function get_bbl_reference(){
    ?><?php
    $key = $this->bbl_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function get_bbl_value($key){
    ?><?php

		$this->validateItemBiblioteca($key);

    return $this->bbl_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function set_bbl_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemBiblioteca($key);

    $this->bbl_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->bbl_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  public  function set_bbl_type($key, $type){
    ?><?php

		$this->validateItemBiblioteca($key);

    $this->bbl_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:38
   */
  private  function configureJoinBiblioteca($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  private  function configureItemsBiblioteca($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->bbl_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->bbl_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function clearBiblioteca($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_bbl_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_bbl_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  private  function validateItemBiblioteca($key){
    ?><?php

    if (!isset($this->bbl_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function setStatementsBiblioteca(){
    ?><?php

    $this->bbl_statements[0] = "bbl_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 10:21:39
   */
  public  function getStatementsBiblioteca(){
    ?><?php

    return $this->bbl_statements;
  }


}