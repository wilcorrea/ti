<?php
/**
 * @copyright array software
 *
 * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:02
 * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:07
 * @category model
 * @package site
 */


class BibliotecaItem
{
  private  $bbi_items = array();
  private  $bbi_properties = array();
  private  $bbi_parents = array();
  private  $bbi_statements = array();

  /**
   * Construtor do Modelo da Classe
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function BibliotecaItem(){
    ?><?php

    $this->bbi_items = array();
    
    // Atributos
    $this->bbi_items["bbi_codigo"] = array("pk"=>1, "fk"=>0, "id"=>"bbi_codigo", "description"=>"Código", "title"=>"Chave primária", "type"=>"pk", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>1, "order"=>1, );
    $this->bbi_items["bbi_descricao"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_descricao", "description"=>"Descrição", "title"=>"Nome dado ao recurso registrado", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>3, "order"=>3, );
    $this->bbi_items["bbi_tipo"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_tipo", "description"=>"Tipo", "title"=>"Determina que tipo de documento o recurso representa", "type"=>"option", "type_content"=>"css,CSS|js,JS", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>4, );
    $this->bbi_items["bbi_rota"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_rota", "description"=>"Rota", "title"=>"Rota do caminho principal até o arquivo do recurso", "type"=>"string", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>1, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>4, "order"=>5, );
    $this->bbi_items["bbi_posicao"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_posicao", "description"=>"Posição", "title"=>"Posição em que o recurso será adicionado ao documento", "type"=>"option", "type_content"=>"header,Acima|bottom,Abaixo", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>6, );
    $this->bbi_items["bbi_processado"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_processado", "description"=>"Processado", "title"=>"Especifica se o conteúdo do Recurso será processado de acordo com parâmetros enviados para o servidor", "type"=>"yes/no", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>1, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>5, "order"=>7, );
    $this->bbi_items["bbi_arquivo"] = array("pk"=>0, "fk"=>0, "id"=>"bbi_arquivo", "description"=>"Arquivo", "title"=>"Arquivo relacionado ao recurso", "type"=>"file", "type_content"=>"", "type_behavior"=>"", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>6, "order"=>8, );


    // Atributos FK
    $this->bbi_items["bbi_cod_BIBLIOTECA"] = array("pk"=>0, "fk"=>1, "id"=>"bbi_cod_BIBLIOTECA", "description"=>"Biblioteca", "title"=>"Relacionamento com a Biblioteca", "type"=>"fk", "type_content"=>"", "type_behavior"=>"foreign", "value"=>"", "action"=>"", "style"=>"", "validate"=>"", "fast"=>0, "grid"=>0, "grid_width"=>"", "form"=>1, "form_width"=>"", "readonly"=>0, "default_view"=>"", "default_sql"=>"", "select"=>1, "update"=>1, "insert"=>1, "line"=>2, "order"=>2, "foreign"=>array("modulo"=>"site", "entity"=>"Biblioteca", "table"=>"TBL_BIBLIOTECA", "prefix"=>"bbl", "tag"=>"biblioteca", "key"=>"bbl_codigo", "description"=>"bbl_descricao", "form"=>"form", "target"=>"div-bbi_cod_BIBLIOTECA-".rand()."-".date("Hisu"), "onchange"=>"", "encode"=>true, "width"=>"400", "where"=>"", "filter"=>""));


    // Atributos CHILD

    
    // Atributos padrao
    $this->bbi_items['bbi_alteracao'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbi_alteracao', 'description'=>'Alteração', 'title'=>'', 'type'=>'alteracao', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->bbi_items['bbi_registro'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbi_registro', 'description'=>'Registro', 'title'=>'', 'type'=>'registro', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);
    $this->bbi_items['bbi_responsavel'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbi_responsavel', 'description'=>'Responsável', 'title'=>'', 'type'=>'responsavel', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>true, 'insert'=>true, 'line'=>0);
    $this->bbi_items['bbi_criador'] = array('pk'=>false, 'fk'=>false, 'id'=>'bbi_criador', 'description'=>'Criador', 'title'=>'', 'type'=>'criador', 'type_content'=>'', 'type_behavior'=>'', 'value'=>'', 'action'=>'', 'style'=>'', 'validate'=>'', 'fast'=>false, 'grid'=>false, 'grid_width'=>'', 'form'=>false, 'form_width'=>'0', 'readonly'=>true, 'default_view'=>'', 'default_sql'=>'', 'update'=>false, 'insert'=>true, 'line'=>0);

    $this->bbi_items = $this->configureItemsBibliotecaItem($this->bbi_items);

    $join = $this->configureJoinBibliotecaItem($this->bbi_items);

    $lines = 0;
    foreach ($this->bbi_items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    #$database = Connection::getPersonalDatabase();
    $database = null;

    $this->bbi_properties = array(
			'rotule'=>'Recursos da Biblioteca',
      'module'=>'site',
      'entity'=>'BibliotecaItem',
      'table'=>'TBL_BIBLIOTECA_ITEM',
			'join'=>$join,
      'tag'=>'bibliotecaitem',
      'prefix'=>'bbi',
      'order'=>'',
      'group'=>'',
      'where'=>'',
      'search'=>'',
      'layout'=>'',
			'checkbox'=>false,
      'saveonly'=>false,//desabilita a edição de entidade
			'editonly'=>false,//desabilita a inserção de itens de entidade
      'readonly'=>false,//desabilita a criação de novos registros
			'database'=>$database,
      'reference'=>'bbi_codigo',
      'description'=>'bbi_descricao',
			'notification'=>false,
			'operations'=>array(
			  //{
			    'save'=>(object) (array("action"=>'save', "label"=>"Salvar", "layout"=>"", "position"=>"toolbar", "type"=>"alias", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro salvo com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
			  //}
			  //{
          'add'=>(object) (array("action"=>'add', "label"=>"Novo", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "redirect", "complete"=>true, "value"=>"", "recover"=>0, "class"=>"", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro criado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
          'search'=>(object) (array("action"=>'search', "label"=>"Pesquisar", "layout"=>"manager", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("find"=>"primary","add"=>"","back"=>""))),
          'find'=>(object) (array("action"=>'list', "label"=>"Localizar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "custom"=>'r=true', "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
          'back'=>(object) (array("action"=>'list', "label"=>"Voltar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
			  //{
  			  'view'=>(object) (array("action"=>'view', "label"=>"Visualizar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>false, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"search-plus", "level"=>0, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("back"=>""))),
  			  'set'=>(object) (array("action"=>'set', "label"=>"Alterar", "get"=>'object', "layout"=>"manager", "position"=>"grid", "type"=>"view", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "icon"=>"edit", "level"=>1, "popup"=>true, "child"=>true, "history"=>true, "operations"=>array("save"=>"primary","search"=>"","add"=>"","back"=>""), "confirm"=>""/*"Deseja realmente Salvar este registro?"*/, "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>array()/*array("column"=>"1")*/, "success"=>"Registro alterado com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel salvar suas altera&ccedil;&otilde;es"))),
  			  'remove'=>(object) (array("action"=>'remove', "label"=>"Excluir", "layout"=>"", "position"=>"grid", "type"=>"post", "complete"=>true, "value"=>"", "recover"=>2, "class"=>"", "icon"=>"trash-o", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array(), "confirm"=>"Deseja realmente excluir este registro?", "settings"=>array("conditions"=>true/*,false,array("prdcodigo"=>0)*/,"remove"=>true/*array("column"=>"1")*/, "success"=>"Registro exlu&iacute;do com sucesso", "fail"=>"N&atilde;o foi poss&iacute;vel excluir o registro"), "execute"=>"Application.form.reloadGrid();")),

  			  //'print'=>(object) (array("action"=>'print', "label"=>"Imprimir", "layout"=>"list", "position"=>"toolbar", "type"=>"resource", "complete"=>true, "value"=>"", "recover"=>1, "class"=>"", "level"=>1, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
  			  //'refresh'=>(object) (array("action"=>'list', "label"=>"Recarregar", "layout"=>"list", "position"=>"toolbar", "type"=>"view", "complete"=>true, "value"=>"", "custom"=>'r=clear', "recover"=>1, "class"=>"", "level"=>2, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array())),
			  //}
        //{
			    'list'=>(object) (array("action"=>'list', "label"=>"Lista", "get"=>'collection', "layout"=>"list", "position"=>"", "type"=>"view", "recover"=>0, "class"=>"", "level"=>0, "popup"=>false, "child"=>false, "history"=>false, "operations"=>array("add"=>"primary","search"=>"","print"=>"","refresh"=>"","view"=>"","set"=>"","remove"=>"")))
			  //}
			),
      'lines'=>$lines
    );
    
    if (!$this->bbi_properties['reference']) {
      foreach ($this->bbi_items as $id=>$array) {
        if ($array['pk'] == 1) {
          $this->bbi_properties['reference'] = $id;
          break;
        }
      }
    }
    if (!$this->bbi_properties['description']) {
      foreach ($this->bbi_items as $id=>$array) {
        if ($array['type'] == "string") {
          $this->bbi_properties['reference'] = $id;
          break;
        }
      }
    }
  }

  /**
   * Recupera as principais propriedades da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function get_bbi_properties(){
    ?><?php
    return $this->bbi_properties;
  }

  /**
   * Recupera todos os atributos da entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function get_bbi_items(){
    ?><?php
    return $this->bbi_items;
  }

  /**
   * Recupera um atributo da entidade com todas as suas propriedades
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function get_bbi_item($key){
    ?><?php

		$this->validateItemBibliotecaItem($key);

    return $this->bbi_items[$key];
  }

  /**
   * 
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function get_bbi_reference(){
    ?><?php
    $key = $this->bbi_properties['reference'];
    return $key;
  }

  /**
   * Recupera o valor de um objeto
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function get_bbi_value($key){
    ?><?php

		$this->validateItemBibliotecaItem($key);

    return $this->bbi_items[$key]['value'];
  }

  /**
   * Altera o valor de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param mixed $value 
   * @param string $reference 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   */
  public  function set_bbi_value($key, $value, $reference = null){
    ?><?php

		$this->validateItemBibliotecaItem($key);

    $this->bbi_items[$key]['value'] = $value;
    if (!is_null($reference)) {
      $this->bbi_items[$key]['reference'] = $reference;
    }

    return $this;
  }

  /**
   * Altera o tipo de um atributo da instância da entidade
   * 
   * @param string $key 
   * @param string $type 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:04
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function set_bbi_type($key, $type){
    ?><?php

		$this->validateItemBibliotecaItem($key);

    $this->bbi_items[$key]['type'] = $type;

    return $this;
  }

  /**
   * Cria as configurações de SQL da entidade
   * 
   * @param array $items 
   * @param array $ignore 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  private  function configureJoinBibliotecaItem($items, $ignore = array()){
    ?><?php

    $j = array();
    foreach ($items as $item) {
      if ($item['fk']) {
        if (isset($item['foreign']) or isset($item['parent'])) {
          $table = "";
					$key = "";
					if (isset($item['foreign'])) {
						$table = $item['foreign']['table'];
						$key = $item['foreign']['key'];
					} else if (isset($item['parent'])) {
						$table = $item['parent']['table'];
						$key = $item['parent']['key'];
					}
					if (!in_array($table, $ignore, true)) {
            $j[$table] = " LEFT JOIN ".$table." ON (".$item['id']." = ".$key.") ";
					}
        }
      }
    }
    $join = " ".join(' ', $j);
    
    return $join;
  }

  /**
   * Configura os atributos de acordo com suas características
   * 
   * @param array $items 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  private  function configureItemsBibliotecaItem($items){
    ?><?php
		
		$lines = 0;
    foreach ($items as $item) {
      $lines = ($item['line'] > $lines) ? $item['line'] : $lines;
    }

    $parents = array();
    $before = 0;
    $after = 0;

		foreach ($items as $id => $item) {
		  
		  $item['hidden'] = 0;

			if ($item['type_behavior'] == 'parent') {

				if (isset($item['parent'])) {

					$parent = $item['parent'];

					$module = $parent['modulo'];
					$class = $parent['entity'];

					System::import('m', $module, $class, 'src', true);
					$object = new $class();

          $get_properties = "get_" . $parent['prefix'] . "_properties";
					$get_items = "get_" . $parent['prefix'] . "_items";

					$properties = $object->$get_properties();
					$parent_items = $object->$get_items();

					$parent_position = $item['type_content'] ? $item['type_content'] : "bottom";
					$parent_lines = $properties['lines'];
    
    			$before = $parent_position === "bottom" ? $parent_lines + $before : $before;
    			$after = $parent_position === "top" ? $lines + $after : $after;
    			$lines = $lines + $parent_lines;

          $this->bbi_parents[] = array("position" => $parent_position, "items" => $parent_items, "lines" => $parent_lines);

          $item['hidden'] = 1;

				}

			}

      $items[$id] = $item;
		}

		foreach ($items as $id => $item) {

		  $item['line'] = $before + $item['line'];
      if ($item['pk']) {
        $item['line'] = 1;
      }
		  $item['external'] = 0;
  		$items[$id] = $item;

		}

		foreach ($this->bbi_parents as $parent) {

		  $parent_items = $parent['items'];

  		foreach ($parent_items as $id => $item) {

  			$item['line'] = $after + $item['line'];
  		  if ($item['pk']) {
          $item['line'] = 1;
          $item['grid'] = 0;
          $item['form'] = 0;
        }
  			$item['insert'] = 0;
  			$item['update'] = 0;
  			$item['external'] = 1;
  			$items[$id] = $item;

  		}

		}
		
		return $items;
  }

  /**
   * Método responsável por setar os dados do objeto como null
   * 
   * @param array $allow 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function clearBibliotecaItem($allow = null){
    ?><?php

    if (is_null($allow)) {

      $allow = array();

    } else if (!is_array($allow)) {

      core_err('0', "O argumento passado não é do tipo <i>array</i>.");
      return;

    }

    $properties = $this->get_bbi_properties();
    $reference = $properties['reference'];
    array_push($allow, $reference);

    $items = $this->get_bbi_items();
    foreach ($items as $key => $item) {
      if (!in_array($key, $allow)) {
        $this->set_ctr_value($key, null);
      }
    }

		return $this;
  }

  /**
   * Responsável por validar se o atributo faz parte da entidade
   * 
   * @param string $key 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  private  function validateItemBibliotecaItem($key){
    ?><?php

    if (!isset($this->bbi_items[$key])) {
      core_err(0, "Atributo $key não encontrado em " . get_class($this));
      exit;
    }

		return $this;
  }

  /**
   * Responsável por manter os Statements relacionados à Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function setStatementsBibliotecaItem(){
    ?><?php

    $this->bbi_statements[0] = "bbi_codigo = '?'";

    return $this;
  }

  /**
   * Responsável por recuperar os Statements relacionados à uma Entidade
   * 
   *
   * @author WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   * <br><b>Updated by</b> WILLIAM MARQUES VICENTE GOMES CORREA - 25/02/2015 11:05:05
   */
  public  function getStatementsBibliotecaItem(){
    ?><?php

    return $this->bbi_statements;
  }


}