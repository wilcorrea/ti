<?php

/**
 * 
 * @class Theme
 * 
 * @author gennesis.io < contato@gennesis.io >
 */
class Theme {

  /**
   *
   * @var type
   */
  public static $library;

  /**
   *
   * @var type
   */
  public static $user;

  /**
   *
   * @var type
   */
  public static $message;

  /**
   *
   * @var type
   */
  public static $return;

  /**
   *
   * @var type
   */
  public static $statements;

  /**
   *
   * @var type
   */
  public static $page = null;

  /**
   *
   * @var String
   */
  public static $root = '';
  
  /**
   *
   * @var type
   */
  public static $top = array();
  
  /**
   *
   * @var type
   */
  public static $footer = array();

  /**
   * 
   * @param $url
   * 
   * @return boolean
   */
  public static function redirect($url) {
    
    $redirect = false;

    if ($url !== null) {

      $redirect = true;
    }

    return $redirect;
  }

  /**
   * 
   * @param string $uri
   * @param string $return
   */
  public static function init($uri, $return) {

    self::$statements = (object) array(
         'table' => "TBL_PAGINA AS A LEFT JOIN TBL_PAGINA AS B ON (A.pgn_cod_PAGINA = B.pgn_codigo)"

       , 'single' => "A.pgn_codigo AS pgn_codigo, A.pgn_title AS pgn_title, A.pgn_breadcrumb_title AS pgn_breadcrumb_title
          , A.pgn_slug AS pgn_slug, A.pgn_breadcrumb_slug AS pgn_breadcrumb_slug, A.pgn_target AS pgn_target, A.pgn_type AS pgn_type, A.pgn_miniature AS pgn_miniature
          , A.pgn_background_image AS pgn_background_image
          , A.pgn_alteracao AS pgn_alteracao"

       , 'all' => "A.pgn_codigo AS pgn_codigo, A.pgn_cod_PAGINA AS pgn_cod_PAGINA, A.pgn_cod_BANNER AS pgn_cod_BANNER, A.pgn_cod_CATEGORIA AS pgn_cod_CATEGORIA
          , A.pgn_cod_RODAPE AS pgn_cod_RODAPE, A.pgn_menu_superior AS pgn_menu_superior, A.pgn_title AS pgn_title, A.pgn_breadcrumb_title AS pgn_breadcrumb_title
          , A.pgn_level AS pgn_level, A.pgn_slug AS pgn_slug, A.pgn_breadcrumb_slug AS pgn_breadcrumb_slug, A.pgn_type AS pgn_type, A.pgn_type_content AS pgn_type_content
          , A.pgn_author AS pgn_author, A.pgn_publish_date AS pgn_publish_date, A.pgn_hit AS pgn_hit, A.pgn_side_bar AS pgn_side_bar, A.pgn_widget AS pgn_widget
          , A.pgn_publish AS pgn_publish, A.pgn_miniature AS pgn_miniature, A.pgn_background_image AS pgn_background_image, A.pgn_featured AS pgn_featured
          , A.pgn_excerpt AS pgn_excerpt, A.pgn_call AS pgn_call, A.pgn_content AS pgn_content, A.pgn_source AS pgn_source, A.pgn_lightbox AS pgn_lightbox
          , A.pgn_width AS pgn_width, A.pgn_miniature_body AS pgn_miniature_body
          , A.pgn_criador AS pgn_criador, B.pgn_slug AS pgn_parent_slug, A.pgn_alteracao AS pgn_alteracao"

       , 'top' => "A.pgn_menu IN (1,3) AND (NOW() >= A.pgn_publish_date OR NOT A.pgn_publish_date) AND A.pgn_publish"
       , 'kid' => "(NOW() >= A.pgn_publish_date OR NOT A.pgn_publish_date) AND A.pgn_level = '1' AND A.pgn_menu = '4' AND A.pgn_publish"
       , 'bottom' => "A.pgn_menu IN (2) AND (NOW() >= A.pgn_publish_date OR NOT A.pgn_publish_date) AND A.pgn_publish"

       //, 'evp' => "SELECT COUNT(evp_codigo) FROM TBL_EVENTO_PAGINA JOIN TBL_EVENTO ON (evp_cod_EVENTO = evt_codigo) WHERE evp_cod_pagina = pgn_codigo"
       //, 'where' => "(((NOT A.pgn_publish_date) OR (" . self::$statements['evp'] . " AND (DATE_FORMAT(NOW(), '%Y-%m-%d') >= evt_inicio_data AND DATE_FORMAT(NOW(), '%H:%i:%s') >= evt_inicio_hora))) AND ((NOT pgn_deadline_date) OR (" . self::$statements['evp'] . " AND (DATE_FORMAT(NOW(), '%Y-%m-%d') <= evt_termino_data AND DATE_FORMAT(NOW(), '%H:%i:%s') <= evt_termino_hora)))) AND A.pgn_publish"
       , 'where' => "A.pgn_publish"

       , 'banner_categoria' => "SELECT ctg_codigo, ctg_descricao, ctg_slug, ctg_cor, ctg_texto FROM TBL_BANNER JOIN TBL_BANNER_IMAGEM ON (bnn_codigo = bni_cod_BANNER) JOIN TBL_CATEGORIA ON (ctg_codigo = bni_cod_CATEGORIA) WHERE {where} GROUP BY ctg_ordem"
       , 'banner_imagem' => "SELECT ctg_codigo, ctg_slug, bni_imagem, bni_texto, bni_link, bni_default FROM TBL_BANNER JOIN TBL_BANNER_IMAGEM ON (bnn_codigo = bni_cod_BANNER) JOIN TBL_CATEGORIA ON (ctg_codigo = bni_cod_CATEGORIA) WHERE {where} ORDER BY ctg_ordem, bni_ordem"
       , 'banner_where' => "bnn_codigo = '?'"

       , 'widgets' => "SELECT wid_id, wid_local, wid_class, IF(pgw_tamanho = '0', wid_tamanho, pgw_tamanho) AS wid_tamanho, IF(pgw_filtro IS NOT NULL, pgw_filtro, '') AS wid_filtro FROM TBL_WIDGET JOIN TBL_PAGINA_WIDGET ON (pgw_cod_WIDGET = wid_codigo) WHERE pgw_cod_PAGINA = '?' AND pgw_local = 'content' ORDER BY pgw_ordem"
       , 'widgets_default' => "SELECT wid_id, wid_local, wid_class, wid_tamanho FROM TBL_WIDGET WHERE wid_default AND wid_local = 'content'  ORDER BY wid_ordem"
       , 'sidebars' => "SELECT wid_id, wid_local, wid_class, IF(pgw_tamanho = '0', wid_tamanho, pgw_tamanho) AS wid_tamanho, IF(pgw_filtro IS NOT NULL, pgw_filtro, '') AS wid_filtro FROM TBL_WIDGET JOIN TBL_PAGINA_WIDGET ON (pgw_cod_WIDGET = wid_codigo) WHERE pgw_cod_PAGINA = '?' AND pgw_local = 'sidebar' ORDER BY pgw_ordem"
       , 'sidebars_default' => "SELECT wid_id, wid_local, wid_class, wid_tamanho FROM TBL_WIDGET WHERE wid_default AND wid_local = 'sidebar'  ORDER BY wid_ordem"

       , 'footer' => "SELECT rdp_codigo FROM TBL_RODAPE WHERE rdp_padrao"
    );
    
    self::load($uri);

    self::$user = Application::$user;
    self::$message = isset(Application::$user->message) ? Application::$user->message : '';
    self::$return = $return;

    

    if ((APP_SERVICE === Application::$admin) or (self::$page->pgn_publish === '2' && !APP_CONECTED)) {

      self::template('admin');
    } else {
      
      self::template('site');
    }
  }

  /**
   *
   * @param type $uri
   */
  public static function load($uri) {

    self::$library = self::library(true);

    System::import('class','base', 'DAO', 'core');

    $dao = new DAO();

    /**
     *
     * load page
     */
    $peaces = explode('/', String::replace($uri, '//', '/'));

    Theme::$root = $peaces[0];

    foreach($peaces as $key => $peace) {
      if (!$peace) {
        unset($peaces[$key]);
      }
    }
    $__slug = filter_var(implode('/', $peaces), FILTER_SANITIZE_STRING);

    $slug = array_pop($peaces);

    //$where_pgn = "(A.pgn_slug = '" . filter_var($slug, FILTER_SANITIZE_STRING) . "' AND A.pgn_level = '" . (count($peaces)) . "')" . ((count($peaces) > 0) ? " AND (B.pgn_slug = '" . filter_var($peaces[(count($peaces) - 1)], FILTER_SANITIZE_STRING) . "')" : "");
    $where_pgn = "A.pgn_breadcrumb_slug = '/" . addslashes($__slug) . "' OR A.pgn_site_slug = '/" . addslashes($__slug) . "'";
    //var_dump($where_pgn);
    if (!$slug) {
      $where_pgn = "A.pgn_home";
    }

    $page_sql = "SELECT " . self::$statements->all . " FROM " . self::$statements->table . " WHERE A.pgn_publish AND " . $where_pgn;
    $page_result = $dao->selectSQL($page_sql, false);

    while ($page_row = $dao->fetchObject($page_result)) {
      self::$page = $page_row;
    }

    if (self::$page === null) {

      self::$page = new Object();
      $where_pgn_2 = "A.pgn_slug = '404'";
      $notfound_sql = "SELECT " . self::$statements->all . " FROM " . self::$statements->table . " WHERE " . $where_pgn_2;
      $notfound_result = $dao->selectSQL($notfound_sql, false);
      while ($notfound_row = $dao->fetchObject($notfound_result)) {
        self::$page = $notfound_row;
      }

    } else {

      $dao->executeSQL("UPDATE TBL_PAGINA SET pgn_hit = pgn_hit + 1 WHERE pgn_codigo = '" . self::$page->pgn_codigo . "'", false, false);

    }
    
    self::$page->pgn_header = String::substring(self::$page->pgn_breadcrumb_title, 3);

    $pgn_codigo = self::$page->pgn_codigo;
    
    /**
     *
     * load top menu
     */
    $where = 'FALSE';
    if (self::$page->pgn_menu_superior === '0') {
      $where = self::$statements->top;
    } else if (self::$page->pgn_menu_superior === '1') {
      $where = "A.pgn_cod_PAGINA = '" . $pgn_codigo . "' AND (" . self::$statements->top . ")";
    }
    $sql = "SELECT " . self::$statements->single . " FROM " . self::$statements->table . " WHERE (" . $where . ") ORDER BY A.pgn_ordem";

    $r = $dao->selectSQL($sql, false);
    while ($menu = $dao->fetchObject($r)) {

      $pgn_codigo = (int) $menu->pgn_codigo;

      $kids = array();
      $sql1 = "SELECT " . self::$statements->single . " FROM " . self::$statements->table . " WHERE A.pgn_cod_PAGINA = '" . $pgn_codigo . "' AND (" . self::$statements->kid . ") ORDER BY A.pgn_ordem";
      //print '[' . $sql1 . ']';
      $r1 = $dao->selectSQL($sql1, false);
      while ($kid = $dao->fetchObject($r1)) {
        $title = $kid->pgn_title;
        $href = $kid->pgn_slug;
        $target = $kid->pgn_target;
        $type = $kid->pgn_type;
        $kids[$href] = (object) array('title' => $title, 'href' => $href, 'target' => $target, 'type' => $type);
      }

      $title = $menu->pgn_title;
      $href = $menu->pgn_slug;
      $target = $menu->pgn_target;

      self::$top[$href] = (object) array('title' => $title, 'href' => $href, 'target' => $target, 'kids' => array());
      self::$top[$href]->kids = $kids;
    }

    /**
     *
     * load banner
     */
    $banner = null;
    if (self::$page->pgn_cod_BANNER) {

      $categorias = array();

      $where_bnn = String::replace(self::$statements->banner_where, '?', self::$page->pgn_cod_BANNER);

      $banner_sql = String::replace(self::$statements->banner_categoria, '{where}', $where_bnn);

      $categoria_result = $dao->selectSQL($banner_sql, false);
      while ($categoria_row = $dao->fetchObject($categoria_result)) {
        $categorias[] = $categoria_row;
      }

      if (count($categorias)) {

        foreach ($categorias as $key => $categoria) {

          $ctg_codigo = $categoria->ctg_codigo;

          $imagens = array();
          $imagem_sql = String::replace(self::$statements->banner_imagem, '{where}', "(" . $where_bnn . ") AND (ctg_codigo = '" . $ctg_codigo . "')");
          $imagem_result = $dao->selectSQL($imagem_sql, false);
          while ($imagem_row = $dao->fetchObject($imagem_result)) {
            $imagens[] = $imagem_row;
          }
          $categoria->images = $imagens;
          
          $categorias[$key] = $categoria;
        }

        $banner = $categorias;
      }

    }
    self::$page->banner = $banner;


    /**
     *
     * load widgets
     */
    $widgets = array();
    /**
     *
     * load sidebars
     */
    $sidebars = array();

    switch (Theme::$page->pgn_widget) {

      case '1':

        $widget_sql = String::replace(self::$statements->widgets, '?', self::$page->pgn_codigo);
        $widget_result = $dao->selectSQL($widget_sql, false);
        if ($widget_result != null) {
          while ($widget_row = $dao->fetchObject($widget_result)) {
            $widgets[] = $widget_row;
          }
        }

        $sidebars_sql = String::replace(self::$statements->sidebars, '?', self::$page->pgn_codigo);
        $sidebars_result = $dao->selectSQL($sidebars_sql, false);
        if ($sidebars_result != null) {
          while ($sidebars_row = $dao->fetchObject($sidebars_result)) {
            $sidebars[] = $sidebars_row;
          }
        }
        break;

      case '2':

        $widget_sql = String::replace(self::$statements->widgets, '?', self::$page->pgn_cod_PAGINA);
        $widget_result = $dao->selectSQL($widget_sql, false);
        if ($widget_result != null) {
          while ($widget_row = $dao->fetchObject($widget_result)) {
            $widgets[] = $widget_row;
          }
        }

        $sidebars_sql = String::replace(self::$statements->sidebars, '?', self::$page->pgn_cod_PAGINA);
        $sidebars_result = $dao->selectSQL($sidebars_sql, false);
        if ($sidebars_result != null) {
          while ($sidebars_row = $dao->fetchObject($sidebars_result)) {
            $sidebars[] = $sidebars_row;
          }
        }
        break;

      case '3':

        $widget_sql = self::$statements->widgets_default;
        $widget_result = $dao->selectSQL($widget_sql, false);

        if ($widget_result != null) {
          while ($widget_row = $dao->fetchObject($widget_result)) {
            $widgets[] = $widget_row;
          }
        }

        $sidebars_sql = self::$statements->sidebars_default;
        $sidebars_result = $dao->selectSQL($sidebars_sql, false);
        if ($sidebars_result != null) {
          while ($sidebars_row = $dao->fetchObject($sidebars_result)) {
            $sidebars[] = $sidebars_row;
          }
        }
        break;

    }

    self::$page->widgets = $widgets;
    self::$page->sidebars = $sidebars;

    /**
     *
     * load footer
     */
    $footer = File::path(APP_PATH, 'files', 'footers', 'default.foo.php');
    if (self::$page->pgn_cod_RODAPE) {
      $footer = File::path(APP_PATH, 'files', 'footers', self::$page->pgn_cod_RODAPE. '.foo.php');
    } else {
      $footer_sql = self::$statements->footer;

      $footer_result = $dao->selectSQL($footer_sql, false);
      while ($footer_row = $dao->fetchObject($footer_result)) {
        $rdp_codigo = $footer_row->rdp_codigo;
        $footer = File::path(APP_PATH, 'files', 'footers', $rdp_codigo. '.foo.php');
      }
    }

    self::$page->footer = $footer;

  }

  /**
   * 
   * @param string $name
   */
  public static function template($template) {

    self::append($template);
  }

  /**
   * 
   * @param string $template
   * @param path $name
   * @param string $extension
   */
  public static function append($template, $name = "", $extension = "php") {

    $filename = ($name === "") ? 'template' . '.' . $template . '.' . $extension : File::path($template, $name  . '.' . $extension);

    $theme = File::path(APP_PATH, Application::$theme, DEFAULT_THEME_CLIENT, 'templates', $filename);

    if (!File::exists($theme)) {

      $theme = File::path(APP_PATH, Application::$theme, DEFAULT_THEME, 'templates', $filename);
    }

    if (File::exists($theme)) {

      include $theme;
    }
  }

  /**
   *
   * @param type $bbl_escopo
   * @param type $bbi_tipo
   * @return type
   */
  public static function library($bbl_escopo = '', $bbi_tipo = '') {
    
    System::import('class','base', 'DAO', 'core');

    $dao = new DAO();
    
    $bibliotecas = [];

    $where = ($bbl_escopo === true) ? ('TRUE') : ($bbl_escopo ? "bbl_escopo = '" . $bbl_escopo . "'" : "bbl_escopo != 'ondemand'");
    $sql = "SELECT bbl_codigo, bbl_id, bbl_tipo, bbl_escopo, bbl_posicao, bbl_estatico, bbl_componentes, bbl_caminho FROM TBL_BIBLIOTECA WHERE " . $where . " ORDER BY bbl_ordem";

    $result = $dao->selectSQL($sql, false);

    while ($biblioteca = $dao->fetchObject($result)) {
 
      $biblioteca->loaded = '0';
      $biblioteca->store = true;

      $bibliotecas[$biblioteca->bbl_id] = $biblioteca;
    }

    return $bibliotecas;
  }

  /**
   *
   * @param string $bbl_codigo
   * @param string $bbi_tipo
   * @param string $bbi_posicao
   * 
   * @return array
   */
  public static function libraryItems($bbl_codigo, $bbi_tipo, $bbi_posicao) {
    
    System::import('class','base', 'DAO', 'core');

    $dao = new DAO();
    
    $bibliotecaItems = [];

    $_where = "bbi_cod_BIBLIOTECA = '" . $bbl_codigo . "' AND " . "bbi_tipo = '" . $bbi_tipo . "' AND bbi_posicao = '" . $bbi_posicao . "'";
    $_sql = "SELECT CONCAT(bbl_caminho, '/', bbi_rota) AS bbi_rota FROM TBL_BIBLIOTECA_ITEM JOIN TBL_BIBLIOTECA ON (bbl_codigo = bbi_cod_BIBLIOTECA) WHERE " . $_where;

    $_result = $dao->selectSQL($_sql, false);

    while ($bibliotecaItem = $dao->fetchObject($_result)) {

      $bibliotecaItems[] = $bibliotecaItem;
    }
     

    return $bibliotecaItems;
  }

  /**
   *
   * @param string $asset
   * @param string $type
   * @param string $id
   * @param string $code
   * @param string $position
   * 
   * @return string
   */
  public static function lib($asset, $type, $id, $code, $position) {

    $uri = implode('/', [$asset, APP_VERSION, $code, $position, $id . '.' . $type]);

    return implode('/' , ['assets', $uri]);
  }

  /**
   * 
   * @param $uri
   * @param $return
   */
  public static function asset($uri, $return) {

    $request = explode('/', $uri);

    if (count($request) >= 4) {

      //$asset, APP_VERSION, $code, $position, $id
      //1     , 2          , 3    , 4        , 5  , 6
      $asset = $request[1];
      $code = (int) $request[3];
      $position = $request[4];
      $id = explode('.', $request[5]);

      $lib = $id[0];
      $type = isset($id[1]) ? $id[1] : '';

      $comment = '';
      $path = '';
      
      // Enable GZip encoding.
      ob_start("ob_gzhandler");
      // Enable caching
      header('Cache-Control: public');
      // Expire in one day
      header('Expires: ' . gmdate('D, d M Y H:i:s', time() + 86400) . ' GMT');

      // Set the correct MIME type, because Apache won't set it for us
      switch ($type) {
        case 'css':
          header("Content-type: text/css; charset=UTF-8");//; charset: iso-8859-1
          $comment = '/* CSS __FILE__ {file} */';
          break;
        case 'js':
          header("Content-type: text/javascript; charset=UTF-8");//; charset: iso-8859-1
          $comment = '/* JS  __FILE__ {file} */';
          break;
      }

      print "/*==================== " . $lib . " ====================*/";
      print PHP_EOL;

      $items = self::libraryItems($code, String::clear($type), String::clear($position));

      if (is_array($items)) {

        foreach ($items as $item) {

          switch ($asset) {
            case 'static':

              $filename = File::path(APP_PATH, 'theme', DEFAULT_THEME_CLIENT, 'public', $item->bbi_rota);
              if (!App::exists($filename)) {

                $filename = File::path(APP_PATH, 'theme', DEFAULT_THEME, 'public', $item->bbi_rota);
              }

              print PHP_EOL;
              print "/* " . String::replace($filename, APP_PATH, '') . " */";
              print PHP_EOL;

              if (App::exists($filename)) {

                $buffer = file_get_contents($filename);

                // Remove comments
                $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
                // Remove space after colons
                $buffer = str_replace(': ', ':', $buffer);
                // Remove whitespace
                $buffer = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $buffer);

                // Write everything out
                echo($buffer);
              }

              print PHP_EOL;
              print "/* " . String::replace($filename, APP_PATH, '') . " */";
              print PHP_EOL;
              break;
          }
        }
      }
    }
  }

  /**
   * 
   * @param object $biblioteca
   * @param string $position
   */
  public static function linkLib($biblioteca, $_position) {

    $_id = $biblioteca->bbl_id;
    $_code = $biblioteca->bbl_codigo;
    $_path = $biblioteca->bbl_caminho;

    $link = App::link(Theme::lib('static', 'css', $_id, $_code, $_position), false, false);

    if ($biblioteca->bbl_estatico) {

      $link = $_path;

      if ($biblioteca->bbl_tipo === 'local') {

        $link = App::link($_path, false, false);
      }
    } 

    self::link($link);
  }

  /**
   * 
   * @param object $biblioteca
   * @param string $position
   */
  public static function scriptLib($biblioteca, $_position) {

    $_id = $biblioteca->bbl_id;
    $_code = $biblioteca->bbl_codigo;
    $_path = $biblioteca->bbl_caminho;

    $script = App::link(Theme::lib('static', 'js', $_id, $_code, $_position), false, false);

    if ($biblioteca->bbl_estatico) {

      $script = $_path;

      if ($biblioteca->bbl_tipo === 'local') {

        $script = App::link($_path, false, false);
      }
    }

    Theme::script($script, false, false);
  }

  /**
   *
   * @param String $src
   */
  public static function script ($src) {
    ?>
      <script src="<?php print $src; ?>" type="text/javascript"></script>
    <?php
  }

  /**
   *
   * @param type $href
   */
  public static function link ($href) {
    ?>
      <link rel="stylesheet" type="text/css" href="<?php print $href;?>"/>
    <?php
  }

  /**
   *
   * @param type $img
   * @param type $default
   * @param type $cache
   */
  public static function img ($img, $default = null, $cache = null) {

    $uniqid = $cache ? '' : '?_id=' . uniqid();

    $path = $img . $uniqid;
    $src = Application::link($path, false, false);
    $data = '';
    if ($cache) {
      $data = '';
    } else if ($default) {
      $data = $path;
      $src = '//placehold.it/' . $default . '/fff/aaa/&text=loading...';
    }

    ?>
      <img src="<?php Application::text($src); ?>" data-image="<?php Application::text($data); ?>" class="img-responsive">
    <?php
  }

  /**
   *
   * @param string $wid_id
   * @param array $widget
   *
   */
  public static function widget($wid_id, $widget = null) {

    $widget = (object) $widget;

    $filename = File::path(APP_PATH . 'files', 'widgets', $wid_id . '.wid.php');

    if (File::exists($filename)) {
      include_once $filename;
    }
  }

  /**
   *
   * @param type $widgets
   *
   */
  public static function widgets($widgets) {

    if (is_array($widgets)) {

      foreach ($widgets as $widget) {

        $filename = File::path(APP_PATH . 'files', 'widgets', $widget->wid_id . '.wid.php');
        $wid_tamanho = $widget->wid_tamanho ? 'col-md-' . $widget->wid_tamanho : '';

        if (File::exists($filename)) {
          ?>
            <div class="<?php print $wid_tamanho; ?> widget-<?php print $widget->wid_local; ?> widget-<?php print $widget->wid_class; ?>">
              <?php
                include_once $filename;
              ?>
            </div>
          <?php
        }
      }
    }
  }

}
	