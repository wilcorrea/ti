<?php

header('Content-Type: application/javascript');

die();
$application = implode(APP_FS , array($base, 'application'));


print PHP_EOL;
print '/*==================== ' . 'application.js' .' ====================*/';
print PHP_EOL;
include File::path(APP_PATH, 'root', 'javascript', '');
print PHP_EOL;

$handle = opendir(File::path(APP_PATH, 'theme', 'default', 'static', $application));
while (false !== ($document = readdir($handle))) {
  print PHP_EOL;
  print '/*==================== ' . $document .' ====================*/';
  print PHP_EOL;
  $extension = File::getExtension($document);
  $file = String::replace(File::getName($document), '.' . $extension, '');
  if (!in_array($document, array('.', '..'))) {
    Theme::import($application, $file, 'static', true, $extension);
    print PHP_EOL;
  }
}
closedir($handle);


$languages = array(
    'app.grid.all'
  , 'app.grid.infos'
  , 'app.grid.noResults'
  , 'app.grid.refresh'
  , 'app.grid.search'
  , 'app.image.upload.select'
  , 'app.image.upload.finish'
  , 'app.combo.placeHolder'
  , 'app.manager.problem'
  , 'app.manager.notify'
  , 'app.manager.detail'
  , 'app.manager.update1'
  , 'app.manager.update2'
  , 'app.manager.update3'
  , 'app.manager.update4'
  , 'app.manager.update5'
  , 'app.manager.update6'
  , 'app.manager.update7'
);

if (isset($languages)) {

  foreach ($languages as $language) {
    $lang = String::replace($language, 'app', 'App.config') . " = '" . App::lang($language) . "';" . PHP_EOL;
    App::text($lang);
  }

}


$l = App::loadLibrary(true);
$library = $l ? Json::encode($l) : '{}';

?>
App.config.system = '<?php print App::$admin; ?>';
App.config.company = '<?php App::text(APP_COMPANY); ?>';
App.config.key = '<?php print APP_SECURITY; ?>';
App.lib.library = <?php print $library; ?>;
