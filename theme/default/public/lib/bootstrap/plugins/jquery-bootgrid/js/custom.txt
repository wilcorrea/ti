    labels: {
	  all: Application.config.grid.all,
	  infos: Application.config.grid.infos,
	  loading: Application.config.grid.loading,
	  noResults: Application.config.grid.noResults,
	  refresh: Application.config.grid.refresh,
	  search: Application.config.grid.search
    },
	
	templates: {
      header: "<div id=\"{{ctx.id}}\" class=\"{{css.header}}\"><div class=\"row\"><div class=\"col-sm-6 customBar\"></div><div class=\"col-sm-6 actionBar\"><p class=\"{{css.search}}\"></p><p class=\"{{css.actions}}\"></p></div></div></div>",
      select: "<input name=\"select[]\" type=\"{{ctx.type}}\" class=\"{{css.selectBox}}\" value=\"{{ctx.value}}\" />"
    }