/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.extraPlugins = 'widget,clipboard,lineutils,filetools,button,toolbar,notification,notificationaggregator,uploadwidget,uploadimage,youtube';
	
	config.filebrowserUploadUrl =  window.CKEDITOR_BASEPATH + 'server/upload/';
	config.filebrowserBrowseUrl = window.CKEDITOR_BASEPATH + 'server/browse/';
  config.filebrowserImageBrowseUrl = window.CKEDITOR_BASEPATH + 'server/browse/';
};
