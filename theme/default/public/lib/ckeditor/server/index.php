<?php

$uri = System::request('uri');

$page = (int) System::request('page', 1);



/**
 * Recuperamos a ação desejada pelo usuário
 * @var string
 */
$action = String::replace($uri, '/', '');

$usu_tipo = sha1(Encode::decrypt(System::getUser('type')));

if ($usu_tipo) {

  System::import('c', 'site', 'Midia', 'src');
  System::import('m', 'site', 'Midia', 'src');


  switch ($action) {
  
    /**
     * Foi solicitado a exibição dos arquivos
     */
    case 'browse':
  
      echo '<?xml version="1.0" encoding="UTF-8" ?>';
      
      System::import('class', 'default', 'Theme', 'theme');
  
      Theme::load('admin');
      ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        
        <html xmlns="http://www.w3.org/1999/xhtml">
        
          <head>

            <title>Mídias</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

            <?php
  
              $bibliotecas = Theme::$library;
  
              foreach ($bibliotecas as $biblioteca) {
  
                $bbl_id = $biblioteca->bbl_id;
                $bbl_escopo = $biblioteca->bbl_escopo;
                $bbl_caminho = urlencode($biblioteca->bbl_caminho);
                $biblioteca_items = $biblioteca->bbl_items;
  
                $js = array();
                $css = array();
                $processed = '';
  
                $append = (($bbl_escopo === 'all') || ($bbl_escopo === 'site' && !Application::$admin)) ? (true) : ($bbl_escopo === 'admin' && Application::$admin);
  
                if ($append) {
  
                  foreach ($biblioteca_items as $biblioteca_item) {
  
                    $processed = (($biblioteca_item->bbi_processado && !$processed) ? $usu_tipo : '');
                    if ($biblioteca_item->bbi_posicao === 'header') {
                      $bbi_rota = urlencode($biblioteca_item->bbi_rota);
                      if ($biblioteca_item->bbi_tipo === 'css') {
                        $css[] = $bbi_rota;
                      } else if ($biblioteca_item->bbi_tipo === 'js') {
                        $js[] = $bbi_rota;
                      }
          
                    }
                  }
  
                 $root = 'theme/default/require.php?l=' . $bbl_id . '&r=' . $bbl_caminho . '&u=' . $processed . '&v=' . VERSION;
  
                  if (count($css)) {
                    Theme::link(Application::link($root . '&i=' . implode(',', $css). '&t=css', false, false));
                  }
  
                  if (count($js)) {
                    Theme::script(Application::link($root . '&i=' . implode(',', $js) . '&t=js', false, false));
                  }
  
                }
  
              }
            ?>
            <script type="text/javascript">
  
              function select(file){

                window.opener.CKEDITOR.tools.callFunction(<?php echo $num = & $_GET['CKEditorFuncNum']; ?>, file);
  
                window.close();
              }
            </script>
            
            <style>
              .midia-miniature {
                height: 130px; 
                width: 100%; 
                background-repeat: no-repeat; 
                background-size: cover;
                margin: 10px 0 10px 0;
              }
              .midia-miniature label {
                background: #000;
                opacity: 0.9;
                color: #fff;
                padding: 0 5px;
                font-weight: normal;
                margin: -13px -4px;
              }
            </style>
  
          </head>
  
          <body>
            <?php

              $midiaCtrl = new MidiaCtrl();

              $walk = 12;
              $start = ($page - 1) * $walk;
              $end = $walk;

              $total = $midiaCtrl->getCountMidiaCtrl("TRUE");
              $last = ceil($total / 12);

              $midias = $midiaCtrl->getMidiaCtrl("", "", "mid_registro DESC", $start, $end);

              $url = String::replace(((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], '&page=' . $page, '');

              if (count($midias)) {

                $pages = [1, 2, 3, 4, 5];
                if ($page > 3 && $page < $last - 3) {
                  $pages = [];
                  for($i = $page - 2; $i <= $page + 2; $i++) {
                    $pages[] = $i;
                  }
                } else if ($page >= $last - 3) {
                  $pages = [];
                  for($i = $last - 4; $i <= $last; $i++) {
                    $pages[] = $i;
                  }
                }

                ?>
                  <div class="col-sm-12">
                    <ul class="pagination">
                      <li class="first" aria-disabled="true"><a href="<?php print $url . '&page=' . 1; ?>" class="button">«</a></li>
                      <li class="prev" aria-disabled="true"><a href="<?php print $url . '&page=' . ($page - 1 > 0 ? $page - 1 : 1); ?>" class="button">&lt;</a></li>
                      <!-- -->
                      <?php
                        foreach ($pages as $_page) {

                          $_active = $_page == $page ? 'active' : '';
                          ?>
                            <li class="page-<?php print $_page; ?> <?php print $_active; ?>">
                              <a href="<?php print $url . '&page=' . $_page; ?>" class="button"><?php print $_page; ?></a>
                            </li>
                          <?php
                        }
                      ?>
                      <!-- -->
                      <li class="next" aria-disabled="false"><a href="<?php print $url . '&page=' . ($page + 1 > $last ? $last : $page + 1); ?>" class="button">&gt;</a></li>
                      <li class="last" aria-disabled="false"><a href="<?php print $url . '&page=' . $last; ?>" class="button">»</a></li>
                    </ul>
                  </div>

                  <!--<div class="col-sm-12">-->
                    <?php
                      foreach ($midias as $midia) {
                        ?>
                          <div class="col-sm-3">
                            <a href="javascript:select('<?php print $midia->get_mid_value('mid_link'); ?>');">
                              <?php
                                $background = '';
                                if (File::getType($midia->get_mid_value('mid_arquivo')) === 'image') {
                                  $background = $midia->get_mid_value('mid_link');
                                }
                                ?>
                                  <div style="background-image:url('<?php print $background; ?>');" class="img-thumbnail img-responsive midia-miniature" alt="<?php print $midia->get_mid_value('mid_slug'); ?>">
                                    <label><?php Application::text($midia->get_mid_value('mid_descricao'), true); ?> [<?php Application::text($midia->get_mid_value('mid_alteracao'), true); ?>]</label>
                                  </div>
                                <?php
                              ?>
                            </a>
                          </div>
                        <?php
                      }
                    ?>
                  <!--</div>-->

                  <div class="col-sm-12">
                    <ul class="pagination">
                      <li class="first" aria-disabled="true"><a href="<?php print $url . '&page=' . 1; ?>" class="button">«</a></li>
                      <li class="prev" aria-disabled="true"><a href="<?php print $url . '&page=' . ($page - 1 > 0 ? $page - 1 : 1); ?>" class="button">&lt;</a></li>
                      <!-- -->
                      <?php
                        foreach ($pages as $_page) {

                          $_active = $_page == $page ? 'active' : '';
                          ?>
                            <li class="page-<?php print $_page; ?> <?php print $_active; ?>">
                              <a href="<?php print $url . '&page=' . $_page; ?>" class="button"><?php print $_page; ?></a>
                            </li>
                          <?php
                        }
                      ?>
                      <!-- -->
                      <li class="next" aria-disabled="false"><a href="<?php print $url . '&page=' . ($page + 1 > $last ? $last : $page + 1); ?>" class="button">&gt;</a></li>
                      <li class="last" aria-disabled="false"><a href="<?php print $url . '&page=' . $last; ?>" class="button">»</a></li>
                    </ul>
                  </div>

                <?
              } else {
                ?>
                  <h1>Nenhum arquivo encontrado.</h1>
                <?php
              }
            ?>
            <br>

          </body>
  
        </html>
      <?php
      break;
  
    case 'upload':
  
      if (isset($_FILES['upload'])) {
  
        //$fileName = preg_replace('/[^\w\d\.]/', '', $_FILES['upload']['name']);

        $original = utf8_decode($_FILES['upload']['name']);

        $add = false;


        if (File::getType($original) !== '' && File::getType($original) !== 'text' ) {

          $extension = File::getExtension($original);

          $fileName = date('YmdHis') . '_' . uniqid() . '.' . $extension;

          $slug = String::slugify(String::replace($_FILES['upload']['name'], '.' . $extension, ''));

          $mid_arquivo = File::path('temp', $fileName);

          if (move_uploaded_file($_FILES['upload']['tmp_name'], File::path(APP_PATH, 'files', $mid_arquivo))) {

            $midiaCtrl = new MidiaCtrl();

            $midia = new Midia();

            $midia->set_mid_value('mid_descricao', String::replace($original, '.' .   $extension, ''));
            $midia->set_mid_value('mid_slug', $slug);
            $midia->set_mid_value('mid_tipo', File::getType($original));
            $midia->set_mid_value('mid_arquivo', $mid_arquivo);

            $add = $midiaCtrl->operationMidiaCtrl('add', $midia->get_mid_items());
          }
          
        }

        if ($add > 0) {

          //print '{"uploaded": 1, "fileName": "' . $fileName . '", "url": "' . Application::link('download/i/_/editor/' . $fileName, false, false) . '" }';
          ?>
            <html dir="ltr" lang="pt-br">
              <head>
                <title>Mídias</title>
              </head>
              <body style="margin: 0; overflow: hidden; background: transparent;">

                <br>
                <p style="font-size: 11px; font-family: verdana; word-wrap: break-word;">
                  Arquivo <?php Application::text(File::getBase($original), true); ?> recebido corretamente
                </p>

                <?php
                  if (File::getType($original) === 'image') {
                    ?>
                      <a href="<?php Application::link('download/a/' . $slug); ?>" target="_blank">
                        <img src="<?php Application::link('download/a/' . $slug); ?>" class="img-thumbnail img-responsive" alt="Thumbnail Image" style="max-width: 100%;">
                      </a>
                    <?php
                  } else {
                    ?>
                      <a href="<?php Application::link('download/a/' . $slug); ?>" target="_blank">Download</a>
                    <?php
                  }
                ?>

              </body>
            </html>
          <?php
        } else {
          
          print '{"uploaded": 0, "error": { "message": "Unknown error." } }';
        }
  
        break;
      }
  
    default:
  
      //Informamos ao usuário que a requisição é inválida
  
      header(sprintf('%s 400 Bad Request', $_SERVER['SERVER_PROTOCOL']), true, 400);
  
      ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
          <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
              <title>Bad Request</title>
            </head>
            <body>
              <h1>400 Bad Request</h1>
              <p>Requisição inválida</p>
            </body>
          </html>
      <?php
  
  }
}
