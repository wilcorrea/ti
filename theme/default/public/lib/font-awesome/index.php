<link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
<style>
  i {
    margin: 10px;
    cursor: pointer;
    transition: font-size .5s;
  }
  i.selected {
    font-size: 72px;
    box-shadow: 2px 2px 10px -2px;
    padding: 10px;
    border-radius: 2px;
  }
  #class {
    position: fixed;
    background-color: #fff;
    padding: 20px;
    width: 100%;
    box-shadow: 0 2px 2px;
    margin: -10px;
  }
</style>

<div id="class"> <input id="selected" value="click to select"/> <input id="search"> </div>
<br>
<br>
<br>
<br>
<i class="fa fa-500px" title="fa fa-500px"></i>
<i class="fa fa-adjust" title="fa fa-adjust"></i>
<i class="fa fa-adn" title="fa fa-adn"></i>
<i class="fa fa-align-center" title="fa fa-align-center"></i>
<i class="fa fa-align-justify" title="fa fa-align-justify"></i>
<i class="fa fa-align-left" title="fa fa-align-left"></i>
<i class="fa fa-align-right" title="fa fa-align-right"></i>
<i class="fa fa-amazon" title="fa fa-amazon"></i>
<i class="fa fa-ambulance" title="fa fa-ambulance"></i>
<i class="fa fa-anchor" title="fa fa-anchor"></i>
<i class="fa fa-android" title="fa fa-android"></i>
<i class="fa fa-angellist" title="fa fa-angellist"></i>
<i class="fa fa-angle-double-down" title="fa fa-angle-double-down"></i>
<i class="fa fa-angle-double-left" title="fa fa-angle-double-left"></i>
<i class="fa fa-angle-double-right" title="fa fa-angle-double-right"></i>
<i class="fa fa-angle-double-up" title="fa fa-angle-double-up"></i>
<i class="fa fa-angle-down" title="fa fa-angle-down"></i>
<i class="fa fa-angle-left" title="fa fa-angle-left"></i>
<i class="fa fa-angle-right" title="fa fa-angle-right"></i>
<i class="fa fa-angle-up" title="fa fa-angle-up"></i>
<i class="fa fa-apple" title="fa fa-apple"></i>
<i class="fa fa-archive" title="fa fa-archive"></i>
<i class="fa fa-area-chart" title="fa fa-area-chart"></i>
<i class="fa fa-arrow-circle-down" title="fa fa-arrow-circle-down"></i>
<i class="fa fa-arrow-circle-left" title="fa fa-arrow-circle-left"></i>
<i class="fa fa-arrow-circle-o-down" title="fa fa-arrow-circle-o-down"></i>
<i class="fa fa-arrow-circle-o-left" title="fa fa-arrow-circle-o-left"></i>
<i class="fa fa-arrow-circle-o-right" title="fa fa-arrow-circle-o-right"></i>
<i class="fa fa-arrow-circle-o-up" title="fa fa-arrow-circle-o-up"></i>
<i class="fa fa-arrow-circle-right" title="fa fa-arrow-circle-right"></i>
<i class="fa fa-arrow-circle-up" title="fa fa-arrow-circle-up"></i>
<i class="fa fa-arrow-down" title="fa fa-arrow-down"></i>
<i class="fa fa-arrow-left" title="fa fa-arrow-left"></i>
<i class="fa fa-arrow-right" title="fa fa-arrow-right"></i>
<i class="fa fa-arrow-up" title="fa fa-arrow-up"></i>
<i class="fa fa-arrows" title="fa fa-arrows"></i>
<i class="fa fa-arrows" title="fa fa-arrows"></i>
<i class="fa fa-arrows-alt" title="fa fa-arrows-alt"></i>
<i class="fa fa-arrows-h" title="fa fa-arrows-h"></i>
<i class="fa fa-arrows-v" title="fa fa-arrows-v"></i>
<i class="fa fa-asterisk" title="fa fa-asterisk"></i>
<i class="fa fa-at" title="fa fa-at"></i>
<i class="fa fa-automobile" title="fa fa-automobile"></i>
<i class="fa fa-backward" title="fa fa-backward"></i>
<i class="fa fa-backward" title="fa fa-backward"></i>
<i class="fa fa-balance-scale" title="fa fa-balance-scale"></i>
<i class="fa fa-ban" title="fa fa-ban"></i>
<i class="fa fa-ban" title="fa fa-ban"></i>
<i class="fa fa-bank" title="fa fa-bank"></i>
<i class="fa fa-bar-chart" title="fa fa-bar-chart"></i>
<i class="fa fa-bar-chart-o" title="fa fa-bar-chart-o"></i>
<i class="fa fa-barcode" title="fa fa-barcode"></i>
<i class="fa fa-bars" title="fa fa-bars"></i>
<i class="fa fa-battery-0" title="fa fa-battery-0"></i>
<i class="fa fa-battery-1" title="fa fa-battery-1"></i>
<i class="fa fa-battery-2" title="fa fa-battery-2"></i>
<i class="fa fa-battery-3" title="fa fa-battery-3"></i>
<i class="fa fa-battery-4" title="fa fa-battery-4"></i>
<i class="fa fa-battery-empty" title="fa fa-battery-empty"></i>
<i class="fa fa-battery-full" title="fa fa-battery-full"></i>
<i class="fa fa-battery-half" title="fa fa-battery-half"></i>
<i class="fa fa-battery-quarter" title="fa fa-battery-quarter"></i>
<i class="fa fa-battery-three-quarters" title="fa fa-battery-three-quarters"></i>
<i class="fa fa-bed" title="fa fa-bed"></i>
<i class="fa fa-beer" title="fa fa-beer"></i>
<i class="fa fa-behance" title="fa fa-behance"></i>
<i class="fa fa-behance-square" title="fa fa-behance-square"></i>
<i class="fa fa-bell" title="fa fa-bell"></i>
<i class="fa fa-bell-o" title="fa fa-bell-o"></i>
<i class="fa fa-bell-slash" title="fa fa-bell-slash"></i>
<i class="fa fa-bell-slash-o" title="fa fa-bell-slash-o"></i>
<i class="fa fa-bicycle" title="fa fa-bicycle"></i>
<i class="fa fa-binoculars" title="fa fa-binoculars"></i>
<i class="fa fa-birthday-cake" title="fa fa-birthday-cake"></i>
<i class="fa fa-bitbucket" title="fa fa-bitbucket"></i>
<i class="fa fa-bitbucket-square" title="fa fa-bitbucket-square"></i>
<i class="fa fa-bitcoin" title="fa fa-bitcoin"></i>
<i class="fa fa-black-tie" title="fa fa-black-tie"></i>
<i class="fa fa-bold" title="fa fa-bold"></i>
<i class="fa fa-bolt" title="fa fa-bolt"></i>
<i class="fa fa-bomb" title="fa fa-bomb"></i>
<i class="fa fa-book" title="fa fa-book"></i>
<i class="fa fa-bookmark" title="fa fa-bookmark"></i>
<i class="fa fa-bookmark-o" title="fa fa-bookmark-o"></i>
<i class="fa fa-briefcase" title="fa fa-briefcase"></i>
<i class="fa fa-btc" title="fa fa-btc"></i>
<i class="fa fa-bug" title="fa fa-bug"></i>
<i class="fa fa-building" title="fa fa-building"></i>
<i class="fa fa-building-o" title="fa fa-building-o"></i>
<i class="fa fa-bullhorn" title="fa fa-bullhorn"></i>
<i class="fa fa-bullseye" title="fa fa-bullseye"></i>
<i class="fa fa-bus" title="fa fa-bus"></i>
<i class="fa fa-buysellads" title="fa fa-buysellads"></i>
<i class="fa fa-cab" title="fa fa-cab"></i>
<i class="fa fa-calculator" title="fa fa-calculator"></i>
<i class="fa fa-calendar" title="fa fa-calendar"></i>
<i class="fa fa-calendar-check-o" title="fa fa-calendar-check-o"></i>
<i class="fa fa-calendar-minus-o" title="fa fa-calendar-minus-o"></i>
<i class="fa fa-calendar-o" title="fa fa-calendar-o"></i>
<i class="fa fa-calendar-plus-o" title="fa fa-calendar-plus-o"></i>
<i class="fa fa-calendar-times-o" title="fa fa-calendar-times-o"></i>
<i class="fa fa-camera" title="fa fa-camera"></i>
<i class="fa fa-camera-retro" title="fa fa-camera-retro"></i>
<i class="fa fa-car" title="fa fa-car"></i>
<i class="fa fa-caret-down" title="fa fa-caret-down"></i>
<i class="fa fa-caret-left" title="fa fa-caret-left"></i>
<i class="fa fa-caret-right" title="fa fa-caret-right"></i>
<i class="fa fa-caret-square-o-down" title="fa fa-caret-square-o-down"></i>
<i class="fa fa-caret-square-o-left" title="fa fa-caret-square-o-left"></i>
<i class="fa fa-caret-square-o-right" title="fa fa-caret-square-o-right"></i>
<i class="fa fa-caret-square-o-up" title="fa fa-caret-square-o-up"></i>
<i class="fa fa-caret-up" title="fa fa-caret-up"></i>
<i class="fa fa-cart-arrow-down" title="fa fa-cart-arrow-down"></i>
<i class="fa fa-cart-plus" title="fa fa-cart-plus"></i>
<i class="fa fa-cc" title="fa fa-cc"></i>
<i class="fa fa-cc-amex" title="fa fa-cc-amex"></i>
<i class="fa fa-cc-diners-club" title="fa fa-cc-diners-club"></i>
<i class="fa fa-cc-discover" title="fa fa-cc-discover"></i>
<i class="fa fa-cc-jcb" title="fa fa-cc-jcb"></i>
<i class="fa fa-cc-mastercard" title="fa fa-cc-mastercard"></i>
<i class="fa fa-cc-paypal" title="fa fa-cc-paypal"></i>
<i class="fa fa-cc-stripe" title="fa fa-cc-stripe"></i>
<i class="fa fa-cc-visa" title="fa fa-cc-visa"></i>
<i class="fa fa-certificate" title="fa fa-certificate"></i>
<i class="fa fa-chain" title="fa fa-chain"></i>
<i class="fa fa-chain-broken" title="fa fa-chain-broken"></i>
<i class="fa fa-check" title="fa fa-check"></i>
<i class="fa fa-check-circle" title="fa fa-check-circle"></i>
<i class="fa fa-check-circle-o" title="fa fa-check-circle-o"></i>
<i class="fa fa-check-square" title="fa fa-check-square"></i>
<i class="fa fa-check-square-o" title="fa fa-check-square-o"></i>
<i class="fa fa-chevron-circle-down" title="fa fa-chevron-circle-down"></i>
<i class="fa fa-chevron-circle-left" title="fa fa-chevron-circle-left"></i>
<i class="fa fa-chevron-circle-right" title="fa fa-chevron-circle-right"></i>
<i class="fa fa-chevron-circle-up" title="fa fa-chevron-circle-up"></i>
<i class="fa fa-chevron-down" title="fa fa-chevron-down"></i>
<i class="fa fa-chevron-left" title="fa fa-chevron-left"></i>
<i class="fa fa-chevron-right" title="fa fa-chevron-right"></i>
<i class="fa fa-chevron-up" title="fa fa-chevron-up"></i>
<i class="fa fa-child" title="fa fa-child"></i>
<i class="fa fa-chrome" title="fa fa-chrome"></i>
<i class="fa fa-circle" title="fa fa-circle"></i>
<i class="fa fa-circle-o" title="fa fa-circle-o"></i>
<i class="fa fa-circle-o-notch" title="fa fa-circle-o-notch"></i>
<i class="fa fa-circle-thin" title="fa fa-circle-thin"></i>
<i class="fa fa-clipboard" title="fa fa-clipboard"></i>
<i class="fa fa-clock-o" title="fa fa-clock-o"></i>
<i class="fa fa-clone" title="fa fa-clone"></i>
<i class="fa fa-close" title="fa fa-close"></i>
<i class="fa fa-cloud" title="fa fa-cloud"></i>
<i class="fa fa-cloud-download" title="fa fa-cloud-download"></i>
<i class="fa fa-cloud-upload" title="fa fa-cloud-upload"></i>
<i class="fa fa-cny" title="fa fa-cny"></i>
<i class="fa fa-code" title="fa fa-code"></i>
<i class="fa fa-code-fork" title="fa fa-code-fork"></i>
<i class="fa fa-codepen" title="fa fa-codepen"></i>
<i class="fa fa-coffee" title="fa fa-coffee"></i>
<i class="fa fa-cog" title="fa fa-cog"></i>
<i class="fa fa-cogs" title="fa fa-cogs"></i>
<i class="fa fa-columns" title="fa fa-columns"></i>
<i class="fa fa-comment" title="fa fa-comment"></i>
<i class="fa fa-comment-o" title="fa fa-comment-o"></i>
<i class="fa fa-commenting" title="fa fa-commenting"></i>
<i class="fa fa-commenting-o" title="fa fa-commenting-o"></i>
<i class="fa fa-comments" title="fa fa-comments"></i>
<i class="fa fa-comments-o" title="fa fa-comments-o"></i>
<i class="fa fa-compass" title="fa fa-compass"></i>
<i class="fa fa-compress" title="fa fa-compress"></i>
<i class="fa fa-connectdevelop" title="fa fa-connectdevelop"></i>
<i class="fa fa-contao" title="fa fa-contao"></i>
<i class="fa fa-copy" title="fa fa-copy"></i>
<i class="fa fa-copyright" title="fa fa-copyright"></i>
<i class="fa fa-creative-commons" title="fa fa-creative-commons"></i>
<i class="fa fa-credit-card" title="fa fa-credit-card"></i>
<i class="fa fa-crop" title="fa fa-crop"></i>
<i class="fa fa-crosshairs" title="fa fa-crosshairs"></i>
<i class="fa fa-css3" title="fa fa-css3"></i>
<i class="fa fa-cube" title="fa fa-cube"></i>
<i class="fa fa-cubes" title="fa fa-cubes"></i>
<i class="fa fa-cut" title="fa fa-cut"></i>
<i class="fa fa-cutlery" title="fa fa-cutlery"></i>
<i class="fa fa-dashboard" title="fa fa-dashboard"></i>
<i class="fa fa-dashcube" title="fa fa-dashcube"></i>
<i class="fa fa-database" title="fa fa-database"></i>
<i class="fa fa-dedent" title="fa fa-dedent"></i>
<i class="fa fa-delicious" title="fa fa-delicious"></i>
<i class="fa fa-desktop" title="fa fa-desktop"></i>
<i class="fa fa-deviantart" title="fa fa-deviantart"></i>
<i class="fa fa-diamond" title="fa fa-diamond"></i>
<i class="fa fa-digg" title="fa fa-digg"></i>
<i class="fa fa-dollar" title="fa fa-dollar"></i>
<i class="fa fa-dot-circle-o" title="fa fa-dot-circle-o"></i>
<i class="fa fa-download" title="fa fa-download"></i>
<i class="fa fa-dribbble" title="fa fa-dribbble"></i>
<i class="fa fa-dropbox" title="fa fa-dropbox"></i>
<i class="fa fa-drupal" title="fa fa-drupal"></i>
<i class="fa fa-edit" title="fa fa-edit"></i>
<i class="fa fa-eject" title="fa fa-eject"></i>
<i class="fa fa-ellipsis-h" title="fa fa-ellipsis-h"></i>
<i class="fa fa-ellipsis-v" title="fa fa-ellipsis-v"></i>
<i class="fa fa-empire" title="fa fa-empire"></i>
<i class="fa fa-envelope" title="fa fa-envelope"></i>
<i class="fa fa-envelope-o" title="fa fa-envelope-o"></i>
<i class="fa fa-envelope-square" title="fa fa-envelope-square"></i>
<i class="fa fa-eraser" title="fa fa-eraser"></i>
<i class="fa fa-eur" title="fa fa-eur"></i>
<i class="fa fa-euro" title="fa fa-euro"></i>
<i class="fa fa-exchange" title="fa fa-exchange"></i>
<i class="fa fa-exclamation" title="fa fa-exclamation"></i>
<i class="fa fa-exclamation-circle" title="fa fa-exclamation-circle"></i>
<i class="fa fa-exclamation-triangle" title="fa fa-exclamation-triangle"></i>
<i class="fa fa-expand" title="fa fa-expand"></i>
<i class="fa fa-expeditedssl" title="fa fa-expeditedssl"></i>
<i class="fa fa-external-link" title="fa fa-external-link"></i>
<i class="fa fa-external-link-square" title="fa fa-external-link-square"></i>
<i class="fa fa-eye" title="fa fa-eye"></i>
<i class="fa fa-eye-slash" title="fa fa-eye-slash"></i>
<i class="fa fa-eyedropper" title="fa fa-eyedropper"></i>
<i class="fa fa-facebook" title="fa fa-facebook"></i>
<i class="fa fa-facebook-f" title="fa fa-facebook-f"></i>
<i class="fa fa-facebook-official" title="fa fa-facebook-official"></i>
<i class="fa fa-facebook-square" title="fa fa-facebook-square"></i>
<i class="fa fa-fast-backward" title="fa fa-fast-backward"></i>
<i class="fa fa-fast-forward" title="fa fa-fast-forward"></i>
<i class="fa fa-fax" title="fa fa-fax"></i>
<i class="fa fa-feed" title="fa fa-feed"></i>
<i class="fa fa-female" title="fa fa-female"></i>
<i class="fa fa-fighter-jet" title="fa fa-fighter-jet"></i>
<i class="fa fa-file" title="fa fa-file"></i>
<i class="fa fa-file-archive-o" title="fa fa-file-archive-o"></i>
<i class="fa fa-file-audio-o" title="fa fa-file-audio-o"></i>
<i class="fa fa-file-code-o" title="fa fa-file-code-o"></i>
<i class="fa fa-file-excel-o" title="fa fa-file-excel-o"></i>
<i class="fa fa-file-image-o" title="fa fa-file-image-o"></i>
<i class="fa fa-file-movie-o" title="fa fa-file-movie-o"></i>
<i class="fa fa-file-o" title="fa fa-file-o"></i>
<i class="fa fa-file-pdf-o" title="fa fa-file-pdf-o"></i>
<i class="fa fa-file-photo-o" title="fa fa-file-photo-o"></i>
<i class="fa fa-file-picture-o" title="fa fa-file-picture-o"></i>
<i class="fa fa-file-powerpoint-o" title="fa fa-file-powerpoint-o"></i>
<i class="fa fa-file-sound-o" title="fa fa-file-sound-o"></i>
<i class="fa fa-file-text" title="fa fa-file-text"></i>
<i class="fa fa-file-text-o" title="fa fa-file-text-o"></i>
<i class="fa fa-file-video-o" title="fa fa-file-video-o"></i>
<i class="fa fa-file-word-o" title="fa fa-file-word-o"></i>
<i class="fa fa-file-zip-o" title="fa fa-file-zip-o"></i>
<i class="fa fa-files-o" title="fa fa-files-o"></i>
<i class="fa fa-film" title="fa fa-film"></i>
<i class="fa fa-filter" title="fa fa-filter"></i>
<i class="fa fa-fire" title="fa fa-fire"></i>
<i class="fa fa-fire-extinguisher" title="fa fa-fire-extinguisher"></i>
<i class="fa fa-firefox" title="fa fa-firefox"></i>
<i class="fa fa-flag" title="fa fa-flag"></i>
<i class="fa fa-flag-checkered" title="fa fa-flag-checkered"></i>
<i class="fa fa-flag-o" title="fa fa-flag-o"></i>
<i class="fa fa-flash" title="fa fa-flash"></i>
<i class="fa fa-flask" title="fa fa-flask"></i>
<i class="fa fa-flickr" title="fa fa-flickr"></i>
<i class="fa fa-floppy-o" title="fa fa-floppy-o"></i>
<i class="fa fa-folder" title="fa fa-folder"></i>
<i class="fa fa-folder-o" title="fa fa-folder-o"></i>
<i class="fa fa-folder-open" title="fa fa-folder-open"></i>
<i class="fa fa-folder-open-o" title="fa fa-folder-open-o"></i>
<i class="fa fa-font" title="fa fa-font"></i>
<i class="fa fa-fonticons" title="fa fa-fonticons"></i>
<i class="fa fa-forumbee" title="fa fa-forumbee"></i>
<i class="fa fa-forward" title="fa fa-forward"></i>
<i class="fa fa-foursquare" title="fa fa-foursquare"></i>
<i class="fa fa-frown-o" title="fa fa-frown-o"></i>
<i class="fa fa-futbol-o" title="fa fa-futbol-o"></i>
<i class="fa fa-gamepad" title="fa fa-gamepad"></i>
<i class="fa fa-gavel" title="fa fa-gavel"></i>
<i class="fa fa-gbp" title="fa fa-gbp"></i>
<i class="fa fa-ge" title="fa fa-ge"></i>
<i class="fa fa-gear" title="fa fa-gear"></i>
<i class="fa fa-gears" title="fa fa-gears"></i>
<i class="fa fa-genderless" title="fa fa-genderless"></i>
<i class="fa fa-get-pocket" title="fa fa-get-pocket"></i>
<i class="fa fa-gg" title="fa fa-gg"></i>
<i class="fa fa-gg-circle" title="fa fa-gg-circle"></i>
<i class="fa fa-gift" title="fa fa-gift"></i>
<i class="fa fa-git" title="fa fa-git"></i>
<i class="fa fa-git-square" title="fa fa-git-square"></i>
<i class="fa fa-github" title="fa fa-github"></i>
<i class="fa fa-github-alt" title="fa fa-github-alt"></i>
<i class="fa fa-github-square" title="fa fa-github-square"></i>
<i class="fa fa-gittip" title="fa fa-gittip"></i>
<i class="fa fa-glass" title="fa fa-glass"></i>
<i class="fa fa-globe" title="fa fa-globe"></i>
<i class="fa fa-google" title="fa fa-google"></i>
<i class="fa fa-google-plus" title="fa fa-google-plus"></i>
<i class="fa fa-google-plus-square" title="fa fa-google-plus-square"></i>
<i class="fa fa-google-wallet" title="fa fa-google-wallet"></i>
<i class="fa fa-graduation-cap" title="fa fa-graduation-cap"></i>
<i class="fa fa-gratipay" title="fa fa-gratipay"></i>
<i class="fa fa-group" title="fa fa-group"></i>
<i class="fa fa-h-square" title="fa fa-h-square"></i>
<i class="fa fa-hacker-news" title="fa fa-hacker-news"></i>
<i class="fa fa-hand-grab-o" title="fa fa-hand-grab-o"></i>
<i class="fa fa-hand-lizard-o" title="fa fa-hand-lizard-o"></i>
<i class="fa fa-hand-o-down" title="fa fa-hand-o-down"></i>
<i class="fa fa-hand-o-left" title="fa fa-hand-o-left"></i>
<i class="fa fa-hand-o-right" title="fa fa-hand-o-right"></i>
<i class="fa fa-hand-o-up" title="fa fa-hand-o-up"></i>
<i class="fa fa-hand-paper-o" title="fa fa-hand-paper-o"></i>
<i class="fa fa-hand-peace-o" title="fa fa-hand-peace-o"></i>
<i class="fa fa-hand-pointer-o" title="fa fa-hand-pointer-o"></i>
<i class="fa fa-hand-rock-o" title="fa fa-hand-rock-o"></i>
<i class="fa fa-hand-scissors-o" title="fa fa-hand-scissors-o"></i>
<i class="fa fa-hand-spock-o" title="fa fa-hand-spock-o"></i>
<i class="fa fa-hand-stop-o" title="fa fa-hand-stop-o"></i>
<i class="fa fa-hdd-o" title="fa fa-hdd-o"></i>
<i class="fa fa-header" title="fa fa-header"></i>
<i class="fa fa-headphones" title="fa fa-headphones"></i>
<i class="fa fa-heart" title="fa fa-heart"></i>
<i class="fa fa-heart-o" title="fa fa-heart-o"></i>
<i class="fa fa-heartbeat" title="fa fa-heartbeat"></i>
<i class="fa fa-history" title="fa fa-history"></i>
<i class="fa fa-home" title="fa fa-home"></i>
<i class="fa fa-hospital-o" title="fa fa-hospital-o"></i>
<i class="fa fa-hotel" title="fa fa-hotel"></i>
<i class="fa fa-hourglass" title="fa fa-hourglass"></i>
<i class="fa fa-hourglass-1" title="fa fa-hourglass-1"></i>
<i class="fa fa-hourglass-2" title="fa fa-hourglass-2"></i>
<i class="fa fa-hourglass-3" title="fa fa-hourglass-3"></i>
<i class="fa fa-hourglass-end" title="fa fa-hourglass-end"></i>
<i class="fa fa-hourglass-half" title="fa fa-hourglass-half"></i>
<i class="fa fa-hourglass-o" title="fa fa-hourglass-o"></i>
<i class="fa fa-hourglass-start" title="fa fa-hourglass-start"></i>
<i class="fa fa-houzz" title="fa fa-houzz"></i>
<i class="fa fa-html5" title="fa fa-html5"></i>
<i class="fa fa-i-cursor" title="fa fa-i-cursor"></i>
<i class="fa fa-ils" title="fa fa-ils"></i>
<i class="fa fa-image" title="fa fa-image"></i>
<i class="fa fa-inbox" title="fa fa-inbox"></i>
<i class="fa fa-indent" title="fa fa-indent"></i>
<i class="fa fa-industry" title="fa fa-industry"></i>
<i class="fa fa-info" title="fa fa-info"></i>
<i class="fa fa-info-circle" title="fa fa-info-circle"></i>
<i class="fa fa-inr" title="fa fa-inr"></i>
<i class="fa fa-instagram" title="fa fa-instagram"></i>
<i class="fa fa-institution" title="fa fa-institution"></i>
<i class="fa fa-internet-explorer" title="fa fa-internet-explorer"></i>
<i class="fa fa-intersex" title="fa fa-intersex"></i>
<i class="fa fa-ioxhost" title="fa fa-ioxhost"></i>
<i class="fa fa-italic" title="fa fa-italic"></i>
<i class="fa fa-joomla" title="fa fa-joomla"></i>
<i class="fa fa-jpy" title="fa fa-jpy"></i>
<i class="fa fa-jsfiddle" title="fa fa-jsfiddle"></i>
<i class="fa fa-key" title="fa fa-key"></i>
<i class="fa fa-keyboard-o" title="fa fa-keyboard-o"></i>
<i class="fa fa-krw" title="fa fa-krw"></i>
<i class="fa fa-language" title="fa fa-language"></i>
<i class="fa fa-laptop" title="fa fa-laptop"></i>
<i class="fa fa-lastfm" title="fa fa-lastfm"></i>
<i class="fa fa-lastfm-square" title="fa fa-lastfm-square"></i>
<i class="fa fa-leaf" title="fa fa-leaf"></i>
<i class="fa fa-leanpub" title="fa fa-leanpub"></i>
<i class="fa fa-legal" title="fa fa-legal"></i>
<i class="fa fa-lemon-o" title="fa fa-lemon-o"></i>
<i class="fa fa-level-down" title="fa fa-level-down"></i>
<i class="fa fa-level-up" title="fa fa-level-up"></i>
<i class="fa fa-life-bouy" title="fa fa-life-bouy"></i>
<i class="fa fa-life-buoy" title="fa fa-life-buoy"></i>
<i class="fa fa-life-ring" title="fa fa-life-ring"></i>
<i class="fa fa-life-saver" title="fa fa-life-saver"></i>
<i class="fa fa-lightbulb-o" title="fa fa-lightbulb-o"></i>
<i class="fa fa-line-chart" title="fa fa-line-chart"></i>
<i class="fa fa-link" title="fa fa-link"></i>
<i class="fa fa-linkedin" title="fa fa-linkedin"></i>
<i class="fa fa-linkedin-square" title="fa fa-linkedin-square"></i>
<i class="fa fa-linux" title="fa fa-linux"></i>
<i class="fa fa-list" title="fa fa-list"></i>
<i class="fa fa-list-alt" title="fa fa-list-alt"></i>
<i class="fa fa-list-ol" title="fa fa-list-ol"></i>
<i class="fa fa-list-ul" title="fa fa-list-ul"></i>
<i class="fa fa-location-arrow" title="fa fa-location-arrow"></i>
<i class="fa fa-lock" title="fa fa-lock"></i>
<i class="fa fa-long-arrow-down" title="fa fa-long-arrow-down"></i>
<i class="fa fa-long-arrow-left" title="fa fa-long-arrow-left"></i>
<i class="fa fa-long-arrow-right" title="fa fa-long-arrow-right"></i>
<i class="fa fa-long-arrow-up" title="fa fa-long-arrow-up"></i>
<i class="fa fa-magic" title="fa fa-magic"></i>
<i class="fa fa-magnet" title="fa fa-magnet"></i>
<i class="fa fa-mail-forward" title="fa fa-mail-forward"></i>
<i class="fa fa-mail-reply" title="fa fa-mail-reply"></i>
<i class="fa fa-mail-reply-all" title="fa fa-mail-reply-all"></i>
<i class="fa fa-male" title="fa fa-male"></i>
<i class="fa fa-map" title="fa fa-map"></i>
<i class="fa fa-map-marker" title="fa fa-map-marker"></i>
<i class="fa fa-map-o" title="fa fa-map-o"></i>
<i class="fa fa-map-pin" title="fa fa-map-pin"></i>
<i class="fa fa-map-signs" title="fa fa-map-signs"></i>
<i class="fa fa-mars" title="fa fa-mars"></i>
<i class="fa fa-mars-double" title="fa fa-mars-double"></i>
<i class="fa fa-mars-stroke" title="fa fa-mars-stroke"></i>
<i class="fa fa-mars-stroke-h" title="fa fa-mars-stroke-h"></i>
<i class="fa fa-mars-stroke-v" title="fa fa-mars-stroke-v"></i>
<i class="fa fa-maxcdn" title="fa fa-maxcdn"></i>
<i class="fa fa-meanpath" title="fa fa-meanpath"></i>
<i class="fa fa-medium" title="fa fa-medium"></i>
<i class="fa fa-medkit" title="fa fa-medkit"></i>
<i class="fa fa-meh-o" title="fa fa-meh-o"></i>
<i class="fa fa-mercury" title="fa fa-mercury"></i>
<i class="fa fa-microphone" title="fa fa-microphone"></i>
<i class="fa fa-microphone-slash" title="fa fa-microphone-slash"></i>
<i class="fa fa-minus" title="fa fa-minus"></i>
<i class="fa fa-minus-circle" title="fa fa-minus-circle"></i>
<i class="fa fa-minus-circle" title="fa fa-minus-circle"></i>
<i class="fa fa-minus-square" title="fa fa-minus-square"></i>
<i class="fa fa-minus-square-o" title="fa fa-minus-square-o"></i>
<i class="fa fa-mobile" title="fa fa-mobile"></i>
<i class="fa fa-mobile-phone" title="fa fa-mobile-phone"></i>
<i class="fa fa-money" title="fa fa-money"></i>
<i class="fa fa-moon-o" title="fa fa-moon-o"></i>
<i class="fa fa-mortar-board" title="fa fa-mortar-board"></i>
<i class="fa fa-motorcycle" title="fa fa-motorcycle"></i>
<i class="fa fa-mouse-pointer" title="fa fa-mouse-pointer"></i>
<i class="fa fa-music" title="fa fa-music"></i>
<i class="fa fa-navicon" title="fa fa-navicon"></i>
<i class="fa fa-neuter" title="fa fa-neuter"></i>
<i class="fa fa-newspaper-o" title="fa fa-newspaper-o"></i>
<i class="fa fa-object-group" title="fa fa-object-group"></i>
<i class="fa fa-object-ungroup" title="fa fa-object-ungroup"></i>
<i class="fa fa-odnoklassniki" title="fa fa-odnoklassniki"></i>
<i class="fa fa-odnoklassniki-square" title="fa fa-odnoklassniki-square"></i>
<i class="fa fa-opencart" title="fa fa-opencart"></i>
<i class="fa fa-openid" title="fa fa-openid"></i>
<i class="fa fa-opera" title="fa fa-opera"></i>
<i class="fa fa-optin-monster" title="fa fa-optin-monster"></i>
<i class="fa fa-outdent" title="fa fa-outdent"></i>
<i class="fa fa-pagelines" title="fa fa-pagelines"></i>
<i class="fa fa-paint-brush" title="fa fa-paint-brush"></i>
<i class="fa fa-paper-plane" title="fa fa-paper-plane"></i>
<i class="fa fa-paper-plane-o" title="fa fa-paper-plane-o"></i>
<i class="fa fa-paperclip" title="fa fa-paperclip"></i>
<i class="fa fa-paragraph" title="fa fa-paragraph"></i>
<i class="fa fa-paste" title="fa fa-paste"></i>
<i class="fa fa-pause" title="fa fa-pause"></i>
<i class="fa fa-paw" title="fa fa-paw"></i>
<i class="fa fa-paypal" title="fa fa-paypal"></i>
<i class="fa fa-pencil" title="fa fa-pencil"></i>
<i class="fa fa-pencil-square" title="fa fa-pencil-square"></i>
<i class="fa fa-pencil-square-o" title="fa fa-pencil-square-o"></i>
<i class="fa fa-phone" title="fa fa-phone"></i>
<i class="fa fa-phone-square" title="fa fa-phone-square"></i>
<i class="fa fa-photo" title="fa fa-photo"></i>
<i class="fa fa-picture-o" title="fa fa-picture-o"></i>
<i class="fa fa-pie-chart" title="fa fa-pie-chart"></i>
<i class="fa fa-pied-piper" title="fa fa-pied-piper"></i>
<i class="fa fa-pied-piper-alt" title="fa fa-pied-piper-alt"></i>
<i class="fa fa-pinterest" title="fa fa-pinterest"></i>
<i class="fa fa-pinterest-p" title="fa fa-pinterest-p"></i>
<i class="fa fa-pinterest-square" title="fa fa-pinterest-square"></i>
<i class="fa fa-plane" title="fa fa-plane"></i>
<i class="fa fa-play" title="fa fa-play"></i>
<i class="fa fa-play" title="fa fa-play"></i>
<i class="fa fa-play-circle" title="fa fa-play-circle"></i>
<i class="fa fa-play-circle-o" title="fa fa-play-circle-o"></i>
<i class="fa fa-plug" title="fa fa-plug"></i>
<i class="fa fa-plus" title="fa fa-plus"></i>
<i class="fa fa-plus-circle" title="fa fa-plus-circle"></i>
<i class="fa fa-plus-square" title="fa fa-plus-square"></i>
<i class="fa fa-plus-square-o" title="fa fa-plus-square-o"></i>
<i class="fa fa-power-off" title="fa fa-power-off"></i>
<i class="fa fa-print" title="fa fa-print"></i>
<i class="fa fa-print" title="fa fa-print"></i>
<i class="fa fa-puzzle-piece" title="fa fa-puzzle-piece"></i>
<i class="fa fa-qq" title="fa fa-qq"></i>
<i class="fa fa-qrcode" title="fa fa-qrcode"></i>
<i class="fa fa-qrcode" title="fa fa-qrcode"></i>
<i class="fa fa-question" title="fa fa-question"></i>
<i class="fa fa-question-circle" title="fa fa-question-circle"></i>
<i class="fa fa-quote-left" title="fa fa-quote-left"></i>
<i class="fa fa-quote-right" title="fa fa-quote-right"></i>
<i class="fa fa-ra" title="fa fa-ra"></i>
<i class="fa fa-random" title="fa fa-random"></i>
<i class="fa fa-rebel" title="fa fa-rebel"></i>
<i class="fa fa-recycle" title="fa fa-recycle"></i>
<i class="fa fa-reddit" title="fa fa-reddit"></i>
<i class="fa fa-reddit-square" title="fa fa-reddit-square"></i>
<i class="fa fa-refresh" title="fa fa-refresh"></i>
<i class="fa fa-registered" title="fa fa-registered"></i>
<i class="fa fa-remove" title="fa fa-remove"></i>
<i class="fa fa-renren" title="fa fa-renren"></i>
<i class="fa fa-reorder" title="fa fa-reorder"></i>
<i class="fa fa-repeat" title="fa fa-repeat"></i>
<i class="fa fa-reply" title="fa fa-reply"></i>
<i class="fa fa-reply-all" title="fa fa-reply-all"></i>
<i class="fa fa-retweet" title="fa fa-retweet"></i>
<i class="fa fa-rmb" title="fa fa-rmb"></i>
<i class="fa fa-road" title="fa fa-road"></i>
<i class="fa fa-rocket" title="fa fa-rocket"></i>
<i class="fa fa-rotate-left" title="fa fa-rotate-left"></i>
<i class="fa fa-rotate-right" title="fa fa-rotate-right"></i>
<i class="fa fa-rouble" title="fa fa-rouble"></i>
<i class="fa fa-rss" title="fa fa-rss"></i>
<i class="fa fa-rss-square" title="fa fa-rss-square"></i>
<i class="fa fa-rub" title="fa fa-rub"></i>
<i class="fa fa-ruble" title="fa fa-ruble"></i>
<i class="fa fa-rupee" title="fa fa-rupee"></i>
<i class="fa fa-safari" title="fa fa-safari"></i>
<i class="fa fa-save" title="fa fa-save"></i>
<i class="fa fa-scissors" title="fa fa-scissors"></i>
<i class="fa fa-search" title="fa fa-search"></i>
<i class="fa fa-search-minus" title="fa fa-search-minus"></i>
<i class="fa fa-search-plus" title="fa fa-search-plus"></i>
<i class="fa fa-sellsy" title="fa fa-sellsy"></i>
<i class="fa fa-send" title="fa fa-send"></i>
<i class="fa fa-send-o" title="fa fa-send-o"></i>
<i class="fa fa-server" title="fa fa-server"></i>
<i class="fa fa-share" title="fa fa-share"></i>
<i class="fa fa-share-alt" title="fa fa-share-alt"></i>
<i class="fa fa-share-alt-square" title="fa fa-share-alt-square"></i>
<i class="fa fa-share-square" title="fa fa-share-square"></i>
<i class="fa fa-share-square-o" title="fa fa-share-square-o"></i>
<i class="fa fa-shekel" title="fa fa-shekel"></i>
<i class="fa fa-sheqel" title="fa fa-sheqel"></i>
<i class="fa fa-shield" title="fa fa-shield"></i>
<i class="fa fa-ship" title="fa fa-ship"></i>
<i class="fa fa-shirtsinbulk" title="fa fa-shirtsinbulk"></i>
<i class="fa fa-shopping-cart" title="fa fa-shopping-cart"></i>
<i class="fa fa-sign-in" title="fa fa-sign-in"></i>
<i class="fa fa-sign-out" title="fa fa-sign-out"></i>
<i class="fa fa-signal" title="fa fa-signal"></i>
<i class="fa fa-simplybuilt" title="fa fa-simplybuilt"></i>
<i class="fa fa-sitemap" title="fa fa-sitemap"></i>
<i class="fa fa-skyatlas" title="fa fa-skyatlas"></i>
<i class="fa fa-skype" title="fa fa-skype"></i>
<i class="fa fa-slack" title="fa fa-slack"></i>
<i class="fa fa-sliders" title="fa fa-sliders"></i>
<i class="fa fa-slideshare" title="fa fa-slideshare"></i>
<i class="fa fa-smile-o" title="fa fa-smile-o"></i>
<i class="fa fa-soccer-ball-o" title="fa fa-soccer-ball-o"></i>
<i class="fa fa-sort" title="fa fa-sort"></i>
<i class="fa fa-sort-alpha-asc" title="fa fa-sort-alpha-asc"></i>
<i class="fa fa-sort-alpha-desc" title="fa fa-sort-alpha-desc"></i>
<i class="fa fa-sort-amount-asc" title="fa fa-sort-amount-asc"></i>
<i class="fa fa-sort-amount-desc" title="fa fa-sort-amount-desc"></i>
<i class="fa fa-sort-asc" title="fa fa-sort-asc"></i>
<i class="fa fa-sort-desc" title="fa fa-sort-desc"></i>
<i class="fa fa-sort-down" title="fa fa-sort-down"></i>
<i class="fa fa-sort-numeric-asc" title="fa fa-sort-numeric-asc"></i>
<i class="fa fa-sort-numeric-desc" title="fa fa-sort-numeric-desc"></i>
<i class="fa fa-sort-up" title="fa fa-sort-up"></i>
<i class="fa fa-soundcloud" title="fa fa-soundcloud"></i>
<i class="fa fa-space-shuttle" title="fa fa-space-shuttle"></i>
<i class="fa fa-spinner" title="fa fa-spinner"></i>
<i class="fa fa-spoon" title="fa fa-spoon"></i>
<i class="fa fa-spotify" title="fa fa-spotify"></i>
<i class="fa fa-square" title="fa fa-square"></i>
<i class="fa fa-square-o" title="fa fa-square-o"></i>
<i class="fa fa-stack-exchange" title="fa fa-stack-exchange"></i>
<i class="fa fa-stack-overflow" title="fa fa-stack-overflow"></i>
<i class="fa fa-star" title="fa fa-star"></i>
<i class="fa fa-star-half" title="fa fa-star-half"></i>
<i class="fa fa-star-half-empty" title="fa fa-star-half-empty"></i>
<i class="fa fa-star-half-full" title="fa fa-star-half-full"></i>
<i class="fa fa-star-half-o" title="fa fa-star-half-o"></i>
<i class="fa fa-star-o" title="fa fa-star-o"></i>
<i class="fa fa-steam" title="fa fa-steam"></i>
<i class="fa fa-steam-square" title="fa fa-steam-square"></i>
<i class="fa fa-step-backward" title="fa fa-step-backward"></i>
<i class="fa fa-step-forward" title="fa fa-step-forward"></i>
<i class="fa fa-stethoscope" title="fa fa-stethoscope"></i>
<i class="fa fa-sticky-note" title="fa fa-sticky-note"></i>
<i class="fa fa-sticky-note-o" title="fa fa-sticky-note-o"></i>
<i class="fa fa-stop" title="fa fa-stop"></i>
<i class="fa fa-street-view" title="fa fa-street-view"></i>
<i class="fa fa-strikethrough" title="fa fa-strikethrough"></i>
<i class="fa fa-stumbleupon" title="fa fa-stumbleupon"></i>
<i class="fa fa-stumbleupon-circle" title="fa fa-stumbleupon-circle"></i>
<i class="fa fa-subscript" title="fa fa-subscript"></i>
<i class="fa fa-subway" title="fa fa-subway"></i>
<i class="fa fa-suitcase" title="fa fa-suitcase"></i>
<i class="fa fa-sun-o" title="fa fa-sun-o"></i>
<i class="fa fa-superscript" title="fa fa-superscript"></i>
<i class="fa fa-support" title="fa fa-support"></i>
<i class="fa fa-table" title="fa fa-table"></i>
<i class="fa fa-tablet" title="fa fa-tablet"></i>
<i class="fa fa-tachometer" title="fa fa-tachometer"></i>
<i class="fa fa-tag" title="fa fa-tag"></i>
<i class="fa fa-tags" title="fa fa-tags"></i>
<i class="fa fa-tasks" title="fa fa-tasks"></i>
<i class="fa fa-taxi" title="fa fa-taxi"></i>
<i class="fa fa-tencent-weibo" title="fa fa-tencent-weibo"></i>
<i class="fa fa-terminal" title="fa fa-terminal"></i>
<i class="fa fa-text-height" title="fa fa-text-height"></i>
<i class="fa fa-text-width" title="fa fa-text-width"></i>
<i class="fa fa-th" title="fa fa-th"></i>
<i class="fa fa-th-large" title="fa fa-th-large"></i>
<i class="fa fa-th-list" title="fa fa-th-list"></i>
<i class="fa fa-thumb-tack" title="fa fa-thumb-tack"></i>
<i class="fa fa-thumbs-down" title="fa fa-thumbs-down"></i>
<i class="fa fa-thumbs-o-down" title="fa fa-thumbs-o-down"></i>
<i class="fa fa-thumbs-o-up" title="fa fa-thumbs-o-up"></i>
<i class="fa fa-thumbs-up" title="fa fa-thumbs-up"></i>
<i class="fa fa-ticket" title="fa fa-ticket"></i>
<i class="fa fa-times" title="fa fa-times"></i>
<i class="fa fa-times-circle" title="fa fa-times-circle"></i>
<i class="fa fa-times-circle-o" title="fa fa-times-circle-o"></i>
<i class="fa fa-tint" title="fa fa-tint"></i>
<i class="fa fa-tint" title="fa fa-tint"></i>
<i class="fa fa-toggle-down" title="fa fa-toggle-down"></i>
<i class="fa fa-toggle-left" title="fa fa-toggle-left"></i>
<i class="fa fa-toggle-off" title="fa fa-toggle-off"></i>
<i class="fa fa-toggle-on" title="fa fa-toggle-on"></i>
<i class="fa fa-toggle-right" title="fa fa-toggle-right"></i>
<i class="fa fa-toggle-up" title="fa fa-toggle-up"></i>
<i class="fa fa-trademark" title="fa fa-trademark"></i>
<i class="fa fa-train" title="fa fa-train"></i>
<i class="fa fa-transgender" title="fa fa-transgender"></i>
<i class="fa fa-transgender-alt" title="fa fa-transgender-alt"></i>
<i class="fa fa-trash" title="fa fa-trash"></i>
<i class="fa fa-trash-o" title="fa fa-trash-o"></i>
<i class="fa fa-tree" title="fa fa-tree"></i>
<i class="fa fa-trello" title="fa fa-trello"></i>
<i class="fa fa-tripadvisor" title="fa fa-tripadvisor"></i>
<i class="fa fa-trophy" title="fa fa-trophy"></i>
<i class="fa fa-truck" title="fa fa-truck"></i>
<i class="fa fa-try" title="fa fa-try"></i>
<i class="fa fa-tty" title="fa fa-tty"></i>
<i class="fa fa-tumblr" title="fa fa-tumblr"></i>
<i class="fa fa-tumblr-square" title="fa fa-tumblr-square"></i>
<i class="fa fa-turkish-lira" title="fa fa-turkish-lira"></i>
<i class="fa fa-tv" title="fa fa-tv"></i>
<i class="fa fa-twitch" title="fa fa-twitch"></i>
<i class="fa fa-twitter" title="fa fa-twitter"></i>
<i class="fa fa-twitter-square" title="fa fa-twitter-square"></i>
<i class="fa fa-umbrella" title="fa fa-umbrella"></i>
<i class="fa fa-underline" title="fa fa-underline"></i>
<i class="fa fa-undo" title="fa fa-undo"></i>
<i class="fa fa-university" title="fa fa-university"></i>
<i class="fa fa-unlink" title="fa fa-unlink"></i>
<i class="fa fa-unlock" title="fa fa-unlock"></i>
<i class="fa fa-unlock-alt" title="fa fa-unlock-alt"></i>
<i class="fa fa-unsorted" title="fa fa-unsorted"></i>
<i class="fa fa-upload" title="fa fa-upload"></i>
<i class="fa fa-usd" title="fa fa-usd"></i>
<i class="fa fa-user" title="fa fa-user"></i>
<i class="fa fa-user-md" title="fa fa-user-md"></i>
<i class="fa fa-user-plus" title="fa fa-user-plus"></i>
<i class="fa fa-user-secret" title="fa fa-user-secret"></i>
<i class="fa fa-user-times" title="fa fa-user-times"></i>
<i class="fa fa-users" title="fa fa-users"></i>
<i class="fa fa-venus" title="fa fa-venus"></i>
<i class="fa fa-venus-double" title="fa fa-venus-double"></i>
<i class="fa fa-venus-mars" title="fa fa-venus-mars"></i>
<i class="fa fa-viacoin" title="fa fa-viacoin"></i>
<i class="fa fa-video-camera" title="fa fa-video-camera"></i>
<i class="fa fa-vimeo" title="fa fa-vimeo"></i>
<i class="fa fa-vimeo-square" title="fa fa-vimeo-square"></i>
<i class="fa fa-vine" title="fa fa-vine"></i>
<i class="fa fa-vk" title="fa fa-vk"></i>
<i class="fa fa-volume-down" title="fa fa-volume-down"></i>
<i class="fa fa-volume-off" title="fa fa-volume-off"></i>
<i class="fa fa-volume-up" title="fa fa-volume-up"></i>
<i class="fa fa-warning" title="fa fa-warning"></i>
<i class="fa fa-wechat" title="fa fa-wechat"></i>
<i class="fa fa-weibo" title="fa fa-weibo"></i>
<i class="fa fa-weixin" title="fa fa-weixin"></i>
<i class="fa fa-whatsapp" title="fa fa-whatsapp"></i>
<i class="fa fa-wheelchair" title="fa fa-wheelchair"></i>
<i class="fa fa-wifi" title="fa fa-wifi"></i>
<i class="fa fa-wikipedia-w" title="fa fa-wikipedia-w"></i>
<i class="fa fa-windows" title="fa fa-windows"></i>
<i class="fa fa-won" title="fa fa-won"></i>
<i class="fa fa-wordpress" title="fa fa-wordpress"></i>
<i class="fa fa-wrench" title="fa fa-wrench"></i>
<i class="fa fa-xing" title="fa fa-xing"></i>
<i class="fa fa-xing-square" title="fa fa-xing-square"></i>
<i class="fa fa-y-combinator" title="fa fa-y-combinator"></i>
<i class="fa fa-y-combinator-square" title="fa fa-y-combinator-square"></i>
<i class="fa fa-yahoo" title="fa fa-yahoo"></i>
<i class="fa fa-yc" title="fa fa-yc"></i>
<i class="fa fa-yc-square" title="fa fa-yc-square"></i>
<i class="fa fa-yelp" title="fa fa-yelp"></i>
<i class="fa fa-yen" title="fa fa-yen"></i>
<i class="fa fa-youtube" title="fa fa-youtube"></i>
<i class="fa fa-youtube-play" title="fa fa-youtube-play"></i>
<i class="fa fa-youtube-square" title="fa fa-youtube-square"></i>

<br>
<br>
<br>
<br>

<script>
  var IS = document.getElementsByTagName('i');

  for (var a = 0; a < IS.length; a++) {
    IS[a].onclick = function() {
      for (var b = 0; b < IS.length; b++) {
        IS[b].className = IS[b].className.replace('selected', '');
      }
      this.className = this.className + ' ' + 'selected';
      document.getElementById('selected').value = this.title;
    }
  }
  
  document.getElementById('search').onkeyup = function() {
    for (var b = 0; b < IS.length; b++) {
      IS[b].style.display = 'inline-block';
      if (this.value) {

        if (IS[b].title.indexOf(this.value) === -1) {
          IS[b].style.display = 'none';
        }
      }
    }
  };
</script>

