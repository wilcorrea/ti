/*!
 * Ambiance - Notification Plugin for jQuery
 * Version 1.0.1
 * @requires jQuery v1.7.2
 *
 * Copyright (c) 2012 Richard Hsu
 * Documentation: http://www.github.com/richardhsu/jquery.ambiance
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */

(function($) {
  $.fn.ambiance = function(options) {

    var defaults = {
      title: "",
      message: "",
      type: "default",
      permanent: false,
      timeout: 2,
      fade: true,
      width: 300
    };

    var options = $.extend(defaults, options)
      , $area = $("#ambiance-notification")
       // Construct the new notification.
      , $note = $(window.document.createElement('div')).addClass("ambiance").addClass("ambiance-" + options['type'])
      //close button
      , $close = null;

    $note.css({width: options['width']});


    // Deal with adding the close feature or not.
    if (!options['permanent']) {
      $close = $(window.document.createElement('a')).addClass("ambiance-close").attr("href", "javascript:void(0);").html("");
      $close.click(function () {
        $(this).parent().remove();
      });
      $note.prepend($close);
    }

    // Deal with adding the title if given.
    if (options['title'] !== "") {
      $note.append($(window.document.createElement('div')).addClass("ambiance-title").append(options['title']));
    }

    // Append the message (this can also be HTML or even an object!).
    $note.hide();
    // Add the notification to the notification area.
    $area.append($note);
    $note.append(options['message']);
    if (options['fade']) {
      $note.show('fast');
    } else {
      $note.show();
    }
    
    // Deal with non-permanent note.
    if (!options['permanent']) {
      if (options['timeout'] != 0) {
        if (options['fade']) {
          window.setTimeout(function() {
            $note.hide('fast', function() {
              $note.remove();
            });
          }, options['timeout'] * 1000);
        } else {
          window.setTimeout(function() {
            $note.remove();
          }, options['timeout'] * 1000);
        }
      }
    }
  };
  $.ambiance = $.fn.ambiance; // Rename for easier calling.
})(jQuery);

jQuery(document).ready(function() {
  // Deal with adding the notification area to the page.
  if (jQuery("#ambiance-notification").length == 0) {
    jQuery('body').append(jQuery(window.document.createElement('div')).attr("id", "ambiance-notification"));
  }
});

// Deal with close event on a note.
/*
jQuery(document).on("click", ".ambiance-close", function () {
  jQuery(this).parent().remove();
  return false;
});
*/
