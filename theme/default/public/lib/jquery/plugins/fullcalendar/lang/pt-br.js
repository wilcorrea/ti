(function(e) {
    "function" == typeof define && define.amd ? define(["jquery", "moment"], e) : e(jQuery, moment)
})(function(e, t) {
    (t.defineLocale || t.lang).call(t, "pt-br", {
        months: "janeiro_fevereiro_mar\u00e7o_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
        monthsShort: "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
        weekdays: "domingo_segunda-feira_ter\u00e7a-feira_quarta-feira_quinta-feira_sexta-feira_s\u00e1bado".split("_"),
        weekdaysShort: "dom_seg_ter_qua_qui_sex_s\u00e1b".split("_"),
        weekdaysMin: "dom_2ª_3ª_4ª_5ª_6ª_s\u00e1b".split("_"),
        longDateFormat: {
            LT: "HH:mm",
            LTS: "LT:ss",
            L: "DD/MM/YYYY",
            LL: "D [de] MMMM [de] YYYY",
            LLL: "D [de] MMMM [de] YYYY [\u00e0s] LT",
            LLLL: "dddd, D [de] MMMM [de] YYYY [\u00e0s] LT"
        },
        calendar: {
            sameDay: "[Hoje \u00e0s] LT",
            nextDay: "[Amanh\u00e3 \u00e0s] LT",
            nextWeek: "dddd [\u00e0s] LT",
            lastDay: "[Ontem \u00e0s] LT",
            lastWeek: function() {
                return 0 === this.day() || 6 === this.day() ? "[\u00faltimo] dddd [\u00e0s] LT" : "[\u00faltima] dddd [\u00e0s] LT"
            },
            sameElse: "L"
        },
        relativeTime: {
            future: "em %s",
            past: "%s atr\u00e1s",
            s: "segundos",
            m: "um minuto",
            mm: "%d minutos",
            h: "uma hora",
            hh: "%d horas",
            d: "um dia",
            dd: "%d dias",
            M: "um m\u00eas",
            MM: "%d meses",
            y: "um ano",
            yy: "%d anos"
        },
        ordinalParse: /\d{1,2}º/,
        ordinal: "%dº"
    }), e.fullCalendar.datepickerLang("pt-br", "pt-BR", {
        closeText: "Fechar",
        prevText: "&#x3C;Anterior",
        nextText: "Pr\u00f3ximo&#x3E;",
        currentText: "Hoje",
        monthNames: ["Janeiro", "Fevereiro", "Mar\u00e7o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        dayNames: ["Domingo", "Segunda-feira", "Ter\u00e7a-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "S\u00e1bado"],
        dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S\u00e1b"],
        dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "S\u00e1b"],
        weekHeader: "Sm",
        dateFormat: "dd/mm/yy",
        firstDay: 0,
        isRTL: !1,
        showMonthAfterYear: !1,
        yearSuffix: ""
    }), e.fullCalendar.lang("pt-br", {
        defaultButtonText: {
            month: "M\u00eas",
            week: "Semana",
            day: "Dia",
            list: "Compromissos"
        },
        allDayText: "dia inteiro",
        eventLimitText: function(e) {
            return "mais +" + e
        }
    })
});