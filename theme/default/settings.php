<?php

$fullscreen = Cookie::get('fullscreen') === '1' ? 'checked' : '';
$cache = Cookie::get('cache') === 'on' ? 'checked' : '';
$utf8 = Cookie::get('utf8') === 'true' ? 'checked' : '';
$design = array('single'=>(Cookie::get('design') === 'single' ? 'checked' : ''), 'tab'=>(Cookie::get('design') === 'tab' ? 'checked' : ''));

?>

<script>
  jQuery('#system-settings #hide').prop('checked', Application.storage.get('hide') === 'on');
</script>

<div id="system-settings">

  <div class="checkbox checkbox-primary">
    <input id="fullscreen" type="checkbox" data-action="system-fullscreen" <?php print $fullscreen; ?>>
    <label for="fullscreen">
      <?php Application::lang('application.settings.text1', true); ?>
    </label>
  </div>

  <div class="checkbox checkbox-primary">
    <input id="hide" type="checkbox" data-action="system-hide">
    <label for="hide">
      <?php Application::lang('application.settings.text8', true); ?>
    </label>
  </div>

  <div class="checkbox checkbox-primary">
    <input id="utf8" type="checkbox" data-action="system-utf8" <?php print $utf8; ?>>
    <label for="utf8">
      <?php Application::lang('application.settings.text7', true); ?>
    </label>
  </div>

  <div class="checkbox checkbox-primary">
    <input id="cache" type="checkbox" data-action="system-cache" <?php print $cache; ?>>
    <label for="cache">
      <?php Application::lang('application.settings.text5', true); ?>
    </label>
  </div>

  <p><?php Application::lang('application.settings.text2', true); ?></p>
  <fieldset>
    <div class="radio radio-primary radio-inline">
        <input type="radio" id="inlineRadio1" value="single" name="radioInline" <?php print $design['single']; ?> data-action="system-design" data-value="single">
        <label for="inlineRadio1"> <?php Application::lang('application.settings.text3', true); ?> </label>
    </div>
    <div class="radio radio-primary radio-inline">
        <input type="radio" id="inlineRadio2" value="tab" name="radioInline" <?php print $design['tab']; ?> data-action="system-design" data-value="tab">
        <label for="inlineRadio2"> <?php Application::lang('application.settings.text4', true); ?> </label>
    </div>
  </fieldset>

  <br>
  <button data-action="system-update" class="btn btn-primary"> <?php Application::lang('application.settings.text6', true); ?> </button>
  <br>

  <hr>

  <div>
    <small> <?php Application::text('dracones.io' . ' :: ' . VERSION); ?> </small> <b>&lt;&#x2F;&gt; &trade;</b> <small> <?php Application::text(COPY_RIGHT); ?> </small>
  </div>

</div>