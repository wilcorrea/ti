<ion-view view-title="{{operation.label}}">

  <ion-content class="template-list">

    <div class="card padding">

      <div class="col-sm-6">

        <span ng-repeat="_operation in operation.actions.toolbar">
          <button class="{{_operation.className}}"  ng-click="resolve(_operation.action, collection)">
            {{_operation.label}}
          </button>
        </span>

      </div>

      <div class="col-sm-6">

        <select ng-model="device" class="form-control">
          <option value="MOBILE">Mobile</option>
          <option value="DESKTOP">Desktop</option>
          <option value="WEB">Web</option>
        </select>

      </div>

    </div>

    <div class="card padding">

      <div class="col-sm-6">

        <ul class="pagination">
          <li class="first">
            <a href="javascript:void(0);" ng-click="pagination(1)">&laquo;</a>
          </li>
          <li class="prev">
            <a href="javascript:void(0);" ng-click="pagination(page - 1)">&lt;</a>
          </li>
          <li class="active">
            <a class="pagination-page">
              <input ng-model="page" ng-keypress="pagination(page, $event)"/>
            </a>
          </li>
          <li class="next">
            <a href="javascript:void(0);" ng-click="pagination(page + 1)">&gt;</a>
          </li>
          <li class="last">
            <a href="javascript:void(0);" ng-click="pagination(pages)">&raquo;</a>
          </li>
        </ul>

      </div>

      <div class="col-sm-6">

        <input type="text" ng-model="settings.query" class="form-control" autocomplete="off" placeholder="Pesquisar...">

      </div>

      <div style="clear: both"><br></div>

      <div class="better-wrapper">

        
        <div class="pinned">
          
          <table class="better">
            <thead>
              <tr>
                <th class="{{entity}} checked">
                  <div> <input type="checkbox" name="all" ng-model="all"> </div>
                </th>
                <th class="{{entity}} options">
                  <div> Opções </div>
                </th>
                <th class="{{entity}} counter better-hide">
                  <div> * </div>
                </th>
                <th class="{{entity}} reference">
                  <div> Código </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="__object in collection">
                <td class="{{entity}} checked">
                  <div> <input type="checkbox" name="{{ (operation.reference + '[' + __object[operation.reference] + ']')  }}" ng-checked="all"> </div>
                </td>
                <td class="{{entity}} options">
                  <div>
                    <span ng-repeat="_operation in operation.actions.grid">
                      <button class="{{_operation.className}}"  ng-click="resolve(_operation.action, __object)">
                        <i class="{{_operation.classIcon}}"></i>
                      </button>
                    </span>
                  </div>
                </td>
                <td class="{{entity}} counter better-hide">
                  <div> {{__object.counter}} </div>
                </td>
                <td class="{{entity}} reference">
                  <div> {{__object[operation.reference]}} </div>
                </td>
              </tr>
            </tbody>
          </table>

        </div>

        <div class="data">
          
          <table class="better">
            <thead>
              <tr>
                <th ng-repeat="_field in operation.list" class="field {{(_field.id == operation.reference) ? 'better-hide' : 'better-show'}}">
                  <div class="{{entity}} {{_field.id}} {{_field.type}}"> {{_field.label}} </div>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr ng-repeat="_object in collection">
                <td ng-repeat="_field in operation.list" class="field {{(_field.id == operation.reference) ? 'better-hide' : 'better-show'}}">
                  <div ng-switch on="_field.type" class="{{entity}} {{_field.type}} formatter">
                    <div ng-switch-when="yes/no">
                      <i ng-class="((_object[_field.id] === '1') ? 'fa fa-check-square-o' : 'fa fa-square-o')"></i>
                    </div>
                    <div ng-switch-default>
                      <input type="text" readonly value="{{_object[_field.id]}}"/>
                    </div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>

        </div>
        
        <div style="clear: both"></div>

      </div>
      
      <div style="clear: both"><br></div>

      <div class="col-sm-6">

        <ul class="pagination">
          <li class="first">
            <a href="javascript:void(0);" ng-click="pagination(1)">&laquo; 1</a>
          </li>
          <li class="prev">
            <a href="javascript:void(0);" ng-click="pagination(page - 1)">&lt;</a>
          </li>
          <li class="active">
            <a class="pagination-page">
              <input ng-model="page" ng-keypress="pagination(page, $event)"/>
            </a>
          </li>
          <li class="next">
            <a href="javascript:void(0);" ng-click="pagination(page + 1)">&gt;</a>
          </li>
          <li class="last">
            <a href="javascript:void(0);" ng-click="pagination(pages)">{{pages}} &raquo;</a>
          </li>
        </ul>

      </div>

      <div class="col-sm-6">

        <div class="pagination-info">
          Exibindo {{collection.length}} na página {{page}}/{{pages}} do total de {{total}} registros
        </div>

      </div>

    </div>

  </ion-content>

</ion-view>
