<ion-view view-title="{{operation.label}}">

  <ion-content class="template-list">

    <div class="card padding">

      <div class="col-sm-6">

        <span ng-repeat="_operation in operation.actions.toolbar">
          <button class="{{_operation.className}}"  ng-click="resolve(_operation.action, collection)">
            {{_operation.label}}
          </button>
        </span>

      </div>

    </div>

    <ion-list scroll="false">

      <ion-scroll delegate-handle="libScroll" on-scroll="checkScroll()" direction="y" style="width: 100%; height: {{height}}px;">

        <ion-item ng-repeat="_object in collection" item="_object">

        <div class="card">

          <div class="item-title">
            {{operation.title(_object)}} / [ {{_object.counter}} ]
          </div>
          <div class="item-subtitle">
            {{operation.subtitle(_object)}}
          </div>
          <div class="item-content">
            <div ng-bind-html="operation.content(_object)"></div>
          </div>

          <div class="item-action">

            <span ng-repeat="_operation in operation.actions.grid">
              <button class="{{_operation.className}}"  ng-click="resolve(_operation.action, _object)">
                <i class="{{_operation.classIcon}}"></i>
              </button>
            </span>
          </div>

          </div>

        </ion-item>

      </ion-scroll>

    </ion-list>

  </ion-content>

</ion-view>
