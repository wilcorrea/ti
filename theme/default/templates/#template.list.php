<ion-view view-title="{{operation.label}}">

  <ion-content class="template-list">

    <div class="web-popup-backdrop {{popup.data ? 'web-popup-open' : ''}}" ng-click="popup.close()" style="height: {{height + 70}}px;"></div>
    <div class="web-popup-content ng-animated-light-spped" ng-show="popup.data" ng-class="{active: popup.data}">
      <div class="web-popup-title">
        {{popup.title}} <span ng-click="popup.close()">&times;</span>
      </div>
      {{popup.data}}
    </div>

    <div class="card padding button-bar">

      <div class="web-header">

        <div class="col-xs-7">

          <input type="checkbox" ng-model="all" ng-click="checkAll(all)" style="margin-left: -15px;">

          <span ng-repeat="_operation in operation.actions.toolbar">
            <button class="btn btn-{{_operation.className}}"  ng-click="resolve(_operation.id, {})">
              {{_operation.label}}
            </button>
          </span>

          <div class="btn-group" ng-show="search.data">
            <a href="javascript:void(0);" class="btn btn-default" ng-click="search.show()">
              <i class="fa fa-filter"></i>
            </a>
            <a href="javascript:void(0);" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li><a href="javascript:void(0);" ng-click="search.clear()">{{search.labels.clear}}</a></li>
              <!--<li class="divider"></li>-->
              <!--<li><a href="javascript:void(0);">Separated link</a></li>-->
            </ul>
          </div>

        </div>

        <div class="col-xs-5">

          <input type="text" ng-model="settings.query" ng-keyup="search.query()" class="form-control input-search" autocomplete="off" placeholder="{{search.labels.input}}">

        </div>
        
      </div>

    </div>

    <div ng-if="collection.length">

      <div class="{{right.data ? 'right-open' : ''}}">
  
        <div class="card web-left" style="height: {{height}}px;">
  
          <div class="web-row {{(__object.checked) ? 'web-row-selected' : ''}}" ng-repeat="__object in collection">
  
            <div class="data-checkbox">
              <input type="checkbox" name="{{ (model.properties.reference + '[' + __object[model.properties.reference] + ']')  }}" ng-model="__object.checked">
            </div>
  
            <div class="data-field-icon" ng-click="popup.open(operation.label, __object)" style="cursor: pointer;">
              <label class="data-field-link"><img src="{{url('image', 'list.jpg')}}"></label>
            </div>
            <div class="data-fields {{right.data === __object ? 'field-in-right' : ''}}" style="width: calc(100% - {{ (operation.actions.grid.length * 62) }}px);" ng-click="right.toggle(__object)">
              <div class="data-field-info">
                <span class="field-description">{{__object[model.properties.description]}}</span>
                <div ng-bind-html="operation.list.row(__object)"></div>
                <!--<div ng-repeat="__fragment in __object.row" class="fragment" title="{{__fragment.label}}: {{__fragment.value}}"> -->
                  <!--<i class="fa {{__fragment.icon}}"></i> {{__fragment.value}}-->
                <!--</div>-->
              </div>
            </div>
  
            <div class="data-options">
  
                <span ng-repeat="_operation in operation.actions.grid">
                  <button class="btn btn-{{_operation.className}}"  ng-click="resolve(_operation.id, __object)">
                    <i class="fa fa-{{_operation.classIcon}}"></i>
                  </button>
                </span>
  
              </div>
  
          </div>
  
        </div>
  
        <!-- /*height: {{ (collection.length * 60) }}px;*/-->
        <div class="card web-right" style="height: {{(height_content - 60)}}px;">
  
          <span ng-click="right.close()">&times;</span>
  
          <div class="web-right-content">
  
            <div ng-repeat="_field in operation.list" class="web-right-line">
              <div ng-if="_field.label">
                <div class="right-title"> {{_field.label}} </div>
                <div class="right-value"> {{right.data[_field.id]}} </div>
              </div>
            </div>
  
          </div>
  
        </div>
  
      </div>

      <div class="web-footer">
  
        <div class="col-xs-6">
  
          <ul class="pagination">
            <li class="first">
              <a href="javascript:void(0);" ng-click="pagination(1)">&laquo; 1</a>
            </li>
            <li class="prev">
              <a href="javascript:void(0);" ng-click="pagination(page - 1)">&lt;</a>
            </li>
            <li class="active">
              <a class="pagination-page">
                <input ng-model="page" ng-keypress="pagination(page, $event)"/>
              </a>
            </li>
            <li class="next">
              <a href="javascript:void(0);" ng-click="pagination(page + 1)">&gt;</a>
            </li>
            <li class="last">
              <a href="javascript:void(0);" ng-click="pagination(pages)">{{pages}} &raquo;</a>
            </li>
          </ul>
  
        </div>
  
        <div class="col-xs-6">
  
          <div class="pagination-info">
            Exibindo {{collection.length}} na página {{page}}/{{pages}} do total de {{total}} registros
          </div>
  
        </div>
  
      </div>

    </div>
    
    <div ng-if="!collection.length">
      
      <div class="card padding">

        <div ng-bind-html="((search.data || settings.query) ? model.properties.emptyFilter : model.properties.emptyCollection)"></div>

      </div>

    </div>

  </ion-content>

</ion-view>
