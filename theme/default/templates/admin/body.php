<div class="wrapper">

  <app ng-app="starter" ng-controller="AppCtrl">
    <ion-nav-view></ion-nav-view>
    
    <div class="notification-area">
      <div class="alert alert-dismissible alert-{{_alert.status}} ng-animated-light-spped" ng-repeat="_alert in alerts.data" ng-show="_alert.visible" ng-class="{active: _alert.visible}">
        <button type="button" class="close" ng-click="alerts.remove(_alert)">&times;</button>
        <strong>{{_alert.title}}</strong> {{_alert.message}}
      </div>
    </div>
  </app>

  <div class="menu-compact-toggle" onclick="App.compact();"><i class="fa fa-arrow-left"></i></div>

</div>