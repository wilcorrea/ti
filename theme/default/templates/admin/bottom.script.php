<script>
  /**
   * 
   * 
   */
  //App.init('<?php print APP_HOST; ?>', '<?php print APP_PAGE; ?>', '<?php print APP_VERSION; ?>');
  
  jQuery('.fadeout').removeClass('fadeout').addClass('fadein');
</script>

<link href="http://apache.io/projetos/ti-ionic/theme/default/public/lib/intro.js/introjs.css" rel="stylesheet">
<style>
  .introjs-helperNumberLayer {
    display: none;
  }
  .introjs-tooltip {
    min-width: 300px;
    max-width: 400px;
    border-radius: 2px;
}
</style>
<script type="text/javascript" src="http://apache.io/projetos/ti-ionic/theme/default/public/lib/intro.js/intro.js"></script>

<script type="text/javascript">

  //Application.lib.require('intro.js', function() {
  
  function introShow() {
  
    var intro = introJs();

    intro.setOptions({
        nextLabel: 'Continuar &raquo;',
        prevLabel: '&laquo; Voltar',
        skipLabel: 'Já sei',
        doneLabel: 'Pronto',
        steps: [{
            intro: "Bem Vindo! Este é seu primeiro acesso ao App da TI e vamos lhe dar algumas dicas iniciais de como usá-lo"
          },{
            element: document.querySelector('ion-side-menu ion-content .scroll'),
            intro: "Este é o menu lateral que permite acesso rápido ao recursos do sistema.",
            position: 'right'
          },{
            element: document.querySelectorAll('ion-side-menu ion-list .side-menu-dropdown')[1],
            intro: 'Com os menus abertos é possível clicar nas funções para abrir as telas de administração',
            position: 'right'
          },{
            element: 'ion-content .card.card-stats',
            intro: "Acompanhe os resultados de forma ágil usando os gráficos",
            position: 'bottom'
          },{
            element: document.querySelectorAll('ion-content ul.timeline li')[0],
            intro: 'Acompanhe a linha do tempo com o resumo semanal de todas as interações feitas com clientes e processos',
            position: 'top'
          },{
            intro: 'Comece a usar agora a nossa ferramenta, caso precise de algum suporte entre em contato conosco ou use nosso material de ajuda!'
          }
        ]
      });

    intro.start();
  }
  
  jQuery(function() {

    window.setTimeout(function() {
        introShow();
      }, 1000
    );

  });
  //});
</script>