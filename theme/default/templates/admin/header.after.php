
<link type="text/css" rel="stylesheet" href="<?php Application::link('app/style/stylesheet.css'); ?>">
<link type="text/css" rel="stylesheet" href="<?php Application::link('app/css/index.css'); ?>">

<script type="text/javascript" src="<?php Application::link('app/javascript/script.js'); ?>"></script>
<script type="text/javascript" src="<?php Application::link('app/js/index.js'); ?>"></script>

<script type="text/javascript">
  App.APP_PAGE = '<?php Application::link(); ?>';
  App.APP_PAGE_LOGOUT = '<?php Application::link(APP_SERVICE_LOGOUT); ?>';
  App.APP_VERSION = '<?php print APP_VERSION; ?>';
</script>
