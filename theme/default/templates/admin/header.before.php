<?php

$token = Application::$cookies['token'];
$session = Application::$cookies['session'];

?>
<script>
  try {

    //App.config.location = '<?php print APP_PAGE; ?>';
  
    window.CKEDITOR_BASEPATH = '<?php print implode('/', [APP_PAGE, 'theme', 'default', 'public', 'lib', 'ckeditor', '']); ?>';

    window.LOCAL_TOKEN = '<?php print $token; ?>';
    window.LOCAL_SESSION = '<?php print $session; ?>';

    window.localStorage.setItem(LOCAL_TOKEN, '<?php print Application::$user->token; ?>');
    window.localStorage.setItem(LOCAL_SESSION, '<?php print Json::encode(Application::$user->user); ?>');

    if ('applicationCache' in window) {
      //applicationCache.update();
    }

    //App.listenner.add('load', 'fadeout', function() {

      //jQuery('.fadeout').removeClass('fadeout').addClass('fadein');
    //});

  } catch (e) {
    
  }
</script>