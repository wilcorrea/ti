<?php

$message = Theme::$message ? Theme::$message : Application::lang('admin.content.text1');

?>
<div class="wrapper">

  <div class="page-title">
    <?php

      Theme::append('site', 'breadcrumb');
    ?>
  </div>

  <div class="container">

    <div class="page-tip fadeout">
      <?php App::text($message); ?>
    </div>
  
    <div class="row">
  
      <div class="col-md-7 col-sm-6 hidden-xs">
  
      </div>
  
      <div class="col-md-5 col-sm-6 col-xs-12">
        <?php
          Theme::append('default', 'login.form');
        ?>
      </div>
  
    </div>

  </div>

</div>