<?php


if (APP_CONECTED) {
  ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>
      li.account .avatar {
        background: url('<?php App::link('download/a/avatar'); ?>');
        background-size: 100px 100px;
      }
    </style>
        <?php

  System::import('class', 'pattern', 'Controller', 'core');

  $categorias = Controller::cachedGet('site', 'Categoria', 'ctg_1:TRUE', null, null, null);
  
  ?>
    <style>
      <?php

      if (is_array($categorias)) {

        foreach($categorias as $categoria) {

          print PHP_EOL 
            . "            .category-" . $categoria->ctg_codigo . " {" . PHP_EOL
            . "              background-color: " . $categoria->ctg_cor . " !important;" . PHP_EOL
            . "              color: " . $categoria->ctg_texto . " !important;" . PHP_EOL
            . "            }" . PHP_EOL;
          print PHP_EOL 
            . "            .category-title-" . $categoria->ctg_codigo . "
                            , .page-category-" . $categoria->ctg_codigo . " .hl.top-zero
                            , .page-category-" . $categoria->ctg_codigo . " .block-header span.title
                            , .page-category-" . $categoria->ctg_codigo . " .h1.top-zero {" . PHP_EOL
            . "              color: " . $categoria->ctg_cor . " !important;" . PHP_EOL
            . "            }" . PHP_EOL;
          print PHP_EOL 
            . "            .category-" . $categoria->ctg_codigo . " a {" . PHP_EOL
            . "              color: " . $categoria->ctg_texto . " !important;" . PHP_EOL
            . "            }" . PHP_EOL;
        }
      }
      ?>
    </style>
  <?php

?>
  <?php
}


