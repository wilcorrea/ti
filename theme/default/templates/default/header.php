<?php

$externals = array(
  //"//fonts.googleapis.com/css?family=Lato:100,300,400"
);

if (APP_SERVICE === 'site') {

  Theme::append('site', 'header.meta');

  Theme::append('site', 'header.analytics');
}

Theme::append(APP_SERVICE, 'header.before');

foreach ($externals as $external) {
  Theme::link($external);
}

$usu_tipo = '';
if (isset(Application::$user) && isset(Application::$user->usu_tipo)) {
  $usu_tipo = sha1(Encode::decrypt(Application::$user->usu_tipo));
}

$bibliotecas = Theme::$library;

$_position = 'header';


foreach ($bibliotecas as $biblioteca) {

  $bbl_escopo = $biblioteca->bbl_escopo;

  if ($biblioteca->bbl_posicao === 'all' or $biblioteca->bbl_posicao === $_position) {
    
    $append = (($bbl_escopo === 'all') || ($bbl_escopo === 'site' && !APP_ADMIN)) ? (true) : ($bbl_escopo === 'admin' && APP_ADMIN);

    if ($append) {

      switch ($biblioteca->bbl_componentes) {

        case 'css':

          Theme::linkLib($biblioteca, $_position);

          break;

        case 'javascript':

          Theme::scriptLib($biblioteca, $_position);

          break;

        case 'all':

          Theme::linkLib($biblioteca, $_position);

          Theme::scriptLib($biblioteca, $_position);

          break;
      }
    }
  }

}

if (APP_ADMIN) {
  
  Theme::append('admin', 'header.after');
}

