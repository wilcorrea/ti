<?php 

$label = APP_CONECTED ? App::lang('site.login.text1') : App::lang('site.login.text2');

$action = App::link(Application::$admin, false, false);
?>

<div class="form-box">
  <h4><?php App::text($label); ?></h4>
  <hr>
  <?php
    if (!APP_CONECTED) {
      ?>
        <form role="form" action="<?php $action; ?>" method="post">

          <fieldset>

            <div class="form-group">
              <input type="login" name="<?php print Application::$credencials['login']; ?>"  value="<?php print App::request(Application::$credencials['login']); ?>" placeholder="<?php App::lang('site.login.text4', true); ?>" class="form-control">
            </div>

            <div class="form-group">
              <input type="password" name="<?php print Application::$credencials['password']; ?>" placeholder="<?php App::lang('site.login.text6', true); ?>" class="form-control" autocomplete="off">
            </div>

            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input name="remember" id="remember" class="_select-box" type="checkbox" value="yes"> <?php App::lang('site.login.text7', true); ?>
                </label>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary"><?php App::lang('site.login.text8', true); ?></button>
              <input type="hidden" name="r" id="r" value="<?php App::text(Theme::$return); ?>"/>
            </div>

          </fieldset>

        </form>
      <?php
    } else {
      ?>
        <p>
          <?php App::lang('site.login.text9', true); ?> <b><?php print App::get() ;?></b>,
        </p>
        <p align="justify">
          <?php App::lang('site.login.text10', true); ?> <a href="<?php App::link(Application::$admin); ?>"><?php App::lang('site.login.text11', true); ?></a> <?php App::lang('site.login.text12', true); ?>
        </p>
      <?php
    }
  ?>

</div>

