<ul id="navbar-profile" class="nav navbar-nav navbar-right collapse-top">
  <?php

    if (APP_CONECTED) {

      $name = App::get();
      $email =  App::get('email');
      
      if (APP_SERVICE === APP_SERVICE_ADMIN) {
        ?>
          <li>
  
            <a href="<?php App::link(APP_SERVICE_LOGOUT, true); ?>">
              <i class="fa fa-power-off"></i> <?php App::lang('site.profile.text2', true); ?>
            </a>

          </li>
        <?php
      }

      if (APP_SERVICE === 'site') {
        ?>
          <li id="cogs-menu">
            <a href="<?php App::link(APP_SERVICE_ADMIN); ?>">
              <i class="fa fa-gears fa-lg"></i>
            </a>
          </li>
        <?php
      }
    } else {
      ?>
        <li>
          <a href="<?php App::link(APP_SERVICE_ADMIN, true); ?>">
            <?php App::lang('site.profile.admin', true); ?>
          </a>
        </li>
      <?php
    }
  ?>
</ul>