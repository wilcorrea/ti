<?php 

$class = APP_ADMIN ? 'scrolled' : '';

?>

<!-- .navbar-fixed-top -->
<nav class="navbar navbar-default navbar-fixed-top">

  <!-- .navbar-container -->
  <div class="navbar-container">

    <!-- .navbar-header -->
    <div class="navbar-header">

      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand navbar-brand-theme-default" href="<?php App::link(''); ?>"><img src="<?php App::link('download/a/logo');?>"/></a>

    </div>
    <!--/.navbar-header -->

    <!-- .nav-collapse -->
    <div id="navbar" class="navbar-collapse collapse">

      <ul class="nav navbar-nav">
        <?php
          foreach (Theme::$top as $top) {
  
            $a = '';
            $li = '';
            $data_toggle = '';
            $href = App::link($top->href, false, false);
            $title = $top->title;
            if (count($top->kids)) {
              $a = 'dropdown-toggle';
              $li = 'dropdown';
              $data_toggle = 'dropdown';
              $href = '#';
              $title = $title . ' <b class="caret"></b>';
            }
            ?>
              <li class="<?php print($li); ?>">
                <a href="<?php print ($href); ?>" target="<?php App::text($top->target, true); ?>" class="<?php App::text($class, true); ?>" data-toggle="<?php App::text($data_toggle, true); ?>">
                  <?php App::text($title, true); ?>
                </a>
                  <?php
                    if (is_array($top->kids)) {
                      ?>
                        <ul class="dropdown-menu">
                          <?php
                            foreach ($top->kids as $_kid) {

                              $kid_ref = $top->href . '/' . $_kid->href;

                              if ($_kid->type === '1') {
                                $kid_ref = String::substring($_kid->href, 1);
                              }
                              ?>
                                <li>
                                  <a href="<?php App::link($kid_ref); ?>">
                                    <?php App::text($_kid->title, true); ?>
                                  </a>
                                </li>
                              <?php
                            }
                          ?>
                        </ul>
                      <?php
                    }
                  ?>
              </li>
            <?php
          }
        ?>
      </ul>
  
      <?php
        Theme::append('default', 'profile');
      ?>

    </div><!--/ .nav-collapse -->

  </div><!--/ navbar-.container -->

<!--/ .navbar-fixed-top -->
</nav>

