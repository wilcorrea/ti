<?php

    
Theme::$page->pgn_header = String::substring(Theme::$page->pgn_breadcrumb_title, 3);

$ls = explode('/', Theme::$page->pgn_breadcrumb_slug);
$lt = explode('/', Theme::$page->pgn_breadcrumb_title);

?>
<div class="container">

  <ul class="breadcrumb">
    <li>
      <a href="<?php Application::link(''); ?>" class="home"><i class="fa fa-home"></i></a>
    </li>
    <?php
  
    $class = 'category-title-' . Theme::$page->pgn_cod_CATEGORIA;
  
    $href = '';
    foreach ($ls as $_lt => $_ls) {
      if ($_ls) {
        $href = $href . $_ls . '/';
        $html = $lt[$_lt];
        ?>
          <li>
            <a href="<?php Application::link($href); ?>" class="<?php print $class; ?>">
              <?php Application::text($html, true); ?>
            </a>
          </li>
        <?php
      }
    }
    ?>
  </ul>

</div>