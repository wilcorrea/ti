<footer class="footer">
  <div class="container">
    <p class="text-muted"><?php App::text(APP_COPY_RIGHT); ?></p>
  </div>
</footer>