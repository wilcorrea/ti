<?php

$url = '';
$image = '';
$site_name = APP_COMPANY;
$description = Theme::$page->pgn_excerpt ? Theme::$page->pgn_excerpt : APP_COPY_RIGHT;

?>
<meta property="og:title" content="<?php App::text(Theme::$page->pgn_title, true); ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?php print $url;?>"/>
<meta property="og:image" content="<?php print $image;?>"/>
<meta property="og:site_name" content="<?php App::text($site_name); ?>"/>
<meta property="og:description" content="<?php App::text($description, true); ?>"/>

<meta name="author" content="<?php print APP_COPY_RIGHT; ?>"/>
<meta name="description" content="<?php print APP_COMPANY; ?>"/>
<meta name="generator" content=""/>
<meta name="keywords" content=""/>