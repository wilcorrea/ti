<?php

$class = [];

if (Cookie::get('fullscreen') === '1') {
  $class[] = 'body-fullscreen';
}

if (Cookie::get('design')) {
  $class[] = Cookie::get('design');
}

?>
<!DOCTYPE html>
<html class="<?php print APP_SERVICE; ?>">

  <head>

    <title><?php App::text(Theme::$page->pgn_header, true); ?> | <?php App::text(APP_COMPANY); ?></title>

    <link rel="shortcut icon" href="<?php App::link('download/a/favicon');?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

      Theme::append('default', 'header');

    ?>

  </head>

  <body class="<?php print implode(' ', $class); ?>" ng-app="starter">
    <?php

      Theme::append('default', 'top');

      if (APP_ADMIN) {

        Theme::append('admin', 'body');
      } else {

        Theme::append('admin', 'login');

        Theme::append('site', 'footer');
      }

      Theme::append('default', 'bottom');

      Theme::append('admin', 'bottom.script');
    ?>
  </body>
</html>