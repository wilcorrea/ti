<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="text/javascript">
      system = {
        init: function(href) {
          var progressbar = document.getElementById('progressbar');
          window.setTimeout(function() {
            for (var i = 0; i < 10; i++) {
              progressbar.style.width = (i * 10) + '%';
            }
            window.location.href = href;
            progressbar.style.width = '100%';
          }, 100);
        }
      };
    </script>
    <style>
      div.notify {
        margin: 10px;
        font-family: arial;
        font-size: 10px;
        box-shadow: 0 0 4px 1px rgba(0,0,0,0.2);
        padding: 5px 20px 15px 20px;
        float: left;
        border-radius: 2px;
        color: #666;
        font-style: italic;
      }
      .progressbar, .progressbar-value {
        height: 2px;
      }
      .progressbar {
        width: 100%;
        border: 1px solid #ccc;
        background: #eee;
      }
      .progressbar-value {
        width: 2%;
        background: #017522;
      }
      .notify-line {
        margin: 3px;
      }
    </style>
  </head>
  <body onload="system.init('<?php print $redirect;?>')">
    <div class="notify">
      <div class="notify-line">
        <b><?php print System::getUser();?></b>
      </div>
      <div class="notify-line">
        <?php
          Application::lang('site.redirect.text1', true);
        ?>
      </div>
      <div class="progressbar">
        <div id="progressbar" class="progressbar-value"></div>
      </div>
    </div>
  </body>
</html>
