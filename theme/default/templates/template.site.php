<?php


$class = [];

$class[] = 'page-category-' . Theme::$page->pgn_cod_CATEGORIA;

?>
<!DOCTYPE html>
<html class="<?php print APP_SERVICE; ?>">

  <head>

    <title><?php App::text(Theme::$page->pgn_header, true); ?> | <?php App::text(APP_COMPANY); ?></title>

    <link rel="shortcut icon" href="<?php App::link('download/a/favicon');?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php

      Theme::append('default', 'header');

    ?>
  </head>

  <body class="<?php print implode(' ', $class); ?>">
    <?php

      Theme::append('default', 'top');

      Theme::append('site', 'body');

      Theme::append('site', 'footer');

      Theme::append('default', 'bottom');

      Theme::append('site', 'bottom.script');
    ?>
  </body>
</html>